DROP TABLE IF EXISTS OpenNet_SubscriberImsPublicUserIdentity;
DROP TABLE IF EXISTS OpenNet_Subscribers;

create TABLE OpenNet_Subscribers (
  MSISDN varchar(50) not null,
  IMSI varchar(50),
  SubscriberLanguage varchar(50),
  Account varchar(50),
  SubscriptionType varchar(50),
  CreditWorthy integer,
  ValidityStart date,
  ValidityEnd date,
  HomeZoneEnabled boolean,
  HomeZoneList varchar(50),
  OcsId varchar(20),
  FriendsAndFamilyEnabled boolean,
  FriendsAndFamilyList varchar(50),
  ClosedUserGroupEnabled boolean,
  ClosedUserGroupList varchar(50),
  ClosedUserGroupPreferred integer,
  CUGIncomingAccessAllowed boolean,
  CUGOutgoingAccessAllowed boolean,
  CfBusy varchar(20),
  CfNoReply varchar(20),
  PromotionList varchar(50),
  PromotionValidityStartDates varchar(50),
  PromotionValidityEndDates varchar(50),
  primary key (MSISDN)
);
COMMENT ON TABLE OpenNet_Subscribers IS 'Store basic subscriber data for OpenNet mvno';

CREATE TABLE OpenNet_SubscriberImsPublicUserIdentity (
  ImsPublicUserId varchar(50) not null,
  SubscriberId varchar(50) not null,
  CONSTRAINT opennet_pk_imsid PRIMARY KEY (ImsPublicUserId),
  CONSTRAINT opennet_fk_subscriberid FOREIGN KEY (SubscriberId)
  REFERENCES OpenNet_Subscribers ON DELETE CASCADE
);
COMMENT ON TABLE OpenNet_SubscriberImsPublicUserIdentity IS 'Store subscriber IMS public user identities for OpenNet mvno';

DROP TABLE IF EXISTS OpenCloud_SubscriberImsPublicUserIdentity;
DROP TABLE IF EXISTS OpenCloud_Subscribers;

CREATE TABLE OpenCloud_Subscribers (
  MSISDN varchar(50) not null,
  IMSI varchar(50),
  SubscriberLanguage varchar(50),
  Account varchar(50),
  CreditWorthy integer,
  SubscriptionType varchar(50),
  ValidityStart date,
  ValidityEnd date,
  HomeZoneEnabled boolean,
  HomeZoneList varchar(50),
  OcsId varchar(20),
  FriendsAndFamilyEnabled boolean,
  FriendsAndFamilyList varchar(50),
  ClosedUserGroupEnabled boolean,
  ClosedUserGroupList varchar(50),
  ClosedUserGroupPreferred integer,
  CUGIncomingAccessAllowed boolean,
  CUGOutgoingAccessAllowed boolean,
  CfBusy varchar(20),
  CfNoReply varchar(20),
  PromotionList varchar(50),
  PromotionValidityStartDates varchar(50),
  PromotionValidityEndDates varchar(50),
  primary key (MSISDN)
  
);
COMMENT ON TABLE OpenCloud_Subscribers IS 'Store basic subscriber data for OpenCloud platform operator';

CREATE TABLE OpenCloud_SubscriberImsPublicUserIdentity (
  ImsPublicUserId varchar(50) not null,
  SubscriberId varchar(50) not null,
  CONSTRAINT opencloud_pk_imsid PRIMARY KEY (ImsPublicUserId),
  CONSTRAINT opencloud_fk_subscriberid FOREIGN KEY (SubscriberId)
  REFERENCES OpenCloud_Subscribers ON DELETE CASCADE
);
COMMENT ON TABLE OpenCloud_SubscriberImsPublicUserIdentity IS 'Store subscriber IMS public user identities for OpenCloud platform operator';

-- DROP INDEX subscriber_data_idx;
-- CREATE INDEX CONCURRENTLY subscriber_data_idx ON subscriber_data (msisdn);



