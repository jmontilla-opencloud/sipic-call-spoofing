DROP TABLE IF EXISTS Zones;
create table Zones (
  ZoneName varchar(50),
  LocationTypes varchar(50),
  MCC varchar(50),
  MNC varchar(50),
  LAC varchar(50),
  CIOrSAC varchar(50),
  LocationDescriptions varchar(100),
  CONSTRAINT zones_pk PRIMARY KEY(ZoneName)
);
COMMENT ON TABLE Zones IS 'Zones Table';

