-- Table: promotionsBuckets

-- DROP TABLE promotionsBuckets;

CREATE TABLE promotionsBuckets
(
  subscriberId varchar2(50) NOT NULL,
  bucketName varchar2(50) NOT NULL,
  availableUnits TT_BIGINT,
  reservedUnits TT_BIGINT,
  PRIMARY KEY (subscriberId, bucketName)
);

