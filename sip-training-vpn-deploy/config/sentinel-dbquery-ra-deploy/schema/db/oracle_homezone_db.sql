DROP table Zones;
create table Zones (
  ZoneName varchar(50),
  LocationTypes char(3)[],
  MCC char(3)[],
  MNC varchar(3)[],
  LAC int,
  CIOrSAC int,
  LocationDescriptions varchar(50)[],
  CONSTRAINT zones_pk PRIMARY KEY(ZoneName)
);
COMMENT ON TABLE Zones IS 'Zones Table';

