-- Table: promotionsBuckets

-- DROP TABLE promotionsBuckets;

CREATE TABLE promotionsBuckets
(
  subscriberId varchar(50) NOT NULL,
  bucketName varchar(50) NOT NULL,
  availableUnits NUMBER,
  reservedUnits NUMBER,
  CONSTRAINT promotionsBuckets_pk PRIMARY KEY (subscriberId, bucketName)
);
CREATE UNIQUE INDEX promotionsBuckets_idx ON promotionsBuckets (subscriberId, bucketName);
COMMENT ON TABLE promotionsBuckets IS 'Store promotions buckets';

