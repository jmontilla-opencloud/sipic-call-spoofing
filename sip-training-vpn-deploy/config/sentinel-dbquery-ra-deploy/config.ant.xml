<?xml version="1.0"?>

<project name="configure-dbquery" default="configure" xmlns:ivy="antlib:org.apache.ivy.ant" xmlns:ac="antlib:net.sf.antcontrib" xmlns:if="ant:if">

    <import file="${client.home}/etc/common.xml"/>

    <property file="${basedir}/config.properties"/>

    <!-- Default properties -->
    <property name="ra.trace.level" value="Info"/>
    <property name="db.type" value="postgres"/>
    <property name="dbquery.activate" value="true"/>

    <property name="postgres.jdbc.dir" value="${client.home}/../lib"/>
    <property name="postgres.host" value="localhost"/>
    <property name="postgres.port" value="5432"/>
    <property name="postgres.dbname" value="sentinel"/>
    <property name="postgres.user" value="sentinel"/>
    <property name="postgres.password" value="sentinel"/>

    <target name="configure" depends="check-db-type, login, create-profile-tables, create-postgres-dbquery-profiles, create-db-tables-postgres, create-mysql-dbquery-profiles, create-oracle-dbquery-profiles, create-db-tables-oracle, create-oracle-timesten-dbquery-profiles, create-db-tables-timesten, create-dbquery-ra-entity, activate-dbquery-ra-entity"/>
    
    <target name="create-profile-tables">
        <slee-management>
          <createprofiletable profilespec="name=Database Query DataSource Configuration,vendor=OpenCloud,version=2.0" tablename="dbquery-ds"/>
          <createprofiletable profilespec="name=Database Query Configuration,vendor=OpenCloud,version=2.0" tablename="dbquery-config"/>
        </slee-management>
    </target>
    
    <target name="create-dbquery-ra-entity" description="Create DB Query RA Entity">
        <slee-management>
            <createraentity resourceadaptorid="name=Database Query,vendor=OpenCloud,version=2.0"
                            entityname="dbquery-0"
                            properties="${dbqueryra.properties}"/>
            <bindralinkname entityname="dbquery-0" linkname="sentinel-dbquery"/>
            <settracerlevel tracername="root" level="${ra.trace.level}">
                <raentitynotificationsource entityname="dbquery-0"/>
            </settracerlevel>
        </slee-management>
    </target>

    <target name="activate-dbquery-ra-entity" if="${dbquery.activate}">
        <slee-management>
            <activateraentity entityname="dbquery-0"/>
        </slee-management>
    </target>

    <target name="create-postgres-dbquery-profiles" description="Create profiles for postgres configuration" if="postgres">
        <slee-management>
            <createprofile tablename="dbquery-ds" profilename="PGDataSource1">
                <profilevalue name="Description" value="${postgres.host} (postgres)"/>
                <profilevalue name="DataSourceClass" value="org.postgresql.ds.PGConnectionPoolDataSource"/>
                <profilevalue name="ServerName" value="localhost"/>
                <profilevalue name="PortNumber" value="${postgres.port}"/>
                <profilevalue name="DatabaseName" value="${postgres.dbname}"/>
                <profilevalue name="DataSourceProperties" value="[loginTimeout/java.lang.Integer/10]"/>
                <profilevalue name="User" value="${postgres.user}"/>
                <profilevalue name="Password" value="${postgres.password}"/>
                <profilevalue name="AdminEnable" value="true"/>
                <profilevalue name="WorkerThreads" value="15"/>
                <profilevalue name="QueryTimeout" value="500"/> 
            </createprofile>
            <createprofile tablename="dbquery-config" profilename="postgres-Config">
                <profilevalue name="DataSourceProfileIDs" value="[dbquery-ds/PGDataSource1]"/>
                <profilevalue name="TestSql" value="SELECT 1"/>
                <profilevalue name="ProfilePollTime" value="5000"/> 
            </createprofile>
        </slee-management>
        <property name="dbqueryra.properties" value="ConfigProfile=dbquery-config/postgres-Config,ConfigProfileAddNodeSuffix=false"/>
    </target>
	
	<target name="create-mysql-dbquery-profiles" description="Create profiles for MySQL configuration" if="mysql">
        <slee-management>
            <createprofile tablename="dbquery-ds" profilename="MySQLDataSource">
                <profilevalue name="Description" value="${mysql.host} (MySQL Direct)"/>
                <profilevalue name="DataSourceClass" value="com.mysql.jdbc.jdbc2.optional.MysqlConnectionPoolDataSource"/>
                <profilevalue name="Url" value="jdbc:mysql://${mysql.host}:${mysql.port}/${mysql.dbname}"/>
                <profilevalue name="User" value="${mysql.user}"/>
                <profilevalue name="Password" value="${mysql.password}"/>
                <profilevalue name="AdminEnable" value="true"/>
                <profilevalue name="WorkerThreads" value="5"/>
                <profilevalue name="QueryTimeout" value="10000"/> <!-- milliseconds -->
            </createprofile>
            <createprofile tablename="dbquery-config" profilename="MySQL-Config">
                <profilevalue name="DataSourceProfileIDs" value="[dbquery-ds/MySQLDataSource]"/>
                <profilevalue name="TestSql" value="SELECT shortCode from mappingTable where shortCode is null"/>
                <profilevalue name="ProfilePollTime" value="5000"/> <!-- milliseconds -->
            </createprofile>
        </slee-management>
        <property name="dbqueryra.properties" value="ConfigProfile=dbquery-config/MySQL-Config,ConfigProfileAddNodeSuffix=false"/>
    </target>
		
	<target name="create-oracle-dbquery-profiles" description="Create profiles for Oracle configuration" if="oracle">
        <slee-management>
            <createprofile tablename="dbquery-ds" profilename="OraDataSource">
                <profilevalue name="Description" value="${oracle.host} (Oracle)"/>
                <profilevalue name="DataSourceClass" value="oracle.jdbc.pool.OracleConnectionPoolDataSource"/>
                <profilevalue name="URL" value="jdbc:oracle:thin:@//${oracle.host}:1521/${oracle.database}"/>
                <profilevalue name="User" value="appuser"/>
                <profilevalue name="Password" value="appuser"/>
                <profilevalue name="AdminEnable" value="true"/>
                <profilevalue name="WorkerThreads" value="15"/>
                <profilevalue name="QueryTimeout" value="500"/> <!-- milliseconds -->
            </createprofile>
            <createprofile tablename="dbquery-config" profilename="Oracle-Config">
                <profilevalue name="DataSourceProfileIDs" value="[dbquery-ds/OraDataSource]"/>
                <profilevalue name="TestSql" value="SELECT shortCode from mappingTable where shortCode is null"/>
                <profilevalue name="ProfilePollTime" value="5000"/> <!-- milliseconds -->
            </createprofile>
        </slee-management>
        <property name="dbqueryra.properties" value="ConfigProfile=dbquery-config/Oracle-Config,ConfigProfileAddNodeSuffix=false"/>
    </target>
	
    <target name="create-oracle-timesten-dbquery-profiles" description="Create profiles for Oracle/TimesTen configuration" if="timesten">
        <slee-management>
            <createprofile tablename="dbquery-ds" profilename="TTDataSource">
                <profilevalue name="Description" value="sentinel (TT Client)"/>
                <profilevalue name="DataSourceClass" value="com.timesten.jdbc.ObservableConnectionDS"/>
                <profilevalue name="Url" value="jdbc:timesten:client:dsn=${timesten.dbname}"/>
                <profilevalue name="User" value="${timesten.user}"/>
                <profilevalue name="Password" value="${timesten.password}"/>
                <profilevalue name="AdminEnable" value="true"/>
                <profilevalue name="WorkerThreads" value="15"/>
                <profilevalue name="QueryTimeout" value="500"/> <!-- milliseconds -->
            </createprofile>
            <createprofile tablename="dbquery-ds" profilename="OraDataSource">
                <profilevalue name="Description" value="${oracle.host} (Oracle)"/>
                <profilevalue name="DataSourceClass" value="oracle.jdbc.pool.OracleConnectionPoolDataSource"/>
                <profilevalue name="URL" value="jdbc:oracle:thin:@//${oracle.host}:1521/${oracle.database}"/>
                <profilevalue name="User" value="appuser"/>
                <profilevalue name="Password" value="appuser"/>
                <profilevalue name="AdminEnable" value="true"/>
                <profilevalue name="WorkerThreads" value="5"/>
                <profilevalue name="QueryTimeout" value="10000"/> <!-- milliseconds -->
            </createprofile>
            <createprofile tablename="dbquery-config" profilename="TT-Ora-Config">
                <profilevalue name="DataSourceProfileIDs" value="[dbquery-ds/TTDataSource,dbquery-ds/OraDataSource]"/>
                <profilevalue name="TestSql" value="select user from dual"/>
                <profilevalue name="ProfilePollTime" value="5000"/> <!-- milliseconds -->
            </createprofile>
        </slee-management>
        <property name="dbqueryra.properties" value="ConfigProfile=dbquery-config/TT-Ora-Config,ConfigProfileAddNodeSuffix=false"/>
    </target>
    
    <!-- Create db tables for postgres -->
    <target name="create-db-tables-postgres" depends="check-db-type" if="postgres">

        <echo message="Postgres DB configuration:"/>
        <echo message="url=jdbc:postgresql://${postgres.host}:${postgres.port}/${postgres.dbname}"/>
        <echo message="user=${postgres.user}"/>
        <echo message="password=${postgres.password}"/>
        <echo message="basedir=${basedir}"/>

        <sql if:true="${db.createtables}"
             driver="org.postgresql.Driver"
             url="jdbc:postgresql://${postgres.host}:${postgres.port}/${postgres.dbname}"
             userid="${postgres.user}"
             password="${postgres.password}">

            <classpath>
                <pathelement path="${postgres.jdbc.dir}/postgresql.jar"/>
            </classpath>

            <transaction src="${basedir}/schema/db/postgres_subscriberdata_db.sql" />
            <transaction src="${basedir}/schema/db/postgres_homezone_db.sql" />
            <transaction src="${basedir}/schema/db/postgres_promotions_db.sql" />
        </sql>

    </target>
	
	<!-- Create db tables for timesten -->
	    <target name="create-db-tables-timesten" depends="check-db-type" if="timesten">
	        <echo message="TimesTen DB configuration:"/>
	        <echo message="url=jdbc:timesten:client:dsn=${timesten.dbname}"/>
	        <echo message="user=${timesten.user}"/>
	        <echo message="password=${timesten.password}"/>

	        <sql if:true="${db.createtables}"
                     driver="com.timesten.jdbc.TimesTenDriver"
	             url="jdbc:timesten:client:dsn=${timesten.dbname}"
	             userid="${timesten.user}"
	             password="${timesten.password}">

	            <classpath>
	                <pathelement path="${timesten.jdbc.dir}/ttjdbc6.jar"/>
	            </classpath>

	            <transaction src="${basedir}/schema/db/timesten_subscriberdata_db.sql" />
	            <transaction src="${basedir}/schema/db/timesten_homezone_db.sql" />
	            <transaction src="${basedir}/schema/db/timesten_promotions_db.sql" />
	        </sql>

	      </target>
	    
	    <!-- Create db tables for oracle -->
	    <target name="create-db-tables-oracle" depends="check-db-type" if="oracle">

	      <echo message="Oracle DB configuration:"/>
	      <echo message="url=jdbc:oracle:thin:@${oracle.host}:1521/${oracle.database}"/>
	      <echo message="user=${oracle.user}"/>
	      <echo message="password=${oracle.password}"/>

	    <!-- Create oracle db tables (subscriberdata, homezone, promotions) from file -->
	      <sql if:true="${db.createtables}"
	        driver="oracle.jdbc.driver.OracleDriver"
	        url="jdbc:oracle:thin:@${oracle.host}:1521/${oracle.database}"
	        userid="${oracle.user}"
	        password="${oracle.password}">
	        <classpath>
	          <pathelement path="${oracle.jdbc.dir}/ojdbc6.jar"/>
	        </classpath>
	        <transaction src="${basedir}/schema/db/oracle_subscriberdata_db.sql" />
	        <transaction src="${basedir}/schema/db/oracle_homezone_db.sql" />
	        <transaction src="${basedir}/schema/db/oracle_promotions_db.sql" />
	        </sql>
	    </target>
    
    <target name="check-db-type">
    	<echo message="Base: ${basedir}"/>
      <condition property="postgres">
            <equals arg1="${db.type}" arg2="postgres" />
      </condition>
        <condition property="oracle">
              <equals arg1="${db.type}" arg2="oracle" />
        </condition>
        <condition property="timesten">
              <equals arg1="${db.type}" arg2="timesten" />
        </condition>
        <condition property="mysql">
              <equals arg1="${db.type}" arg2="mysql" />
        </condition>
    </target>


</project>
