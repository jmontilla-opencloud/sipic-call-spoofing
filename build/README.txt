This directory contains the common build infrastructure used by all modules in
an SDK environment.

The contents of this directory should generally not be modified as the entire
directory may be replaced during SDK version updates. Extensions to the build
infrastructure can be made via the build extension mechanism
(build-extension.xml antlib). See SDK documentation for further details.

The target/ subdirectory contains libraries which are used by the build infrastructure.

The contents of the target/ directory are dynamically populated once upon first build,
and repopulated upon any changes to build/deps.properties during SDK version
updates.

The contents of the lib directory should generally not be modified. In the event
the directory is deleted, it will be recreated by the build infrastructure from
the ivy dependencies listed in build/build-ivy.xml.
