#!/usr/bin/env python3
#
# This script gives an example of how to use the Sentinel REST API with
# Python.
#
# Requirements:
#   - Python 3
#   - 'requests' library for Python 3
#
# It supports six commands:
#   list-resources: List all the available resources in the Sentinel
#     installation. The response to this request is always in XML, so this
#     serves as an example of how to handle XML responses.
#   list-featureexecutionscripts: List all available feature execution
#     scripts. This and all following commands request a JSON-encoded response
#     by setting the "Accept: application/json" header. JSON can be easily
#     converted to dictionaries and is therefore a better choice for Python
#     scripts than XML. The implementation also demonstrates how to retrieve
#     all content from a paginated response.
#   show-featureexecutionscript: Show the source of a specific feature
#     execution script.
#   add-featureexecutionscript: Add a new feature execution script. This does
#     a POST request with JSON-encoded data.
#   show-subscriber: Show a subscriber data record. This prints out the
#     complete JSON response.
#   add-subscriber: Add a new subscriber data record. It will only add the
#     mandatory "MSISDN" field to the subscriberData list, the rest of the
#     fields will be populated with defaults by Sentinel. It would be easy to
#     extend the script to add additional fields, however.
#   enable-homezone: Sets "HomeZoneEnabled" to True for a particular
#     subscriber. This demonstrates how to update existing data with a PUT
#     request.

import argparse
import json
import pprint
import requests
from requests.exceptions import HTTPError
import sys
from xml.etree import ElementTree

DEFAULT_HOSTNAME = "localhost"
DEFAULT_PORT = "8080"
DEFAULT_USERNAME = "emadm"
DEFAULT_PASSWORD = "password"
DEFAULT_INSTANCEID = "Local"
DEFAULT_SELECTIONKEY = "OpenCloud::::"

PP = pprint.PrettyPrinter(indent=4)

baseuri = ""
args = {}


def parse_args():
    parser = argparse.ArgumentParser(description="Sentinel REST API example")
    parser.add_argument("-H", "--hostname", help="the hostname of the REM server, default '" + DEFAULT_HOSTNAME + "'", default=DEFAULT_HOSTNAME)
    parser.add_argument("-p", "--port", help="the port of the REM server, default '" + DEFAULT_PORT + "'", default=DEFAULT_PORT)
    parser.add_argument("-u", "--username", help="the username used for authentication, default '" + DEFAULT_USERNAME + "'", default=DEFAULT_USERNAME)
    parser.add_argument("-P", "--password", help="the password used for authentication, default '" + DEFAULT_PASSWORD + "'", default=DEFAULT_PASSWORD)
    parser.add_argument("-i", "--instanceid", help="the name of the Rhino instance configured in REM, default '" + DEFAULT_INSTANCEID + "'", default=DEFAULT_INSTANCEID)
    parser.add_argument("-s", "--selectionkey", help="the Sentinel selection key to use, default '" + DEFAULT_SELECTIONKEY + "'", default=DEFAULT_SELECTIONKEY)
    parser.add_argument("-v", "--verbose", action="store_true", default=False,
                        help="increase output verbosity")

    subparsers = parser.add_subparsers(title="commands",
                                       description="Command to run with this script. Add '-h' after the command name to get command-specific help.")

    parser_list_resources = subparsers.add_parser("list-resources")
    parser_list_resources.set_defaults(func=cmd_list_resources)

    parser_list_featureexecutionscripts = subparsers.add_parser("list-featureexecutionscripts")
    parser_list_featureexecutionscripts.set_defaults(func=cmd_list_featureexecutionscripts)

    parser_show_featureexecutionscript = subparsers.add_parser("show-featureexecutionscript")
    parser_show_featureexecutionscript.add_argument("name", help="the name of the script to show")
    parser_show_featureexecutionscript.set_defaults(func=cmd_show_featureexecutionscript)

    parser_add_featureexecutionscript = subparsers.add_parser("add-featureexecutionscript")
    parser_add_featureexecutionscript.add_argument("name", help="the name of the new script")
    parser_add_featureexecutionscript.add_argument("src", help="the source of the new script")
    parser_add_featureexecutionscript.set_defaults(func=cmd_add_featureexecutionscript)

    parser_show_subscriber = subparsers.add_parser("show-subscriber")
    parser_show_subscriber.add_argument("subscriberId", help="the MSISDN of the subscriber to show")
    parser_show_subscriber.set_defaults(func=cmd_show_subscriber)

    # Call this command like this:
    # sentinel-rest-example -s "OpenCloud:OpenCloud:call::" add-subscriber 1234
    parser_add_subscriber = subparsers.add_parser("add-subscriber")
    parser_add_subscriber.add_argument("subscriberId", help="the ID of the subscriber to add")
    parser_add_subscriber.set_defaults(func=cmd_add_subscriber)

    parser_enable_homezone = subparsers.add_parser("enable-homezone")
    parser_enable_homezone.add_argument("subscriberId", help="the ID of the subscriber for which to enable HomeZone")
    parser_enable_homezone.set_defaults(func=cmd_enable_homezone)

    return parser.parse_args()


def verbose(msg):
    """Print given message if the `--verbose` command line option was used"""

    if args.verbose:
        print("[verbose] " + msg)


######################################################
#              Request helper functions              #
######################################################

def handle_error(response):
    """Check a response for error conditions and print relevant information in
    the case of a failure"""

    code = response.status_code
    if code >= 400:
        print(str(code) + " " + response.reason, file=sys.stderr)
        # 400, 404 and 500 responses contain additional information in the body
        if code == 400 or code == 404 or code == 500:
            try:
                body = response.json()
                print("Error type: " + body["type"], file=sys.stderr)
                print("Message: " + body["message"], file=sys.stderr)
            except Exception:
                print(response.text, file=sys.stderr)
        sys.exit(1)


def get_root():
    """Request the root of the API page with a list of all the available
    resources as XML"""

    verbose("Connecting to " + baseuri)
    r = requests.get(baseuri, auth=(args.username, args.password),
                     params={"alt": "application/atomsvc+xml"})
    handle_error(r)
    return r


def get_url(url, params=None):
    """Request a specific URL with optional parameters, adding the "Accept:
    application/json" header

    :param url: The URL to send the GET request to
    :param params: Optional dictionary with URL parameters
    :returns: A dictionary with the response decoded from JSON

    """

    headers = {"Accept": "application/json"}

    verbose("Connecting to " + url + " with additional parameters: " + str(params))
    r = requests.get(url, auth=(args.username, args.password),
                     headers=headers, params=params)

    handle_error(r)
    return r.json()


def get_path(path):
    """Request a specified path, adding the `rhinoInstanceId` and
    `selectionKey` parameters

    :param path: The API path, for example "/featureexecutionscripts/scripts"
    :returns: A dictionary with the response decoded from JSON

    """

    params = {"rhinoInstanceId": args.instanceid,
              "selectionKey": args.selectionkey}
    return get_url(baseuri + path, params)


def get_all_pages(path, mainelem):
    """Request all pages in case of a paginated response

    :param path: The API path, for example "/featureexecutionscripts/scripts"
    :param mainelem: The name of the main content field in the response
    :returns: A concatenated list of all the `mainelem` field contents

    """

    r = get_path(path)
    result_list = r[mainelem]

    while "next" in r:
        r = get_url(r["next"])
        result_list.extend(r[mainelem])

    return result_list


def send(path, data, method="POST"):
    """Send data to the specified path

    :param path: The API path, for example "/subscriberdata/records"
    :param data: The JSON-encoded data to send
    :param method: The request method to use, "POST" or "PUT"
    :returns: A requests.Response object

    """

    params = {"rhinoInstanceId": args.instanceid,
              "selectionKey": args.selectionkey}
    headers = {"Accept": "application/json",
               "Content-Type": "application/json"}
    verbose("Sending data to " + baseuri + path + " with " + method + ": " + data)
    r = requests.request(method, baseuri + path, auth=(args.username, args.password),
                         data=data, headers=headers, params=params)

    handle_error(r)
    return r


######################################################
#              Command implementations               #
######################################################

def cmd_list_resources(args):
    """List all available resources together with their URLs"""

    r = get_root()
    root = ElementTree.fromstring(r.text)
    # The "findall" method supports XPATH queries
    for collection in root.findall(".//{http://www.w3.org/2007/app}collection"):
        verbose(str(collection))
        title = collection.find("{http://www.w3.org/2005/Atom}title").text
        url = collection.attrib["href"]
        print(title + ": " + url)


def cmd_list_featureexecutionscripts(args):
    """List all feature execution scripts together with their resource URLs"""

    scripts = get_all_pages("/featureexecutionscripts/scripts", "scripts")
    for script in scripts:
        verbose(str(script))
        print(script["name"] + ": " + script["resources"][0]["href"])


def cmd_show_featureexecutionscript(args):
    """Show the definition of a feature execution script"""

    script = get_path("/featureexecutionscripts/scripts/" + args.name)
    print("Name: " + script["name"])
    print("Source: " + script["src"])


def cmd_add_featureexecutionscript(args):
    """Add a new feature execution script"""

    data = {"name": args.name, "src": args.src}
    r = send("/featureexecutionscripts/scripts", json.dumps(data))
    print("Response: " + r.reason)


def cmd_show_subscriber(args):
    """Show an individual subscriber data record"""

    subscriber = get_path("/subscriberdata/records/" + args.subscriberId)
    PP.pprint(subscriber)


def cmd_add_subscriber(args):
    """Add a new subscriber data record"""

    # The data we want to associate with the new subscriber. The "MSISDN"
    # field is mandatory, everything else will be filled with defaults if not
    # specified.
    data = {"name": args.subscriberId,
            "subscriberData": [{"name": "MSISDN",
                                "valueString": args.subscriberId,
                                "type": "String"}] }
    r = send("/subscriberdata/records", json.dumps(data))
    print("Response: " + r.reason)


def cmd_enable_homezone(args):
    """Enable HomeZone for a subscriber"""

    # Retrieve the requested subscriber and update the HomeZoneEnabled field
    subscriber = get_path("/subscriberdata/records/" + args.subscriberId)
    for item in subscriber["subscriberData"]:
        if item["name"] == "HomeZoneEnabled":
            item["valueBoolean"] = True
            break

    # Send the updated record back
    # Updating an existing record requires the "PUT" method.
    send("/subscriberdata/records/" + args.subscriberId, json.dumps(subscriber), method="PUT")


######################################################
#                   Main function                    #
######################################################

def main():
    global args
    args = parse_args()

    global baseuri
    baseuri = "http://" + args.hostname + ":" + args.port + "/rem/sentinel/api"

    # Assume list-resources as default command if none was given
    if "func" not in args:
        cmd_list_resources(args)
    else:
        args.func(args)


if __name__ == "__main__":
    sys.exit(main())
