package com.opencloud.sentinel.swisscom.sipic.common.cdr;

public interface SipCdrUsageStats extends com.opencloud.sentinel.feature.spi.SentinelFeatureStats {

    void incrementSipCdrWritten(long l);
    void incrementSipCdrNotWritten(long l);
}
