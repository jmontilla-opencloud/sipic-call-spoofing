package com.opencloud.sentinel.swisscom.sipic.common.cdr;

import java.io.IOException;

import com.opencloud.rhino.facilities.ExtendedTracer;
import com.opencloud.sce.fsmtool.Facilities;
import com.opencloud.sentinel.annotations.SentinelFeature;
import com.opencloud.sentinel.common.SentinelSelectionKey;
import com.opencloud.sentinel.common.SentinelSipSessionState;
import com.opencloud.sentinel.feature.ExecutionPhase;
import com.opencloud.sentinel.feature.impl.BaseFeature;
import com.opencloud.sentinel.feature.spi.SentinelSipMultiLegFeatureEndpoint;
import com.opencloud.sentinel.feature.spi.SipFeature;
import com.opencloud.sentinel.feature.spi.init.InjectFeatureStats;
import com.opencloud.sentinel.feature.spi.init.InjectMapperLibrary;
import com.opencloud.sentinel.feature.spi.init.InjectResourceAdaptorProvider;
import com.opencloud.slee.annotation.BinderTargets;
import com.opencloud.slee.annotation.SBBPartReference;
import com.opencloud.slee.annotation.SBBPartReferences;
import com.opencloud.sentinel.mapper.Mapper;
import com.opencloud.sentinel.mapper.MapperException;
import com.opencloud.sentinel.mapper.MapperFacilities;
import com.opencloud.sentinel.mapper.MapperLibrary;
import com.opencloud.sentinel.mapper.SipMappingPoint;
import com.opencloud.slee.resources.cdr.CDRProvider;
import com.opencloud.slee.resources.cdr.WriteTimeoutException;
import com.google.protobuf.Message;

import javax.slee.ActivityContextInterface;
import javax.slee.annotation.ComponentId;
import javax.slee.facilities.Tracer;


@SentinelFeature(
        featureName = SipicCallSpoofingSipCdrFeature.NAME,
        componentName = "@component.name@",
        featureVendor = "@component.vendor@",
        featureVersion = "@component.version@",
        useMapperLibrary=true,
        mappingExecutionPointEnum=SipMappingPoint.class,
        raProviderJndiNames= { "slee/resources/cdr/provider" },
        featureGroup = SentinelFeature.SIP_FEATURE_GROUP,
        usageStatistics = SipCdrUsageStats.class,
        executionPhases = ExecutionPhase.SipSessionPhase
)
@SBBPartReferences(
        sbbPartRefs = {
                @SBBPartReference(id = @ComponentId(name = "@sentinel-sip-spi.SentinelSipFeatureSPI SBB Part.name@", vendor = "@sentinel-sip-spi.SentinelSipFeatureSPI SBB Part.vendor@", version = "@sentinel-sip-spi.SentinelSipFeatureSPI SBB Part.version@")),
        }
)

@BinderTargets(services = "sip")
@SuppressWarnings("unused")
public class SipicCallSpoofingSipCdrFeature
        extends BaseFeature<SentinelSipSessionState, SentinelSipMultiLegFeatureEndpoint>
        implements SipFeature,
        InjectResourceAdaptorProvider,
        InjectMapperLibrary<SipMappingPoint>,
        InjectFeatureStats<SipCdrUsageStats> {

    public SipicCallSpoofingSipCdrFeature(SentinelSipMultiLegFeatureEndpoint caller, Facilities facilities, SentinelSipSessionState sessionState) {
        super(caller, facilities, sessionState);
    }

    public static final String NAME = "SipicCallSpoofingSipCdr";

    /**
     * All features must have a unique name.
     *
     * @return the name of this feature
     */
    @Override
    public String getFeatureName() {
        return NAME;
    }

    /**
     * Kick off the feature.
     *
     * @param trigger
     *            a triggering context. The feature implementation must be able to cast this to a useful type for it to run
     * @param activity
     *            the slee activity object this feature is related to (may be null)
     * @param aci
     *            the activity context interface of the slee activity this feature is related to
     */
    @Override
    public void startFeature(Object trigger, Object activity, ActivityContextInterface aci) {

        ExtendedTracer tracer = getTracer();
        tracer.finest("################################################");
        tracer.finest("Starting {}", NAME);
        tracer.finest("################################################");

        SentinelSelectionKey ssk = getSessionState().getSentinelSelectionKey();
        if (ssk == null) {
            tracer.finest("Session state SentinelSelectionKey is not set.");
            getCaller().featureHasFinished();
            return;
        }
        final Message cdr;
        try {
            cdr = mapArgumentToCDR(null, byte[].class, SipMappingPoint.EndSessionCDR);
        } catch (MapperException e) {
            featureStats.incrementSipCdrNotWritten(1);
            tracer.warning("Failed to create CDR", e);
            return;
        }

        try {
            cdrProvider.writeCDR(cdr);
            featureStats.incrementSipCdrWritten(1);
        }
        catch (WriteTimeoutException e) {
            featureStats.incrementSipCdrNotWritten(1);
            tracer.warning("Timed out writing CDR", e);
                
        }
        catch (IOException e) {
            featureStats.incrementSipCdrNotWritten(1);
            tracer.warning("IOException writing CDR", e);
        }              
   
        getCaller().featureHasFinished();
    }

    /**
     * Map an argument of a certain type, to a protobuf message (a CDR)
     *
     * @param arg the input argument
     * @param argClass class to use for mapper lookup. Included for the cases where arg might be null, or arg.getClass() would return an anonymous implementation
     * @return the CDR
     * @throws MapperException if there is a failure whilst trying to build the CDR
     */
    private Message mapArgumentToCDR(Object arg, Class<?> argClass, SipMappingPoint mappingPoint) throws MapperException {
            final Mapper<SentinelSipSessionState> mapper = mapperLibrary.findMapper(getSessionState().getSentinelSelectionKey(), argClass, Message.class, mappingPoint);

        final Object mappingResult = mapper.map(arg, getSessionState(), mapperFacilities);
        if (mappingResult == null) {
            throw new MapperException("Mapper " + mapper + " returned null");
        }

        if (!(mappingResult instanceof Message)) {
            throw new MapperException("Mapper " + mapper + " returned incorrect class: " + mappingResult.getClass());
        }

        return (Message) mappingResult;
    }
    /**
     * Implement {@link InjectFeatureStats#injectFeatureStats}
     */
    @Override
    public void injectFeatureStats(SipCdrUsageStats featureStats) {
        this.featureStats = featureStats;
    }
    @Override
    public void injectResourceAdaptorProvider(Object cdrProvider) {
        this.cdrProvider = (CDRProvider)cdrProvider;
    }    
    @Override
    public void injectMapperLibrary(MapperLibrary<SipMappingPoint> mapperLibrary, MapperFacilities mapperFacilities) {
        this.mapperLibrary = mapperLibrary;
        this.mapperFacilities = mapperFacilities;
    }
    @SuppressWarnings("FieldCanBeLocal")
    private SipCdrUsageStats featureStats;
    private CDRProvider cdrProvider;
    private MapperFacilities mapperFacilities;
    private MapperLibrary<SipMappingPoint> mapperLibrary;
}
