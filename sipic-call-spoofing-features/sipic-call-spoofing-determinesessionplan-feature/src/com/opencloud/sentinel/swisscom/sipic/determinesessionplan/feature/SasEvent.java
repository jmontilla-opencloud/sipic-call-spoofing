//
// NOTE: This class was automatically generated.  Do not edit.
//

package com.opencloud.sentinel.swisscom.sipic.determinesessionplan.feature;

import com.opencloud.rhino.facilities.sas.SasEventEnum;

/**
 * Java enum to represent the SasEvent event types.
 */
public enum SasEvent implements SasEventEnum {

    PLAN_ID_SSK_DETERMINED ( 0 ),
    PLAN_ID_SSK_NOT_DETERMINED ( 1 ),
    ;

    SasEvent(int id) {
        this.id = id;
    }

    @Override
    public int id() {
        return id;
    }

    private int id;
}
