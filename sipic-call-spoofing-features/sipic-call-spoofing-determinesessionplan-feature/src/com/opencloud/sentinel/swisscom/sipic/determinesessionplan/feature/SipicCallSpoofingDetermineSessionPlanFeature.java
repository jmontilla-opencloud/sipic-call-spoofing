package com.opencloud.sentinel.swisscom.sipic.determinesessionplan.feature;

import com.opencloud.rhino.facilities.sas.InvokingTrailAccessor;
import com.opencloud.sce.fsmtool.Facilities;
import com.opencloud.sentinel.annotations.SentinelFeature;
import com.opencloud.sentinel.common.SentinelSelectionKey;
import com.opencloud.sentinel.common.SentinelSipSessionState;
import com.opencloud.sentinel.feature.ExecutionPhase;
import com.opencloud.sentinel.feature.impl.BaseFeature;
import com.opencloud.sentinel.feature.spi.FeatureError;
import com.opencloud.sentinel.feature.spi.SentinelSipMultiLegFeatureEndpoint;
import com.opencloud.sentinel.feature.spi.SipFeature;
import com.opencloud.sentinel.feature.spi.init.InjectFeatureStats;
import com.opencloud.sentinel.multileg.Leg;
import com.opencloud.sentinel.multileg.LegManager;
import com.opencloud.slee.annotation.BinderTargets;
import com.opencloud.slee.annotation.SBBPartReference;
import com.opencloud.slee.annotation.SBBPartReferences;

import org.jainslee.resources.sip.SipRequest;
import org.jainslee.resources.sip.URI;

import javax.slee.ActivityContextInterface;
import javax.slee.annotation.ComponentId;

@SentinelFeature(
        featureName = SipicCallSpoofingDetermineSessionPlanFeature.NAME,
        componentName = "@component.name@",
        featureVendor = "@component.vendor@",
        featureVersion = "@component.version@",
        featureGroup = SentinelFeature.SIP_FEATURE_GROUP,
        executionPhases = ExecutionPhase.SipSessionPhase)
@SBBPartReferences(
        sbbPartRefs = {
                @SBBPartReference(id = @ComponentId(name = "@sentinel-sip-spi.SentinelSipFeatureSPI SBB Part.name@", vendor = "@sentinel-sip-spi.SentinelSipFeatureSPI SBB Part.vendor@", version = "@sentinel-sip-spi.SentinelSipFeatureSPI SBB Part.version@")),
        })
@BinderTargets(services = "sip")
@SuppressWarnings("unused")
public class SipicCallSpoofingDetermineSessionPlanFeature
        extends BaseFeature<SentinelSipSessionState, SentinelSipMultiLegFeatureEndpoint>
        implements SipFeature {

    public SipicCallSpoofingDetermineSessionPlanFeature(SentinelSipMultiLegFeatureEndpoint caller, Facilities facilities, SentinelSipSessionState sessionState) {
        super(caller, facilities, sessionState);
    }

    public static final String NAME = "SipicCallSpoofingDetermineSessionPlan";
    private static final String SIPIC_SESSION_PLAN = "sipic";
    private static final String PLANID_HEADER = "x-swisscom-planid";

    /**
     * All features must have a unique name.
     *
     * @return the name of this feature
     */
    @Override
    public String getFeatureName() {
        return NAME;
    }

    /**
     * Kick off the feature.
     *
     * @param trigger
     *            a triggering context. The feature implementation must be able to cast this to a useful type for it to run
     * @param activity
     *            the slee activity object this feature is related to (may be null)
     * @param aci
     *            the activity context interface of the slee activity this feature is related to
     */
    @Override
    public void startFeature(Object trigger, Object activity, ActivityContextInterface aci) {

        getTracer().finest("################################################");
        getTracer().finest("Starting {}", NAME);
        getTracer().finest("################################################");

        SentinelSelectionKey ssk = getSessionState().getSentinelSelectionKey();
        if (ssk == null) {
            getTracer().finest("Error! - Unable to set session PlanID, Reason: Could not access Sentinel selection key");
            getCaller().featureFailedToExecute(new FeatureError(FeatureError.Cause.invalidSessionState, "Could not access Sentinel selection key"));
            return;
        }

        SipRequest sipRequest = getTriggeringRequest(trigger);
        if (sipRequest == null) {
            getTracer().finest("Error! - Unable to set session PlanID, Reason: Trigger was not an initial INVITE, MESSAGE or SUBSCRIBE");
            getCaller().featureFailedToExecute(new FeatureError(FeatureError.Cause.unsupportedTriggerEvent, "Trigger was not an initial INVITE, MESSAGE or SUBSCRIBE"));
            return;
        }

        LegManager legManager = getCaller().getLegManager();
        Leg leg = legManager != null ? legManager.getCallingPartyLeg() : null;
        String planIdFromHeader = leg != null ? leg.getInviteRequest().getHeader(PLANID_HEADER) : null;
        String planIdToSet;

        if (planIdFromHeader != null) {
            getTracer().finest("Detected internal header. Session Plan ID: {}", planIdFromHeader);
            leg.getInviteRequest().removeHeader(PLANID_HEADER);
            planIdToSet = planIdFromHeader;
        } else {
            getTracer().finest("Internal header not detected: {}", PLANID_HEADER);
            planIdToSet = SIPIC_SESSION_PLAN;
        }

        String planId = ssk.getPlanId();
        if (planId.isEmpty()) {
            ssk.setPlanId(planIdToSet);
            InvokingTrailAccessor.getInvokingTrail().event(SasEvent.PLAN_ID_SSK_DETERMINED).varParam(planIdToSet).report();
            getTracer().finest("Setting Sentinel selection key to: {}", ssk);
            getSessionState().setSentinelSelectionKey(ssk);
        }
        else {
            getTracer().finest("PlanID already set, selection key will not be changed");
            InvokingTrailAccessor.getInvokingTrail().event(SasEvent.PLAN_ID_SSK_NOT_DETERMINED).report();
        }

        getCaller().featureHasFinished();
    }

    private SipRequest getTriggeringRequest(Object trigger) {
        if(!(trigger instanceof SipRequest)) {
            getTracer().finest("Trigger is not a SIP Request");
            return null;
        }

        SipRequest request = (SipRequest) trigger;
        if(!request.isInitial()) {
            getTracer().finest("Trigger is not an initial SIP Request");
            return null;
        }

        if(!SipRequest.INVITE.equals(request.getMethod())) {
            getTracer().finest("Trigger is not a INVITE request");
            return null;
        }

        return request;
    }

}
