package com.opencloud.sentinel.swisscom.sipic.feature;

import com.opencloud.rhino.facilities.ExtendedTracer;
import com.opencloud.rhino.facilities.sas.InvokingTrailAccessor;
import com.opencloud.rhino.facilities.sas.Trail;
import com.opencloud.sce.fsmtool.Facilities;
import com.opencloud.sentinel.annotations.ConfigurationReader;
import com.opencloud.sentinel.annotations.FeatureProvisioning;
import com.opencloud.sentinel.annotations.ProvisioningConfig;
import com.opencloud.sentinel.annotations.ProvisioningField;
import com.opencloud.sentinel.annotations.ProvisioningProfile;
import com.opencloud.sentinel.annotations.ProvisioningProfileId;
import com.opencloud.sentinel.annotations.SentinelFeature;
import com.opencloud.sentinel.feature.ExecutionPhase;
import com.opencloud.sentinel.feature.impl.BaseFeature;
import com.opencloud.sentinel.feature.spi.FeatureError;
import com.opencloud.sentinel.feature.spi.SentinelSipMultiLegFeatureEndpoint;
import com.opencloud.sentinel.feature.spi.init.InjectFeatureConfigurationReader;
import com.opencloud.sentinel.feature.spi.init.InjectFeatureStats;
import com.opencloud.sentinel.swisscom.sipic.common.SipicCallSpoofingSessionState;
import com.opencloud.sentinel.swisscom.sipic.config.MMIAntiSpoofingNetworkNamesConfiguration;
import com.opencloud.sentinel.swisscom.sipic.config.MMIAntiSpoofingTrunkContextsConfiguration;
import com.opencloud.sentinel.swisscom.sipic.config.MMICallingNumberAllowedLengthConfiguration;
import com.opencloud.sentinel.swisscom.sipic.config.MMIENUMConfiguration;
import com.opencloud.sentinel.swisscom.sipic.config.MMIINAPrefixesConfiguration;
import com.opencloud.sentinel.swisscom.sipic.config.SipicCallSpoofingConfigProfileReader;
import com.opencloud.sentinel.swisscom.sipic.config.SipicCallSpoofingConfigReader;
import com.opencloud.slee.annotation.BinderTargets;
import com.opencloud.slee.annotation.SBBPartReference;
import com.opencloud.slee.annotation.SBBPartReferences;

import javax.slee.ActivityContextInterface;
import javax.slee.annotation.ComponentId;
import javax.slee.annotation.LibraryReference;
import javax.slee.annotation.LibraryReferences;
import javax.slee.annotation.ProfileReference;
import javax.slee.annotation.ProfileReferences;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

/**
 *
 * SIPIC Call Spoofing Main Feature
 */
@SentinelFeature(
        featureName = SipicCallSpoofingFeature.NAME,
        componentName = "@component.name@",
        featureVendor = "@component.vendor@",
        featureVersion = "@component.version@",
        featureGroup = SentinelFeature.SIP_FEATURE_GROUP,
        configurationReader = @ConfigurationReader(
                readerInterface = SipicCallSpoofingConfigReader.class,
                readerClass = SipicCallSpoofingConfigProfileReader.class
        ),
        usageStatistics = SipicCallSpoofingUsageStats.class,
        executionPhases = {ExecutionPhase.SipSessionPhase},
        provisioning = @FeatureProvisioning(
                displayName = "Swisscom SIPIC Call Spoofing",
                configs = {
                        @ProvisioningConfig(
                                type = "MMIENUMConfig",
                                displayName = "MMI ENUM Config",
                                fields = {
                                        @ProvisioningField(name="AllowedTransitInternationalPrefixes", type="int[]",    displayName="Allowed Transit International Prefixes", description="Allowed Transit International Prefixes"),
                                        @ProvisioningField(name="AnnouncementNetworkName",             type="String",   displayName="Announcement Network Name",              description="Announcement Network Name"),
                                        @ProvisioningField(name="AntiSpoofingAction",                  type="String",   displayName="AntiSpoofing Action",                    description="AntiSpoofing Action"),
                                        @ProvisioningField(name="AntiSpoofingInvalidNumberAction",     type="String",   displayName="AntiSpoofing Invalid Number Action",     description="AntiSpoofing Invalid Number Action"),
                                        @ProvisioningField(name="AntiSpoofingDisasterSwitch",          type="int",      displayName="AntiSpoofing Disaster Switch",           description="AntiSpoofing Disaster Switch"),
                                        @ProvisioningField(name="AntiSpoofingTrunkContextCheck",       type="boolean",  displayName="AntiSpoofing Trunk Context Check",       description="AntiSpoofing Trunk Context Check"),
                                        @ProvisioningField(name="CDPIDPrefixes",                       type="int[]",    displayName="CDPID Prefixes",                         description="CDPID Prefixes"),
                                        @ProvisioningField(name="CorporatePrefix",                     type="int[]",    displayName="Corporate Prefix",                       description="Corporate Prefix"),
                                        @ProvisioningField(name="CSCPrefixes",                         type="int[]",    displayName="CSC Prefixes",                           description="CSC Prefixes"),
                                        @ProvisioningField(name="DefaultNetworkName",                  type="String",   displayName="Default Network Name",                   description="Default Network Name"),
                                        @ProvisioningField(name="HomeCountryCodes",                    type="int[]",    displayName="Home Country Codes",                     description="Home Country Codes"),
                                        @ProvisioningField(name="NativeCorporateRoutingPrefixes",      type="String[]", displayName="Native Corporate Routing Prefixes",      description="Native Corporate Routing Prefixes"),
                                        @ProvisioningField(name="NoAntiSpoofingCalledPrefixes",        type="String[]", displayName="No AntiSpoofing Called Prefixes",        description="No AntiSpoofing Called Prefixes"),
                                        @ProvisioningField(name="NoAntiSpoofingProducts",              type="String[]", displayName="No AntiSpoofing Products",               description="No AntiSpoofing Products"),
                                        @ProvisioningField(name="NoTransitNPRN",                       type="int[]",    displayName="No Transit NPRN",                        description="No Transit NPRN"),
                                        @ProvisioningField(name="NPRNPrefix",                          type="int[]",    displayName="NPRN Prefix",                            description="NPRN Prefix"),
                                        @ProvisioningField(name="O90xNumberPrefix",                    type="int",      displayName="O90x Number Prefix",                     description="O90x Number Prefix"),
                                        @ProvisioningField(name="OtherPrefixes",                       type="String[]", displayName="Other Prefixes",                         description="Announcement Network Name"),
                                        @ProvisioningField(name="ShortNumberNPRN",                     type="int",      displayName="Short Number NPRN",                      description="Short Number NPRN"),
                                        @ProvisioningField(name="SwisscomGenericNRN",                  type="String[]", displayName="Swisscom Generic NRN",                   description="Swisscom Generic NRN"),
                                        @ProvisioningField(name="SwisscomMobileNPRN",                  type="int[]",    displayName="Swisscom Mobile NPRN",                   description="Swisscom Mobile NPRN"),
                                        @ProvisioningField(name="SwisscomMobileNRN",                   type="int[]",    displayName="Swisscom Mobile NRN",                    description="Swisscom Mobile NRN"),
                                        @ProvisioningField(name="SwisscomNPRN",                        type="int",      displayName="Swisscom NPRN",                          description="Swisscom NPRN"),
                                        @ProvisioningField(name="SwisscomNRNPrefixes",                 type="int[]",    displayName="Swisscom NRN Prefixes",                  description="Swisscom NRN Prefixes"),
                                        @ProvisioningField(name="TDMTrunkContexts",                    type="String[]", displayName="TimeToLive In Dynamic Whitelist",        description="TimeToLive In Dynamic Whitelist"),
                                        @ProvisioningField(name="TimeToLiveInDynamicWhitelist",        type="int",      displayName="AntiSpoofing Disaster Switch",           description="AntiSpoofing Disaster Switch"),
                                        @ProvisioningField(name="VoicemailPrefix",                     type="int",      displayName="Voicemail Prefix",                       description="Voicemail Prefix"),
                                        @ProvisioningField(name="VPNPrefix",                           type="int",      displayName="VPN Prefix",                             description="VPN Prefix")
                                },
                                profile = @ProvisioningProfile(
                                        tableName = "MMIENUMConfigTable",
                                        specification = @ProvisioningProfileId(name = "@mmi-enum-config-profile.name@", vendor = "@mmi-enum-config-profile.vendor@", version = "@mmi-enum-config-profile.version@")
                                )
                        ),
                        @ProvisioningConfig(
                                type = "MMIINAPrefixes",
                                displayName = "MMI INA Prefixes",
                                fields = {
                                        @ProvisioningField(name="INAPrefixes", type="int[]", displayName="INA Prefixes", description="INA Prefixes")
                                },
                                profile = @ProvisioningProfile(
                                        tableName = "MMIINAPrefixesTable",
                                        specification = @ProvisioningProfileId(name = "@mmi-ina-prefixes-profile.name@", vendor = "@mmi-ina-prefixes-profile.vendor@", version = "@mmi-ina-prefixes-profile.version@")
                                )
                        ),
                        @ProvisioningConfig(
                                type = "MMIAntiSpoofingNetworkNames",
                                displayName = "MMI AntiSpoofing Network Names",
                                fields = {
                                        @ProvisioningField(name="NetworkName", type="String", displayName="Network Name", description="Network Name", id=true)
                                },
                                profile = @ProvisioningProfile(
                                        tableName = "MMIAntiSpoofingNetworkNames",
                                        specification = @ProvisioningProfileId(name = "@mmi-antispoofing-network-names-profile.name@", vendor = "@mmi-antispoofing-network-names-profile.vendor@", version = "@mmi-antispoofing-network-names-profile.version@")
                                )
                        ),
                        @ProvisioningConfig(
                                type = "MMIAntiSpoofingTrunkContexts",
                                displayName = "MMI AntiSpoofing Trunk Contexts",
                                fields = {
                                        @ProvisioningField(name="TrunkContext", type="String", displayName="Trunk Context", description="Trunk Context", id=true)
                                },
                                profile = @ProvisioningProfile(
                                        tableName = "MMIAntiSpoofingTrunkContexts",
                                        specification = @ProvisioningProfileId(name = "@mmi-antispoofing-trunk-contexts-profile.name@", vendor = "@mmi-antispoofing-trunk-contexts-profile.vendor@", version = "@mmi-antispoofing-trunk-contexts-profile.version@")
                                )
                        ),
                        @ProvisioningConfig(
                                type = "MMICallingNumberAllowedLengthTable",
                                displayName = "MMI Calling Number Allowed Length Table",
                                fields = {
                                        @ProvisioningField(name="CallingNumberRange", type="String", displayName="Calling Number Range", description="Calling Number Range", id=true),
                                        @ProvisioningField(name="AllowedLengths", type="int[]", displayName="Allowed Lengths", description="Allowed Lengths")
                                },
                                profile = @ProvisioningProfile(
                                        tableName = "MMICallingNumberAllowedLengthTable",
                                        specification = @ProvisioningProfileId(name = "@mmi-calling-number-allowed-length-profile.name@", vendor = "@mmi-calling-number-allowed-length-profile.vendor@", version = "@mmi-calling-number-allowed-length-profile.version@")
                                )
                        )
                }
        )
)
@SBBPartReferences(
        sbbPartRefs = {
                @SBBPartReference(id = @ComponentId(name = "@sentinel-sip-spi.SentinelSipFeatureSPI SBB Part.name@", vendor = "@sentinel-sip-spi.SentinelSipFeatureSPI SBB Part.vendor@", version = "@sentinel-sip-spi.SentinelSipFeatureSPI SBB Part.version@")),
        }
)
@LibraryReferences(
        libraryRefs = {
                @LibraryReference(library = @ComponentId(
                        name = "@sipic-call-spoofing-common-library.name@",
                        vendor = "@sipic-call-spoofing-common-library.vendor@",
                        version = "@sipic-call-spoofing-common-library.version@")),
        }
)
@ProfileReferences(
        profileRefs = {
                @ProfileReference(profile = @ComponentId(name="@mmi-enum-config-profile.name@", vendor="@mmi-enum-config-profile.vendor@", version="@mmi-enum-config-profile.version@")),
                @ProfileReference(profile = @ComponentId(name="@mmi-ina-prefixes-profile.name@", vendor="@mmi-ina-prefixes-profile.vendor@", version="@mmi-ina-prefixes-profile.version@")),
                @ProfileReference(profile = @ComponentId(name="@mmi-antispoofing-network-names-profile.name@", vendor="@mmi-antispoofing-network-names-profile.vendor@", version="@mmi-antispoofing-network-names-profile.version@")),
                @ProfileReference(profile = @ComponentId(name="@mmi-antispoofing-trunk-contexts-profile.name@", vendor="@mmi-antispoofing-trunk-contexts-profile.vendor@", version="@mmi-antispoofing-trunk-contexts-profile.version@")),
                @ProfileReference(profile = @ComponentId(name="@mmi-calling-number-allowed-length-profile.name@", vendor="@mmi-calling-number-allowed-length-profile.vendor@", version="@mmi-calling-number-allowed-length-profile.version@"))
        }
)

@BinderTargets(services = "sip")
@SuppressWarnings("unused")
public class SipicCallSpoofingFeature extends BaseFeature<SipicCallSpoofingSessionState, SentinelSipMultiLegFeatureEndpoint>
        implements InjectFeatureStats<SipicCallSpoofingUsageStats>,
        InjectFeatureConfigurationReader<SipicCallSpoofingConfigReader> {

    public SipicCallSpoofingFeature(SentinelSipMultiLegFeatureEndpoint caller, Facilities facilities, SipicCallSpoofingSessionState sessionState) {
        super(caller, facilities, sessionState);
    }

    public static final String NAME = "SipicCallSpoofing";

    private SipicCallSpoofingConfigReader configReader;

    @SuppressWarnings("FieldCanBeLocal")
    private SipicCallSpoofingUsageStats featureStats;

    /**
     * All features must have a unique name.
     *
     * @return the name of this feature
     */
    @Override
    public String getFeatureName() { return NAME; }

    /**
     * Implement {@link InjectFeatureStats#injectFeatureStats}
     */
    @Override
    public void injectFeatureStats(SipicCallSpoofingUsageStats featureStats) {
        this.featureStats = featureStats;
    }

    /**
     * Implement {@link InjectFeatureConfigurationReader#injectFeatureConfigurationReader}
     */
    @Override
    public void injectFeatureConfigurationReader(SipicCallSpoofingConfigReader configurationReader) {
        this.configReader = configurationReader;
    }
    /**
     * Kick off the feature.
     *
     * @param trigger  a triggering context. The feature implementation must be able to cast this to a useful type for it to run
     * @param activity the slee activity object this feature is related to (may be null)
     * @param aci      the activity context interface of the slee activity this feature is related to
     */
    @Override
    public void startFeature(Object trigger, Object activity, ActivityContextInterface aci) {

        ExtendedTracer tracer = getTracer();
        tracer.finest("################################################");
        tracer.finest("Starting {}", NAME);
        tracer.finest("################################################");

        printProfilesData();

        // Report a SAS event
        Trail sasTrail = InvokingTrailAccessor.getInvokingTrail();
        sasTrail.event(SasEvent.SIPIC_CALL_SPOOFING_SAS_TRACE).staticParam(0).varParam(trigger).varParam(activity).report();

        getCaller().featureHasFinished();

        featureStats.incrementFeatureStarted(1);
    }

    private void printProfilesData() {

        ExtendedTracer tracer = getTracer();

        // MMIENUMConfig Profile Table -- RHINO PROFILE
        MMIENUMConfiguration mmiEnumConfig;
        mmiEnumConfig = configReader.getMMIENUMConfiguration();
        if (mmiEnumConfig == null) {
            tracer.finest("MMIENUMConfiguration configuration doesn't exist");
            getCaller().featureCannotStart(new FeatureError(FeatureError.Cause.invalidConfiguration));
            getCaller().featureHasFinished();
            return;
        } else {
            tracer.finest("MMIENUMConfigTable:MMIENUMConfig ============> RHINO PROFILE <============ ");
            tracer.finest("MMIENUMConfigTable:MMIENUMConfig ShortNumberNPRN = {}", mmiEnumConfig.getShortNumberNPRN());
            tracer.finest("MMIENUMConfigTable:MMIENUMConfig AntiSpoofingAction = {}", mmiEnumConfig.getAntiSpoofingAction());
            tracer.finest("MMIENUMConfigTable:MMIENUMConfig AntiSpoofingInvalidNumberAction = {}", mmiEnumConfig.getAntiSpoofingInvalidNumberAction());
            tracer.finest("MMIENUMConfigTable:MMIENUMConfig AntiSpoofingDisasterSwitch = {}", mmiEnumConfig.getAntiSpoofingDisasterSwitch());
            tracer.finest("MMIENUMConfigTable:MMIENUMConfig AntiSpoofingTrunkContextCheck = {}", mmiEnumConfig.getAntiSpoofingTrunkContextCheck());
            tracer.finest("MMIENUMConfigTable:MMIENUMConfig AnnouncementNetworkName = {}", mmiEnumConfig.getAnnouncementNetworkName());
            tracer.finest("MMIENUMConfigTable:MMIENUMConfig DefaultNetworkName = {}", mmiEnumConfig.getDefaultNetworkName());
            tracer.finest("MMIENUMConfigTable:MMIENUMConfig SwisscomNPRN = {}", mmiEnumConfig.getSwisscomNPRN());
            tracer.finest("MMIENUMConfigTable:MMIENUMConfig VPNPrefix = {}", mmiEnumConfig.getVPNPrefix());
            tracer.finest("MMIENUMConfigTable:MMIENUMConfig VoicemailPrefix = {}", mmiEnumConfig.getVoicemailPrefix());
            tracer.finest("MMIENUMConfigTable:MMIENUMConfig O90xNumberPrefix = {}", mmiEnumConfig.getO90xNumberPrefix());
            tracer.finest("MMIENUMConfigTable:MMIENUMConfig TimeToLiveInDynamicWhitelist = {}", mmiEnumConfig.getTimeToLiveInDynamicWhitelist());
            tracer.finest("MMIENUMConfigTable:MMIENUMConfig NoAntiSpoofingProducts = {}", Arrays.toString(mmiEnumConfig.getNoAntiSpoofingProducts()));
            tracer.finest("MMIENUMConfigTable:MMIENUMConfig NoAntiSpoofingCalledPrefixes = {}", Arrays.toString(mmiEnumConfig.getNoAntiSpoofingCalledPrefixes()));
            tracer.finest("MMIENUMConfigTable:MMIENUMConfig SwisscomGenericNRN = {}", Arrays.toString(mmiEnumConfig.getSwisscomGenericNRN()));
            tracer.finest("MMIENUMConfigTable:MMIENUMConfig NativeCorporateRoutingPrefixes = {}", Arrays.toString(mmiEnumConfig.getNativeCorporateRoutingPrefixes()));
            tracer.finest("MMIENUMConfigTable:MMIENUMConfig AllowedTransitInternationalPrefixes = {}", Arrays.toString(mmiEnumConfig.getAllowedTransitInternationalPrefixes()));
            tracer.finest("MMIENUMConfigTable:MMIENUMConfig HomeCountryCodes = {}", Arrays.toString(mmiEnumConfig.getHomeCountryCodes()));
            tracer.finest("MMIENUMConfigTable:MMIENUMConfig CSCPrefixes = {}", Arrays.toString(mmiEnumConfig.getCSCPrefixes()));
            tracer.finest("MMIENUMConfigTable:MMIENUMConfig CDPIDPrefixes = {}", Arrays.toString(mmiEnumConfig.getCDPIDPrefixes()));
            tracer.finest("MMIENUMConfigTable:MMIENUMConfig SwisscomMobileNPRN = {}", Arrays.toString(mmiEnumConfig.getSwisscomMobileNPRN()));
            tracer.finest("MMIENUMConfigTable:MMIENUMConfig SwisscomMobileNRN = {}", Arrays.toString(mmiEnumConfig.getSwisscomMobileNRN()));
            tracer.finest("MMIENUMConfigTable:MMIENUMConfig SwisscomNRNPrefixes = {}", Arrays.toString(mmiEnumConfig.getSwisscomNRNPrefixes()));
            tracer.finest("MMIENUMConfigTable:MMIENUMConfig NPRNPrefix = {}", Arrays.toString(mmiEnumConfig.getNPRNPrefix()));
            tracer.finest("MMIENUMConfigTable:MMIENUMConfig NoTransitNPRN = {}", Arrays.toString(mmiEnumConfig.getNoTransitNPRN()));
            tracer.finest("MMIENUMConfigTable:MMIENUMConfig CorporatePrefix = {}", Arrays.toString(mmiEnumConfig.getCorporatePrefix()));
            tracer.finest("MMIENUMConfigTable:MMIENUMConfig OtherPrefixes = {}", Arrays.toString(mmiEnumConfig.getOtherPrefixes()));
        }

        // MMIENUMConfig Profile Table -- SENTINEL SSK PROFILE
        mmiEnumConfig = configReader.getMMIENUMConfiguration(getSessionState().getSentinelSelectionKey());
        if (mmiEnumConfig == null) {
            tracer.finest("MMIENUMConfiguration configuration doesn't exist");
            getCaller().featureCannotStart(new FeatureError(FeatureError.Cause.invalidConfiguration));
            getCaller().featureHasFinished();
            return;
        } else {
            tracer.finest("MMIENUMConfigTable:MMIENUMConfig ============> SENTINEL SSK PROFILE <============ ");
            tracer.finest("MMIENUMConfigTable:MMIENUMConfig ShortNumberNPRN = {}", mmiEnumConfig.getShortNumberNPRN());
            tracer.finest("MMIENUMConfigTable:MMIENUMConfig AntiSpoofingAction = {}", mmiEnumConfig.getAntiSpoofingAction());
            tracer.finest("MMIENUMConfigTable:MMIENUMConfig AntiSpoofingInvalidNumberAction = {}", mmiEnumConfig.getAntiSpoofingInvalidNumberAction());
            tracer.finest("MMIENUMConfigTable:MMIENUMConfig AntiSpoofingDisasterSwitch = {}", mmiEnumConfig.getAntiSpoofingDisasterSwitch());
            tracer.finest("MMIENUMConfigTable:MMIENUMConfig AntiSpoofingTrunkContextCheck {}", mmiEnumConfig.getAntiSpoofingTrunkContextCheck());
            tracer.finest("MMIENUMConfigTable:MMIENUMConfig AnnouncementNetworkName = {}", mmiEnumConfig.getAnnouncementNetworkName());
            tracer.finest("MMIENUMConfigTable:MMIENUMConfig DefaultNetworkName = {}", mmiEnumConfig.getDefaultNetworkName());
            tracer.finest("MMIENUMConfigTable:MMIENUMConfig SwisscomNPRN = {}", mmiEnumConfig.getSwisscomNPRN());
            tracer.finest("MMIENUMConfigTable:MMIENUMConfig VPNPrefix = {}", mmiEnumConfig.getVPNPrefix());
            tracer.finest("MMIENUMConfigTable:MMIENUMConfig VoicemailPrefix = {}", mmiEnumConfig.getVoicemailPrefix());
            tracer.finest("MMIENUMConfigTable:MMIENUMConfig O90xNumberPrefix = {}", mmiEnumConfig.getO90xNumberPrefix());
            tracer.finest("MMIENUMConfigTable:MMIENUMConfig TimeToLiveInDynamicWhitelist = {}", mmiEnumConfig.getTimeToLiveInDynamicWhitelist());
            tracer.finest("MMIENUMConfigTable:MMIENUMConfig NoAntiSpoofingProducts = {}", Arrays.toString(mmiEnumConfig.getNoAntiSpoofingProducts()));
            tracer.finest("MMIENUMConfigTable:MMIENUMConfig NoAntiSpoofingCalledPrefixes = {}", Arrays.toString(mmiEnumConfig.getNoAntiSpoofingCalledPrefixes()));
            tracer.finest("MMIENUMConfigTable:MMIENUMConfig SwisscomGenericNRN = {}", Arrays.toString(mmiEnumConfig.getSwisscomGenericNRN()));
            tracer.finest("MMIENUMConfigTable:MMIENUMConfig NativeCorporateRoutingPrefixes = {}", Arrays.toString(mmiEnumConfig.getNativeCorporateRoutingPrefixes()));
            tracer.finest("MMIENUMConfigTable:MMIENUMConfig AllowedTransitInternationalPrefixes = {}", Arrays.toString(mmiEnumConfig.getAllowedTransitInternationalPrefixes()));
            tracer.finest("MMIENUMConfigTable:MMIENUMConfig HomeCountryCodes = {}", Arrays.toString(mmiEnumConfig.getHomeCountryCodes()));
            tracer.finest("MMIENUMConfigTable:MMIENUMConfig CSCPrefixes = {}", Arrays.toString(mmiEnumConfig.getCSCPrefixes()));
            tracer.finest("MMIENUMConfigTable:MMIENUMConfig CDPIDPrefixes = {}", Arrays.toString(mmiEnumConfig.getCDPIDPrefixes()));
            tracer.finest("MMIENUMConfigTable:MMIENUMConfig SwisscomMobileNPRN = {}", Arrays.toString(mmiEnumConfig.getSwisscomMobileNPRN()));
            tracer.finest("MMIENUMConfigTable:MMIENUMConfig SwisscomMobileNRN = {}", Arrays.toString(mmiEnumConfig.getSwisscomMobileNRN()));
            tracer.finest("MMIENUMConfigTable:MMIENUMConfig SwisscomNRNPrefixes = {}", Arrays.toString(mmiEnumConfig.getSwisscomNRNPrefixes()));
            tracer.finest("MMIENUMConfigTable:MMIENUMConfig NPRNPrefix = {}", Arrays.toString(mmiEnumConfig.getNPRNPrefix()));
            tracer.finest("MMIENUMConfigTable:MMIENUMConfig NoTransitNPRN = {}", Arrays.toString(mmiEnumConfig.getNoTransitNPRN()));
            tracer.finest("MMIENUMConfigTable:MMIENUMConfig CorporatePrefix = {}", Arrays.toString(mmiEnumConfig.getCorporatePrefix()));
            tracer.finest("MMIENUMConfigTable:MMIENUMConfig OtherPrefixes = {}", Arrays.toString(mmiEnumConfig.getOtherPrefixes()));
        }

        // MMIINAPrefixes Profile Table -- RHINO PROFILE
        MMIINAPrefixesConfiguration mmiInaPrefixesConfig;
        mmiInaPrefixesConfig = configReader.getMMIINAPrefixesConfiguration();
        if (mmiInaPrefixesConfig == null) {
            tracer.finest("MMIINAPrefixesConfiguration configuration doesn't exist");
            getCaller().featureCannotStart(new FeatureError(FeatureError.Cause.invalidConfiguration));
            getCaller().featureHasFinished();
            return;
        } else {
            tracer.finest("MMIINAPrefixesTable:INAPrefixes ============> RHINO PROFILE <============ ");
            tracer.finest("MMIINAPrefixesTable:INAPrefixes = {}", Arrays.toString(mmiInaPrefixesConfig.getINAPrefixes()));
        }

        // MMIINAPrefixes Profile Table -- SENTINEL SSK PROFILE
        mmiInaPrefixesConfig = configReader.getMMIINAPrefixesConfiguration(getSessionState().getSentinelSelectionKey());
        if (mmiInaPrefixesConfig == null) {
            tracer.finest("MMIINAPrefixesConfiguration configuration doesn't exist");
            getCaller().featureCannotStart(new FeatureError(FeatureError.Cause.invalidConfiguration));
            getCaller().featureHasFinished();
            return;
        } else {
            tracer.finest("MMIINAPrefixesTable:INAPrefixes ============> SENTINEL SSK PROFILE <============ ");
            tracer.finest("MMIINAPrefixesTable:INAPrefixes = {}", Arrays.toString(mmiInaPrefixesConfig.getINAPrefixes()));
        }

        // MMIAntispoofingNetworkNames Profile Table -- RHINO PROFILE
        tracer.finest("MMIAntispoofingNetworkNames:  ============> RHINO PROFILE <============ ");
        MMIAntiSpoofingNetworkNamesConfiguration mmiAntispoofingNetworkNamesConfig;
        mmiAntispoofingNetworkNamesConfig = configReader.getMMIAntispoofingNetworkNamesConfiguration();
        if (mmiAntispoofingNetworkNamesConfig == null) {
            tracer.finest("MMIAntispoofingNetworkNamesConfiguration configuration doesn't exist");
            getCaller().featureCannotStart(new FeatureError(FeatureError.Cause.invalidConfiguration));
            getCaller().featureHasFinished();
            return;
        } else {
            tracer.finest("MMIAntispoofingNetworkNames: ");
            ArrayList<String> networkNames = mmiAntispoofingNetworkNamesConfig.getNetworkNames();
            for (String networkName : networkNames) {
                tracer.finest("NetworkName: {}", networkName);
            }
        }

        String network = "Swisscom Mobile";
        tracer.finest("MMIAntispoofingNetworkNames: network '{}' in MMIAntispoofingNetworkNames? --> {}", network, configReader.isNetworkInMMIAntispoofingNetworkNames(network));
        network = "Telefonica";
        tracer.finest("MMIAntispoofingNetworkNames: network '{}' in MMIAntispoofingNetworkNames? --> {}", network, configReader.isNetworkInMMIAntispoofingNetworkNames(network));

        // MMIAntispoofingNetworkNames Profile Table -- SENTINEL SSK PROFILE
        tracer.finest("MMIAntispoofingNetworkNames:  ============> SENTINEL SSK PROFILE <============ ");
        mmiAntispoofingNetworkNamesConfig = configReader.getMMIAntispoofingNetworkNamesConfiguration(getSessionState().getSentinelSelectionKey());
        if (mmiAntispoofingNetworkNamesConfig == null) {
            tracer.finest("MMIAntispoofingNetworkNamesConfiguration configuration doesn't exist");
            getCaller().featureCannotStart(new FeatureError(FeatureError.Cause.invalidConfiguration));
            getCaller().featureHasFinished();
            return;
        } else {
            tracer.finest("MMIAntispoofingNetworkNames: ");
            ArrayList<String> networkNames = mmiAntispoofingNetworkNamesConfig.getNetworkNames();
            for (String networkName : networkNames) {
                tracer.finest("NetworkName: {}", networkName);
            }
        }

        network = "Swisscom Mobile";
        tracer.finest("MMIAntispoofingNetworkNames: network '{}' in MMIAntispoofingNetworkNames? --> {}", network, configReader.isNetworkInMMIAntispoofingNetworkNames(network, getSessionState().getSentinelSelectionKey()));
        network = "Telefonica";
        tracer.finest("MMIAntispoofingNetworkNames: network '{}' in MMIAntispoofingNetworkNames? --> {}", network, configReader.isNetworkInMMIAntispoofingNetworkNames(network, getSessionState().getSentinelSelectionKey()));

        // MMIAntispoofingTrunkContexts Profile Table -- RHINO PROFILE
        tracer.finest("MMIAntispoofingNetworkNames:  ============> RHINO PROFILE <============ ");
        MMIAntiSpoofingTrunkContextsConfiguration mmiAntispoofingTrunkContextsConfig;
        mmiAntispoofingTrunkContextsConfig = configReader.getMMIAntiSpoofingTrunkContextsConfiguration();
        if (mmiAntispoofingTrunkContextsConfig == null) {
            tracer.finest("MMIAntiSpoofingTrunkContextsConfiguration configuration doesn't exist");
            getCaller().featureCannotStart(new FeatureError(FeatureError.Cause.invalidConfiguration));
            getCaller().featureHasFinished();
            return;
        } else {
            tracer.finest("MMIAntispoofingTrunkContexts: ");
            ArrayList<String> trunkContexts = mmiAntispoofingTrunkContextsConfig.getTrunkContexts();
            for (String context : trunkContexts) {
                tracer.finest("TrunkContext: {}", context);
            }
        }
        String contextName = "upse.olo1.ch";
        tracer.finest("MMIAntispoofingTrunkContexts: context '{}' in MMIAntispoofingTrunkContexts? --> {}", contextName, configReader.isContextInMMIAntiSpoofingTrunkContexts(contextName));
        contextName = "upse.olo1.com";
        tracer.finest("MMIAntispoofingTrunkContexts: context '{}' in MMIAntispoofingTrunkContexts? --> {}", contextName, configReader.isContextInMMIAntiSpoofingTrunkContexts(contextName));

        // MMIAntispoofingTrunkContexts Profile Table -- SENTINEL SSK PROFILE
        tracer.finest("MMIAntispoofingNetworkNames:  ============> SENTINEL SSK PROFILE <============ ");
        mmiAntispoofingTrunkContextsConfig = configReader.getMMIAntiSpoofingTrunkContextsConfiguration(getSessionState().getSentinelSelectionKey());
        if (mmiAntispoofingTrunkContextsConfig == null) {
            tracer.finest("MMIAntiSpoofingTrunkContextsConfiguration configuration doesn't exist");
            getCaller().featureCannotStart(new FeatureError(FeatureError.Cause.invalidConfiguration));
            getCaller().featureHasFinished();
            return;
        } else {
            tracer.finest("MMIAntispoofingTrunkContexts: ");
            ArrayList<String> trunkContexts = mmiAntispoofingTrunkContextsConfig.getTrunkContexts();
            for (String context : trunkContexts) {
                tracer.finest("TrunkContext: {}", context);
            }
        }
        contextName = "upse.olo1.ch";
        tracer.finest("MMIAntispoofingTrunkContexts: context '{}' in MMIAntispoofingTrunkContexts? --> {}", contextName, configReader.isContextInMMIAntiSpoofingTrunkContexts(contextName, getSessionState().getSentinelSelectionKey()));
        contextName = "upse.olo1.com";
        tracer.finest("MMIAntispoofingTrunkContexts: context '{}' in MMIAntispoofingTrunkContexts? --> {}", contextName, configReader.isContextInMMIAntiSpoofingTrunkContexts(contextName, getSessionState().getSentinelSelectionKey()));

        // MMICallingNumberAllowedLengthTable Profile Table -- RHINO PROFILE
        MMICallingNumberAllowedLengthConfiguration mmiCallingNumberAllowedLengthConfig;
        mmiCallingNumberAllowedLengthConfig = configReader.getMMICallingNumberAllowedLengthConfiguration();
        if (mmiCallingNumberAllowedLengthConfig == null) {
            tracer.finest("MMICallingNumberAllowedLengthConfiguration configuration doesn't exist");
            getCaller().featureCannotStart(new FeatureError(FeatureError.Cause.invalidConfiguration));
            getCaller().featureHasFinished();
            return;
        } else {
            tracer.finest("MMICallingNumberAllowedLengthTable: ");
            Map<String, int[]> allowedLengths = mmiCallingNumberAllowedLengthConfig.getCallingNumberAllowedLengths();
            for(String key: allowedLengths.keySet()) {
                tracer.finest("CallingNumberRange: {} -> AllowedLengths: {}", key, Arrays.toString(allowedLengths.get(key)));
            }
        }
        String callingRange = "2";
        tracer.finest("MMICallingNumberAllowedLengthTable: CallingNumberRange '{}' in MMICallingNumberAllowedLengthTable? --> {}", contextName, configReader.isCallingRangeInMMICallingNumberAllowedLength(callingRange));
        callingRange = "222";
        tracer.finest("MMICallingNumberAllowedLengthTable: CallingNumberRange ''{}' in MMICallingNumberAllowedLengthTable? --> {}", contextName, configReader.isCallingRangeInMMICallingNumberAllowedLength(callingRange));

        // MMICallingNumberAllowedLengthTable Profile Table -- SENTINEL SSK PROFILE
        mmiCallingNumberAllowedLengthConfig = configReader.getMMICallingNumberAllowedLengthConfiguration(getSessionState().getSentinelSelectionKey());
        if (mmiCallingNumberAllowedLengthConfig == null) {
            tracer.finest("MMICallingNumberAllowedLengthConfiguration configuration doesn't exist");
            getCaller().featureCannotStart(new FeatureError(FeatureError.Cause.invalidConfiguration));
            getCaller().featureHasFinished();
            return;
        } else {
            tracer.finest("MMICallingNumberAllowedLengthTable: ");
            Map<String, int[]> allowedLengths = mmiCallingNumberAllowedLengthConfig.getCallingNumberAllowedLengths();
            for(String key: allowedLengths.keySet()) {
                tracer.finest("CallingNumberRange: {} -> AllowedLengths: {}", key, Arrays.toString(allowedLengths.get(key)));
            }
        }
        callingRange = "2";
        tracer.finest("MMICallingNumberAllowedLengthTable: CallingNumberRange '{}' in MMICallingNumberAllowedLengthTable? --> {}", contextName, configReader.isCallingRangeInMMICallingNumberAllowedLength(callingRange, getSessionState().getSentinelSelectionKey()));
        callingRange = "222";
        tracer.finest("MMICallingNumberAllowedLengthTable: CallingNumberRange '{}' in MMICallingNumberAllowedLengthTable? --> {}", contextName, configReader.isCallingRangeInMMICallingNumberAllowedLength(callingRange, getSessionState().getSentinelSelectionKey()));

    }

}