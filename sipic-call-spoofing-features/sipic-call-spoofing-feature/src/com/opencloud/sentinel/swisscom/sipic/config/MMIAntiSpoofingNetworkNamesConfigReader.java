package com.opencloud.sentinel.swisscom.sipic.config;

import com.opencloud.sentinel.common.SentinelSelectionKey;

@SuppressWarnings("unused")
public interface MMIAntiSpoofingNetworkNamesConfigReader {

    MMIAntiSpoofingNetworkNamesConfiguration getMMIAntispoofingNetworkNamesConfiguration(SentinelSelectionKey key);
    boolean isNetworkInMMIAntispoofingNetworkNames(String network, SentinelSelectionKey key);
}

