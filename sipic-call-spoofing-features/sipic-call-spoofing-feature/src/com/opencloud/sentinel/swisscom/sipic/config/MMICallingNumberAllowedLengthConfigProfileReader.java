package com.opencloud.sentinel.swisscom.sipic.config;

import com.opencloud.sce.fsmtool.Facilities;
import com.opencloud.sentinel.common.BaseConfigProfileReader;
import com.opencloud.sentinel.common.SentinelSelectionKey;
import com.opencloud.sentinel.swisscom.sipic.profile.MMICallingNumberAllowedLengthLocal;

import javax.slee.profile.ProfileTable;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class MMICallingNumberAllowedLengthConfigProfileReader extends BaseConfigProfileReader<MMICallingNumberAllowedLengthLocal> implements MMICallingNumberAllowedLengthConfigReader {

    public static final String MMI_CALLING_NUMBER_ALLOWED_LENGTH_PROFILE_TABLE_NAME = "MMICallingNumberAllowedLengthTable";

    public MMICallingNumberAllowedLengthConfigProfileReader(Facilities facilities) {
        super(facilities.getProfileFacility(), facilities.getTracer(), MMI_CALLING_NUMBER_ALLOWED_LENGTH_PROFILE_TABLE_NAME);
    }

    @Override
    public MMICallingNumberAllowedLengthConfiguration getMMICallingNumberAllowedLengthConfiguration(SentinelSelectionKey key) {
        ProfileTable profileTable;
        try {
            profileTable = getProfileFacility().getProfileTable(MMI_CALLING_NUMBER_ALLOWED_LENGTH_PROFILE_TABLE_NAME);
        } catch (Exception e) {
            getTracer().fine("SipicCallSpoofingConfigProfileReader: Error when retrieving Profile Table {}", MMI_CALLING_NUMBER_ALLOWED_LENGTH_PROFILE_TABLE_NAME);
            return null;
        }

        Map<String, int[]> result = new HashMap<>();
        Collection allProfiles = profileTable.findAll();
        for (Object aProfile : allProfiles) {
            final MMICallingNumberAllowedLengthLocal profile = (MMICallingNumberAllowedLengthLocal) aProfile;
            if (key == null || profile.getProfileName().contains(key.asString())) {
                result.put(profile.getCallingNumberRange(), profile.getAllowedLengths());
            }
        }

        return new MMICallingNumberAllowedLengthConfiguration() {
            @Override
            public Map<String, int[]> getCallingNumberAllowedLengths() {
                return result;
            }
        };
    }

    @Override
    public boolean isCallingRangeInMMICallingNumberAllowedLength(String callingRange, SentinelSelectionKey key) {
        if (key != null) { // Profile is accessed via SentinelSelectionKey
            callingRange = key.asString() + ":" + callingRange;
        }

        ProfileTable profileTable;
        try {
            profileTable = getProfileFacility().getProfileTable(MMI_CALLING_NUMBER_ALLOWED_LENGTH_PROFILE_TABLE_NAME);
        } catch (Exception e) {
            getTracer().fine("SipicCallSpoofingConfigProfileReader: Error when retrieving Profile Table {}", MMI_CALLING_NUMBER_ALLOWED_LENGTH_PROFILE_TABLE_NAME);
            return false;
        }

        return profileTable.find(callingRange) != null;
    }
}
