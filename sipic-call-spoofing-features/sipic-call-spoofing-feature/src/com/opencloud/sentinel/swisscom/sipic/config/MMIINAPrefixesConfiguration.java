package com.opencloud.sentinel.swisscom.sipic.config;

@SuppressWarnings("unused")
public interface MMIINAPrefixesConfiguration {

	int[] getINAPrefixes();

}
