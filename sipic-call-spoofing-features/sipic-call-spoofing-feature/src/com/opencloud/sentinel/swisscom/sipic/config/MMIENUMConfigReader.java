package com.opencloud.sentinel.swisscom.sipic.config;

import com.opencloud.sentinel.common.SentinelSelectionKey;

@SuppressWarnings("unused")
public interface MMIENUMConfigReader {

    MMIENUMConfiguration getMMIENUMConfiguration(SentinelSelectionKey key);
}

