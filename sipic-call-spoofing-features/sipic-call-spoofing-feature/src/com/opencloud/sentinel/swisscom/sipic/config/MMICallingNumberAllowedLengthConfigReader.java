package com.opencloud.sentinel.swisscom.sipic.config;

import com.opencloud.sentinel.common.SentinelSelectionKey;

@SuppressWarnings("unused")
public interface MMICallingNumberAllowedLengthConfigReader {

    MMICallingNumberAllowedLengthConfiguration getMMICallingNumberAllowedLengthConfiguration(SentinelSelectionKey key);
    boolean isCallingRangeInMMICallingNumberAllowedLength(String callingRange, SentinelSelectionKey key);

}

