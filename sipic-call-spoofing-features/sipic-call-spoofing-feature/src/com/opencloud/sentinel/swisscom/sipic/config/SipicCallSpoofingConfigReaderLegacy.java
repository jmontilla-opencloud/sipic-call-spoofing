package com.opencloud.sentinel.swisscom.sipic.config;

@SuppressWarnings("unused")
public interface SipicCallSpoofingConfigReaderLegacy {

    MMIENUMConfiguration getMMIENUMConfiguration();
    MMIINAPrefixesConfiguration getMMIINAPrefixesConfiguration();
    MMIAntiSpoofingNetworkNamesConfiguration getMMIAntispoofingNetworkNamesConfiguration();
    boolean isNetworkInMMIAntispoofingNetworkNames(String network);
    MMIAntiSpoofingTrunkContextsConfiguration getMMIAntiSpoofingTrunkContextsConfiguration();
    boolean isContextInMMIAntiSpoofingTrunkContexts(String context);
    MMICallingNumberAllowedLengthConfiguration getMMICallingNumberAllowedLengthConfiguration();
    boolean isCallingRangeInMMICallingNumberAllowedLength(String callingRange);
}

