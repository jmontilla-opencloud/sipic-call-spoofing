package com.opencloud.sentinel.swisscom.sipic.config;

import com.opencloud.sce.fsmtool.Facilities;
import com.opencloud.sentinel.common.BaseConfigProfileReader;
import com.opencloud.sentinel.common.SentinelSelectionKey;
import com.opencloud.sentinel.swisscom.sipic.profile.MMIAntiSpoofingTrunkContextsLocal;

import javax.slee.profile.ProfileTable;
import java.util.ArrayList;
import java.util.Collection;

public class MMIAntiSpoofingTrunkContextsConfigProfileReader extends BaseConfigProfileReader<MMIAntiSpoofingTrunkContextsLocal> implements MMIAntiSpoofingTrunkContextsConfigReader {

    public static final String MMI_ANTISPOOFING_TRUNK_CONTEXTS_PROFILE_TABLE_NAME = "MMIAntiSpoofingTrunkContexts";

    public MMIAntiSpoofingTrunkContextsConfigProfileReader(Facilities facilities) {
        super(facilities.getProfileFacility(), facilities.getTracer(), MMI_ANTISPOOFING_TRUNK_CONTEXTS_PROFILE_TABLE_NAME);
    }

    @Override
    public MMIAntiSpoofingTrunkContextsConfiguration getMMIAntiSpoofingTrunkContextsConfiguration(SentinelSelectionKey key) {
        ProfileTable profileTable;
        try {
            profileTable = getProfileFacility().getProfileTable(MMI_ANTISPOOFING_TRUNK_CONTEXTS_PROFILE_TABLE_NAME);
        } catch (Exception e) {
            getTracer().warning("SipicCallSpoofingConfigProfileReader: Error when retrieving Profile Table {}", MMI_ANTISPOOFING_TRUNK_CONTEXTS_PROFILE_TABLE_NAME);
            return null;
        }

        ArrayList<String> result = new ArrayList<>();

        Collection allProfiles = profileTable.findAll();
        for (Object aProfile : allProfiles) {
            final MMIAntiSpoofingTrunkContextsLocal profile = (MMIAntiSpoofingTrunkContextsLocal) aProfile;
            if (key == null || profile.getProfileName().contains(key.asString())) {
                result.add(profile.getTrunkContext());
            }
        }

        return new MMIAntiSpoofingTrunkContextsConfiguration() {
            @Override
            public ArrayList<String> getTrunkContexts() {
                return result;
            }
        };

    }

    @Override
    public boolean isContextInMMIAntiSpoofingTrunkContexts(String context, SentinelSelectionKey key) {
        if (key != null) { // Profile is accessed via SentinelSelectionKey
            context = key.asString() + ":" + context;
        }

        ProfileTable profileTable;
        try {
            profileTable = getProfileFacility().getProfileTable(MMI_ANTISPOOFING_TRUNK_CONTEXTS_PROFILE_TABLE_NAME);
        } catch (Exception e) {
            getTracer().warning("SipicCallSpoofingConfigProfileReader: Error when retrieving Profile Table {}", MMI_ANTISPOOFING_TRUNK_CONTEXTS_PROFILE_TABLE_NAME);
            return false;
        }

        return profileTable.find(context) != null;
    }
}
