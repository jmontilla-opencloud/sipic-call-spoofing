package com.opencloud.sentinel.swisscom.sipic.config;

import com.opencloud.sce.fsmtool.Facilities;
import com.opencloud.sentinel.common.BaseConfigProfileReader;
import com.opencloud.sentinel.common.SentinelSelectionKey;
import com.opencloud.sentinel.swisscom.sipic.profile.MMIENUMConfigurationLocal;

import javax.slee.profile.ProfileTable;

public class MMIENUMConfigProfileReader extends BaseConfigProfileReader<MMIENUMConfigurationLocal> implements MMIENUMConfigReader {

    public static final String MMI_ENUM_CONFIG_PROFILE_TABLE_NAME = "MMIENUMConfigTable";
    public static final String MMI_ENUM_CONFIG_PROFILE_NAME = "MMIENUMConfig";

    public MMIENUMConfigProfileReader(Facilities facilities) {
        super(facilities.getProfileFacility(), facilities.getTracer(), MMI_ENUM_CONFIG_PROFILE_TABLE_NAME);
    }

    @Override
    public MMIENUMConfiguration getMMIENUMConfiguration(SentinelSelectionKey key) {

        final MMIENUMConfigurationLocal profile;
        if (key != null) { // Profile is accessed via SentinelSelectionKey
            profile = getConfigProfile(key);

        } else {
            ProfileTable profileTable;
            try {
                profileTable = getProfileFacility().getProfileTable(MMI_ENUM_CONFIG_PROFILE_TABLE_NAME);
            } catch (Exception e) {
                getTracer().fine("SipicCallSpoofingConfigProfileReader: Error when retrieving Profile Table {}", MMI_ENUM_CONFIG_PROFILE_TABLE_NAME);
                return null;
            }

            profile = (MMIENUMConfigurationLocal) profileTable.find(MMI_ENUM_CONFIG_PROFILE_NAME);
            if (profile == null) {
                getTracer().fine("SipicCallSpoofingConfigProfileReader: Error when retrieving Profile {}", MMI_ENUM_CONFIG_PROFILE_NAME);
                return null;
            }
        }

        return new MMIENUMConfiguration() {
            @Override
            public int[] getHomeCountryCodes() {
                return profile.getHomeCountryCodes();
            }

            @Override
            public String[] getTDMTrunkContexts() {
                return profile.getTDMTrunkContexts();
            }

            @Override
            public int getShortNumberNPRN() {
                return profile.getShortNumberNPRN();
            }

            @Override
            public int[] getCSCPrefixes() {
                return profile.getCSCPrefixes();
            }

            @Override
            public int getSwisscomNPRN() {
                return profile.getSwisscomNPRN();
            }

            @Override
            public int[] getNPRNPrefix() {
                return profile.getNPRNPrefix();
            }

            @Override
            public int[] getSwisscomNRNPrefixes() {
                return profile.getSwisscomNRNPrefixes();
            }

            @Override
            public int[] getCDPIDPrefixes() {
                return profile.getCDPIDPrefixes();
            }

            @Override
            public int getVoicemailPrefix() {
                return profile.getVoicemailPrefix();
            }

            @Override
            public int[] getNoTransitNPRN() {
                return profile.getNoTransitNPRN();
            }

            @Override
            public int getVPNPrefix() {
                return profile.getVPNPrefix();
            }

            @Override
            public int[] getCorporatePrefix() {
                return profile.getCorporatePrefix();
            }

            @Override
            public int[] getOtherPrefixes() {
                return profile.getCorporatePrefix();
            }

            @Override
            public String getDefaultNetworkName() {
                return profile.getDefaultNetworkName();
            }

            @Override
            public int getO90xNumberPrefix() {
                return profile.getO90xNumberPrefix();
            }

            @Override
            public int[] getAllowedTransitInternationalPrefixes() {
                return profile.getAllowedTransitInternationalPrefixes();
            }

            @Override
            public String getAnnouncementNetworkName() {
                return profile.getAnnouncementNetworkName();
            }

            @Override
            public int getAntiSpoofingDisasterSwitch() {
                return profile.getAntiSpoofingDisasterSwitch();
            }

            @Override
            public String getAntiSpoofingAction() {
                return profile.getAntiSpoofingAction();
            }

            @Override
            public String getAntiSpoofingInvalidNumberAction() {
                return profile.getAntiSpoofingInvalidNumberAction();
            }

            @Override
            public int getTimeToLiveInDynamicWhitelist() {
                return profile.getTimeToLiveInDynamicWhitelist();
            }

            @Override
            public String[] getNoAntiSpoofingCalledPrefixes() {
                return profile.getNoAntiSpoofingCalledPrefixes();
            }

            @Override
            public int[] getSwisscomMobileNPRN() {
                return profile.getSwisscomMobileNPRN();
            }

            @Override
            public int[] getSwisscomMobileNRN() {
                return profile.getSwisscomMobileNRN();
            }

            @Override
            public String[] getSwisscomGenericNRN() {
                return profile.getSwisscomGenericNRN();
            }

            @Override
            public boolean getAntiSpoofingTrunkContextCheck() {
                return profile.getAntiSpoofingTrunkContextCheck();
            }

            @Override
            public String[] getNativeCorporateRoutingPrefixes() {
                return profile.getNativeCorporateRoutingPrefixes();
            }

            @Override
            public String[] getNoAntiSpoofingProducts() {
                return profile.getNoAntiSpoofingProducts();
            }
        };
    }
}
