package com.opencloud.sentinel.swisscom.sipic.config;

import com.opencloud.sentinel.swisscom.sipic.profile.MMIAntiSpoofingNetworkNamesLocal;

import java.util.ArrayList;

@SuppressWarnings("unused")
public interface MMIAntiSpoofingNetworkNamesConfiguration {

	ArrayList<String> getNetworkNames();

}
