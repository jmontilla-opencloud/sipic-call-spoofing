package com.opencloud.sentinel.swisscom.sipic.config;


import com.opencloud.sce.fsmtool.Facilities;
import com.opencloud.sentinel.swisscom.sipic.profile.MMIAntiSpoofingNetworkNamesLocal;
import com.opencloud.sentinel.swisscom.sipic.profile.MMIAntiSpoofingTrunkContextsLocal;
import com.opencloud.sentinel.swisscom.sipic.profile.MMICallingNumberAllowedLengthLocal;
import com.opencloud.sentinel.swisscom.sipic.profile.MMIENUMConfigurationLocal;
import com.opencloud.sentinel.swisscom.sipic.profile.MMIINAPrefixesLocal;

import javax.slee.profile.ProfileTable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class SipicCallSpoofingConfigProfileReaderLegacy implements SipicCallSpoofingConfigReaderLegacy {

    public SipicCallSpoofingConfigProfileReaderLegacy(Facilities facilities) {
        this.facilities = facilities;
    }

    private final Facilities facilities;
    public static final String MMI_ENUM_CONFIG_PROFILE_TABLE_NAME = "MMIENUMConfigTable";
    public static final String MMI_ENUM_CONFIG_PROFILE_NAME = "MMIENUMConfig";
    public static final String MMI_INA_PREFIXES_PROFILE_TABLE_NAME = "MMIINAPrefixesTable";
    public static final String MMI_INA_PREFIXES_PROFILE_NAME = "INAPrefixes";
    public static final String MMI_ANTISPOOFING_NETWORK_NAMES_PROFILE_TABLE_NAME = "MMIAntiSpoofingNetworkNames";
    public static final String MMI_ANTISPOOFING_TRUNK_CONTEXTS_PROFILE_TABLE_NAME = "MMIAntiSpoofingTrunkContexts";
    public static final String MMI_CALLING_NUMBER_ALLOWED_LENGTH_PROFILE_TABLE_NAME = "MMICallingNumberAllowedLengthTable";

    @Override
    public MMIENUMConfiguration getMMIENUMConfiguration() {

        ProfileTable profileTable;
        try {
            profileTable = facilities.getProfileFacility().getProfileTable(MMI_ENUM_CONFIG_PROFILE_TABLE_NAME);
        } catch (Exception e) {
            facilities.getTracer().fine("SipicCallSpoofingConfigProfileReader: Error when retrieving Profile Table {}", MMI_ENUM_CONFIG_PROFILE_TABLE_NAME);
            return null;
        }

        final MMIENUMConfigurationLocal profile = (MMIENUMConfigurationLocal) profileTable.find(MMI_ENUM_CONFIG_PROFILE_NAME);
        if (profile == null) {
            facilities.getTracer().fine("SipicCallSpoofingConfigProfileReader: Error when retrieving Profile {}", MMI_ENUM_CONFIG_PROFILE_NAME);
            return null;
        }

        return new MMIENUMConfiguration() {
            @Override
            public int[] getHomeCountryCodes() { return profile.getHomeCountryCodes(); }
            @Override
            public String[] getTDMTrunkContexts() { return profile.getTDMTrunkContexts(); }
            @Override
            public int getShortNumberNPRN() { return profile.getShortNumberNPRN(); }
            @Override
            public int[] getCSCPrefixes() { return profile.getCSCPrefixes(); }
            @Override
            public int getSwisscomNPRN() { return profile.getSwisscomNPRN(); }
            @Override
            public int[] getNPRNPrefix() { return profile.getNPRNPrefix(); }
            @Override
            public int[] getSwisscomNRNPrefixes() { return profile.getSwisscomNRNPrefixes(); }
            @Override
            public int[] getCDPIDPrefixes() { return profile.getCDPIDPrefixes(); }
            @Override
            public int getVoicemailPrefix() { return profile.getVoicemailPrefix(); }
            @Override
            public int[] getNoTransitNPRN() { return profile.getNoTransitNPRN(); }
            @Override
            public int getVPNPrefix() { return profile.getVPNPrefix(); }
            @Override
            public int[] getCorporatePrefix() { return profile.getCorporatePrefix(); }
            @Override
            public int[] getOtherPrefixes() { return profile.getCorporatePrefix(); }
            @Override
            public String getDefaultNetworkName() { return profile.getDefaultNetworkName(); }
            @Override
            public int getO90xNumberPrefix() { return profile.getO90xNumberPrefix(); }
            @Override
            public int[] getAllowedTransitInternationalPrefixes() { return profile.getAllowedTransitInternationalPrefixes(); }
            @Override
            public String getAnnouncementNetworkName() { return profile.getAnnouncementNetworkName(); }
            @Override
            public int getAntiSpoofingDisasterSwitch() { return profile.getAntiSpoofingDisasterSwitch(); }
            @Override
            public String getAntiSpoofingAction() { return profile.getAntiSpoofingAction(); }
            @Override
            public String getAntiSpoofingInvalidNumberAction() { return profile.getAntiSpoofingInvalidNumberAction(); }
            @Override
            public int getTimeToLiveInDynamicWhitelist() { return profile.getTimeToLiveInDynamicWhitelist(); }
            @Override
            public String[] getNoAntiSpoofingCalledPrefixes() { return profile.getNoAntiSpoofingCalledPrefixes(); }
            @Override
            public int[] getSwisscomMobileNPRN() { return profile.getSwisscomMobileNPRN(); }
            @Override
            public int[] getSwisscomMobileNRN() { return profile.getSwisscomMobileNRN(); }
            @Override
            public String[] getSwisscomGenericNRN() { return profile.getSwisscomGenericNRN(); }
            @Override
            public boolean getAntiSpoofingTrunkContextCheck() { return profile.getAntiSpoofingTrunkContextCheck(); }
            @Override
            public String[] getNativeCorporateRoutingPrefixes() { return profile.getNativeCorporateRoutingPrefixes(); }
            @Override
            public String[] getNoAntiSpoofingProducts() { return profile.getNoAntiSpoofingProducts(); }
        };
    }

    @Override
    public MMIINAPrefixesConfiguration getMMIINAPrefixesConfiguration() {

        ProfileTable profileTable;
        try {
            profileTable = facilities.getProfileFacility().getProfileTable(MMI_INA_PREFIXES_PROFILE_TABLE_NAME);
        } catch (Exception e) {
            facilities.getTracer().warning("SipicCallSpoofingConfigProfileReader: Error when retrieving Profile Table {}", MMI_INA_PREFIXES_PROFILE_TABLE_NAME);
            return null;
        }

        final MMIINAPrefixesLocal profile = (MMIINAPrefixesLocal) profileTable.find(MMI_INA_PREFIXES_PROFILE_NAME);
        if (profile == null) {
            facilities.getTracer().warning("SipicCallSpoofingConfigProfileReader: Error when retrieving Profile {}", MMI_INA_PREFIXES_PROFILE_NAME);
            return null;
        }

        return new MMIINAPrefixesConfiguration() {
            @Override
            public int[] getINAPrefixes() { return profile.getINAPrefixes(); }
        };
    }

    @Override
    public MMIAntiSpoofingNetworkNamesConfiguration getMMIAntispoofingNetworkNamesConfiguration() {
        ProfileTable profileTable;
        try {
            profileTable = facilities.getProfileFacility().getProfileTable(MMI_ANTISPOOFING_NETWORK_NAMES_PROFILE_TABLE_NAME);
        } catch (Exception e) {
            facilities.getTracer().warning("SipicCallSpoofingConfigProfileReader: Error when retrieving Profile Table {}", MMI_ANTISPOOFING_NETWORK_NAMES_PROFILE_TABLE_NAME);
            return null;
        }

        ArrayList<String> result = new ArrayList<>();
        Collection allProfiles = profileTable.findAll();
        for (Object aProfile : allProfiles) {
            final MMIAntiSpoofingNetworkNamesLocal profile = (MMIAntiSpoofingNetworkNamesLocal) aProfile;
            result.add(profile.getNetworkName());
        }

        return new MMIAntiSpoofingNetworkNamesConfiguration() {
            @Override
            public ArrayList<String> getNetworkNames() { return result; }
        };
    }

    @Override
    public boolean isNetworkInMMIAntispoofingNetworkNames(String network) {
        ProfileTable profileTable;
        try {
            profileTable = facilities.getProfileFacility().getProfileTable(MMI_ANTISPOOFING_NETWORK_NAMES_PROFILE_TABLE_NAME);
        } catch (Exception e) {
            facilities.getTracer().warning("SipicCallSpoofingConfigProfileReader: Error when retrieving Profile Table " + MMI_ANTISPOOFING_NETWORK_NAMES_PROFILE_TABLE_NAME);
            return false;
        }

        return (MMIAntiSpoofingNetworkNamesLocal) profileTable.find(network) != null;
    }

    @Override
    public MMIAntiSpoofingTrunkContextsConfiguration getMMIAntiSpoofingTrunkContextsConfiguration() {
        ProfileTable profileTable;
        try {
            profileTable = facilities.getProfileFacility().getProfileTable(MMI_ANTISPOOFING_TRUNK_CONTEXTS_PROFILE_TABLE_NAME);
        } catch (Exception e) {
            facilities.getTracer().warning("SipicCallSpoofingConfigProfileReader: Error when retrieving Profile Table {}", MMI_ANTISPOOFING_TRUNK_CONTEXTS_PROFILE_TABLE_NAME);
            return null;
        }

        ArrayList<String> result = new ArrayList<>();
        Collection allProfiles = profileTable.findAll();
        for (Object aProfile : allProfiles) {
            final MMIAntiSpoofingTrunkContextsLocal profile = (MMIAntiSpoofingTrunkContextsLocal) aProfile;
            result.add(profile.getTrunkContext());
        }

        return new MMIAntiSpoofingTrunkContextsConfiguration() {
            @Override
            public ArrayList<String> getTrunkContexts() { return result; }
        };
    }

    @Override
    public boolean isContextInMMIAntiSpoofingTrunkContexts(String context) {
        ProfileTable profileTable;
        try {
            profileTable = facilities.getProfileFacility().getProfileTable(MMI_ANTISPOOFING_TRUNK_CONTEXTS_PROFILE_TABLE_NAME);
        } catch (Exception e) {
            facilities.getTracer().warning("SipicCallSpoofingConfigProfileReader: Error when retrieving Profile Table {}", MMI_ANTISPOOFING_TRUNK_CONTEXTS_PROFILE_TABLE_NAME);
            return false;
        }

        return (MMIAntiSpoofingTrunkContextsLocal) profileTable.find(context) != null;
    }

    @Override
    public MMICallingNumberAllowedLengthConfiguration getMMICallingNumberAllowedLengthConfiguration() {
        ProfileTable profileTable;
        try {
            profileTable = facilities.getProfileFacility().getProfileTable(MMI_CALLING_NUMBER_ALLOWED_LENGTH_PROFILE_TABLE_NAME);
        } catch (Exception e) {
            facilities.getTracer().fine("SipicCallSpoofingConfigProfileReader: Error when retrieving Profile Table {}", MMI_CALLING_NUMBER_ALLOWED_LENGTH_PROFILE_TABLE_NAME);
            return null;
        }

        Map<String, int[]> result = new HashMap<>();
        Collection allProfiles = profileTable.findAll();
        for (Object aProfile : allProfiles) {
            final MMICallingNumberAllowedLengthLocal profile = (MMICallingNumberAllowedLengthLocal) aProfile;
            result.put(profile.getCallingNumberRange(), profile.getAllowedLengths());
        }

        return new MMICallingNumberAllowedLengthConfiguration() {
            @Override
            public Map<String, int[]> getCallingNumberAllowedLengths() { return result; }
        };
    }

    @Override
    public boolean isCallingRangeInMMICallingNumberAllowedLength(String callingRange) {
        ProfileTable profileTable;
        try {
            profileTable = facilities.getProfileFacility().getProfileTable(MMI_CALLING_NUMBER_ALLOWED_LENGTH_PROFILE_TABLE_NAME);
        } catch (Exception e) {
            facilities.getTracer().fine("SipicCallSpoofingConfigProfileReader: Error when retrieving Profile Table {}", MMI_CALLING_NUMBER_ALLOWED_LENGTH_PROFILE_TABLE_NAME);
            return false;
        }

        return (MMICallingNumberAllowedLengthLocal) profileTable.find(callingRange) != null;
    }

}
