package com.opencloud.sentinel.swisscom.sipic.config;

import com.opencloud.sce.fsmtool.Facilities;
import com.opencloud.sentinel.common.BaseConfigProfileReader;
import com.opencloud.sentinel.common.SentinelSelectionKey;
import com.opencloud.sentinel.swisscom.sipic.profile.MMIINAPrefixesLocal;

import javax.slee.profile.ProfileTable;

public class MMIINAPrefixesConfigProfileReader extends BaseConfigProfileReader<MMIINAPrefixesLocal> implements MMIINAPrefixesConfigReader {

    public static final String MMI_INA_PREFIXES_PROFILE_TABLE_NAME = "MMIINAPrefixesTable";
    public static final String MMI_INA_PREFIXES_PROFILE_NAME = "INAPrefixes";

    public MMIINAPrefixesConfigProfileReader(Facilities facilities) {
        super(facilities.getProfileFacility(), facilities.getTracer(), MMI_INA_PREFIXES_PROFILE_TABLE_NAME);
    }

    @Override
    public MMIINAPrefixesConfiguration getMMIINAPrefixesConfiguration(SentinelSelectionKey key) {
        final MMIINAPrefixesLocal profile;
        if (key != null) { // Profile is accessed via SentinelSelectionKey
            profile = getConfigProfile(key);

        } else {
            ProfileTable profileTable;
            try {
                profileTable = getProfileFacility().getProfileTable(MMI_INA_PREFIXES_PROFILE_TABLE_NAME);
            } catch (Exception e) {
                getTracer().warning("SipicCallSpoofingConfigProfileReader: Error when retrieving Profile Table {}", MMI_INA_PREFIXES_PROFILE_TABLE_NAME);
                return null;
            }

            profile = (MMIINAPrefixesLocal) profileTable.find(MMI_INA_PREFIXES_PROFILE_NAME);
            if (profile == null) {
                getTracer().warning("SipicCallSpoofingConfigProfileReader: Error when retrieving Profile {}", MMI_INA_PREFIXES_PROFILE_NAME);
                return null;
            }
        }
        return new MMIINAPrefixesConfiguration() {
            @Override
            public int[] getINAPrefixes() {
                return profile.getINAPrefixes();
            }
        };
    }
}
