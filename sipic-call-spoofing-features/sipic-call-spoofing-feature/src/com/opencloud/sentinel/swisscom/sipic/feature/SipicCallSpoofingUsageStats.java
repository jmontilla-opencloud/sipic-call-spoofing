package com.opencloud.sentinel.swisscom.sipic.feature;

/**
 * SIPIC Call Spoofing usage statistics interface.
 */
public interface SipicCallSpoofingUsageStats extends com.opencloud.sentinel.feature.spi.SentinelFeatureStats {

    void incrementFeatureStarted(long l);
    void incrementInternalErrorProfileTableNotFound(long l);
    void incrementInternalErrorProfileNotFound(long l);

}
