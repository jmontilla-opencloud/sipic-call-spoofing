package com.opencloud.sentinel.swisscom.sipic.config;

import java.util.Map;

@SuppressWarnings("unused")
public interface MMICallingNumberAllowedLengthConfiguration {
	Map<String, int[]> getCallingNumberAllowedLengths();
	
}
