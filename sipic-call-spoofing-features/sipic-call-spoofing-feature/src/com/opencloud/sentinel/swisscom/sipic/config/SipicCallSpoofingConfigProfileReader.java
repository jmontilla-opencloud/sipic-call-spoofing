package com.opencloud.sentinel.swisscom.sipic.config;

import com.opencloud.sce.fsmtool.Facilities;
import com.opencloud.sentinel.common.BaseConfigProfileReader;
import com.opencloud.sentinel.common.SentinelSelectionKey;
import com.opencloud.sentinel.swisscom.sipic.profile.MMIAntiSpoofingNetworkNamesLocal;
import com.opencloud.sentinel.swisscom.sipic.profile.MMIAntiSpoofingTrunkContextsLocal;
import com.opencloud.sentinel.swisscom.sipic.profile.MMICallingNumberAllowedLengthLocal;
import com.opencloud.sentinel.swisscom.sipic.profile.MMIENUMConfigurationLocal;
import com.opencloud.sentinel.swisscom.sipic.profile.MMIINAPrefixesLocal;

import javax.slee.profile.ProfileTable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class SipicCallSpoofingConfigProfileReader implements SipicCallSpoofingConfigReader {

    private final Facilities facilities;
    public SipicCallSpoofingConfigProfileReader(Facilities facilities) {
        this.facilities = facilities;
    }

    @Override
    public MMIENUMConfiguration getMMIENUMConfiguration() {
        return new MMIENUMConfigProfileReader(this.facilities).getMMIENUMConfiguration(null);
    }

    @Override
    public MMIENUMConfiguration getMMIENUMConfiguration(SentinelSelectionKey key) {
        return new MMIENUMConfigProfileReader(this.facilities).getMMIENUMConfiguration(key);
    }

    @Override
    public MMIINAPrefixesConfiguration getMMIINAPrefixesConfiguration() {
        return new MMIINAPrefixesConfigProfileReader(this.facilities).getMMIINAPrefixesConfiguration(null);
    }

    @Override
    public MMIINAPrefixesConfiguration getMMIINAPrefixesConfiguration(SentinelSelectionKey key) {
        return new MMIINAPrefixesConfigProfileReader(this.facilities).getMMIINAPrefixesConfiguration(key);
    }

    @Override
    public MMIAntiSpoofingNetworkNamesConfiguration getMMIAntispoofingNetworkNamesConfiguration() {
        return new MMIAntiSpoofingNetworkNamesConfigProfileReader(this.facilities).getMMIAntispoofingNetworkNamesConfiguration(null);
    }

    @Override
    public MMIAntiSpoofingNetworkNamesConfiguration getMMIAntispoofingNetworkNamesConfiguration(SentinelSelectionKey key) {
        return new MMIAntiSpoofingNetworkNamesConfigProfileReader(this.facilities).getMMIAntispoofingNetworkNamesConfiguration(key);
    }

    @Override
    public boolean isNetworkInMMIAntispoofingNetworkNames(String network) {
        return new MMIAntiSpoofingNetworkNamesConfigProfileReader(this.facilities).isNetworkInMMIAntispoofingNetworkNames(network, null);
    }

    @Override
    public boolean isNetworkInMMIAntispoofingNetworkNames(String network, SentinelSelectionKey key) {
        return new MMIAntiSpoofingNetworkNamesConfigProfileReader(this.facilities).isNetworkInMMIAntispoofingNetworkNames(network, key);
    }

    @Override
    public MMIAntiSpoofingTrunkContextsConfiguration getMMIAntiSpoofingTrunkContextsConfiguration() {
        return new MMIAntiSpoofingTrunkContextsConfigProfileReader(this.facilities).getMMIAntiSpoofingTrunkContextsConfiguration(null);
    }

    @Override
    public MMIAntiSpoofingTrunkContextsConfiguration getMMIAntiSpoofingTrunkContextsConfiguration(SentinelSelectionKey key) {
        return new MMIAntiSpoofingTrunkContextsConfigProfileReader(this.facilities).getMMIAntiSpoofingTrunkContextsConfiguration(key);
    }

    @Override
    public boolean isContextInMMIAntiSpoofingTrunkContexts(String context) {
        return new MMIAntiSpoofingTrunkContextsConfigProfileReader(this.facilities).isContextInMMIAntiSpoofingTrunkContexts(context, null);
    }

    @Override
    public boolean isContextInMMIAntiSpoofingTrunkContexts(String context, SentinelSelectionKey key) {
        return new MMIAntiSpoofingTrunkContextsConfigProfileReader(this.facilities).isContextInMMIAntiSpoofingTrunkContexts(context, key);
    }

    @Override
    public MMICallingNumberAllowedLengthConfiguration getMMICallingNumberAllowedLengthConfiguration() {
        return new MMICallingNumberAllowedLengthConfigProfileReader(this.facilities).getMMICallingNumberAllowedLengthConfiguration(null);
    }

    @Override
    public MMICallingNumberAllowedLengthConfiguration getMMICallingNumberAllowedLengthConfiguration(SentinelSelectionKey key) {
        return new MMICallingNumberAllowedLengthConfigProfileReader(this.facilities).getMMICallingNumberAllowedLengthConfiguration(key);
    }

    @Override
    public boolean isCallingRangeInMMICallingNumberAllowedLength(String callingRange) {
        return new MMICallingNumberAllowedLengthConfigProfileReader(this.facilities).isCallingRangeInMMICallingNumberAllowedLength(callingRange, null);
    }

    @Override
    public boolean isCallingRangeInMMICallingNumberAllowedLength(String callingRange, SentinelSelectionKey key) {
        return new MMICallingNumberAllowedLengthConfigProfileReader(this.facilities).isCallingRangeInMMICallingNumberAllowedLength(callingRange, key);
    }
}

