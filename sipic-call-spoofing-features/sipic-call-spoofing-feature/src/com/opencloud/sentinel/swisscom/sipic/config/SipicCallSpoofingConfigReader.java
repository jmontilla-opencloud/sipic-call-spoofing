package com.opencloud.sentinel.swisscom.sipic.config;

import com.opencloud.sentinel.common.SentinelSelectionKey;

@SuppressWarnings("unused")
public interface SipicCallSpoofingConfigReader {

    MMIENUMConfiguration getMMIENUMConfiguration();
    MMIENUMConfiguration getMMIENUMConfiguration(SentinelSelectionKey key);
    MMIINAPrefixesConfiguration getMMIINAPrefixesConfiguration();
    MMIINAPrefixesConfiguration getMMIINAPrefixesConfiguration(SentinelSelectionKey key);

    MMIAntiSpoofingNetworkNamesConfiguration getMMIAntispoofingNetworkNamesConfiguration();
    MMIAntiSpoofingNetworkNamesConfiguration getMMIAntispoofingNetworkNamesConfiguration(SentinelSelectionKey key);
    boolean isNetworkInMMIAntispoofingNetworkNames(String network);
    boolean isNetworkInMMIAntispoofingNetworkNames(String network, SentinelSelectionKey key);

    MMIAntiSpoofingTrunkContextsConfiguration getMMIAntiSpoofingTrunkContextsConfiguration();
    MMIAntiSpoofingTrunkContextsConfiguration getMMIAntiSpoofingTrunkContextsConfiguration(SentinelSelectionKey key);
    boolean isContextInMMIAntiSpoofingTrunkContexts(String context);
    boolean isContextInMMIAntiSpoofingTrunkContexts(String context, SentinelSelectionKey key);

    MMICallingNumberAllowedLengthConfiguration getMMICallingNumberAllowedLengthConfiguration();
    MMICallingNumberAllowedLengthConfiguration getMMICallingNumberAllowedLengthConfiguration(SentinelSelectionKey key);
    boolean isCallingRangeInMMICallingNumberAllowedLength(String callingRange);
    boolean isCallingRangeInMMICallingNumberAllowedLength(String callingRange, SentinelSelectionKey key);
}

