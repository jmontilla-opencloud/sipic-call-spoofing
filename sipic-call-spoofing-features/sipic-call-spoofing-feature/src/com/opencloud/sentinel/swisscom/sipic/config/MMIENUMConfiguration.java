package com.opencloud.sentinel.swisscom.sipic.config;

@SuppressWarnings("unused")
public interface MMIENUMConfiguration {

	int[] getHomeCountryCodes();
	String[] getTDMTrunkContexts();
	int getShortNumberNPRN();
	int[] getCSCPrefixes();
	int getSwisscomNPRN();
	int[] getNPRNPrefix();
	int[] getSwisscomNRNPrefixes();
	int[] getCDPIDPrefixes();
	int getVoicemailPrefix();
	int[] getNoTransitNPRN();
	int getVPNPrefix();
	int[] getCorporatePrefix();
	int[] getOtherPrefixes();
	String getDefaultNetworkName();
	int getO90xNumberPrefix();
	int[] getAllowedTransitInternationalPrefixes();
	String getAnnouncementNetworkName();
	int getAntiSpoofingDisasterSwitch();
	String getAntiSpoofingAction();
	String getAntiSpoofingInvalidNumberAction();
	int getTimeToLiveInDynamicWhitelist();
	String[] getNoAntiSpoofingCalledPrefixes();
	int[] getSwisscomMobileNPRN();
	int[] getSwisscomMobileNRN();
	String[] getSwisscomGenericNRN();
	boolean getAntiSpoofingTrunkContextCheck();
	String[] getNativeCorporateRoutingPrefixes();
	String[] getNoAntiSpoofingProducts();

}
