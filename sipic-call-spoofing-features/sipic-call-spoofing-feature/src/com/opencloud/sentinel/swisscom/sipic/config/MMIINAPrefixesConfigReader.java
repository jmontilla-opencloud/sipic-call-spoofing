package com.opencloud.sentinel.swisscom.sipic.config;

import com.opencloud.sentinel.common.SentinelSelectionKey;

@SuppressWarnings("unused")
public interface MMIINAPrefixesConfigReader {

    MMIINAPrefixesConfiguration getMMIINAPrefixesConfiguration(SentinelSelectionKey key);
}

