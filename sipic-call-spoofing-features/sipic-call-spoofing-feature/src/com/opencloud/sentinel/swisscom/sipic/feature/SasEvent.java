//
// NOTE: This class was automatically generated.  Do not edit.
//

package com.opencloud.sentinel.swisscom.sipic.feature;

import com.opencloud.rhino.facilities.sas.SasEventEnum;

/**
 * Java enum to represent the SasEvent event types.
 */
public enum SasEvent implements SasEventEnum {

    SIPIC_CALL_SPOOFING_SAS_TRACE ( 0 ),
    ;

    SasEvent(int id) {
        this.id = id;
    }

    @Override
    public int id() {
        return id;
    }

    private int id;
}
