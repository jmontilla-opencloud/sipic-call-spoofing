package com.opencloud.sentinel.swisscom.sipic.config;

import com.opencloud.sce.fsmtool.Facilities;
import com.opencloud.sentinel.common.BaseConfigProfileReader;
import com.opencloud.sentinel.common.SentinelSelectionKey;
import com.opencloud.sentinel.swisscom.sipic.profile.MMIAntiSpoofingNetworkNamesLocal;

import javax.slee.profile.ProfileTable;
import java.util.ArrayList;
import java.util.Collection;

public class MMIAntiSpoofingNetworkNamesConfigProfileReader extends BaseConfigProfileReader<MMIAntiSpoofingNetworkNamesLocal> implements MMIAntiSpoofingNetworkNamesConfigReader {

    public static final String MMI_ANTISPOOFING_NETWORK_NAMES_PROFILE_TABLE_NAME = "MMIAntiSpoofingNetworkNames";

    public MMIAntiSpoofingNetworkNamesConfigProfileReader(Facilities facilities) {
        super(facilities.getProfileFacility(), facilities.getTracer(), MMI_ANTISPOOFING_NETWORK_NAMES_PROFILE_TABLE_NAME);
    }

    @Override
    public MMIAntiSpoofingNetworkNamesConfiguration getMMIAntispoofingNetworkNamesConfiguration(SentinelSelectionKey key) {
        ProfileTable profileTable;
        try {
            profileTable = getProfileFacility().getProfileTable(MMI_ANTISPOOFING_NETWORK_NAMES_PROFILE_TABLE_NAME);
        } catch (Exception e) {
            getTracer().warning("SipicCallSpoofingConfigProfileReader: Error when retrieving Profile Table {}", MMI_ANTISPOOFING_NETWORK_NAMES_PROFILE_TABLE_NAME);
            return null;
        }

        ArrayList<String> result = new ArrayList<>();
        Collection allProfiles = profileTable.findAll();
        for (Object aProfile : allProfiles) {
            final MMIAntiSpoofingNetworkNamesLocal profile = (MMIAntiSpoofingNetworkNamesLocal) aProfile;
            if (key == null || profile.getProfileName().contains(key.asString())) {
                result.add(profile.getNetworkName());
            }
        }

        return new MMIAntiSpoofingNetworkNamesConfiguration() {
            @Override
            public ArrayList<String> getNetworkNames() {
                return result;
            }
        };

    }

    @Override
    public boolean isNetworkInMMIAntispoofingNetworkNames(String network, SentinelSelectionKey key) {
        if (key != null) { // Profile is accessed via SentinelSelectionKey
            network = key.asString() + ":" + network;
        }

        ProfileTable profileTable;
        try {
            profileTable = getProfileFacility().getProfileTable(MMI_ANTISPOOFING_NETWORK_NAMES_PROFILE_TABLE_NAME);
        } catch (Exception e) {
            getTracer().warning("SipicCallSpoofingConfigProfileReader: Error when retrieving Profile Table {}", MMI_ANTISPOOFING_NETWORK_NAMES_PROFILE_TABLE_NAME);
            return false;
        }

        return profileTable.find(network) != null;
    }
}
