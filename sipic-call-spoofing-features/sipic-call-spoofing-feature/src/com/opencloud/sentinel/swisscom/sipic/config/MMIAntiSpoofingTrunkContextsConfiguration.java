package com.opencloud.sentinel.swisscom.sipic.config;

import java.util.ArrayList;

@SuppressWarnings("unused")
public interface MMIAntiSpoofingTrunkContextsConfiguration {

	ArrayList<String> getTrunkContexts();

}
