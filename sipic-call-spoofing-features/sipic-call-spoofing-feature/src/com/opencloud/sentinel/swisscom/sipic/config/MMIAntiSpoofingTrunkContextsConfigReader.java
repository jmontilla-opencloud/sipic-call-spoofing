package com.opencloud.sentinel.swisscom.sipic.config;

import com.opencloud.sentinel.common.SentinelSelectionKey;

@SuppressWarnings("unused")
public interface MMIAntiSpoofingTrunkContextsConfigReader {

    MMIAntiSpoofingTrunkContextsConfiguration getMMIAntiSpoofingTrunkContextsConfiguration(SentinelSelectionKey key);
    boolean isContextInMMIAntiSpoofingTrunkContexts(String context, SentinelSelectionKey key);
}

