@Library(
    id = @ComponentId(name = "@component.name@", vendor = "@component.vendor@", version = "@component.version@"),
    libraryRefs = {
        @LibraryReference(library = @ComponentId(name = "@sentinel-sip-spi.SentinelSipFeatureSPI.name@", vendor = "@sentinel-sip-spi.SentinelSipFeatureSPI.vendor@", version = "@sentinel-sip-spi.SentinelSipFeatureSPI.version@")),
    }
)

@LibraryDeployableUnit(
        securityPermissions = @SecurityPermissions(securityPermissionSpec = "grant { permission java.security.AllPermission; };")
)

package com.opencloud.sentinel.swisscom.sipic.common;

import javax.slee.annotation.ComponentId;
import javax.slee.annotation.Library;
import javax.slee.annotation.LibraryReference;
import javax.slee.annotation.SecurityPermissions;
import javax.slee.annotation.du.LibraryDeployableUnit;
