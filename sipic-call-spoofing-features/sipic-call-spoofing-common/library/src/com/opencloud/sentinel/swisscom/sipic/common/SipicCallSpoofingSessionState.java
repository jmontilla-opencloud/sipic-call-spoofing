package com.opencloud.sentinel.swisscom.sipic.common;

import com.opencloud.sentinel.annotations.SessionStateField;
import com.opencloud.sentinel.annotations.SessionStateInterface;
import com.opencloud.sentinel.common.SentinelSipSessionState;


@SessionStateInterface()
public interface SipicCallSpoofingSessionState extends SentinelSipSessionState {

    @SessionStateField()
    void setShortNumberNPRN(int number);
    int getShortNumberNPRN();

}
