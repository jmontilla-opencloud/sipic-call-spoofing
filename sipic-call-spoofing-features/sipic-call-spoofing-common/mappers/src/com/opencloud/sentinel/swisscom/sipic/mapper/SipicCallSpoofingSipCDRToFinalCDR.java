package com.opencloud.sentinel.swisscom.sipic.mapper;

import com.google.protobuf.Message;
import com.opencloud.rhino.facilities.ExtendedTracer;
import com.opencloud.sentinel.annotations.Mapping;
import com.opencloud.sentinel.annotations.SentinelMapper;
import com.opencloud.sentinel.common.SentinelSelectionKey;
import com.opencloud.sentinel.mapper.Mapper;
import com.opencloud.sentinel.mapper.MapperException;
import com.opencloud.sentinel.mapper.MapperFacilities;
import com.opencloud.sentinel.swisscom.sipic.cdr.SipicCallSpoofingSipCdrFormat;
import com.opencloud.sentinel.swisscom.sipic.common.SipicCallSpoofingSessionState;

import javax.slee.annotation.ComponentId;
import javax.slee.annotation.LibraryReference;
import javax.slee.annotation.LibraryReferences;
import java.util.LinkedList;
import java.util.List;
import java.util.TimeZone;

@SentinelMapper(
        mappings = {
                @Mapping(name = "SipicCallSpoofingSipCDRToFinalCDR", fromClass = byte[].class, toClass = Message.class)
        })
@LibraryReferences(
        libraryRefs = {
                @LibraryReference(library = @ComponentId(name = "@sentinel-sip-spi.SentinelSipFeatureSPI.name@", vendor = "@sentinel-sip-spi.SentinelSipFeatureSPI.vendor@", version = "@sentinel-sip-spi.SentinelSipFeatureSPI.version@")),
                @LibraryReference(library = @ComponentId(name = "@sipic-call-spoofing-common-library.name@", vendor = "@sipic-call-spoofing-common-library.vendor@", version = "@sipic-call-spoofing-common-library.version@"))
        })

public class SipicCallSpoofingSipCDRToFinalCDR  implements Mapper<SipicCallSpoofingSessionState> {

    private final String MAPPER_NAME = "SipicCallSpoofingSipCDRToFinalCDR";
    private ExtendedTracer tracer;

    SipicCallSpoofingSipCdrFormat.SipicCallSpoofingSipCdr.Time getCdrTime (long timeMillis){
        return SipicCallSpoofingSipCdrFormat.SipicCallSpoofingSipCdr.Time.newBuilder()
                .setMillisecondsSinceEpoch(timeMillis)
                .setZoneoffsetMinutes(TimeZone.getDefault().getOffset(timeMillis) / (60 * 1000))
                .build();
    }

    public final Object map(Object arg, SipicCallSpoofingSessionState sessionState, MapperFacilities facilities) throws MapperException {

        tracer = facilities.getTracer();
        tracer.finest("{} - Creating CDR", MAPPER_NAME);

        final SipicCallSpoofingSipCdrFormat.SipicCallSpoofingSipCdr.Builder cdrBuilder = SipicCallSpoofingSipCdrFormat.SipicCallSpoofingSipCdr.newBuilder();

/*
        Object currentCDR = sessionState.getCDR();
        try {
            if (currentCDR != null)
                cdrBuilder.mergeFrom((byte[]) currentCDR);
        } catch (InvalidProtocolBufferException e) {
            throw new MapperException("Could not parse existing CDR", e);
        }
*/
        try{
            if (sessionState.getSessionInitiated() > 0) {
                cdrBuilder.setSessionInitiated(getCdrTime(sessionState.getSessionInitiated()));
            }
            if (sessionState.getSessionEstablished() > 0) {
                cdrBuilder.setSessionEstablished(getCdrTime(sessionState.getSessionEstablished()));
            }
            final long endTime = sessionState.getSessionEnded();
            if (endTime == 0) {
                cdrBuilder.setSessionEnded(getCdrTime(System.currentTimeMillis()));
            } else if (endTime > 0) {
                cdrBuilder.setSessionEnded(getCdrTime(endTime));
            }
        } catch (Exception e) {
            tracer.warning("Could not set call times in CDR", e);
        }

        if (sessionState.getCallId() != null) {
            cdrBuilder.setCallId(sessionState.getCallId());
        }
        final SentinelSelectionKey selectionKey = sessionState.getSentinelSelectionKey();
        cdrBuilder.setSentinelSelectionKey(selectionKey.asString());

        if (sessionState.getCallingPartyAddress() != null)
            cdrBuilder.setCallingPartyAddress(sessionState.getCallingPartyAddress());

        // Called Party Number
        if (sessionState.getCalledPartyAddress() != null)
            cdrBuilder.setCalledPartyAddress(sessionState.getCalledPartyAddress());

        // Subscription Id
        if (sessionState.getSubscriber() != null)
            cdrBuilder.setSubscriber(sessionState.getSubscriber());

        // Announcement IDs
        if (sessionState.getPlayedAnnouncementIDs() != null && sessionState.getPlayedAnnouncementIDs().length > 0) {
            List<Integer> ids = new LinkedList<Integer>();
            for (int id : sessionState.getPlayedAnnouncementIDs()) {
                ids.add(id);
            }
            cdrBuilder.addAllAnnouncementIDs(ids);
        }

        // End session cause
        if (sessionState.getEndSessionCause() != null)
            cdrBuilder.setEndSessionCause(sessionState.getEndSessionCause());

        // Call Type
        if (sessionState.getCallType() != null) {
            switch (sessionState.getCallType()) {
            case MobileTerminating:
                cdrBuilder.setCallType(SipicCallSpoofingSipCdrFormat.SipicCallSpoofingSipCdr.CallType.MTC);
                break;
            case MobileOriginating:
                cdrBuilder.setCallType(SipicCallSpoofingSipCdrFormat.SipicCallSpoofingSipCdr.CallType.MOC);
                break;
            case MobileForwarded:
                cdrBuilder.setCallType(SipicCallSpoofingSipCdrFormat.SipicCallSpoofingSipCdr.CallType.MFC);
                break;
            case NetworkInitiated:
                cdrBuilder.setCallType(SipicCallSpoofingSipCdrFormat.SipicCallSpoofingSipCdr.CallType.MOC_3RDPTY);
                break;
            case EmergencyCall:
                cdrBuilder.setCallType(SipicCallSpoofingSipCdrFormat.SipicCallSpoofingSipCdr.CallType.EMERGENCY_CALL);
                break;
            }
        }

        // Service Type
        if (null != sessionState.getSipServiceType()) {
            switch (sessionState.getSipServiceType()) {
            case SipCall:
                cdrBuilder.setServiceType(SipicCallSpoofingSipCdrFormat.SipicCallSpoofingSipCdr.SipServiceType.SipCall);
                break;
            case Message:
                cdrBuilder.setServiceType(SipicCallSpoofingSipCdrFormat.SipicCallSpoofingSipCdr.SipServiceType.Message);
                break;
            case Subscription:
                cdrBuilder.setServiceType(SipicCallSpoofingSipCdrFormat.SipicCallSpoofingSipCdr.SipServiceType.Subscription);
                break;
            default:
                cdrBuilder.setServiceType(SipicCallSpoofingSipCdrFormat.SipicCallSpoofingSipCdr.SipServiceType.Unknown);
                break;
            }
        } else {
            cdrBuilder.setServiceType(SipicCallSpoofingSipCdrFormat.SipicCallSpoofingSipCdr.SipServiceType.Unknown);
        }

        // Terminating Domain
       //SCCTADSSessionState tadsSS = (SCCTADSSessionState) sessionState;
       //if (tadsSS.getTerminatingDomain() != null && !tadsSS.getTerminatingDomain().equals(""))
       //    cdrBuilder.setTerminatingDomain(tadsSS.getTerminatingDomain());

        // // add any extensions to the CDR
        // addCdrExtensions(cdrBuilder, sessionState, facilities);
        //addBlacklistCDRFields(cdrBuilder, sessionState);

        tracer.finest("{} - CDR Created", MAPPER_NAME);

        return cdrBuilder.build();
    }

}
