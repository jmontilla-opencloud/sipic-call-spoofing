
@SBBPart(
    id = @ComponentId(name = "@component.name@", vendor = "@component.vendor@", version = "@component.version@"),
    sbbPartRefs = {
        /* the sip spi sbb part ref is needed for the binder to infer that this sbb-part should be bound to the sip service */
        @SBBPartReference(id = @ComponentId(name = "@sentinel-sip-spi.SentinelSipFeatureSPI SBB Part.name@", vendor = "@sentinel-sip-spi.SentinelSipFeatureSPI SBB Part.vendor@", version = "@sentinel-sip-spi.SentinelSipFeatureSPI SBB Part.version@")),
        @SBBPartReference(id = @ComponentId(name = "sentinel-sip-mapper-classes", vendor = "OpenCloud", version = "@sentinel-sip.component.version@"))
    }
)

package com.opencloud.sentinel.swisscom.sipic.mapper;

import com.opencloud.slee.annotation.SBBPart;
import com.opencloud.slee.annotation.SBBPartReference;

import javax.slee.annotation.ComponentId;


