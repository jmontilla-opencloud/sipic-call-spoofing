@ProfilesDeployableUnit(
    securityPermissions = @SecurityPermissions(securityPermissionSpec = "grant { permission java.security.AllPermission; };")
)
package com.opencloud.sentinel.swisscom.sipic.profile;

import javax.slee.annotation.SecurityPermissions;
import javax.slee.annotation.du.ProfilesDeployableUnit;