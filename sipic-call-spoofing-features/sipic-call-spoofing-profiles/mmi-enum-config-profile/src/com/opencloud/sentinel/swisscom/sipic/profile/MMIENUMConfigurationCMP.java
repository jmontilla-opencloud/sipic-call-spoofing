package com.opencloud.sentinel.swisscom.sipic.profile;


import javax.slee.annotation.ComponentId;
import javax.slee.annotation.LibraryReference;
import javax.slee.annotation.Profile;
import javax.slee.annotation.ProfileClasses;
import javax.slee.annotation.ProfileLocalInterface;

@Profile(
        vendorExtensionID = "@component.name@",
        description = "MMIENUMConfiguration Configuration Profile",
        id = @ComponentId(name = "@component.name@", vendor = "@component.vendor@", version = "@component.version@"),
        libraryRefs = {
                @LibraryReference(library = @ComponentId(name = "@sentinel-profile-util-library.name@", vendor = "@sentinel-profile-util-library.vendor@", version = "@sentinel-profile-util-library.version@"))
        },
        profileClasses = @ProfileClasses(
                profileLocal = @ProfileLocalInterface(interfaceName = "com.opencloud.sentinel.swisscom.sipic.profile.MMIENUMConfigurationLocal")
        ),
        singleProfile = true
)
@SuppressWarnings("unused")
public interface MMIENUMConfigurationCMP {
    int[] getHomeCountryCodes();
    void setHomeCountryCodes(int[] list);

    String[] getTDMTrunkContexts();
    void setTDMTrunkContexts(String[] list);

    int getShortNumberNPRN();
    void setShortNumberNPRN(int l); 

    int[] getCSCPrefixes();
    void setCSCPrefixes(int[] list);

    int getSwisscomNPRN();
    void setSwisscomNPRN(int l); 
    
    int[] getSwisscomNRNPrefixes();
    void setSwisscomNRNPrefixes(int[] list);

    int[] getSwisscomMobileNPRN();
    void setSwisscomMobileNPRN(int[] i);

    int[] getSwisscomMobileNRN();
    void setSwisscomMobileNRN(int[] list);

    String[] getSwisscomGenericNRN();
    void setSwisscomGenericNRN(String[] list);

    int[] getNPRNPrefix();
    void setNPRNPrefix(int[] list);

    int[] getCDPIDPrefixes();
    void setCDPIDPrefixes(int[] list);

    int getVoicemailPrefix();
    void setVoicemailPrefix(int l);

    int[] getNoTransitNPRN();
    void setNoTransitNPRN(int[] list);
    
    int getVPNPrefix();
    void setVPNPrefix(int l);

    int[] getCorporatePrefix();
    void setCorporatePrefix(int[] list);

    int[] getOtherPrefixes();
    void setOtherPrefixes(int[] list);
    
    String getDefaultNetworkName();
    void setDefaultNetworkName(String s);

    int getO90xNumberPrefix();
    void setO90xNumberPrefix(int l);

    int[] getAllowedTransitInternationalPrefixes();
    void setAllowedTransitInternationalPrefixes(int[] list);

    String getAnnouncementNetworkName();
    void setAnnouncementNetworkName(String s);

    int getAntiSpoofingDisasterSwitch();
    void setAntiSpoofingDisasterSwitch(int i);

    String getAntiSpoofingAction();
    void setAntiSpoofingAction(String s);

    String getAntiSpoofingInvalidNumberAction();
    void setAntiSpoofingInvalidNumberAction(String s);

    int getTimeToLiveInDynamicWhitelist();
    void setTimeToLiveInDynamicWhitelist(int i);

    String[] getNoAntiSpoofingCalledPrefixes();
    void setNoAntiSpoofingCalledPrefixes(String[] list);

    boolean getAntiSpoofingTrunkContextCheck();
    void setAntiSpoofingTrunkContextCheck(boolean flag);

    String[] getNativeCorporateRoutingPrefixes();
    void setNativeCorporateRoutingPrefixes(String[] list);

    String[] getNoAntiSpoofingProducts();
    void setNoAntiSpoofingProducts(String[] list);

}
