package com.opencloud.sentinel.swisscom.sipic.profile;


import javax.slee.annotation.ComponentId;
import javax.slee.annotation.LibraryReference;
import javax.slee.annotation.Profile;
import javax.slee.annotation.ProfileClasses;
import javax.slee.annotation.ProfileLocalInterface;

@Profile(
        vendorExtensionID = "@component.name@",
        description = "MMIENUMConfiguration Configuration Profile",
        id = @ComponentId(name = "@component.name@", vendor = "@component.vendor@", version = "@component.version@"),
        libraryRefs = {
                @LibraryReference(library = @ComponentId(name = "@sentinel-profile-util-library.name@", vendor = "@sentinel-profile-util-library.vendor@", version = "@sentinel-profile-util-library.version@"))
        },
        profileClasses = @ProfileClasses(
                profileLocal = @ProfileLocalInterface(interfaceName = "com.opencloud.sentinel.swisscom.sipic.profile.MMIAntiSpoofingTrunkContextsLocal")
        ),
        singleProfile = true
)
@SuppressWarnings("unused")
public interface MMIAntiSpoofingTrunkContextsCMP {
    String getTrunkContext();
    void setTrunkContext(String name);
}