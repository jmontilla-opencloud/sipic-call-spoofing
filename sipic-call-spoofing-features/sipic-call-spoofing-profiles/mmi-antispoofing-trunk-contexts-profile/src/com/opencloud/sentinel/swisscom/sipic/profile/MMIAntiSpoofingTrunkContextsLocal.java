package com.opencloud.sentinel.swisscom.sipic.profile;
 
import com.opencloud.rhino.slee.PropagateUncheckedThrowables;

import javax.slee.profile.ProfileLocalObject;

@PropagateUncheckedThrowables
public interface MMIAntiSpoofingTrunkContextsLocal extends ProfileLocalObject {
    String getTrunkContext();
}
