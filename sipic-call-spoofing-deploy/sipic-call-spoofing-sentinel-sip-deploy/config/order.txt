
# Generated based on dependencies of [opencloud#sentinel-sip-full-deploy#sentinel-sip/4.0.0;4.0.0.11]
sentinel-core-http-parameter-to-network-operator-profile
sentinel-core-http-determine-protocol-config-profile
sentinel-core-normalizer-config-profile
sentinel-core-ocs-round-robin-config-profile
sentinel-core-session-tracing-config-profile
sentinel-core-subscriber-data-ims-public-user-identity-profile
sentinel-core-subscriber-data-lookup-config-profile
sentinel-core-subscriber-data-lookup-field-definition-profile
sentinel-core-subscriber-data-lookup-sql-config-profile
sentinel-core-subscriber-data-record-profile
sentinel-dbquery-ra-deploy
sentinel-diameter-mediation-promotion-bucket-config-profile
sentinel-diameter-mediation-promotion-bucket-record-profile
sentinel-diameter-mediation-promotion-config-profile
sentinel-diameter-mediation-promotions-db-query-config-profile
sentinel-core-ocs-param-config-feature-profile
sentinel-diameter-ra-deploy
sentinel-diameter-ra-deploy
sentinel-rf-control-ra-deploy
sentinel-cdr-ra-deploy
sentinel-cdr-ra-deploy
sentinel-http-ra-deploy
sentinel-management-ra-deploy
sentinel-ocsip-deploy
sentinel-sip-home-plmnid-profile
sentinel-sis-sip-deploy
sentinel-sip-common-deploy
sentinel-sip-b2bua-subscription-activityguard-feature-profile
sentinel-sip-determine-call-type-profile
sentinel-sip-determine-network-operator-feature
sentinel-sip-error-code-announcement-mapping-profile
sentinel-sip-north-american-numbering-plan-area-codes-profile
sentinel-sip-prefix-analysis-feature
sentinel-sip-session-tracing-config-profile
sentinel-sip-set-language-profile
sentinel-sip-short-code-feature
sentinel-sip-vertical-service-code-actions-profile
sentinel-sip-vertical-service-code-address-list
sentinel-sip-sis-setup
sentinel-sip-b2bua-profile
sentinel-sip-callduration-feature-profile
sentinel-sip-cap-charging-profile
sentinel-sip-determine-charging-profile
sentinel-sip-diameter-service-specific-info-profile
sentinel-sip-session-replication-profile
sentinel-sip-session-refresh-feature-profile
sentinel-sip-cdf-destination-profile
sentinel-sip-interim-cdr-feature-profile
sentinel-sip-remove-headers-profile
sentinel-sip-sequential-forked-sdp-mediation-profile
mobile-network-code-profile
sentinel-sip-subscribe-downstream-forking-feature-profile
sentinel-uniqueid-ra-deploy
sentinel-sip-full-deploy
sentinel-sip-full-deploy
