This directory will contain all the deployment-modules of Sentinel and the SIPIC Call Spoofing service
- sipic-call-spoofing-sentinel-sip-deploy (name from blacklist): Sentinel SDK SIP with some exclusions (customized RAs)
- sipic-call-spoofing-resource-adaptors
    - DB Query
    - SIS SIP
- sipic-call-spoofing-standard-deploy: customized RAS + espin-deploy-sentinel-sip-service + sipic-call-spoofing-features
- sipic-call-spoofing-package (from MCP): creates a deployable package as a zip file. This can be used with the [VM Build Container][VMBC], or to manually deploy the service as a VM.
