#!/usr/bin/env python3

import argparse
import sys
from pathlib import Path
from typing import List, NamedTuple, Optional

import yaml


class Conditions(NamedTuple):
    false: List[str]
    true: Optional[str]


class NodeInfo(NamedTuple):
    name: str
    conditions: Conditions


def parse_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser(
        description="Print possible paths through a routing plan"
    )
    parser.add_argument("plan", type=Path, help="the routing plan YAML file")
    return parser.parse_args()


def assemble_paths(
    nodes: dict, node: dict, cur_conditions: Conditions, cur_path: List[NodeInfo]
) -> List[List[NodeInfo]]:
    if "transitions" not in node:
        return [cur_path + [NodeInfo(node["name"], cur_conditions)]]

    paths = []
    false_conditions: List[str] = []
    for transition in node["transitions"]:
        # Detect loops
        if [node for node in cur_path if node.name == transition["node"]]:
            continue

        next_node = [node for node in nodes if node["name"] == transition["node"]][0]
        paths += assemble_paths(
            nodes,
            next_node,
            Conditions(false_conditions[:], transition.get("condition")),
            cur_path + [NodeInfo(node["name"], cur_conditions)],
        )
        if "condition" in transition:
            false_conditions.append(transition["condition"])
    return paths


def main(args: argparse.Namespace) -> int:
    with open(args.plan) as f:
        plan = yaml.safe_load(f)

    start_node = [
        node for node in plan["nodes"] if node.get("position", None) == "start"
    ][0]

    paths = assemble_paths(plan["nodes"], start_node, Conditions([], None), [])

    for path in paths:
        indent = 0
        for node in path:
            for condition in node.conditions.false:
                print(" " * indent + "[NOT " + condition + "]")
            if node.conditions.true:
                print(" " * indent + "[" + node.conditions.true + "]")
            print(" " * indent + node.name)
            indent += 2

    return 0


if __name__ == "__main__":
    sys.exit(main(parse_args()))
