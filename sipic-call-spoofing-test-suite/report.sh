#!/bin/sh

if [ "$1" = "--long" ]; then

  grep "stu.scope" `find target/results/ -name stu.log | sort` | grep INFO | cut -f2- -d":"

else

  grep "stu.scope" `find target/results/ -name stu.log` | grep INFO | cut -f2- -d":" | grep "Test" | sed -E 's/^=(.*)main(.*)Test (.*) (.*)$/\3 \4 \1/' | sort | awk '{ color="32"; total++; if($2 == "failed") { color="31"; failed++;} else { passed++; } printf("%s %s -- %-70s \033[%sm%s\033[0m\n", $3, $4, $1, color, toupper($2)); } END { printf("Total: %d Passed: %d Failed: %d\n", total, passed, failed); }'

fi
