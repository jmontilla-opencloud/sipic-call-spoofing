## Test Suite ##

This is the base directory for the `mobile-control-point` test suite.

### Running the tests ###

First, run a local deploy (this can be container or non-container).  See the [main repository README][Home] for how to do this.

Use the `run_mcp_tests` alias to run scenario tests.  This takes an argument that is pattern matched against all the scenarios in this test suite.
Using this alias also generates `.pcap` files for the tests under `/target/results`.

- Example 1: `run_mcp_tests success-001` - this runs the `success-001` test only.
- Example 2: `run_mcp_tests sip-error` - this runs all the `sip-error` tests.
- Example 3: `run_mcp_tests` - this runs all the tests.

Alternatively, run `ant` commands directly:

- `ant run-integration-tests` will run all the tests.
- `ant run-integration-tests -Dincluded-tests=**/<stringtomatch>*/` will run all tests matching the specified string.

---

\[[Home][Home]\]

[Home]: ../../main "mobile-control-point"
