ifc-001-missing-route-header-parameter  [description "The INVITE's Route header is missing the parameter used to set the MCP plan ID.\nExpected to reject request with a 501 not implemented response."]  (FORMAT 1.0) {
   (ROLES) {
    scscf ;
    Sentinel ;
  }
   (DIALOGS) {
    leg-a  [color "#3300cc"]  (ROLE_A scscf, ROLE_B Sentinel, SCHEMA sip, VERSION 2.6) {
      applicationContext "SIP/2.0";
    }
  }
   (TABLES) {
    ScscfHost {
      {
        "as-request-uri";
        "caller-display";
        "caller-user-uri";
        "caller-digits-sip-uri";
        "caller-hostport-uri";
        "called-display";
        "called-user-uri";
        "called-digits-sip-uri";
        "called-hostport-uri";
        "as-hostport-uri";
        "scscf-hostport-uri";
        "p-asserted-identity";
        "p-served-user-orig";
        "p-served-user-term";
        "diverted-to-request-uri";
        "caller-user-uri-no-port";
        "called-user-uri-no-port";
      }
    }
    IC3Consultation {
      {
        "ic3-http-host";
        "caller-E164";
        "callee-E164";
        "call-type-mo";
        "call-type-mt";
        "temporary-routing-number";
        "temporary-routing-number-uri";
      }
    }
  }
  INVITE  (DIALOG leg-a, DIRECTION A_TO_B) {
    Request-URI {
      classic-URI {
        uri  (ALIAS as-request-uri, SELECT "ScscfHost[as-request-uri]") ;
      }
    }
    headers {
      Call-ID  (ALIAS leg-a-call-id) "abc-123";
      Contact {
        contact-param {
          address {
            display-name "SCSCF";
            URI {
              classic-URI {
                uri  (SELECT "ScscfHost[caller-user-uri]") ;
              }
            }
          }
        }
      }
      Content-Length  (AUTO) ;
      Content-Type {
        m-type "application";
        m-subtype "sdp";
      }
      CSeq  (ALIAS leg-a-cseq) {
        seq-id  (ALIAS leg-a-seq-number) "20";
        method  (AUTO) ;
      }
      From  (ALIAS leg-a-from) {
        address {
          URI {
            classic-URI {
              uri  (ALIAS from-uri, SELECT "ScscfHost[caller-user-uri-no-port]") ;
            }
          }
        }
        tag  (AUTO) ;
      }
      Max-Forwards  (AUTO) ;
      Route {
        route {
          address {
            display-name "Sentinel";
            URI {
              classic-URI {
                uri  (SELECT "ScscfHost[as-hostport-uri]") ;
                uri-parameters {
                  uri-parameter {
                    pname "lr";
                  }
                  uri-parameter {
                    pname "transport";
                    pvalue "tcp";
                  }
                }
              }
            }
          }
        }
        next {
          route {
            address {
              display-name "SCSCF";
              URI {
                classic-URI {
                  uri  (SELECT "ScscfHost[scscf-hostport-uri]") ;
                  uri-parameters {
                    uri-parameter {
                      pname "lr";
                    }
                  }
                }
              }
            }
          }
        }
      }
      Supported {
        option-tag "100rel";
      }
      To {
        address  (ALIAS leg-a-to-address) {
          URI {
            classic-URI {
              uri  (SELECT "ScscfHost[as-request-uri]") ;
            }
          }
        }
        tag  (COUNT 0) ;
      }
      Via {
        via {
          address  (AUTO) ;
          transport  (AUTO) ;
          branch  (AUTO) ;
        }
      }
      UserDefined {
        header-name "P-Asserted-Identity";
        header-value  (ALIAS pai, SELECT "ScscfHost[p-asserted-identity]") ;
      }
      UserDefined {
        header-name "P-Served-User";
        header-value  (ALIAS psu-orig, SELECT "ScscfHost[p-served-user-orig]") ;
      }
      UserDefined {
        header-name "P-Access-Network-Info";
        header-value  (ALIAS pani) "3GPP-UTRAN-FDD";
      }
      UserDefined {
        header-name "Session-Expires";
        header-value "1800";
      }
    }
    Message-Body {
      Sdp-Message-Body  (ALIAS sdp-o) {
        protocol-version "0";
        origin {
          o-username "IPX-MSX7";
          session-id  (ALIAS sdp-o-id) "20861089";
          session-version  (ALIAS sdp-o-v) "0";
          network-type "IN";
          address-type "IP4";
          origin-address "91.206.235.144";
        }
        session-name "sip call";
        Time-Description-Parts {
          Time-Description-Part {
            timing {
              timing-start-time  (AUTO) ;
              timing-stop-time  (AUTO) ;
            }
          }
        }
        connection-data {
          network-type "IN";
          address-type "IP4";
          connection-address "195.81.247.99";
        }
        Attributes {
          attribute {
            attribute-name "ptime";
            attribute-value "20";
          }
          attribute {
            attribute-name "sqn";
            attribute-value "0";
          }
          attribute {
            attribute-name "cdsc";
            attribute-value "1 audio RTP/AVP 0";
          }
          attribute {
            attribute-name "cdsc";
            attribute-value "2 image udptl t38";
          }
          attribute {
            attribute-name "cpar";
            attribute-value "a=T38FaxVersion:0";
          }
          attribute {
            attribute-name "cpar";
            attribute-value "a=T38FaxRateManagement:transferredTCF";
          }
          attribute {
            attribute-name "cpar";
            attribute-value "a=T38FaxMaxDatagram:160";
          }
          attribute {
            attribute-name "cpar";
            attribute-value "a=T38FaxUdpEC:t38UDPRedundancy";
          }
          attribute {
            attribute-name "X-sqn";
            attribute-value "0";
          }
          attribute {
            attribute-name "X-cap";
            attribute-value "1 image udptl t38";
          }
        }
        Media-Description-Parts {
          Media-Description-Part {
            media-descriptions {
              media "audio";
              port "9938";
              number-of-ports "1";
              sdp-protocol "RTP/AVP";
              Media-Format-List {
                media-format-description "0";
              }
            }
            Attributes {
              attribute {
                attribute-name "sendrecv";
              }
            }
          }
        }
      }
    }
  }
  100  (DIALOG leg-a, DIRECTION B_TO_A) {
    headers {
      Call-ID  (AUTO) ;
      CSeq  (ALIAS leg-a-cseq) ;
      From  (ALIAS leg-a-from) ;
      To {
        address  (ALIAS leg-a-to-address) ;
      }
      Via {
        via {
          address  (AUTO) ;
          transport  (AUTO) ;
          branch  (AUTO) ;
        }
      }
    }
  }
  501  (DIALOG leg-a, DIRECTION B_TO_A) {
    headers {
      Call-ID  (AUTO) ;
      CSeq  (ALIAS leg-a-cseq) ;
      From  (ALIAS leg-a-from) ;
      To {
        address  (ALIAS leg-a-to-address) ;
      }
      Via {
        via {
          address  (AUTO) ;
          transport  (AUTO) ;
          branch  (AUTO) ;
        }
      }
    }
  }
}