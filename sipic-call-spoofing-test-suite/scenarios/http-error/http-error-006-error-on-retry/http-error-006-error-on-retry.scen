http-error-006-error-on-retry  [description "IC3 responds with a retryable error. MCP retries the request to which IC3 responds with a non-retryable error. The INVITE is not redirected to the TRN."]  (FORMAT 1.0) {
   (ROLES) {
    scscf ;
    Sentinel ;
    IC3 ;
  }
   (DIALOGS) {
    leg-a  [color "#3300cc"]  (ROLE_A scscf, ROLE_B Sentinel, SCHEMA sip, VERSION 2.6) {
      applicationContext "SIP/2.0";
    }
    ic3-query  (ROLE_A Sentinel, ROLE_B IC3, SCHEMA http, VERSION 1.0c) {
      applicationContext "HTTP/1.1";
    }
    ic3-query-retry  [color "#999999"]  (ROLE_A Sentinel, ROLE_B IC3, SCHEMA http, VERSION 1.0c) {
      applicationContext "HTTP/1.1";
    }
    leg-b  [color "#009933"]  (ROLE_A Sentinel, ROLE_B scscf, SCHEMA sip, VERSION 2.6) {
      applicationContext "SIP/2.0";
    }
  }
   (TABLES) {
    ScscfHost {
      {
        "as-request-uri";
        "caller-display";
        "caller-user-uri";
        "caller-digits-sip-uri";
        "caller-hostport-uri";
        "called-display";
        "called-user-uri";
        "called-digits-sip-uri";
        "called-hostport-uri";
        "as-hostport-uri";
        "scscf-hostport-uri";
        "p-asserted-identity";
        "p-served-user-orig";
        "p-served-user-term";
        "diverted-to-request-uri";
        "caller-user-uri-no-port";
        "called-user-uri-no-port";
      }
    }
    IC3Consultation {
      {
        "ic3-http-host";
        "caller-E164";
        "callee-E164";
        "call-type-mo";
        "call-type-mt";
        "temporary-routing-number";
        "temporary-routing-number-uri";
      }
    }
  }
  INVITE  (DIALOG leg-a, DIRECTION A_TO_B) {
    Request-URI {
      classic-URI {
        uri  (ALIAS as-request-uri, SELECT "ScscfHost[as-request-uri]") ;
      }
    }
    headers {
      Call-ID  (ALIAS leg-a-call-id) "abc-123";
      Contact {
        contact-param {
          address {
            display-name "SCSCF";
            URI {
              classic-URI {
                uri  (SELECT "ScscfHost[caller-user-uri]") ;
              }
            }
          }
        }
      }
      Content-Length  (AUTO) ;
      Content-Type {
        m-type "application";
        m-subtype "sdp";
      }
      CSeq  (ALIAS leg-a-cseq) {
        seq-id  (ALIAS leg-a-seq-number) "20";
        method  (AUTO) ;
      }
      From  (ALIAS leg-a-from) {
        address {
          URI {
            classic-URI {
              uri  (ALIAS from-uri, SELECT "ScscfHost[caller-user-uri-no-port]") ;
            }
          }
        }
        tag  (AUTO) ;
      }
      Max-Forwards  (AUTO) ;
      Route {
        route {
          address {
            display-name "Sentinel";
            URI {
              classic-URI {
                uri  (SELECT "ScscfHost[as-hostport-uri]") ;
                uri-parameters {
                  uri-parameter {
                    pname "lr";
                  }
                  uri-parameter {
                    pname "transport";
                    pvalue "tcp";
                  }
                  uri-parameter {
                    pname "service";
                    pvalue "mcp";
                  }
                }
              }
            }
          }
        }
        next {
          route {
            address {
              display-name "SCSCF";
              URI {
                classic-URI {
                  uri  (SELECT "ScscfHost[scscf-hostport-uri]") ;
                  uri-parameters {
                    uri-parameter {
                      pname "lr";
                    }
                  }
                }
              }
            }
          }
        }
      }
      Supported {
        option-tag "100rel";
      }
      To {
        address  (ALIAS leg-a-to-address) {
          URI {
            classic-URI {
              uri  (SELECT "ScscfHost[as-request-uri]") ;
            }
          }
        }
        tag  (COUNT 0) ;
      }
      Via {
        via {
          address  (AUTO) ;
          transport  (AUTO) ;
          branch  (AUTO) ;
        }
      }
      UserDefined {
        header-name "P-Asserted-Identity";
        header-value  (ALIAS pai, SELECT "ScscfHost[p-asserted-identity]") ;
      }
      UserDefined {
        header-name "P-Served-User";
        header-value  (ALIAS psu-orig, SELECT "ScscfHost[p-served-user-orig]") ;
      }
      UserDefined {
        header-name "P-Access-Network-Info";
        header-value  (ALIAS pani) "3GPP-UTRAN-FDD";
      }
      UserDefined {
        header-name "Session-Expires";
        header-value "1800";
      }
    }
    Message-Body {
      Sdp-Message-Body  (ALIAS sdp-o) {
        protocol-version "0";
        origin {
          o-username "IPX-MSX7";
          session-id  (ALIAS sdp-o-id) "20861089";
          session-version  (ALIAS sdp-o-v) "0";
          network-type "IN";
          address-type "IP4";
          origin-address "91.206.235.144";
        }
        session-name "sip call";
        Time-Description-Parts {
          Time-Description-Part {
            timing {
              timing-start-time  (AUTO) ;
              timing-stop-time  (AUTO) ;
            }
          }
        }
        connection-data {
          network-type "IN";
          address-type "IP4";
          connection-address "195.81.247.99";
        }
        Attributes {
          attribute {
            attribute-name "ptime";
            attribute-value "20";
          }
          attribute {
            attribute-name "sqn";
            attribute-value "0";
          }
          attribute {
            attribute-name "cdsc";
            attribute-value "1 audio RTP/AVP 0";
          }
          attribute {
            attribute-name "cdsc";
            attribute-value "2 image udptl t38";
          }
          attribute {
            attribute-name "cpar";
            attribute-value "a=T38FaxVersion:0";
          }
          attribute {
            attribute-name "cpar";
            attribute-value "a=T38FaxRateManagement:transferredTCF";
          }
          attribute {
            attribute-name "cpar";
            attribute-value "a=T38FaxMaxDatagram:160";
          }
          attribute {
            attribute-name "cpar";
            attribute-value "a=T38FaxUdpEC:t38UDPRedundancy";
          }
          attribute {
            attribute-name "X-sqn";
            attribute-value "0";
          }
          attribute {
            attribute-name "X-cap";
            attribute-value "1 image udptl t38";
          }
        }
        Media-Description-Parts {
          Media-Description-Part {
            media-descriptions {
              media "audio";
              port "9938";
              number-of-ports "1";
              sdp-protocol "RTP/AVP";
              Media-Format-List {
                media-format-description "0";
              }
            }
            Attributes {
              attribute {
                attribute-name "sendrecv";
              }
            }
          }
        }
      }
    }
  }
  100  (DIALOG leg-a, DIRECTION B_TO_A) {
    headers {
      Call-ID  (AUTO) ;
      CSeq  (ALIAS leg-a-cseq) ;
      From  (ALIAS leg-a-from) ;
      To {
        address  (ALIAS leg-a-to-address) ;
      }
      Via {
        via {
          address  (AUTO) ;
          transport  (AUTO) ;
          branch  (AUTO) ;
        }
      }
    }
  }
  POST  (DIALOG ic3-query, DIRECTION A_TO_B) {
    Request-URI {
      Structured {
        scheme-specific {
          hierarchical {
            path {
              segment "api";
              segment "v1.0";
              segment "mcp";
              segment "consult";
            }
          }
        }
      }
    }
    headers {
      Content-Length  (AUTO) ;
      Content-Type "application/json";
      Host  (SELECT "IC3Consultation[ic3-http-host]") ;
    }
    Message-Body {
      application_json_UTF-8 {
        Object {
          Name-Value {
            Name "callerNumber";
            Value {
              Primitive {
                String  (SELECT "IC3Consultation[caller-E164]") ;
              }
            }
          }
          Name-Value {
            Name "calleeNumber";
            Value {
              Primitive {
                String  (SELECT "IC3Consultation[callee-E164]") ;
              }
            }
          }
          Name-Value {
            Name "flow";
            Value {
              Primitive {
                String  (SELECT "IC3Consultation[call-type-mo]") ;
              }
            }
          }
        }
      }
    }
  }
  500  (DIALOG ic3-query, DIRECTION B_TO_A) {
    headers {
      Content-Length  (AUTO) ;
      Content-Type "application/json";
      Date  (AUTO) ;
    }
    Message-Body {
      application_json_UTF-8 {
        Object {
          Name-Value {
            Name "error";
            Value {
              Object {
                Name-Value {
                  Name "errorCode";
                  Value {
                    Primitive {
                      String "\"TemporaryRoutingNumberExhaustion\"";
                    }
                  }
                }
                Name-Value {
                  Name "message";
                  Value {
                    Primitive {
                      String "\"A human-readable representation of the error.\"";
                    }
                  }
                }
                Name-Value {
                  Name "details";
                  Value {
                    Array {
                      Value {
                        Object {
                          Name-Value {
                            Name "code";
                            Value {
                              Primitive {
                                String "\"TBD\"";
                              }
                            }
                          }
                        }
                      }
                      Value {
                        Object {
                          Name-Value {
                            Name "message";
                            Value {
                              Primitive {
                                String "\"A human-readable representation of the error.\"";
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  POST  (DIALOG ic3-query-retry, DIRECTION A_TO_B) {
    Request-URI {
      Structured {
        scheme-specific {
          hierarchical {
            path {
              segment "api";
              segment "v1.0";
              segment "mcp";
              segment "consult";
            }
          }
        }
      }
    }
    headers {
      Content-Length  (AUTO) ;
      Content-Type "application/json";
      Host  (SELECT "IC3Consultation[ic3-http-host]") ;
    }
    Message-Body {
      application_json_UTF-8 {
        Object {
          Name-Value {
            Name "callerNumber";
            Value {
              Primitive {
                String  (SELECT "IC3Consultation[caller-E164]") ;
              }
            }
          }
          Name-Value {
            Name "calleeNumber";
            Value {
              Primitive {
                String  (SELECT "IC3Consultation[callee-E164]") ;
              }
            }
          }
          Name-Value {
            Name "flow";
            Value {
              Primitive {
                String  (SELECT "IC3Consultation[call-type-mo]") ;
              }
            }
          }
        }
      }
    }
  }
  400  (DIALOG ic3-query-retry, DIRECTION B_TO_A) {
    headers {
      Content-Length  (AUTO) ;
      Content-Type "application/json";
      Date  (AUTO) ;
    }
    Message-Body {
      application_json_UTF-8 {
        Object {
          Name-Value {
            Name "error";
            Value {
              Object {
                Name-Value {
                  Name "errorCode";
                  Value {
                    Primitive {
                      String "\"BadArgument\"";
                    }
                  }
                }
                Name-Value {
                  Name "message";
                  Value {
                    Primitive {
                      String "\"A human-readable representation of the error.\"";
                    }
                  }
                }
                Name-Value {
                  Name "details";
                  Value {
                    Array {
                      Value {
                        Object {
                          Name-Value {
                            Name "code";
                            Value {
                              Primitive {
                                String "\"NullValue\" or \"InvalidNumber\"";
                              }
                            }
                          }
                        }
                      }
                      Value {
                        Object {
                          Name-Value {
                            Name "message";
                            Value {
                              Primitive {
                                String "\"A human-readable representation of the error.\"";
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  INVITE  (DIALOG leg-b, DIRECTION A_TO_B) {
    Request-URI {
      classic-URI {
        uri  (ALIAS as-request-uri) ;
      }
    }
    headers {
      Call-ID  (AUTO, ALIAS leg-b-call-id) ;
      Content-Length  (AUTO) ;
      Content-Type {
        m-type "application";
        m-subtype "sdp";
      }
      CSeq  (ALIAS leg-b-cseq) ;
      From  (ALIAS leg-b-from) {
        address {
          URI {
            classic-URI {
              uri  (ALIAS from-uri) ;
            }
          }
        }
        tag  (AUTO, ALIAS leg-b-from-tag) ;
      }
      Max-Forwards  (AUTO) ;
      Route {
        route {
          address  (ALIAS leg-b-route-address) ;
        }
      }
      Supported {
        option-tag "100rel";
      }
      To {
        address  (ALIAS leg-b-to-address) {
          URI {
            classic-URI {
              uri  (SELECT "ScscfHost[as-request-uri]") ;
            }
          }
        }
      }
      Via  (ALIAS leg-b-via) {
        via {
          address  (AUTO) ;
          transport  (AUTO) ;
          branch  (AUTO) ;
        }
      }
      UserDefined {
        header-name "P-Asserted-Identity";
        header-value  (ALIAS pai) ;
      }
      UserDefined {
        header-name "P-Served-User";
        header-value  (ALIAS psu-orig) ;
      }
      UserDefined {
        header-name "P-Access-Network-Info";
        header-value  (ALIAS pani) ;
      }
      UserDefined {
        header-name "Session-Expires";
        header-value "1800";
      }
    }
    Message-Body {
      Sdp-Message-Body  (ALIAS sdp-o) ;
    }
  }
  100  (DIALOG leg-b, DIRECTION B_TO_A) {
    headers {
      Call-ID  (ALIAS leg-b-call-id) ;
      CSeq  (ALIAS leg-b-cseq) ;
      From  (ALIAS leg-b-from) ;
      To {
        address  (ALIAS leg-b-to-address) ;
      }
      Via  (ALIAS leg-b-via) ;
    }
  }
  180  (DIALOG leg-b, DIRECTION B_TO_A) {
    headers {
      Call-ID  (ALIAS leg-b-call-id) ;
      CSeq  (ALIAS leg-b-cseq) ;
      From  (ALIAS leg-b-from) ;
      Record-Route {
        record-route {
          address  (ALIAS leg-b-route-address) ;
        }
      }
      To  (ALIAS leg-b-to-with-tag) {
        address  (ALIAS leg-b-to-address) ;
        tag  (AUTO) ;
      }
      Via  (ALIAS leg-b-via) ;
    }
  }
  180  (DIALOG leg-a, DIRECTION B_TO_A) {
    headers {
      Call-ID  (ALIAS leg-a-call-id) ;
      Contact {
        contact-param {
          address {
            URI {
              classic-URI {
                uri  (ALIAS leg-a-sentinel-contact-uri) ;
              }
            }
          }
        }
      }
      CSeq  (ALIAS leg-a-cseq) ;
      From  (ALIAS leg-a-from) ;
      To  (ALIAS leg-a-to-with-tag) ;
      Via {
        via {
          address  (AUTO) ;
          transport  (AUTO) ;
          branch  (AUTO) ;
        }
      }
    }
  }
  200  (DIALOG leg-b, DIRECTION B_TO_A) {
    headers {
      Call-ID  (ALIAS leg-b-call-id) ;
      CSeq  (ALIAS leg-b-cseq) ;
      From  (ALIAS leg-b-from) ;
      Record-Route {
        record-route {
          address  (ALIAS leg-b-route-address) ;
        }
      }
      To  (ALIAS leg-b-to-with-tag) ;
      Via  (ALIAS leg-b-via) ;
    }
  }
  200  (DIALOG leg-a, DIRECTION B_TO_A) {
    headers {
      Call-ID  (ALIAS leg-a-call-id) ;
      CSeq  (ALIAS leg-a-cseq) ;
      From  (ALIAS leg-a-from) ;
      To  (ALIAS leg-a-to-with-tag) ;
      Via {
        via {
          address  (AUTO) ;
          transport  (AUTO) ;
          branch  (AUTO) ;
        }
      }
    }
  }
  ACK  (DIALOG leg-a, DIRECTION A_TO_B) {
    Request-URI {
      classic-URI {
        uri  (ALIAS leg-a-sentinel-contact-uri) ;
      }
    }
    headers {
      Call-ID  (ALIAS leg-a-call-id) ;
      CSeq {
        seq-id  (ALIAS leg-a-seq-number) ;
        method "ACK";
      }
      From  (ALIAS leg-a-from) ;
      Max-Forwards  (AUTO) ;
      To  (ALIAS leg-a-to-with-tag) ;
      Via {
        via {
          address  (AUTO) ;
          transport  (AUTO) ;
          branch  (AUTO) ;
        }
      }
    }
  }
  ACK  (DIALOG leg-b, DIRECTION A_TO_B) {
    Request-URI {
      classic-URI {
        uri  (ALIAS leg-b-ack-request-uri) ;
      }
    }
    headers {
      Call-ID  (ALIAS leg-b-call-id) ;
      CSeq  (ALIAS leg-b-ack-cseq) ;
      From  (ALIAS leg-b-from) ;
      Max-Forwards  (AUTO) ;
      To  (ALIAS leg-b-to-with-tag) ;
      Via {
        via {
          address  (AUTO) ;
          transport  (AUTO) ;
          branch  (AUTO) ;
        }
      }
    }
  }
  BYE  (DIALOG leg-a, DIRECTION A_TO_B, DELAY 500) {
    Request-URI {
      classic-URI {
        uri  (ALIAS leg-a-sentinel-contact-uri) ;
      }
    }
    headers {
      Call-ID  (ALIAS leg-a-call-id) ;
      CSeq  (ALIAS leg-a-bye-cseq) {
        seq-id  (AUTO) ;
        method  (AUTO) ;
      }
      From  (ALIAS leg-a-from) ;
      Max-Forwards  (AUTO) ;
      To  (ALIAS leg-a-to-with-tag) ;
      Via {
        via {
          address  (AUTO) ;
          transport  (AUTO) ;
          branch  (AUTO) ;
        }
      }
    }
  }
  BYE  (DIALOG leg-b, DIRECTION A_TO_B) {
    Request-URI {
      classic-URI {
        uri  (ALIAS leg-b-bye-request-uri) ;
      }
    }
    headers {
      Call-ID  (ALIAS leg-b-call-id) ;
      CSeq  (ALIAS leg-b-bye-cseq) ;
      From  (ALIAS leg-b-from) ;
      Max-Forwards  (AUTO) ;
      Route {
        route {
          address  (ALIAS leg-b-route-address) ;
        }
      }
      To  (ALIAS leg-b-to-with-tag) ;
      Via  (ALIAS leg-b-bye-via-alias) {
        via {
          address  (AUTO) ;
          transport  (AUTO) ;
          branch  (AUTO) ;
        }
      }
    }
  }
  200  (DIALOG leg-b, DIRECTION B_TO_A) {
    headers {
      Call-ID  (ALIAS leg-b-call-id) ;
      CSeq  (ALIAS leg-b-bye-cseq) ;
      From  (ALIAS leg-b-from) ;
      Record-Route {
        record-route {
          address  (ALIAS leg-b-route-address) ;
        }
      }
      To  (ALIAS leg-b-to-with-tag) ;
      Via  (ALIAS leg-b-bye-via-alias) {
        via {
          address  (AUTO) ;
          transport  (AUTO) ;
          branch  (AUTO) ;
        }
      }
    }
  }
  200  (DIALOG leg-a, DIRECTION B_TO_A) {
    headers {
      Call-ID  (ALIAS leg-a-call-id) ;
      CSeq  (ALIAS leg-a-bye-cseq) ;
      From  (ALIAS leg-a-from) ;
      To  (ALIAS leg-a-to-with-tag) ;
      Via {
        via {
          address  (AUTO) ;
          transport  (AUTO) ;
          branch  (AUTO) ;
        }
      }
    }
  }
}