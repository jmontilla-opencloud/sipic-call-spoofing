loop-avoidance-001-loop-avoidance-header-present  [description "An INVITE with the loop avoidance header `X-MSFT-FMC` with a value of v1.0 is not redirected to the TRN."]  (FORMAT 1.0) {
   (ROLES) {
    scscf ;
    Sentinel ;
  }
   (DIALOGS) {
    leg-a  [color "#3300cc"]  (ROLE_A scscf, ROLE_B Sentinel, SCHEMA sip, VERSION 2.6) {
      applicationContext "SIP/2.0";
    }
    leg-b  [color "#009933"]  (ROLE_A Sentinel, ROLE_B scscf, SCHEMA sip, VERSION 2.6) {
      applicationContext "SIP/2.0";
    }
  }
   (TABLES) {
    ScscfHost {
      {
        "as-request-uri";
        "caller-display";
        "caller-user-uri";
        "caller-digits-sip-uri";
        "caller-hostport-uri";
        "called-display";
        "called-user-uri";
        "called-digits-sip-uri";
        "called-hostport-uri";
        "as-hostport-uri";
        "scscf-hostport-uri";
        "p-asserted-identity";
        "p-served-user-orig";
        "p-served-user-term";
        "diverted-to-request-uri";
        "caller-user-uri-no-port";
        "called-user-uri-no-port";
      }
    }
  }
  INVITE  (DIALOG leg-a, DIRECTION A_TO_B) {
    Request-URI {
      classic-URI  (ALIAS leg-a-req-uri) {
        uri  (SELECT "ScscfHost[as-request-uri]") ;
      }
    }
    headers {
      Call-ID  (ALIAS leg-a-call-id) "abc050";
      Contact  (ALIAS leg-a-contact) {
        contact-param {
          address {
            URI {
              classic-URI {
                uri  (SELECT "ScscfHost[caller-user-uri]") ;
              }
            }
          }
        }
      }
      Content-Length  (AUTO) ;
      Content-Type {
        m-type "application";
        m-subtype "sdp";
      }
      CSeq  (ALIAS leg-a-cseq-inv) {
        seq-id  (ALIAS leg-a-seq-number) "20";
        method "INVITE";
      }
      From  (ALIAS leg-a-from) {
        address  (ALIAS leg-a-from-addr) {
          display-name  (SELECT "ScscfHost[caller-display]") ;
          URI {
            classic-URI {
              uri  (SELECT "ScscfHost[caller-user-uri-no-port]") ;
            }
          }
        }
        tag  (AUTO) ;
      }
      Max-Forwards  (AUTO) ;
      P-Charging-Vector  (ALIAS p-c-v) {
        icid-value "P-CSCFabcd5105a35000000225";
        icid-generated-at "192.168.0.48";
        orig-ioi "some.domain.com";
      }
      Record-Route {
        record-route {
          address  (ALIAS leg-a-scscf-record-route-address) {
            URI {
              classic-URI {
                uri  (SELECT "ScscfHost[scscf-hostport-uri]") ;
                uri-parameters {
                  uri-parameter {
                    pname "lr";
                  }
                }
              }
            }
          }
        }
      }
      Route  (ALIAS leg-a-route) {
        route {
          address {
            URI {
              classic-URI {
                uri  (SELECT "ScscfHost[as-hostport-uri]") ;
                uri-parameters {
                  uri-parameter {
                    pname "lr";
                  }
                  uri-parameter {
                    pname "regstate";
                    pvalue "reg";
                  }
                  uri-parameter {
                    pname "transport";
                    pvalue "tcp";
                  }
                  uri-parameter {
                    pname "service";
                    pvalue "mcp";
                  }
                }
              }
            }
          }
        }
        next {
          route  (ALIAS route-1) {
            address {
              URI {
                classic-URI {
                  uri  (SELECT "ScscfHost[scscf-hostport-uri]") ;
                  uri-parameters {
                    uri-parameter {
                      pname "lr";
                    }
                    uri-parameter {
                      pname "transport";
                      pvalue "tcp";
                    }
                  }
                }
              }
            }
          }
        }
      }
      Supported {
        option-tag "100rel";
      }
      To  (ALIAS leg-a-to) {
        address  (ALIAS a-to-address) {
          URI {
            classic-URI {
              uri  (SELECT "ScscfHost[as-request-uri]") ;
            }
          }
        }
      }
      User-Agent {
        product "IMS-Client";
      }
      Via  (ALIAS leg-a-as1-big-via) {
        via {
          address  (AUTO) ;
          transport  (AUTO) ;
          branch  (AUTO) ;
        }
      }
      UserDefined {
        header-name "P-Asserted-Identity";
        header-value  (ALIAS pai, SELECT "ScscfHost[p-asserted-identity]") ;
      }
      UserDefined {
        header-name "P-Access-Network-Info";
        header-value  (ALIAS pani) "3GPP-UTRAN-FDD";
      }
      UserDefined {
        header-name "Session-Expires";
        header-value "1800";
      }
      UserDefined {
        header-name "X-MSFT-FMC";
        header-value "v1.0";
      }
    }
    Message-Body  (ALIAS leg-a-sdp-active) {
      Sdp-Message-Body {
        protocol-version  (AUTO) ;
        origin {
          o-username "IPX-MSX7";
          session-id "1";
          session-version "0";
          network-type "IN";
          address-type "IP4";
          origin-address "127.0.0.1";
        }
        session-name "sip call";
        Time-Description-Parts {
          Time-Description-Part {
            timing {
              timing-start-time  (AUTO) ;
              timing-stop-time  (AUTO) ;
            }
          }
        }
        connection-data {
          network-type "IN";
          address-type "IP4";
          connection-address "1.1.1.1";
        }
        Media-Description-Parts {
          Media-Description-Part {
            media-descriptions {
              media "audio";
              port "9938";
              sdp-protocol "RTP/AVP";
              Media-Format-List {
                media-format-description "0";
              }
            }
            Attributes {
              attribute {
                attribute-name "sendrecv";
              }
            }
          }
        }
      }
    }
  }
  100  (DIALOG leg-a, DIRECTION B_TO_A) {
    headers {
      Call-ID  (ALIAS leg-a-call-id) ;
      Content-Length  (AUTO) ;
      CSeq  (ALIAS leg-a-cseq-inv) ;
      From  (ALIAS leg-a-from) ;
      To  (ALIAS leg-a-to) ;
      Via  (ALIAS leg-a-as1_via) ;
    }
  }
  INVITE  (DIALOG leg-b, DIRECTION A_TO_B, TIMEOUT 60000) {
    Request-URI {
      classic-URI {
        uri  (SELECT "ScscfHost[as-request-uri]") ;
      }
    }
    headers {
      Call-ID  (AUTO, ALIAS leg-b-call-id) ;
      Contact  (ALIAS leg-a-contact) ;
      Content-Length  (AUTO) ;
      Content-Type {
        m-type "application";
        m-subtype "sdp";
      }
      CSeq  (ALIAS leg-b-cseq-inv) ;
      From  (ALIAS leg-b-from) ;
      Max-Forwards  (AUTO) ;
      P-Charging-Vector  (ALIAS leg-b-pcv) ;
      Record-Route  (ALIAS leg-b-record-route) ;
      Route  (ALIAS leg-b-route) ;
      Supported {
        option-tag "100rel";
      }
      To  (ALIAS leg-b-to) {
        address  (ALIAS leg-b-to-address) ;
      }
      User-Agent {
        product "IMS-Client";
      }
      Via  (ALIAS leg-b-via) ;
      UserDefined {
        header-name "P-Asserted-Identity";
        header-value  (ALIAS pai) ;
      }
      UserDefined {
        header-name "P-Access-Network-Info";
        header-value  (ALIAS pani) ;
      }
      UserDefined {
        header-name "Session-Expires";
        header-value "1800";
      }
    }
    Message-Body  (ALIAS leg-a-sdp-active) ;
  }
  100  (DIALOG leg-b, DIRECTION B_TO_A) {
    headers {
      Call-ID  (ALIAS leg-b-call-id) ;
      Content-Length  (AUTO) ;
      CSeq  (ALIAS leg-b-cseq-inv) ;
      From  (ALIAS leg-b-from) ;
      To  (ALIAS leg-b-to) ;
      Via {
        via {
          address  (AUTO) ;
          transport  (AUTO) ;
          branch  (AUTO) ;
        }
      }
    }
  }
  180  (DIALOG leg-b, DIRECTION B_TO_A) {
    headers {
      Call-ID  (ALIAS leg-b-call-id) ;
      Contact  (ALIAS leg-b-user-contact) {
        contact-param {
          address  (AUTO) {
            URI {
              classic-URI {
                uri  (ALIAS leg-b-user-contact-uri) ;
              }
            }
          }
        }
      }
      Content-Length  (AUTO) ;
      CSeq  (ALIAS leg-b-cseq-inv) ;
      From  (ALIAS leg-b-from) ;
      To  (ALIAS leg-b-to-with-tag) {
        address  (ALIAS leg-b-to-address) ;
        tag  (AUTO) ;
      }
      Via  (ALIAS leg-b-via) ;
    }
  }
  180  (DIALOG leg-a, DIRECTION B_TO_A) {
    headers {
      Call-ID  (ALIAS leg-a-call-id) ;
      Contact  (ALIAS leg-b-user-contact) ;
      Content-Length  (AUTO) ;
      CSeq  (ALIAS leg-a-cseq-inv) ;
      From  (ALIAS leg-a-from) ;
      Record-Route {
        record-route {
          address  (ALIAS leg-a-record-route-address-1) ;
        }
        next {
          record-route {
            address  (AUTO, ALIAS leg-a-record-route-address-2) ;
          }
        }
      }
      To  (ALIAS leg-a-to-with-tag) ;
      Via  (ALIAS leg-a-180-via) ;
      UserDefined  (COUNT 0) ;
    }
  }
  200  (DIALOG leg-b, DIRECTION B_TO_A, DELAY 100) {
    headers {
      Call-ID  (ALIAS leg-b-call-id) ;
      Contact  (ALIAS leg-b-user-contact) ;
      Content-Length  (AUTO) ;
      Content-Type {
        m-type "application";
        m-subtype "sdp";
      }
      CSeq  (ALIAS leg-b-cseq-inv) ;
      From  (ALIAS leg-b-from) ;
      Record-Route {
        record-route {
          address {
            URI {
              classic-URI {
                uri  (SELECT "ScscfHost[scscf-hostport-uri]") ;
                uri-parameters {
                  uri-parameter {
                    pname "lr";
                  }
                }
              }
            }
          }
        }
      }
      To  (ALIAS leg-b-to-with-tag) ;
      Via  (ALIAS leg-b-via) ;
    }
    Message-Body  (ALIAS leg-b-sdp-answer) {
      Sdp-Message-Body {
        protocol-version  (AUTO) ;
        origin {
          o-username "leg-d";
          session-id "4";
          session-version "1";
          network-type "IN";
          address-type "IP4";
          origin-address "127.0.0.4";
        }
        session-name "leg-b-sdp";
        Time-Description-Parts {
          Time-Description-Part {
            timing {
              timing-start-time  (AUTO) ;
              timing-stop-time  (AUTO) ;
            }
          }
        }
        connection-data {
          network-type "IN";
          address-type "IP4";
          connection-address "4.4.4.4";
        }
        Media-Description-Parts {
          Media-Description-Part {
            media-descriptions {
              media "audio";
              port "4444";
              sdp-protocol "RTP/AVP";
              Media-Format-List {
                media-format-description "0";
              }
            }
            Attributes {
              attribute {
                attribute-name "rtpmap";
                attribute-value "0 PCMU/8000";
              }
            }
          }
        }
      }
    }
  }
  200  (DIALOG leg-a, DIRECTION B_TO_A) {
    headers {
      Call-ID  (ALIAS leg-a-call-id) ;
      Contact  (ALIAS leg-b-user-contact) ;
      Content-Length  (AUTO) ;
      Content-Type {
        m-type "application";
        m-subtype "sdp";
      }
      CSeq  (ALIAS leg-a-cseq-inv) ;
      From  (ALIAS leg-a-from) ;
      Record-Route {
        record-route {
          address  (ALIAS leg-a-record-route-address-1) ;
        }
      }
      To  (ALIAS leg-a-to-with-tag) ;
      Via  (ALIAS leg-a-200-via) ;
      UserDefined  (COUNT 0) ;
    }
    Message-Body  (ALIAS leg-b-sdp-answer) ;
  }
  ACK  (DIALOG leg-a, DIRECTION A_TO_B, DELAY 100) {
    Request-URI {
      classic-URI  (ALIAS leg-a-req-uri) ;
    }
    headers {
      Call-ID  (ALIAS leg-a-call-id) ;
      Content-Length  (AUTO) ;
      CSeq {
        seq-id  (ALIAS leg-a-seq-number) ;
        method "ACK";
      }
      From  (ALIAS leg-a-from) ;
      Max-Forwards  (AUTO) ;
      Route  (ALIAS leg-a-new-request-route) {
        route {
          address  (ALIAS leg-a-record-route-address-1) ;
        }
      }
      To  (ALIAS leg-a-to-with-tag) ;
      Via {
        via {
          address  (AUTO) ;
          transport  (AUTO) ;
          branch  (AUTO) ;
        }
      }
    }
  }
  ACK  (DIALOG leg-b, DIRECTION A_TO_B) {
    Request-URI {
      classic-URI {
        uri  (ALIAS leg-b-user-contact-uri) ;
      }
    }
    headers {
      Call-ID  (ALIAS leg-b-call-id) ;
      Content-Length  (AUTO) ;
      CSeq  (ALIAS leg-b-cseq-ack) ;
      From  (ALIAS leg-b-from) ;
      Max-Forwards  (AUTO) ;
      To  (ALIAS leg-b-to-with-tag) ;
      Via {
        via {
          address  (AUTO) ;
          transport  (AUTO) ;
          branch  (AUTO) ;
        }
      }
    }
  }
  BYE  (DIALOG leg-a, DIRECTION A_TO_B, DELAY 200) {
    Request-URI {
      classic-URI  (ALIAS leg-a-req-uri) ;
    }
    headers {
      Call-ID  (ALIAS leg-a-call-id) ;
      Contact  (ALIAS leg-a-contact) ;
      Content-Length  (AUTO) ;
      CSeq  (ALIAS leg-a-cseq-bye) {
        seq-id "500";
        method "BYE";
      }
      From  (ALIAS leg-a-from) ;
      Max-Forwards  (AUTO) ;
      Route  (ALIAS leg-a-new-request-route) ;
      To  (ALIAS leg-a-to-with-tag) ;
      Via  (ALIAS leg-a-as1-big-via) ;
    }
  }
  BYE  (DIALOG leg-b, DIRECTION A_TO_B) {
    Request-URI {
      classic-URI {
        uri  (ALIAS leg-b-bye-req-uri) ;
      }
    }
    headers {
      Call-ID  (ALIAS leg-b-call-id) ;
      Contact  (ALIAS leg-b-contact) ;
      Content-Length  (AUTO) ;
      CSeq  (ALIAS leg-b-cseq-bye) ;
      From  (ALIAS leg-b-from) ;
      Max-Forwards  (AUTO) ;
      Route  (ALIAS leg-b-new-request-route) ;
      To  (ALIAS leg-b-to-with-tag) ;
      Via  (ALIAS leg-b-bye-via) ;
    }
  }
  200  (DIALOG leg-b, DIRECTION B_TO_A, DELAY 100) {
    headers {
      Call-ID  (ALIAS leg-b-call-id) ;
      Content-Length  (AUTO) ;
      CSeq  (ALIAS leg-b-cseq-bye) ;
      From  (ALIAS leg-b-from) ;
      To  (ALIAS leg-b-to-with-tag) ;
      Via  (ALIAS leg-b-bye-via) ;
    }
  }
  200  (DIALOG leg-a, DIRECTION B_TO_A) {
    headers {
      Call-ID  (ALIAS leg-a-call-id) ;
      Content-Length  (AUTO) ;
      CSeq  (ALIAS leg-a-cseq-bye) ;
      From  (ALIAS leg-a-from) ;
      To  (ALIAS leg-a-to-with-tag) ;
      Via  (ALIAS leg-a-bye-via) ;
    }
  }
}