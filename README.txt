Sentinel SDK

This is the SDK for OpenCloud's Sentinel product.

Full documentation for getting started with and customising the SDK is
available from:
 https://docs.opencloud.com/ocdoc/go/product/sentinel-documentation/4.0.0

