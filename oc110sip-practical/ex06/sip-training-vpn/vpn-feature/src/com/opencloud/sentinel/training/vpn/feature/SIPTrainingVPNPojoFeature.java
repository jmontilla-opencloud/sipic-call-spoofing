package com.opencloud.sentinel.training.vpn.feature;

import javax.slee.ActivityContextInterface;
import javax.slee.annotation.ComponentId;
import javax.slee.facilities.Tracer;

import org.jainslee.resources.sip.SipRequest;

import com.opencloud.sce.fsmtool.Facilities;
import com.opencloud.sentinel.annotations.SentinelFeature;
import com.opencloud.sentinel.common.CallType;
import com.opencloud.sentinel.common.SentinelSipSessionState;
import com.opencloud.sentinel.feature.ExecutionPhase;
import com.opencloud.sentinel.feature.impl.BaseFeature;
import com.opencloud.sentinel.feature.spi.FeatureError;
import com.opencloud.sentinel.feature.spi.SentinelSipMultiLegFeatureEndpoint;
import com.opencloud.sentinel.multileg.Leg;
import com.opencloud.sentinel.multileg.LegManager;
import com.opencloud.slee.annotation.SBBPartReference;
import com.opencloud.slee.annotation.SBBPartReferences;

/**
 *
 * An The VPN feature.
 */

 /* TODO add raProviderJndiNames */ 
@SentinelFeature(
    featureName = SIPTrainingVPNPojoFeature.NAME,
    componentName = "@component.name@",
    featureVendor = "@component.vendor@",
    featureVersion = "@component.version@",
    featureGroup = SentinelFeature.SIP_FEATURE_GROUP,
    executionPhases = {ExecutionPhase.SipSessionPhase}    
)
@SBBPartReferences(
    sbbPartRefs = {
            @SBBPartReference(id = @ComponentId(name = "@sentinel-sip-spi.SentinelSipFeatureSPI SBB Part.name@", vendor = "@sentinel-sip-spi.SentinelSipFeatureSPI SBB Part.vendor@", version = "@sentinel-sip-spi.SentinelSipFeatureSPI SBB Part.version@"))
    }
)
/* TODO: class implements RA Injection interface*/
public class SIPTrainingVPNPojoFeature
        extends BaseFeature<SentinelSipSessionState, SentinelSipMultiLegFeatureEndpoint> {

    /** Feature name */
    public static final String NAME = "SIPTrainingVPNPojoFeature";

    public SIPTrainingVPNPojoFeature(SentinelSipMultiLegFeatureEndpoint caller,
            Facilities facilities, SentinelSipSessionState sessionState) {
        super(caller, facilities, sessionState);
    }

    /* TODO: add variables for DBQuery and SIP providers */
    /* TODO: add RA injection method */


    /**
     * All features must have a unique name.
     *
     * @return the name of this feature
     */
    @Override
    public String getFeatureName() { return NAME; }

    /**
     * Kick off the feature.
     *
     * @param trigger  a triggering context. The feature implementation must be able to cast this to a useful type for it to run
     * @param activity the slee activity object this feature is related to (may be null)
     * @param aci      the activity context interface of the slee activity this feature is related to
     */
    @Override
    public void startFeature(Object trigger, Object activity, ActivityContextInterface aci) {

        Tracer tracer = getTracer();

        if (tracer.isInfoEnabled()) {
             tracer.info("Starting " + NAME);
        }

        //TODO: Check if the request is Initial and MO (DetermineCallType)
                
        //TODO: Get the Leg Manager

        //TODO: Get the Incoming Invite Calling Party Leg

        //TODO: Get the income INVITE Request

        //TODO: Get the outgoing leg that is automatically created by the Sentinel B2BUA

        //TODO: Data retrieved from SubscriberDetermination

        //TODO: Get just the digits from URI

        /*TODO: implement feature logic */

        getCaller().featureHasFinished();

    }

    /*TODO: implement getVPNId method */

    /*TODO: implement getVPNMsisdn method */

    /*TODO: implement getDialledDigits method */
}
