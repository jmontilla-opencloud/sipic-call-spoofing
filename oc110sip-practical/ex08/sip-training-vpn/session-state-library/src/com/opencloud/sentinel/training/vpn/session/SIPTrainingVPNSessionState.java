package com.opencloud.sentinel.training.vpn.session;

import com.opencloud.sentinel.annotations.SessionStateInterface;
import com.opencloud.sentinel.common.SentinelSipSessionState;

@SessionStateInterface()
public interface SIPTrainingVPNSessionState extends SentinelSipSessionState  {
    /*TODO add session state getters and setters */
}