package com.opencloud.sentinel.training.vpn.feature;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sip.header.ToHeader;
import javax.slee.ActivityContextInterface;
import javax.slee.annotation.ComponentId;
import javax.slee.facilities.Tracer;

import org.jainslee.resources.sip.Address;
import org.jainslee.resources.sip.SipFactory;
import org.jainslee.resources.sip.SipParseException;
import org.jainslee.resources.sip.SipRequest;
import org.jainslee.resources.sip.SipURI;
import org.jainslee.resources.sip.URI;

import com.opencloud.sce.fsmtool.Facilities;
import com.opencloud.sentinel.annotations.SentinelFeature;
import com.opencloud.sentinel.common.CallType;
import com.opencloud.sentinel.common.SentinelSipSessionState;
import com.opencloud.sentinel.feature.ExecutionPhase;
import com.opencloud.sentinel.feature.impl.BaseFeature;
import com.opencloud.sentinel.feature.spi.FeatureError;
import com.opencloud.sentinel.feature.spi.SentinelSipMultiLegFeatureEndpoint;
import com.opencloud.sentinel.feature.spi.init.InjectResourceAdaptorProvider;
import com.opencloud.sentinel.multileg.Leg;
import com.opencloud.sentinel.multileg.LegManager;
import com.opencloud.slee.annotation.SBBPartReference;
import com.opencloud.slee.annotation.SBBPartReferences;
import com.opencloud.slee.resources.dbquery.DatabaseFutureResult;
import com.opencloud.slee.resources.dbquery.DatabaseQueryProvider;
import com.opencloud.slee.resources.dbquery.QueryInfo;

/**
 *
 * An The VPN feature.
 */

 /* TODO add raProviderJndiNames */ 
@SentinelFeature(
    featureName = SIPTrainingVPNPojoFeature.NAME,
    componentName = "@component.name@",
    featureVendor = "@component.vendor@",
    featureVersion = "@component.version@",
    featureGroup = SentinelFeature.SIP_FEATURE_GROUP,
    executionPhases = {ExecutionPhase.SipSessionPhase},
    raProviderJndiNames = {"slee/resources/dbquery/provider","slee/resources/sip/provider"}
)
@SBBPartReferences(
    sbbPartRefs = {
            @SBBPartReference(id = @ComponentId(name = "@sentinel-sip-spi.SentinelSipFeatureSPI SBB Part.name@", vendor = "@sentinel-sip-spi.SentinelSipFeatureSPI SBB Part.vendor@", version = "@sentinel-sip-spi.SentinelSipFeatureSPI SBB Part.version@"))
    }
)
public class SIPTrainingVPNPojoFeature
        extends BaseFeature<SentinelSipSessionState, SentinelSipMultiLegFeatureEndpoint> 
		implements InjectResourceAdaptorProvider {

    /** Feature name */
    public static final String NAME = "SIPTrainingVPNPojoFeature";

    public SIPTrainingVPNPojoFeature(SentinelSipMultiLegFeatureEndpoint caller,
            Facilities facilities, SentinelSipSessionState sessionState) {
        super(caller, facilities, sessionState);
    }

    private DatabaseQueryProvider dbQueryProvider;
    private SipFactory sipProvider;
    
    @Override
    public void injectResourceAdaptorProvider(Object provider) {
        if (provider instanceof DatabaseQueryProvider) {
            this.dbQueryProvider = (DatabaseQueryProvider) provider;
        } else if (provider instanceof SipFactory) {
            this.sipProvider = (SipFactory) provider;
        } else {
            if (provider != null && getTracer().isFineEnabled()) {
                getTracer().fine("Unknown provider " + provider);
            }
        }
    }

    /**
     * All features must have a unique name.
     *
     * @return the name of this feature
     */
    @Override
    public String getFeatureName() { return NAME; }

    /**
     * Kick off the feature.
     *
     * @param trigger  a triggering context. The feature implementation must be able to cast this to a useful type for it to run
     * @param activity the slee activity object this feature is related to (may be null)
     * @param aci      the activity context interface of the slee activity this feature is related to
     */
    @Override
    public void startFeature(Object trigger, Object activity, ActivityContextInterface aci) {

        Tracer tracer = getTracer();

        if (tracer.isInfoEnabled()) {
             tracer.info("Starting " + NAME);
        }

        //Check if the request is Initial and MO (DetermineCallType)
        if (getSessionState().getCallType() != CallType.MobileOriginating) {
            getTracer().finer("Not handling this trigger");
            getCaller().featureHasFinished();
            return;
        }
        
        //Get the Leg Manager
        final LegManager legManager = getCaller().getLegManager();
        if(legManager == null) {
            if(getTracer().isFineEnabled()) getTracer().fine("Error! - Unable retrieve incoming INVITE request, Reason: Could not access SIP leg manager");
            getCaller().featureHasFinished();
            return;
        }

        //Get the Incoming Invite Calling Party Leg
        final Leg incomingLeg = legManager.getCallingPartyLeg();
        if(incomingLeg == null) {
            if(getTracer().isFineEnabled()) getTracer().fine("Error! - Unable retrieve incoming INVITE request, Reason: Could not obtain calling party leg");
            getCaller().featureHasFinished();
            return;
        }

        //Get the income INVITE Request
        final SipRequest request = incomingLeg.getInviteRequest();
        if(request == null) {
            if(getTracer().isFineEnabled()) getTracer().fine("Error! - Unable to run feature, Reason: Could not access incoming SIP INVITE");
            getCaller().featureHasFinished();
            return;
        }

        //Get the outgoing leg that is automatically created by the Sentinel B2BUA
        final Leg outgoingLeg = legManager.getLeg(legManager.getCallingParty()).getLinkedLeg();
        if(outgoingLeg == null) {
            if(getTracer().isFineEnabled()) getTracer().fine("Error! - Unable retrieve outgoing INVITE request, Reason: Could not obtain linked leg");
            getCaller().featureHasFinished();
            return;
        }

        // Data retrieved from SipSubscriberDetermination
        final String subscriberURI = getSessionState().getSubscriber();
        if (subscriberURI == null) {
            // error - we expect the subscriber to be Present
            getCaller().featureCannotStart(
                    new FeatureError(FeatureError.Cause.invalidSubscriber, "Subscriber is missing"));
            getCaller().featureHasFinished();
            return;
        }
        
        //GET just the digits from URI
        final String msisdn = subscriberURI.substring(subscriberURI.indexOf(':') + 1,
            subscriberURI.indexOf('@'));
        if (getTracer().isFineEnabled()) {
            getTracer().fine("MO Call, Subscriber = " + msisdn);
        }

        final String dialledDigits = getDialledDigits(request);
        if (getTracer().isFineEnabled())
            getTracer().fine("Subscriber dialled digits = " + dialledDigits);
        final String vpnId;
        final String translatedLongNumber;
        try {
            vpnId = getVPNId(msisdn);
            translatedLongNumber = vpnId != null ? getVPNMsisdn(vpnId, dialledDigits) : null;
        } catch (Exception e) {
            if (getTracer().isFineEnabled())
                    getTracer().fine ("Error! - Exception looking up VPN info", e);
            getCaller().featureFailedToExecute(new FeatureError(FeatureError.Cause.unclassified));
            getCaller().featureHasFinished();
            return;
        }
        if (translatedLongNumber != null) {
            if (translatedLongNumber.equals(msisdn)) {
                if (getTracer().isFineEnabled()) {
                    getTracer().fine("Error ! - Calling and Called Party the same "
                            + msisdn + "=" + translatedLongNumber);
                }
                //Send 603 Decline
                getCaller().getLegManager().endSession(603);
                getCaller().featureHasFinished();
                return;
            }        	
            if (getTracer().isFineEnabled()) getTracer().fine("Call is a VPN call.  VPN = " + vpnId
                        + ", shortCode="+dialledDigits+", long number = " + translatedLongNumber);
            // Get Outgoing Leg request from Leg manager
            SipRequest outgoingRequest = (SipRequest) outgoingLeg.getMessagesToSend().peek();
            if (outgoingRequest == null){
                if (getTracer().isFineEnabled()) getTracer().fine ("Error! - Can't get outgoing INVITE");
                getCaller().featureFailedToExecute(new FeatureError(FeatureError.Cause.unclassified));
                getCaller().featureHasFinished();
                return;
            }
            try {
                // Set Outgoing request URI, P-Served-User, and  TO URI's to the translated number
                URI outRequestURI = null;
                if (outgoingRequest.getRequestURI().isSipURI()){
                    String host = ((SipURI) outgoingRequest.getRequestURI()).getHost();
                    int port = ((SipURI) outgoingRequest.getRequestURI()).getPort();
                    outRequestURI = sipProvider.createSipURI(translatedLongNumber, host + ":" + port);
                } else {
                        // TEL URL
                    outRequestURI = sipProvider.createURI("tel:" + translatedLongNumber);
                }
                outgoingRequest.setRequestURI(outRequestURI);
                Address outToAddress = sipProvider.createAddress(outRequestURI);
                outgoingRequest.setAddressHeader(ToHeader.NAME, outToAddress);
                outgoingRequest.setAddressHeader("P-Asserted-Identity", outToAddress);
                outgoingLeg.setInviteRequest(outgoingRequest);
                outgoingLeg.getMessagesToSend().remove();
                outgoingLeg.getMessagesToSend().add(outgoingRequest);
            } catch (SipParseException e) {
                if (getTracer().isFineEnabled())
                        getTracer().fine ("Exception updating Address Headers", e);
                getCaller().featureFailedToExecute(new FeatureError(FeatureError.Cause.unclassified));
                getCaller().featureHasFinished();
                return;
            }
        } else {
            getTracer().fine("The call is not a VPN call.");
        }                
        getCaller().featureHasFinished();

    }

    private static final int TIMEOUT = 1000;
    private static final String SQL_GET_VPN = "SELECT VPNId FROM VPNMembers WHERE msisdn = ?";
    private String getVPNId(final String msisdn) {
        try {
            final DatabaseFutureResult futureResult = dbQueryProvider.sendQuery(new QueryInfo() {
                public String getSql() {    return SQL_GET_VPN; }
                public void setParameters(PreparedStatement ps) throws SQLException {
                    ps.setString(1, msisdn);
                }
                public ExecuteType getExecuteType() {
                    return ExecuteType.QUERY;
                }
                public RetryBehaviour getRetryBehaviour() {
                    return RetryBehaviour.TRY_FIRST_AVAILABLE;
                }
                public StatementType getStatementType() {
                    return StatementType.PREPARED;
                }
            });
            final ResultSet rs = futureResult.getResultSet(TIMEOUT);
            String result = rs.next() ? rs.getString(1) : null;
            rs.close();
            return result;
        } catch (Exception e) {
            if (getTracer().isFineEnabled())
                getTracer().fine("Error retrieving VPN Id from database.", e);
            return null;
        }
    }

    private static final String SQL_MSISDN = "SELECT msisdn FROM VPNMembers WHERE VPNId = ? AND shortCode = ?";
    private String getVPNMsisdn(final String VPNId, final String shortCode) {
        try {
            final DatabaseFutureResult futureResult = dbQueryProvider.sendQuery(new QueryInfo() {
                public String getSql() {
                    return SQL_MSISDN;
                }
                public void setParameters(PreparedStatement ps) throws SQLException {
                    ps.setString(1, VPNId);
                    ps.setString(2, shortCode);
                }
                public ExecuteType getExecuteType() {
                    return ExecuteType.QUERY;
                }
                public RetryBehaviour getRetryBehaviour() {
                    return RetryBehaviour.TRY_FIRST_AVAILABLE;
                }
                public StatementType getStatementType() {
                    return StatementType.PREPARED;
                }
            });
            final ResultSet rs = futureResult.getResultSet(TIMEOUT);
            String result = rs.next() ? rs.getString(1) : null;
            rs.close();
            return result;
        } catch (Exception e) {
            if (getTracer().isFineEnabled()) 
                    getTracer().fine("Error retrieving VPN MSISDN from database.", e);
            return null;
        }
    }

    private String getDialledDigits(SipRequest request) {
        // Data retrieved from SubscriberDetermination
        final String calledPartyURI = getSessionState().getCalledPartyAddress();
        final String dialledDigits = calledPartyURI.substring(
                calledPartyURI.indexOf(':') + 1, calledPartyURI.indexOf('@'));
        // Removes country code +886
        if (dialledDigits.startsWith("+886")) {
            return dialledDigits.substring(4);
        } else {
            return dialledDigits;
        }
    }
}
