package com.opencloud.sentinel.train.vpn.addresslistprofile;

import com.opencloud.rhino.slee.PropagateUncheckedThrowables;
import com.opencloud.sentinel.feature.common.lists.profiles.AddressListEntryProfileLocal;

@PropagateUncheckedThrowables
public interface SIPTrainingVPNListEntryProfileLocal extends AddressListEntryProfileLocal {
    boolean getIsWhiteListed();
}
