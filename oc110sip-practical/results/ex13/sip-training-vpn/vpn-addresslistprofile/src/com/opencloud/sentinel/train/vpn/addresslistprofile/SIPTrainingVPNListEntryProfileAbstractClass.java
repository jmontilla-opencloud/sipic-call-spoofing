package com.opencloud.sentinel.train.vpn.addresslistprofile;

import com.opencloud.sentinel.common.BaseProfileAbstractClass;

import java.util.List;
@SuppressWarnings("unused")
public abstract class SIPTrainingVPNListEntryProfileAbstractClass extends BaseProfileAbstractClass
        implements SIPTrainingVPNListEntryProfileCMP {

    public SIPTrainingVPNListEntryProfileAbstractClass() {

    }

    @Override
    protected void verify(List<String> errors) {

    }
}
