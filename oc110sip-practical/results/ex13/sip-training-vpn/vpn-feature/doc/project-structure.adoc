== Project Structure

A project contains build infrastructure, modules, project and environment information.
Each project has a project root directory.

At minimum a project has:

* A sdk.properties file
* A deps.properties file
* A build.xml file
* A build infrastructure directory (+build+)


An example of an empty project is shown below

----
project-dir/sdk.properties
project-dir/build.xml
project-dir/deps.properties
project-dir/build
----

An example of a +deps.properties+ file is shown below

----
include::{includedir}/../../../../deps.properties[]
----


=== Build Infrastructure

The build infrastructure is based on Apache Ant (http://ant.apache.org) and Apache Ivy (http://ant.apache.org/ivy).
Ant is used for build, deploy, configuration, and to glue various tools together.

Apache Ivy is used for modules and dependency management.

A project has some common build infrastructure in a subdirectory of the project root.
The subdirectory name is +build+.

=== Modules

Each module is located in a sub-directory of the project's root directory.


=== Project and Environment information

Project and Environment information includes, but is not limited to:

* the filesystem location of common build tasks
* the location of the +rhino-client+ directory - so that querying and installation of Rhino can be performed
* Artifact server host/URL, and username password

This information is stored in the Project root's sdk.properties file.

Local overrides can be added to the Project root's sdk.local.properties file.

=== Example Project

This example shows a project located in a directory named 'project-root'.

This project includes a single module called 'example-module'.

The 'example-module' module contains another module, this is called example-module2

----
project-root/sdk.properties
project-root/sdk.local.properties
project-root/build/module-common.xml
project-root/build/module-defaults.xml
project-root/example-module/build.xml
project-root/example-module/ivy.xml
project-root/example-module/module.properties
project-root/example-module/src
project-root/example-module/doc
project-root/example-module/example-module2/build.xml
project-root/example-module/example-module2/ivy.xml
project-root/example-module/example-module2/module.properties
project-root/example-module/example-module2/src
project-root/example-module/example-module2/doc
project-root/example-module/example-module2/test
----
