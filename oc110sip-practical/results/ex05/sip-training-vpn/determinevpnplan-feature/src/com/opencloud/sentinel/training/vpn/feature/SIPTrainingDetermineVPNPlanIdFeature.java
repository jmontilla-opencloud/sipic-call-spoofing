package com.opencloud.sentinel.training.vpn.feature;

import javax.slee.ActivityContextInterface;
import javax.slee.annotation.ComponentId;

import org.jainslee.resources.sip.SipRequest;
import org.jainslee.resources.sip.SipURI;
import org.jainslee.resources.sip.TelURL;
import org.jainslee.resources.sip.URI;

import com.opencloud.sce.fsmtool.Facilities;
import com.opencloud.sentinel.annotations.SentinelFeature;
import com.opencloud.sentinel.common.CallType;
import com.opencloud.sentinel.common.SentinelSelectionKey;
import com.opencloud.sentinel.common.SentinelSipSessionState;
import com.opencloud.sentinel.feature.ExecutionPhase;
import com.opencloud.sentinel.feature.impl.BaseFeature;
import com.opencloud.sentinel.feature.spi.FeatureError;
import com.opencloud.sentinel.feature.spi.SentinelSipMultiLegFeatureEndpoint;
import com.opencloud.slee.annotation.SBBPartReference;
import com.opencloud.slee.annotation.SBBPartReferences;


@SentinelFeature(
    featureName = SIPTrainingDetermineVPNPlanIdFeature.NAME,
    componentName = "@component.name@",
    featureVendor = "@component.vendor@",
    featureVersion = "@component.version@",
    featureGroup = SentinelFeature.SIP_FEATURE_GROUP,
    executionPhases = {ExecutionPhase.SipSessionPhase}    
)
@SBBPartReferences(
    sbbPartRefs = {
            @SBBPartReference(id = @ComponentId(name = "@sentinel-sip-spi.SentinelSipFeatureSPI SBB Part.name@", vendor = "@sentinel-sip-spi.SentinelSipFeatureSPI SBB Part.vendor@", version = "@sentinel-sip-spi.SentinelSipFeatureSPI SBB Part.version@"))
    }
)

public class SIPTrainingDetermineVPNPlanIdFeature
        extends BaseFeature<SentinelSipSessionState, SentinelSipMultiLegFeatureEndpoint> {

    /** Feature name */
    public static final String NAME = "SIPTrainingDetermineVPNPlanIdFeature";

    public SIPTrainingDetermineVPNPlanIdFeature(SentinelSipMultiLegFeatureEndpoint caller,
            Facilities facilities, SentinelSipSessionState sessionState) {
        super(caller, facilities, sessionState);
    }

    /**
     * All features must have a unique name.
     *
     * @return the name of this feature
     */
    @Override
    public String getFeatureName() { return NAME; }

    /**
     * Kick off the feature.
     *
     * @param trigger  a triggering context. The feature implementation must be able to cast this to a useful type for it to run
     * @param activity the slee activity object this feature is related to (may be null)
     * @param aci      the activity context interface of the slee activity this feature is related to
     */
    @Override
    public void startFeature(Object trigger, Object activity, ActivityContextInterface aci) {

        try {
            if (getTracer().isFineEnabled()) getTracer().fine("SIPTrainingDetermineVPNPlanFeature feature starting");

            SentinelSelectionKey selectionKey = getSessionState().getSentinelSelectionKey();
            if (selectionKey == null) {
                if (getTracer().isFineEnabled())
                    getTracer().fine("Error! - Unable to set session PlanID, Reason: Could not access Sentinel selection key");
                getCaller().featureFailedToExecute(new FeatureError(FeatureError.Cause.invalidSessionState, "Could not access Sentinel selection key"));
                return;
            }

            SipRequest sipRequest = getTriggeringRequest(trigger);
            if(sipRequest == null) {
                if (getTracer().isFineEnabled())
                    getTracer().fine("Error! - Unable to set session PlanID, Reason: Trigger was not an initial INVITE, MESSAGE or SUBSCRIBE");
                getCaller().featureFailedToExecute(new FeatureError(FeatureError.Cause.unsupportedTriggerEvent, "Trigger was not an initial INVITE, MESSAGE or SUBSCRIBE"));
                return;
            }

            String planId = selectionKey.getPlanId();
            if(planId.isEmpty()) {
                if (isVPNCall(sipRequest)) {
                    selectionKey.setPlanId("vpn");
                    if (getTracer().isFinerEnabled()) getTracer().finer("Setting Sentinel selection key to: " + selectionKey);
                    getSessionState().setSentinelSelectionKey(selectionKey);
                }else {
                    if (getTracer().isFinerEnabled()) getTracer().finer("Not a VPN call");
                }
            }
            else if (getTracer().isFineEnabled()) getTracer().fine("PlanID already set, selection key will not be changed");
        }
        finally {
            if (getTracer().isFineEnabled()) getTracer().fine(getFeatureName() + "  has finished");
            getCaller().featureHasFinished();
        }

    }
    
     private SipRequest getTriggeringRequest(Object trigger) {
        if(!(trigger instanceof SipRequest)) {
            if(getTracer().isFineEnabled()) getTracer().fine("Trigger is not a SIP Request");
            return null;
        }

        SipRequest request = (SipRequest) trigger;
        if(!request.isInitial()) {
            if(getTracer().isFineEnabled()) getTracer().fine("Trigger is not an initial SIP Request");
            return null;
        }

        if(!SipRequest.INVITE.equals(request.getMethod())) {
            if(getTracer().isFineEnabled()) getTracer().fine("Trigger is not a INVITE request");
            return null;
        }
        
        return request;
    }
    
     private boolean isVPNCall(SipRequest sipRequest){
         return getSessionState().getCallType().equals(CallType.MobileOriginating) && isShortNumber(sipRequest);
     }
    
     private boolean isShortNumber(SipRequest request) {
         String dialledNumer = null;
         if (request == null || request.getTo() == null || request.getTo().getURI() == null) {
             return false;
         }
         URI toURI = request.getTo().getURI();
         if (toURI.isSipURI()) {
             dialledNumer = ((SipURI) toURI).getUser();
         } else {
             dialledNumer = ((TelURL) toURI).getPhoneNumber();
         }
         return dialledNumer.length() < 6;
     }

}
