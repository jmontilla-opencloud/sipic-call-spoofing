/**
 * Copyright (c) 2014 Open Cloud Limited, a company incorporated in England and Wales (Registration Number 6000941) with its principal place of business at Edinburgh House, St John's Innovation Park, Cowley Road, Cambridge CB4 0DS.
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1  Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2  Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3  The name of the author may not be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * 4  The source code may not be used to create, develop, use or distribute software for use on any platform other than the Open Cloud Rhino and Open Cloud Rhino Sentinel platforms or any successor products.
 *
 * 5  Full license terms may be found https://developer.opencloud.com/devportal/display/OCDEV/Feature+Source+License
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE ARE EXCLUDED TO THE FULLEST EXTENT PERMITTED BY LAW.
 *
 * TO THE FULLEST EXTENT PERMISSIBLE BUY LAW, THE AUTHOR SHALL NOT BE LIABLE FOR ANY LOSS OF REVENUE, LOSS OF PROFIT, LOSS OF FUTURE BUSINESS, LOSS OF DATA OR ANY INDIRECT, SPECIAL, CONSEQUENTIAL, PUNITIVE OR OTHER LOSS OR DAMAGES ARISING OUT OF OR IN CONNECTION WITH THE SOFTWARE, WHETHER ARISING IN CONTRACT, TORT (INCLUDING NEGLIGENCE) MISREPRESENTATION OR OTHERWISE AND REGARDLESS OF WHETHER OPEN CLOUD HAS BEEN ADVISED OF THE POSSIBILITY OF ANY SUCH LOSS OR DAMAGE. THE AUTHORS MAXIMUM AGGREGATE LIABILITY WHETHER IN CONTRACT, TORT (INCLUDING NEGLIGENCE) OR OTHERWISE, SHALL NOT EXCEED EUR100.
 *
 * NOTHING IN THIS LICENSE SHALL LIMIT THE LIABILITY OF THE AUTHOR FOR DEATH OR PERSONAL INJURY RESULTING FROM NEGLIGENCE, FRAUD OR FRAUDULENT MISREPRESENTATION.
 *
 * Visit Open Cloud Developer's Portal for how-to guides, examples, documentation, forums and more: http://developer.opencloud.com
 */
package com.opencloud.sentinel.training.vpn.cdrfeature;

import com.google.protobuf.Message;
import com.opencloud.cdrformat.AvpCdrFormat;
import com.opencloud.rhino.facilities.sas.InvokingTrailAccessor;
import com.opencloud.sce.fsmtool.Facilities;
import com.opencloud.sentinel.charging.ChargingInstance;
import com.opencloud.sentinel.common.MccMnc;
import com.opencloud.sentinel.common.SentinelError;
import com.opencloud.sentinel.common.SentinelSelectionKey;
import com.opencloud.sentinel.common.SentinelSipSessionState;
import com.opencloud.sentinel.feature.common.cdr.util.SipAvpCdrUtil;
import com.opencloud.sentinel.feature.impl.BaseFeature;
import com.opencloud.sentinel.feature.spi.SentinelSipMultiLegFeatureEndpoint;
import com.opencloud.sentinel.feature.spi.SipFeature;
import com.opencloud.sentinel.feature.spi.init.InjectFeatureStats;
import com.opencloud.sentinel.feature.spi.init.InjectResourceAdaptorProvider;
import com.opencloud.sentinel.training.vpn.session.SIPTrainingVPNSessionState;
import com.opencloud.sentinel.util.ExceptionTraceHelper;
import com.opencloud.sentinel.util.OpenCloudDiameter;
import com.opencloud.slee.resources.cdr.CDRProvider;
import com.opencloud.slee.resources.cdr.WriteTimeoutException;
import org.jainslee.resources.diameter.base.DiameterAvp;
import org.jainslee.resources.diameter.base.DiameterMessageFactory;
import org.jainslee.resources.diameter.base.NoSuchAvpException;
import org.jainslee.resources.diameter.cca.types.SubscriptionId;
import org.jainslee.resources.diameter.cca.types.SubscriptionIdType;
import org.jainslee.resources.diameter.ro.RoProviderFactory;
import org.jainslee.resources.diameter.ro.types.vcb0.ImsInformation;
import org.jainslee.resources.diameter.ro.types.vcb0.MultipleServicesCreditControl;
import org.jainslee.resources.diameter.ro.vcb0.RoProvider;

import javax.slee.ActivityContextInterface;
import java.io.IOException;
import java.util.Collection;
import java.util.Date;

import static com.opencloud.sentinel.charging.SentinelSipAVPs.OC_ACCESS_NETWORK_MCC_MNC_AVP;
import static com.opencloud.sentinel.charging.SentinelSipAVPs.OC_CALL_ID_AVP;
import static com.opencloud.sentinel.charging.SentinelSipAVPs.OC_CALL_TYPE_AVP;
import static com.opencloud.sentinel.charging.SentinelSipAVPs.OC_CHARGING_RESULT_AVP;
import static com.opencloud.sentinel.charging.SentinelSipAVPs.OC_END_SESSION_CAUSE_AVP;
import static com.opencloud.sentinel.charging.SentinelSipAVPs.OC_EVENT_ID_AVP;
import static com.opencloud.sentinel.charging.SentinelSipAVPs.OC_IMSI_MCC_MNC_AVP;
import static com.opencloud.sentinel.charging.SentinelSipAVPs.OC_OCS_SESSION_ID_AVP;
import static com.opencloud.sentinel.charging.SentinelSipAVPs.OC_OCS_SESSION_TERMINATION_CAUSE_AVP;
import static com.opencloud.sentinel.charging.SentinelSipAVPs.OC_PLAY_ANNOUNCEMENT_ID_AVP;
import static com.opencloud.sentinel.charging.SentinelSipAVPs.OC_SENTINEL_ERROR_AVP;
import static com.opencloud.sentinel.charging.SentinelSipAVPs.OC_SENTINEL_SELECTION_KEY_AVP;
import static com.opencloud.sentinel.charging.SentinelSipAVPs.OC_SERVICE_TYPE_AVP;
import static com.opencloud.sentinel.charging.SentinelSipAVPs.OC_SESSION_END_TIME_AVP;
import static com.opencloud.sentinel.charging.SentinelSipAVPs.OC_SESSION_ESTABLISHED_TIME_AVP;
import static com.opencloud.sentinel.charging.SentinelSipAVPs.OC_SESSION_START_TIME_AVP;
import static com.opencloud.sentinel.charging.SentinelSipAVPs.OC_VISITED_NETWORK_MCC_MNC_AVP;
import static com.opencloud.sentinel.charging.SentinelSipAVPs.SERVICE_CONTEXT_AVP;
import static com.opencloud.sentinel.charging.SentinelSipAVPs.SESSION_ID_AVP;

public class BaseSipAvpCdrFeature extends BaseFeature<SIPTrainingVPNSessionState , SentinelSipMultiLegFeatureEndpoint>
    implements SipFeature, InjectResourceAdaptorProvider, InjectFeatureStats<CDRStats> {

    public BaseSipAvpCdrFeature(SentinelSipMultiLegFeatureEndpoint caller, Facilities facilities, SIPTrainingVPNSessionState  sessionState) {
        super(caller, facilities, sessionState);
    }

    @Override
    public void startFeature(Object trigger, Object activity, ActivityContextInterface aci) {
        //This is just the base feature and cannot be run on its own
    }

    protected void writeCdr() {
        if (getTracer().isFinestEnabled())
            getTracer().finest("Writing CDR");

        Message cdr = createCdr();

        try {
            cdrProvider.writeCDR(cdr);
            usage.incrementCDRwritten(1);
        } catch (WriteTimeoutException e) {
            usage.incrementCDRnotWritten(1);
            ExceptionTraceHelper.traceException(getTracer(), "Timed out writing CDR", e);
            InvokingTrailAccessor.getInvokingTrail().event(SasEvent.TIMED_OUT).varParam(getFeatureName()).report();
        } catch (IOException e) {
            usage.incrementCDRnotWritten(1);
            ExceptionTraceHelper.traceException(getTracer(), "IOException writing CDR", e);
            InvokingTrailAccessor.getInvokingTrail().event(SasEvent.IOEXCEPTION).varParam(getFeatureName()).report();
        }
    }

    protected Message createCdr() {
        final SentinelSipSessionState sessionState = getSessionState();

        final AvpCdrFormat.AvpCdr.Builder cdrBuilder = AvpCdrFormat.AvpCdr.newBuilder();

        final RoProvider roProvider = getRoProviderFactory().getRoProviderVcb0().getRoProviderVcb0();
        final DiameterMessageFactory messageFactory = roProvider.getBaseProvider().getDiameterMessageFactory();

        /*
         * Standard AVPs
         */

        // Subscription-Id
        if (sessionState.getSubscriptionIdType()!= null && sessionState.getSubscriptionId() != null) {
            SubscriptionId subscriptionId = roProvider.getRoMessageFactory().createSubscriptionId(sessionState.getSubscriptionIdType(), sessionState.getSubscriptionId());
            SipAvpCdrUtil.addAvp(cdrBuilder, messageFactory, subscriptionId, getTracer());
        }

        // IMS-Information
        final ImsInformation imsInformation = SipAvpCdrUtil.buildImsInformation(sessionState, false, true, getTracer());

        SipAvpCdrUtil.addAvp(cdrBuilder, messageFactory, imsInformation, getTracer());

        // Service Context Id
        if (sessionState.getDiameterServiceContextId() != null && !sessionState.getDiameterServiceContextId().isEmpty()) {
            try {
                DiameterAvp serviceContextId = messageFactory.createAvp(SERVICE_CONTEXT_AVP.getCode(), sessionState.getDiameterServiceContextId());
                SipAvpCdrUtil.addAvp(cdrBuilder, messageFactory, serviceContextId, getTracer());
            }
            catch (NoSuchAvpException e) {
                ExceptionTraceHelper.traceException(getTracer(), "Unable to create Service-Context-Id AVP", e);
                InvokingTrailAccessor.getInvokingTrail().event(SasEvent.UNABLE_TO_CREATE_AVP).varParam(getFeatureName()).staticParam(SasAvpType.Service_Context_Id).report();
            }
        }

        /*
         * Custom AVPs
         */

        if (sessionState.getPaniMccsMncs() != null && !sessionState.getPaniMccsMncs().isEmpty()) {
            for (MccMnc mccMnc : sessionState.getPaniMccsMncs()) {
                DiameterAvp diameterAvp = SipAvpCdrUtil.createOcMccMncAvp(messageFactory, OC_ACCESS_NETWORK_MCC_MNC_AVP.getCode(), OC_ACCESS_NETWORK_MCC_MNC_AVP.getName(), mccMnc, getTracer());
                if (diameterAvp != null)
                    SipAvpCdrUtil.addAvp(cdrBuilder, messageFactory, diameterAvp, getTracer());
            }
        }

        if (sessionState.getPvniMccMnc() != null) {
            DiameterAvp diameterAvp = SipAvpCdrUtil.createOcMccMncAvp(messageFactory, OC_VISITED_NETWORK_MCC_MNC_AVP.getCode(), OC_VISITED_NETWORK_MCC_MNC_AVP.getName(), getSessionState().getPvniMccMnc(), getTracer());
            if (diameterAvp != null)
                SipAvpCdrUtil.addAvp(cdrBuilder, messageFactory, diameterAvp, getTracer());
        }

        if (sessionState.getImsiMccMnc() != null) {
            DiameterAvp diameterAvp = SipAvpCdrUtil.createOcMccMncAvp(messageFactory, OC_IMSI_MCC_MNC_AVP.getCode(), OC_IMSI_MCC_MNC_AVP.getName(), getSessionState().getImsiMccMnc(), getTracer());
            if (diameterAvp != null)
                SipAvpCdrUtil.addAvp(cdrBuilder, messageFactory, diameterAvp, getTracer());

        }

        // OC-Session-Start-Time
        if (getSessionState().getSessionInitiated() > 0) {
            SipAvpCdrUtil.addCustomAvp(cdrBuilder, messageFactory, OC_SESSION_START_TIME_AVP.getCode(), OC_SESSION_START_TIME_AVP.getName(), new Date(getSessionState().getSessionInitiated()), getTracer());
        }

        // OC-Session-Established-Time
        if (getSessionState().getSessionEstablished() > 0) {
            SipAvpCdrUtil.addCustomAvp(cdrBuilder, messageFactory, OC_SESSION_ESTABLISHED_TIME_AVP.getCode(), OC_SESSION_ESTABLISHED_TIME_AVP.getName(), new Date(getSessionState().getSessionEstablished()), getTracer());
        }

        // OC-Session-End-Time
        final long endTime = getSessionState().getSessionEnded();
        if (endTime == 0) {
            SipAvpCdrUtil.addCustomAvp(cdrBuilder, messageFactory, OC_SESSION_END_TIME_AVP.getCode(), OC_SESSION_END_TIME_AVP.getName(), new Date(System.currentTimeMillis()), getTracer());
        } else {
            SipAvpCdrUtil.addCustomAvp(cdrBuilder, messageFactory, OC_SESSION_END_TIME_AVP.getCode(), OC_SESSION_END_TIME_AVP.getName(), new Date(endTime), getTracer());
        }

        // Selection Key
        final SentinelSelectionKey selectionKey = sessionState.getSentinelSelectionKey();
        SipAvpCdrUtil.addCustomAvp(cdrBuilder, messageFactory, OC_SENTINEL_SELECTION_KEY_AVP.getCode(), OC_SENTINEL_SELECTION_KEY_AVP.getName(), selectionKey.asString(), getTracer());

        // Announcement IDs
        if (sessionState.getPlayedAnnouncementIDs() != null && sessionState.getPlayedAnnouncementIDs().length > 0) {
            for (int id : sessionState.getPlayedAnnouncementIDs()) {
                SipAvpCdrUtil.addCustomAvp(cdrBuilder, messageFactory, OC_PLAY_ANNOUNCEMENT_ID_AVP.getCode(), OC_PLAY_ANNOUNCEMENT_ID_AVP.getName(), id, getTracer());
            }
        }

        // Call Type
        if (sessionState.getCallType() != null) {
            switch (sessionState.getCallType()) {
                case MobileTerminating:
                    SipAvpCdrUtil.addCustomAvp(cdrBuilder, messageFactory, OC_CALL_TYPE_AVP.getCode(), OC_CALL_TYPE_AVP.getName(), SipAvpCdrUtil.CallType.MTC, getTracer());
                    break;
                case MobileOriginating:
                    SipAvpCdrUtil.addCustomAvp(cdrBuilder, messageFactory, OC_CALL_TYPE_AVP.getCode(), OC_CALL_TYPE_AVP.getName(), SipAvpCdrUtil.CallType.MOC, getTracer());
                    break;
                case MobileForwarded:
                    SipAvpCdrUtil.addCustomAvp(cdrBuilder, messageFactory, OC_CALL_TYPE_AVP.getCode(), OC_CALL_TYPE_AVP.getName(), SipAvpCdrUtil.CallType.MFC, getTracer());
                    break;
                case NetworkInitiated:
                    SipAvpCdrUtil.addCustomAvp(cdrBuilder, messageFactory, OC_CALL_TYPE_AVP.getCode(), OC_CALL_TYPE_AVP.getName(), SipAvpCdrUtil.CallType.MOC_3RDPTY, getTracer());
                    break;
                case EmergencyCall:
                    SipAvpCdrUtil.addCustomAvp(cdrBuilder, messageFactory, OC_CALL_TYPE_AVP.getCode(), OC_CALL_TYPE_AVP.getName(), SipAvpCdrUtil.CallType.EMERGENCY_CALL, getTracer());
                    break;
            }
        }

        // Service Type
        if (null != sessionState.getSipServiceType()) {
            switch(sessionState.getSipServiceType()) {
                case SipCall:
                    SipAvpCdrUtil.addCustomAvp(cdrBuilder, messageFactory, OC_SERVICE_TYPE_AVP.getCode(), OC_SERVICE_TYPE_AVP.getName(), SipAvpCdrUtil.SipServiceType.SipCall, getTracer());
                    break;
                case Message:
                    SipAvpCdrUtil.addCustomAvp(cdrBuilder, messageFactory, OC_SERVICE_TYPE_AVP.getCode(), OC_SERVICE_TYPE_AVP.getName(), SipAvpCdrUtil.SipServiceType.Message, getTracer());
                    break;
                case Subscription:
                    SipAvpCdrUtil.addCustomAvp(cdrBuilder, messageFactory, OC_SERVICE_TYPE_AVP.getCode(), OC_SERVICE_TYPE_AVP.getName(), SipAvpCdrUtil.SipServiceType.Subscription, getTracer());
                    break;
                default:
                    SipAvpCdrUtil.addCustomAvp(cdrBuilder, messageFactory, OC_SERVICE_TYPE_AVP.getCode(), OC_SERVICE_TYPE_AVP.getName(), SipAvpCdrUtil.SipServiceType.Unknown, getTracer());
                    break;
            }
        } else {
            SipAvpCdrUtil.addCustomAvp(cdrBuilder, messageFactory, OC_SERVICE_TYPE_AVP.getCode(), OC_SERVICE_TYPE_AVP.getName(), SipAvpCdrUtil.SipServiceType.Unknown, getTracer());
        }

        // Charging result
        // The Result-Code AVP only exists in ACA/CCA messages, so we have to use a custom AVP here
        SipAvpCdrUtil.addCustomAvp(cdrBuilder, messageFactory, OC_CHARGING_RESULT_AVP.getCode(), OC_CHARGING_RESULT_AVP.getName(), sessionState.getChargingResult(), getTracer());

        // Write list of all OCS sessions that have been opened to the
        // OCS in this session to the CDR
        final String[] ocsSessionIds = sessionState.getOcsSessionIds();
        if (ocsSessionIds != null) {
            for (String sessionId : ocsSessionIds) {
                SipAvpCdrUtil.addCustomAvp(cdrBuilder, messageFactory, OC_OCS_SESSION_ID_AVP.getCode(), OC_OCS_SESSION_ID_AVP.getName(), sessionId, getTracer());
            }

            // Write the session ID of the first OCS session as the Session-ID AVP
            if (ocsSessionIds.length > 0) {
                try {
                    String firstSessionId = ocsSessionIds[0];
                    DiameterAvp sessionID = messageFactory.createAvp(SESSION_ID_AVP.getCode(), firstSessionId);
                    SipAvpCdrUtil.addAvp(cdrBuilder, messageFactory, sessionID, getTracer());
                } catch (NoSuchAvpException e) {
                    ExceptionTraceHelper.traceException(getTracer(), "Unable to create Session-Id AVP", e);
                    InvokingTrailAccessor.getInvokingTrail().event(SasEvent.UNABLE_TO_CREATE_AVP).varParam(getFeatureName()).staticParam(SasAvpType.Session_Id).report();
                }
            }
        }

        // OCS session termination cause
        if (sessionState.getOcsSessionTerminationCause() != null && SipAvpCdrUtil.isUsingUnitReservationCharging(getCaller().getChargingManager(), getTracer())) {
            SipAvpCdrUtil.addCustomAvp(cdrBuilder, messageFactory, OC_OCS_SESSION_TERMINATION_CAUSE_AVP.getCode(), OC_OCS_SESSION_TERMINATION_CAUSE_AVP.getName(),
                sessionState.getOcsSessionTerminationCause(), getTracer());
        }


        // Sentinel Error
        if (sessionState.getLatestOcsAnswer() != null
            && OpenCloudDiameter.ccaHasOpenCloudResult(sessionState.getLatestOcsAnswer())) {

            int resultCode = OpenCloudDiameter.getOpenCloudResult(sessionState.getLatestOcsAnswer());

            SentinelError error = SentinelError.fromValue(resultCode);

            if (error == null)
                SipAvpCdrUtil.addCustomAvp(cdrBuilder, messageFactory, OC_SENTINEL_ERROR_AVP.getCode(), OC_SENTINEL_ERROR_AVP.getName(),
                    SipAvpCdrUtil.SentinelErrorAvpValues.OtherError, getTracer());
            else
                switch(error) {
                    case ocsTimeout:
                        SipAvpCdrUtil.addCustomAvp(cdrBuilder, messageFactory, OC_SENTINEL_ERROR_AVP.getCode(), OC_SENTINEL_ERROR_AVP.getName(),
                                SipAvpCdrUtil.SentinelErrorAvpValues.OcsTimeout, getTracer());
                        break;

                    case ocsCommunicationFailure:
                        SipAvpCdrUtil.addCustomAvp(cdrBuilder, messageFactory, OC_SENTINEL_ERROR_AVP.getCode(), OC_SENTINEL_ERROR_AVP.getName(),
                                SipAvpCdrUtil.SentinelErrorAvpValues.OcsCommunicationFailure, getTracer());
                        break;

                    case sentinelOverload:
                        SipAvpCdrUtil.addCustomAvp(cdrBuilder, messageFactory, OC_SENTINEL_ERROR_AVP.getCode(), OC_SENTINEL_ERROR_AVP.getName(),
                                SipAvpCdrUtil.SentinelErrorAvpValues.SentinelOverload, getTracer());
                        break;

                    case protocolError:
                        SipAvpCdrUtil.addCustomAvp(cdrBuilder, messageFactory, OC_SENTINEL_ERROR_AVP.getCode(), OC_SENTINEL_ERROR_AVP.getName(),
                                SipAvpCdrUtil.SentinelErrorAvpValues.ProtocolError, getTracer());
                        break;

                    case internalError:
                        SipAvpCdrUtil.addCustomAvp(cdrBuilder, messageFactory, OC_SENTINEL_ERROR_AVP.getCode(), OC_SENTINEL_ERROR_AVP.getName(),
                                SipAvpCdrUtil.SentinelErrorAvpValues.InternalError, getTracer());
                        break;

                    case mappingError:
                        SipAvpCdrUtil.addCustomAvp(cdrBuilder, messageFactory, OC_SENTINEL_ERROR_AVP.getCode(), OC_SENTINEL_ERROR_AVP.getName(),
                                SipAvpCdrUtil.SentinelErrorAvpValues.MappingError, getTracer());
                        break;

                    default:
                        SipAvpCdrUtil.addCustomAvp(cdrBuilder, messageFactory, OC_SENTINEL_ERROR_AVP.getCode(), OC_SENTINEL_ERROR_AVP.getName(),
                                SipAvpCdrUtil.SentinelErrorAvpValues.OtherError, getTracer());
                        break;
                }
        } else {
            SipAvpCdrUtil.addCustomAvp(cdrBuilder, messageFactory, OC_SENTINEL_ERROR_AVP.getCode(), OC_SENTINEL_ERROR_AVP.getName(), SipAvpCdrUtil.SentinelErrorAvpValues.None, getTracer());
        }

        // MultipleServicesCreditControl AVPs from final OCS CCA
        if (sessionState.getLatestOcsAnswer() != null) {
            final MultipleServicesCreditControl[] msccs =
                sessionState.getLatestOcsAnswer().getMultipleServicesCreditControls();
            if (msccs != null) {
                for (MultipleServicesCreditControl mscc : msccs) {
                    SipAvpCdrUtil.addAvp(cdrBuilder, messageFactory, mscc, getTracer());
                }
            }
        }

        // Charging instances
        final Collection<ChargingInstance> chargingInstances =
            getCaller().getChargingManager().getChargingInstances();

        if (chargingInstances != null)
            for (ChargingInstance chargingInstance : chargingInstances)
                SipAvpCdrUtil.addChargingInstance(cdrBuilder, messageFactory, chargingInstance, getTracer());

        // Event Id
        if (sessionState.getEventId() != null)
            SipAvpCdrUtil.addCustomAvp(cdrBuilder, messageFactory, OC_EVENT_ID_AVP.getCode(), OC_EVENT_ID_AVP.getName(), sessionState.getEventId(), getTracer());

        // Call Id
        if (sessionState.getCallId() != null)
            SipAvpCdrUtil.addCustomAvp(cdrBuilder, messageFactory, OC_CALL_ID_AVP.getCode(), OC_CALL_ID_AVP.getName(), sessionState.getCallId(), getTracer());

        // End Session Cause
        if (sessionState.getEndSessionCause()!=null)
            SipAvpCdrUtil.addCustomAvp(cdrBuilder, messageFactory, OC_END_SESSION_CAUSE_AVP.getCode(), OC_END_SESSION_CAUSE_AVP.getName(), sessionState.getEndSessionCause(), getTracer());


        if (sessionState.getUserEquipmentInfo() != null)
            SipAvpCdrUtil.addAvp(cdrBuilder,messageFactory, sessionState.getUserEquipmentInfo(), getTracer());
        
        addVpnCdrs(cdrBuilder, messageFactory);

        return cdrBuilder.build();
    }

    private void addVpnCdrs(final AvpCdrFormat.AvpCdr.Builder cdrBuilder,
            final DiameterMessageFactory messageFactory) {
        if (getSessionState().getVPNId() != null)
            SipAvpCdrUtil.addCustomAvp(cdrBuilder, messageFactory, 10, "VPN",
                    getSessionState().getVPNId(), getTracer());
        if (getSessionState().getShortCode() != null)
            SipAvpCdrUtil.addCustomAvp(cdrBuilder, messageFactory, 11, "Short-Code",
                    getSessionState().getShortCode(), getTracer());
    }
    
    @Override
    public String getFeatureName() {
        return null;
    }

    protected RoProviderFactory getRoProviderFactory() {
        return roProviderFactory;
    }

    @Override
    public void injectFeatureStats(CDRStats usage) {
        this.usage = usage;
    }

    @Override
    public void injectResourceAdaptorProvider(Object provider) {
        if (provider instanceof RoProviderFactory)
            this.roProviderFactory = (RoProviderFactory) provider;
        else if (provider instanceof CDRProvider)
            this.cdrProvider = (CDRProvider) provider;
        else {
            if (getFacilities().getTracer().isFineEnabled()) getFacilities().getTracer().fine("Warning! - Unexpected provider type injected!");
            if (getFacilities().getTracer().isFinerEnabled()) getFacilities().getTracer().finer("     Provider: " + provider);
        }
    }

    private CDRProvider cdrProvider;
    private RoProviderFactory roProviderFactory;
    private CDRStats usage;

}
