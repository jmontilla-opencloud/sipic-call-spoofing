/**
 * Copyright (c) 2014 Open Cloud Limited, a company incorporated in England and Wales (Registration Number 6000941) with its principal place of business at Edinburgh House, St John's Innovation Park, Cowley Road, Cambridge CB4 0DS.
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1  Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2  Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3  The name of the author may not be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * 4  The source code may not be used to create, develop, use or distribute software for use on any platform other than the Open Cloud Rhino and Open Cloud Rhino Sentinel platforms or any successor products.
 *
 * 5  Full license terms may be found https://developer.opencloud.com/devportal/display/OCDEV/Feature+Source+License
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE ARE EXCLUDED TO THE FULLEST EXTENT PERMITTED BY LAW.
 *
 * TO THE FULLEST EXTENT PERMISSIBLE BUY LAW, THE AUTHOR SHALL NOT BE LIABLE FOR ANY LOSS OF REVENUE, LOSS OF PROFIT, LOSS OF FUTURE BUSINESS, LOSS OF DATA OR ANY INDIRECT, SPECIAL, CONSEQUENTIAL, PUNITIVE OR OTHER LOSS OR DAMAGES ARISING OUT OF OR IN CONNECTION WITH THE SOFTWARE, WHETHER ARISING IN CONTRACT, TORT (INCLUDING NEGLIGENCE) MISREPRESENTATION OR OTHERWISE AND REGARDLESS OF WHETHER OPEN CLOUD HAS BEEN ADVISED OF THE POSSIBILITY OF ANY SUCH LOSS OR DAMAGE. THE AUTHORS MAXIMUM AGGREGATE LIABILITY WHETHER IN CONTRACT, TORT (INCLUDING NEGLIGENCE) OR OTHERWISE, SHALL NOT EXCEED EUR100.
 *
 * NOTHING IN THIS LICENSE SHALL LIMIT THE LIABILITY OF THE AUTHOR FOR DEATH OR PERSONAL INJURY RESULTING FROM NEGLIGENCE, FRAUD OR FRAUDULENT MISREPRESENTATION.
 *
 * Visit Open Cloud Developer's Portal for how-to guides, examples, documentation, forums and more: http://developer.opencloud.com
 */
package com.opencloud.sentinel.training.vpn.cdrfeature;

import com.opencloud.sce.fsmtool.Facilities;
import com.opencloud.sentinel.annotations.SentinelFeature;
import com.opencloud.sentinel.common.SentinelSipSessionState;
import com.opencloud.sentinel.feature.ExecutionPhase;
import com.opencloud.sentinel.feature.SipFeatureScriptExecutionPoint;
import com.opencloud.sentinel.feature.spi.SentinelSipMultiLegFeatureEndpoint;
import com.opencloud.sentinel.training.vpn.session.SIPTrainingVPNSessionState;
import com.opencloud.slee.annotation.BinderTargets;
import com.opencloud.slee.annotation.SBBPartReference;
import com.opencloud.slee.annotation.SBBPartReferences;

import javax.slee.ActivityContextInterface;
import javax.slee.annotation.ComponentId;
import javax.slee.annotation.LibraryReference;
import javax.slee.annotation.LibraryReferences;


@SentinelFeature(featureName = SipAvpCdrFeature.NAME,
                 featureGroup = SentinelFeature.SIP_FEATURE_GROUP,
                 componentName = "@component.name@",
                 featureVendor = "@component.vendor@",
                 featureVersion = "@component.version@",
                 raProviderJndiNames = {
                     "slee/resources/cdr/provider",
                     "slee/resources/diameterro/sentinel/provider"
                 },
                 executionPhases = { ExecutionPhase.SipSessionPhase },
                 usageStatistics = CDRStats.class
)
@SBBPartReferences(
    sbbPartRefs = {
        @SBBPartReference(
            id = @ComponentId(name    = "@sentinel-sip-spi.SentinelSipFeatureSPI SBB Part.name@",
                              vendor  = "@sentinel-sip-spi.SentinelSipFeatureSPI SBB Part.vendor@",
                              version = "@sentinel-sip-spi.SentinelSipFeatureSPI SBB Part.version@")
        ),
        @SBBPartReference(
            id = @ComponentId(name    = "@sentinel-sip-avp-cdr-util.name@",
                              vendor  = "@sentinel-sip-avp-cdr-util.vendor@",
                              version = "@sentinel-sip-avp-cdr-util.version@"))
    }
)
@LibraryReferences(
    libraryRefs = {
        @LibraryReference(
            library = @ComponentId(name    = "@sentinel-sip-spi.SentinelSipFeatureSPI.name@",
                                   vendor  = "@sentinel-sip-spi.SentinelSipFeatureSPI.vendor@",
                                   version = "@sentinel-sip-spi.SentinelSipFeatureSPI.version@")),
        @LibraryReference(
            library = @ComponentId(name    = "@avp-cdr-format.name@",
                                   vendor  = "@avp-cdr-format.vendor@",
                                   version = "@avp-cdr-format.version@")),
        @LibraryReference(library = @ComponentId(
                name = "@sip-training-vpn-session-state-library.name@", 
                vendor = "@sip-training-vpn-session-state-library.vendor@", 
                version = "@sip-training-vpn-session-state-library.version@"))        
    }
)
@BinderTargets(services = "sip")
public class SipAvpCdrFeature extends BaseSipAvpCdrFeature {

    public SipAvpCdrFeature(SentinelSipMultiLegFeatureEndpoint caller, Facilities facilities, SIPTrainingVPNSessionState  sessionState) {
        super(caller, facilities, sessionState);
    }

    @Override
    public void startFeature(Object trigger, Object activity, ActivityContextInterface aci) {
        if (getTracer().isFineEnabled()) getTracer().fine(NAME + " started");

        if (getSessionState().getCurrentSipFeatureExecutionPoint().equals(SipFeatureScriptExecutionPoint.SipEndSession))
            super.writeCdr();
        else if (getTracer().isFineEnabled())
            getTracer().fine("Feature called at unexpected execution point; aborting");

        getCaller().featureHasFinished();
        if (getTracer().isFineEnabled()) getTracer().fine(NAME + " has finished");
    }

    public static final String NAME = "SIPTrainingAvpCdr";

    @Override
    public String getFeatureName() {
        return NAME;
    }

}
