package com.opencloud.sentinel.training.vpn.feature;

import com.opencloud.sentinel.feature.spi.SentinelFeatureStats;

public interface SIPTrainingVPNUsageStats extends SentinelFeatureStats {

    /**
     * Increments the VPN Calls counter
     */
    void incrementVPNCalls(long l);    
    /**
     * Increments the Non-VPN Calls counter
     */
    void incrementNonVPNCalls(long l);
    
}
