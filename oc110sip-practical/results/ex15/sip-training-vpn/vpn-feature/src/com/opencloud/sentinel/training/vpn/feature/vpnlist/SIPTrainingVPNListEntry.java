package com.opencloud.sentinel.training.vpn.feature.vpnlist;


import com.opencloud.sentinel.feature.common.lists.AddressListEntry;

public interface SIPTrainingVPNListEntry extends AddressListEntry {
    public static final String SCHEMA_NAME = "SIPTrainingVPNList";

    boolean getIsWhiteListed();
}
