package com.opencloud.sentinel.training.vpn.feature;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.slee.ActivityContextInterface;
import javax.slee.annotation.ComponentId;
import javax.slee.annotation.LibraryReference;
import javax.slee.annotation.LibraryReferences;

import org.jainslee.resources.sip.SipRequest;
import org.jainslee.resources.sip.SipURI;
import org.jainslee.resources.sip.TelURL;
import org.jainslee.resources.sip.URI;

import com.opencloud.sce.fsmtool.Facilities;
import com.opencloud.sentinel.annotations.SentinelFeature;
import com.opencloud.sentinel.common.CallType;
import com.opencloud.sentinel.common.SentinelSelectionKey;
import com.opencloud.sentinel.feature.ExecutionPhase;
import com.opencloud.sentinel.feature.impl.BaseFeature;
import com.opencloud.sentinel.feature.spi.FeatureError;
import com.opencloud.sentinel.feature.spi.SentinelSipMultiLegFeatureEndpoint;
import com.opencloud.sentinel.feature.spi.init.InjectResourceAdaptorProvider;
import com.opencloud.sentinel.training.vpn.session.SIPTrainingVPNSessionState;
import com.opencloud.slee.annotation.SBBPartReference;
import com.opencloud.slee.annotation.SBBPartReferences;
import com.opencloud.slee.resources.dbquery.DatabaseFutureResult;
import com.opencloud.slee.resources.dbquery.DatabaseQueryProvider;
import com.opencloud.slee.resources.dbquery.QueryInfo;


@SentinelFeature(
    featureName = SIPTrainingDetermineVPNPlanIdFeature.NAME,
    componentName = "@component.name@",
    featureVendor = "@component.vendor@",
    featureVersion = "@component.version@",
    featureGroup = SentinelFeature.SIP_FEATURE_GROUP,
    executionPhases = {ExecutionPhase.SipSessionPhase},
    raProviderJndiNames = {"slee/resources/dbquery/provider"}   
)
@SBBPartReferences(
    sbbPartRefs = {
            @SBBPartReference(id = @ComponentId(name = "@sentinel-sip-spi.SentinelSipFeatureSPI SBB Part.name@", vendor = "@sentinel-sip-spi.SentinelSipFeatureSPI SBB Part.vendor@", version = "@sentinel-sip-spi.SentinelSipFeatureSPI SBB Part.version@"))
    }
)
@LibraryReferences(
		   libraryRefs = {
		          @LibraryReference(library = @ComponentId(
		               name = "@sip-training-vpn-session-state-library.name@", 
		               vendor = "@sip-training-vpn-session-state-library.vendor@", 
		               version = "@sip-training-vpn-session-state-library.version@")),
		   }
		)

public class SIPTrainingDetermineVPNPlanIdFeature
        extends BaseFeature<SIPTrainingVPNSessionState, SentinelSipMultiLegFeatureEndpoint> 
		implements InjectResourceAdaptorProvider {

    /** Feature name */
    public static final String NAME = "SIPTrainingDetermineVPNPlanIdFeature";

    public SIPTrainingDetermineVPNPlanIdFeature(SentinelSipMultiLegFeatureEndpoint caller,
            Facilities facilities, SIPTrainingVPNSessionState sessionState) {
        super(caller, facilities, sessionState);
    }

    private DatabaseQueryProvider dbQueryProvider;
    @Override
    public void injectResourceAdaptorProvider(Object provider) {
        if (provider instanceof DatabaseQueryProvider) {
            this.dbQueryProvider = (DatabaseQueryProvider) provider;
        } else {
            if (provider != null && getTracer().isFineEnabled()) {
                getTracer().fine("Unknown provider " + provider);
            }
        }
    }    
    /**
     * All features must have a unique name.
     *
     * @return the name of this feature
     */
    @Override
    public String getFeatureName() { return NAME; }

    /**
     * Kick off the feature.
     *
     * @param trigger  a triggering context. The feature implementation must be able to cast this to a useful type for it to run
     * @param activity the slee activity object this feature is related to (may be null)
     * @param aci      the activity context interface of the slee activity this feature is related to
     */
    @Override
    public void startFeature(Object trigger, Object activity, ActivityContextInterface aci) {

        try {
            if (getTracer().isFineEnabled()) getTracer().fine("SIPTrainingDetermineVPNPlanFeature feature starting");

            SentinelSelectionKey selectionKey = getSessionState().getSentinelSelectionKey();
            if (selectionKey == null) {
                if (getTracer().isFineEnabled())
                    getTracer().fine("Error! - Unable to set session PlanID, Reason: Could not access Sentinel selection key");
                getCaller().featureFailedToExecute(new FeatureError(FeatureError.Cause.invalidSessionState, "Could not access Sentinel selection key"));
                return;
            }

            SipRequest sipRequest = getTriggeringRequest(trigger);
            if(sipRequest == null) {
                if (getTracer().isFineEnabled())
                    getTracer().fine("Error! - Unable to set session PlanID, Reason: Trigger was not an initial INVITE, MESSAGE or SUBSCRIBE");
                getCaller().featureFailedToExecute(new FeatureError(FeatureError.Cause.unsupportedTriggerEvent, "Trigger was not an initial INVITE, MESSAGE or SUBSCRIBE"));
                return;
            }

            String planId = selectionKey.getPlanId();
            if(planId.isEmpty()) {
                String vpnId = getVPNCall(sipRequest);
                if (vpnId != null) { 
                    selectionKey.setPlanId("vpn");
                    if (getTracer().isFinerEnabled()) getTracer().finer("Setting Sentinel selection key to: " + selectionKey);
                    getSessionState().setSentinelSelectionKey(selectionKey);            
                    String shortCode = getDialledDigits(sipRequest);            
                    getSessionState().setVPNId(vpnId);            
                    getSessionState().setShortCode(shortCode);
                    if (getTracer().isFinerEnabled()) getTracer().finer("Session State fields stored: VPNId="+vpnId+", ShortCode="+shortCode);      
                }else {
                    if (getTracer().isFinerEnabled()) getTracer().finer("Not a VPN call");
                }            	
            }
            else if (getTracer().isFineEnabled()) getTracer().fine("PlanID already set, selection key will not be changed");
        }
        finally {
            if (getTracer().isFineEnabled()) getTracer().fine(getFeatureName() + " has finished");
            getCaller().featureHasFinished();
        }

    }
    
     private SipRequest getTriggeringRequest(Object trigger) {
        if(!(trigger instanceof SipRequest)) {
            if(getTracer().isFineEnabled()) getTracer().fine("Trigger is not a SIP Request");
            return null;
        }

        SipRequest request = (SipRequest) trigger;
        if(!request.isInitial()) {
            if(getTracer().isFineEnabled()) getTracer().fine("Trigger is not an initial SIP Request");
            return null;
        }

        if(!SipRequest.INVITE.equals(request.getMethod())) {
            if(getTracer().isFineEnabled()) getTracer().fine("Trigger is not a INVITE request");
            return null;
        }
        
        return request;
    }
    
     private boolean isVPNCall(SipRequest sipRequest){
         return getVPNCall(sipRequest) != null;
     }
     private String getVPNCall(SipRequest sipRequest) {
         final String msisdn = getMSISDN(sipRequest);
         final String vpnId = getVPNId(msisdn);
         if (getTracer().isFineEnabled())
             getTracer().fine("getVPNCall("+ msisdn +") = " + vpnId);            
         return getSessionState().getCallType().equals(CallType.MobileOriginating)? vpnId:null;
     }    
     private String getMSISDN(SipRequest request) {
         String aParty = null;
         if (request == null || request.getFrom() == null || request.getFrom().getURI() == null) {
             return null;
         }
         URI fromURI = request.getFrom().getURI();
         if (fromURI.isSipURI()) {
             aParty = ((SipURI) fromURI).getUser();
         } else {
             return ((TelURL) fromURI).getPhoneNumber();
         }
         return aParty;
     }    
     private boolean isShortNumber(SipRequest request) {
         String dialledNumer = null;
         if (request == null || request.getTo() == null || request.getTo().getURI() == null) {
             return false;
         }
         URI toURI = request.getTo().getURI();
         if (toURI.isSipURI()) {
             dialledNumer = ((SipURI) toURI).getUser();
         } else {
             dialledNumer = ((TelURL) toURI).getPhoneNumber();
         }
         return dialledNumer.length() < 6;
     }
     private String getDialledDigits(SipRequest request) {
         if (request == null || request.getRequestURI() == null || request.getRequestURI() == null) {
             return null;
         }
         URI uri = request.getRequestURI();
         if (uri.isSipURI()) {
             return ((SipURI) uri).getUser();
         } else {
             return ((TelURL) uri).getPhoneNumber();
         }
     }
     private static final int TIMEOUT = 1000;
     private static final String SQL_GET_VPN = "SELECT VPNId FROM VPNMembers WHERE msisdn = ?";
     private String getVPNId(final String msisdn) {
         try {
             final DatabaseFutureResult futureResult = dbQueryProvider.sendQuery(new QueryInfo() {
                 public String getSql() {    return SQL_GET_VPN; }
                 public void setParameters(PreparedStatement ps) throws SQLException {
                     ps.setString(1, msisdn);
                 }
                 public ExecuteType getExecuteType() {
                     return ExecuteType.QUERY;
                 }
                 public RetryBehaviour getRetryBehaviour() {
                     return RetryBehaviour.TRY_FIRST_AVAILABLE;
                 }
                 public StatementType getStatementType() {
                     return StatementType.PREPARED;
                 }
             });
             final ResultSet rs = futureResult.getResultSet(TIMEOUT);
             String result = rs.next() ? rs.getString(1) : null;
             rs.close();
             return result;
         } catch (Exception e) {
             if (getTracer().isFineEnabled())
                 getTracer().fine("Error retrieving VPN Id from database.", e);
             return null;
         }
     }
}
