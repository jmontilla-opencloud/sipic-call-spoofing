== Module Structure on Disk

The module uses Apache Ant (http://ant.apache.org) as the build system, and Apache Ivy (http://ant.apache.org/ivy) as the module and dependency system.


Each module has a build.xml file, ivy.xml, module.properties file in the modules root directory.
If a module contains source code to be built, this is located in a 'src' subdirectory.
Documentation for a module is located in the 'docs' subdirectory.
Unit tests are located in the 'test' subdirectory.

A module may contain another module, within a directory tree structure. There is no restriction on the depth of the module directory structure, unless imposed by an Operating System.
The capability of nesting modules within a module directory structure is provided to enable a developer to provide a logical grouping of related modules.
For example, if there were several features related to playing announcements, then they might be grouped together under a 'play-announcement' directory.
This grouping has no effect on the structure of a module, nor how it publishes or retrieves artifacts.
Directories that are not module directories yet contain module directories within them are also allowed within this directory structure.

Directory names can include any valid character according to the host Operating System. However for simplicity it is recommended to avoid:

* upper case characters
* white space

Instead of using a 'space' character it is recommend to use a 'hypen' character ('-') instead. As an example, instead of using the string "Example Module" as a directory and module name, use "example-module".

=== Examples

==== An empty module

This module is the empty case. It contains the following files:

----
empty-module/build.xml
empty-module/ivy.xml
empty-module/module.properties
----


It's +build.xml+ file contains targets that do nothing.
It's +ivy.xml+ file publishes nothing, and depends on nothing.

It's module.properties file includes:

* SLEE component ID values to populate when publishing a component into the slee-component Ivy configuration
** by default the slee component's name is the same as the Ivy module name
** by default the slee component's version is the same as the Ivy module's publish version
** by default the slee component's vendor is the same as the Ivy module's organization
* files that should be included when the module is bundled as an example
* files that should be excluded when the module is bundled as an example

An example of such a +module.properties+ file is below

----
include::{includedir}/../module.properties[]
----



==== A module with source

This module contains some source, some unit tests, and documentation.

As a directory listing, it looks like the following

----
module-dir/build.xml
module-dir/ivy.xml
module-dir/module.properties
module-dir/src
module-dir/docs
module-dir/test
----

It's +build.xml+ file contains targets to

* compile
* package components
* run unit tests
* package documentation 

TBD: what are the target names?

It's +ivy.xml+ file:

* depends on other modules needed for compilation
* publishes artifacts to various Ivy configurations, including
** the 'docs' configuration, for documentation
** the 'api' configuration, so that other modules can rely on its APIs
** the 'slee-component' configuration, so that it's components can be deployed into the SLEE 

An example of such an +ivy.xml+ file is as follows
[source,xml]
----
include::{includedir}/../ivy.xml[]
----


==== A module that contains other modules

The module 'module1' contains two modules - module2 and module3.
The module 'module3' contains another module - module4.

----
module1/build.xml
module1/ivy.xml
module1/module.properties
module1/src
module1/docs
module1/test
module1/module2
module1/module2/build.xml
module1/module2/ivy.xml
module1/module2/module.properties
module1/module2/src
module1/module2/docs
module1/module2/test
module1/module3/build.xml
module1/module3/ivy.xml
module1/module3/module.properties
module1/module3/src
module1/module3/docs
module1/module3/test
module1/module3/module4/build.xml
module1/module3/module4/ivy.xml
module1/module3/module4/module.properties
module1/module3/module4/src
module1/module3/module4/docs
module1/module3/module4/test
----

This structure may be repeated to any depth supported by filename length limits in the Operating System.

It should be noted that the two modules are in fact independent - until the Ivy configuration specifies dependencies.
The directory structure is provided in order for convenience and logical grouping.

So a nested module structure as shown above is equivalent to the modules all being present in a flat directory structure.

----
module1/build.xml
module1/ivy.xml
module1/module.properties
module1/src
module1/docs
module1/test
module2/build.xml
module2/ivy.xml
module2/module.properties
module2/src
module2/docs
module2/test
module3/build.xml
module3/ivy.xml
module3/module.properties
module3/src
module3/docs
module3/test
module4/build.xml
module4/ivy.xml
module4/module.properties
module4/src
module4/docs
module4/test
----

==== A module contained within a non-module directory

This example shows two modules, module-a and module-b in a directory structure.
The module module-b is within the directory structure underneath module-a. This directory structure contains a directory that is not a module directory.

----
module-a/build.xml
module-a/ivy.xml
module-a/module.properties
module-a/src
module-a/docs
module-a/test
module-a/other/README
module-a/other/module-b
module-a/other/module-b/build.xml
module-a/other/module-b/ivy.xml
module-a/other/module-b/module.properties
module-a/other/module-b/src
module-a/other/module-b/docs
module-a/other/module-b/test
---- 
