/**
 * Copyright (c) 2014 Open Cloud Limited, a company incorporated in England and Wales (Registration Number 6000941) with its principal place of business at Edinburgh House, St John's Innovation Park, Cowley Road, Cambridge CB4 0DS.
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1  Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2  Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3  The name of the author may not be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * 4  The source code may not be used to create, develop, use or distribute software for use on any platform other than the Open Cloud Rhino and Open Cloud Rhino Sentinel platforms or any successor products.
 *
 * 5  Full license terms may be found https://developer.opencloud.com/devportal/display/OCDEV/Feature+Source+License
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE ARE EXCLUDED TO THE FULLEST EXTENT PERMITTED BY LAW.
 *
 * TO THE FULLEST EXTENT PERMISSIBLE BUY LAW, THE AUTHOR SHALL NOT BE LIABLE FOR ANY LOSS OF REVENUE, LOSS OF PROFIT, LOSS OF FUTURE BUSINESS, LOSS OF DATA OR ANY INDIRECT, SPECIAL, CONSEQUENTIAL, PUNITIVE OR OTHER LOSS OR DAMAGES ARISING OUT OF OR IN CONNECTION WITH THE SOFTWARE, WHETHER ARISING IN CONTRACT, TORT (INCLUDING NEGLIGENCE) MISREPRESENTATION OR OTHERWISE AND REGARDLESS OF WHETHER OPEN CLOUD HAS BEEN ADVISED OF THE POSSIBILITY OF ANY SUCH LOSS OR DAMAGE. THE AUTHORS MAXIMUM AGGREGATE LIABILITY WHETHER IN CONTRACT, TORT (INCLUDING NEGLIGENCE) OR OTHERWISE, SHALL NOT EXCEED EUR100.
 *
 * NOTHING IN THIS LICENSE SHALL LIMIT THE LIABILITY OF THE AUTHOR FOR DEATH OR PERSONAL INJURY RESULTING FROM NEGLIGENCE, FRAUD OR FRAUDULENT MISREPRESENTATION.
 *
 * Visit Open Cloud Developer's Portal for how-to guides, examples, documentation, forums and more: http://developer.opencloud.com
 */
package com.opencloud.sentinel.train.example.feature;

import com.opencloud.rhino.facilities.sas.InvokingTrailAccessor;
import com.opencloud.rhino.facilities.sas.Trail;
import com.opencloud.sce.fsmtool.Facilities;
import com.opencloud.sentinel.annotations.ConfigurationReader;
import com.opencloud.sentinel.annotations.FeatureProvisioning;
import com.opencloud.sentinel.annotations.ProvisioningConfig;
import com.opencloud.sentinel.annotations.ProvisioningField;
import com.opencloud.sentinel.annotations.ProvisioningProfile;
import com.opencloud.sentinel.annotations.ProvisioningProfileId;
import com.opencloud.sentinel.annotations.SentinelFeature;
import com.opencloud.sentinel.common.NullSentinelSessionState;
import com.opencloud.sentinel.feature.ExecutionPhase;
import com.opencloud.sentinel.feature.impl.BaseFeature;
import com.opencloud.sentinel.feature.spi.FeatureEndpoint;
import com.opencloud.sentinel.feature.spi.init.InjectFeatureConfigurationReader;
import com.opencloud.sentinel.feature.spi.init.InjectFeatureStats;
import com.opencloud.slee.annotation.BinderTargets;
import com.opencloud.slee.annotation.SBBPartReference;
import com.opencloud.slee.annotation.SBBPartReferences;

import javax.slee.ActivityContextInterface;
import javax.slee.annotation.ComponentId;
import javax.slee.annotation.ProfileReference;
import javax.slee.annotation.ProfileReferences;
import javax.slee.facilities.Tracer;

/**
 *
 * An example feature.
 */
@SentinelFeature(
    featureName = ExamplePojoFeature.NAME,
    componentName = "@component.name@",
    featureVendor = "@component.vendor@",
    featureVersion = "@component.version@",
    featureGroup = SentinelFeature.CORE_FEATURE_GROUP,
    configurationReader = @ConfigurationReader(
        readerInterface = ExampleConfigReader.class,
        readerClass = ExampleConfigProfileReader.class
    ),
    usageStatistics = ExampleUsageStats.class,
    executionPhases = ExecutionPhase.SipSessionPhase,
    provisioning = @FeatureProvisioning(
        displayName = "SIP POJO Feature",
        configs = {
            @ProvisioningConfig(
                type = "SipPojoFeatureConfig",
                displayName = "Config",
                fields = {
                    @ProvisioningField(
                        name = "aValue",
                        displayName = "A value",
                        type = "int",
                        description = "An example value to set."
                    )
                },
                profile = @ProvisioningProfile(
                    tableName = "ExampleConfigProfileTable",
                    specification = @ProvisioningProfileId(name = "@train-sip-example-profile.name@", vendor = "@train-sip-example-profile.vendor@", version = "@train-sip-example-profile.version@")
                )
            )
        }
    )
)
@SBBPartReferences(
    sbbPartRefs = {
        @SBBPartReference(id = @ComponentId(name = "@sentinel-sip-spi.SentinelSipFeatureSPI SBB Part.name@", vendor = "@sentinel-sip-spi.SentinelSipFeatureSPI SBB Part.vendor@", version = "@sentinel-sip-spi.SentinelSipFeatureSPI SBB Part.version@")),
        @SBBPartReference(id = @ComponentId(name = "@train-sip-example-mapper.name@", vendor = "@train-sip-example-mapper.vendor@", version = "@train-sip-example-mapper.version@"))
    }
)
@ProfileReferences(
    profileRefs = {
        @ProfileReference(profile = @ComponentId(name="@train-sip-example-profile.name@", vendor="@train-sip-example-profile.vendor@", version="@train-sip-example-profile.version@"))
    }
)

@BinderTargets(services = "sip")
@SuppressWarnings("unused")
public class ExamplePojoFeature extends BaseFeature<NullSentinelSessionState, FeatureEndpoint> 
implements InjectFeatureConfigurationReader<ExampleConfigReader>, InjectFeatureStats<ExampleUsageStats> {

    public ExamplePojoFeature(FeatureEndpoint caller, Facilities facilities, NullSentinelSessionState sessionState) {
        super(caller, facilities, sessionState);
    }

    public static final String NAME = "SipTrainingPojoFeature";
    @SuppressWarnings("FieldCanBeLocal")
    private ExampleConfigReader configReader;
    private ExampleUsageStats featureStats;

    /**
     * All features must have a unique name.
     *
     * @return the name of this feature
     */
    @Override
    public String getFeatureName() { return NAME; }

    /**
     * Kick off the feature.
     *
     * @param trigger  a triggering context. The feature implementation must be able to cast this to a useful type for it to run
     * @param activity the slee activity object this feature is related to (may be null)
     * @param aci      the activity context interface of the slee activity this feature is related to
     */
    @Override
    public void startFeature(Object trigger, Object activity, ActivityContextInterface aci) {

        Tracer tracer = getTracer();
        if (tracer.isInfoEnabled()) {
             tracer.info("Starting " + NAME);
        }

        // Report a SAS event
        Trail sasTrail = InvokingTrailAccessor.getInvokingTrail();
        sasTrail.event(SasEvent.EXAMPLE_POJO_SAS_TRACE).staticParam(0).varParam(trigger).varParam(activity).report();
        
        if (tracer.isFineEnabled()) {
            tracer.fine("Hello World Again!");
  }
        getCaller().featureHasFinished();

        featureStats.incrementFeatureStarted(1);
    }
    
    public void injectFeatureConfigurationReader(ExampleConfigReader configurationReader) {
        this.configReader = configurationReader;
    }

    /**
     * Implement {@link InjectFeatureStats#injectFeatureStats}
     */
    @Override
    public void injectFeatureStats(ExampleUsageStats featureStats) {
        this.featureStats = featureStats;
    }

}

