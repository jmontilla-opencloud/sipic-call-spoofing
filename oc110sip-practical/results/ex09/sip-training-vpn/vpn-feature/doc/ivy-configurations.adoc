== Ivy Configurations

The following Configurations establish the convention for OpenCloud oriented Ivy Modules

=== slee-component

This configuration contains zero or 1 slee-components jar files. An example is an SBB jar file, or a Profile jar file.

In order to produce multiple slee-components, it is recommended that multiple modules are created, with Ivy dependencies as appropriate.

=== doc

This configuration contains zero or more documentation artifacts. These are typically javadoc zip files, PDF files, html files, and so-on.

=== template

This configuration contains a module - when it is used as a template for the creation of another module.
An example of this process are "example modules". Example modules are modules that publish themselves into the template configuration.
In this way they may be renamed as part of creating a new module based on another module.

=== config

TBD

=== self

This is a private configuration, retrieval of other artifacts is written into self.





