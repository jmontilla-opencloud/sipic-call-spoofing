-- DROP TABLE Zones;
CREATE table Zones (
  ZoneName varchar2(50) NOT NULL PRIMARY KEY,
  LocationTypes varchar2(50),
  MCC varchar2(50),
  MNC varchar2(50),
  LAC varchar2(50),
  CIOrSAC varchar2(50),
  LocationDescriptions varchar2(100)
);

