-- Table: promotionsBuckets

DROP TABLE IF EXISTS promotionsBuckets;
CREATE TABLE promotionsBuckets
(
  subscriberId character varying(50) NOT NULL,
  bucketName character varying(50) NOT NULL,
  availableUnits bigint,
  reservedUnits bigint,
  CONSTRAINT promotionsBuckets_pkey PRIMARY KEY (subscriberId, bucketName)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE promotionsBuckets OWNER TO sentinel;

COMMENT ON TABLE promotionsBuckets IS 'Table for promotion buckets for all subscribers.';

-- DROP INDEX promotionBuckets_idx;
-- CREATE INDEX CONCURRENTLY promotionBuckets_idx ON promotionBuckets_idx (subscriberId, bucketName);

