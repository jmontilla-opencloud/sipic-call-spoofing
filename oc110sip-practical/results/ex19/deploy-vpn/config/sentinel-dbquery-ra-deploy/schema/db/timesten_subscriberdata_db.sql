-- DROP TABLE OpenNet_SubscriberImsPublicUserIdentity;
-- DROP TABLE OpenNet_Subscribers;

CREATE TABLE OpenNet_Subscribers (
  MSISDN varchar2(50) NOT NULL PRIMARY KEY,
  IMSI varchar2(50),
  subscriberlanguage varchar2(50),
  Account varchar2(50),
  SubscriptionType varchar2(50),
  CreditWorthy number(1),
  ValidityStart date,
  ValidityEnd date,
  HomeZoneEnabled number(1),
  HomeZoneList varchar2(3000),
  ocsId varchar2(50),
  FriendsAndFamilyEnabled char(1),
  FriendsAndFamilyList varchar2(3000),
  ClosedUserGroupEnabled char(1),
  ClosedUserGroupList varchar2(3000),
  ClosedUserGroupPreferred int,
  CUGIncomingAccessAllowed char(1),
  CUGOutgoingAccessAllowed char(1),
  CfBusy varchar2(50),
  CfNoReply varchar2(50),
  PromotionList varchar2(50),
  PromotionValidityStartDates varchar2(50),
  PromotionValidityEndDates varchar2(50)
);

CREATE TABLE OpenNet_SubscriberImsPublicUserIdentity (
  ImsPublicUserId varchar2(50) NOT NULL PRIMARY KEY,
  SubscriberId varchar2(50) NOT NULL,
  CONSTRAINT pk_imsid PRIMARY KEY (ImsPublicUserId),
  CONSTRAINT fk_subscriberid FOREIGN KEY (SubscriberId)
  REFERENCES OpenNet_Subscribers ON DELETE CASCADE
);

-- DROP TABLE OpenCloud_SubscriberImsPublicUserIdentity;
-- DROP TABLE OpenCloud_Subscribers;

CREATE TABLE OpenCloud_Subscribers (
  MSISDN varchar2(50) NOT NULL PRIMARY KEY,
  IMSI varchar2(50),
  subscriberlanguage varchar2(50),
  Account varchar2(50),
  SubscriptionType varchar2(50),
  CreditWorthy number(1),
  ValidityStart date,
  ValidityEnd date,
  HomeZoneEnabled number(1),
  HomeZoneList varchar2(3000),
  ocsId varchar2(50),
  FriendsAndFamilyEnabled char(1),
  FriendsAndFamilyList varchar2(3000),
  ClosedUserGroupEnabled char(1),
  ClosedUserGroupList varchar2(3000),
  ClosedUserGroupPreferred int,
  CUGIncomingAccessAllowed char(1),
  CUGOutgoingAccessAllowed char(1),
  CfBusy varchar2(50),
  CfNoReply varchar2(50),
  PromotionList varchar2(50),
  PromotionValidityStartDates varchar2(50),
  PromotionValidityEndDates varchar2(50)
);

CREATE TABLE OpenCloud_SubscriberImsPublicUserIdentity (
  ImsPublicUserId varchar2(50) NOT NULL,
  SubscriberId varchar2(50) NOT NULL,
  CONSTRAINT pk_imsid PRIMARY KEY (ImsPublicUserId),
  CONSTRAINT fk_subscriberid FOREIGN KEY (SubscriberId)
  REFERENCES OpenCloud_Subscribers ON DELETE CASCADE
);
