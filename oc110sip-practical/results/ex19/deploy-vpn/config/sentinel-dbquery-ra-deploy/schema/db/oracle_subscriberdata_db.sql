DROP table OpenNet_SubscriberImsPublicUserIdentity;
DROP table OpenNet_Subscribers;

create table OpenNet_Subscribers (
  MSISDN varchar(50) not null,
  IMSI varchar(50),
  SubscriberLanguage varchar(50),
  Account varchar(50),
  SubscriptionType varchar(50),
  CreditWorthy number(1),
  ValidityStart date,
  ValidityEnd date,
  HomeZoneEnabled number(1),
  HomeZoneList varchar(50)[],
  ocsId varchar2(50),
  FriendsAndFamilyEnabled char(1),
  FriendsAndFamilyList varchar2(3000),
  ClosedUserGroupEnabled char(1),
  ClosedUserGroupList varchar2(3000),
  ClosedUserGroupPreferred int,
  CUGIncomingAccessAllowed char(1),
  CUGOutgoingAccessAllowed char(1),
  CfBusy varchar2(50),
  CfNoReply varchar2(50),
  PromotionList varchar2(50),
  PromotionValidityStartDates varchar2(50),
  PromotionValidityEndDates varchar2(50),
  CONSTRAINT pk_msisdn PRIMARY KEY (MSISDN) USING INDEX opennet_subscribers_idx
);
CREATE UNIQUE INDEX opennet_subscribers_idx ON OpenNet_Subscribers (MSISDN);
COMMENT ON TABLE OpenNet_Subscribers IS 'Store basic subscriber data for OpenNet mvno';

create table OpenNet_SubscriberImsPublicUserIdentity (
  ImsPublicUserId varchar(50) not null,
  SubscriberId varchar(50) not null,
  CONSTRAINT pk_imsid PRIMARY KEY (ImsPublicUserId) USING INDEX opennet_subscriberimsidentity_idx,
  CONSTRAINT fk_subscriberid FOREIGN KEY (SubscriberId)
  REFERENCES OpenNet_Subscribers ON DELETE CASCADE
);
CREATE UNIQUE INDEX opennet_subscriberimsidentity_idx ON OpenNet_SubscriberImsPublicUserIdentity (ImsPublicUserId);
COMMENT ON TABLE OpenNet_SubscriberImsPublicUserIdentity IS 'Store subscriber IMS public user identities for OpenNet mvno';

DROP table OpenCloud_SubscriberImsPublicUserIdentity;
DROP table OpenCloud_Subscribers;

create table OpenCloud_Subscribers (
  MSISDN varchar(50) not null,
  IMSI varchar(50),
  SubscriberLanguage varchar(50),
  Account varchar(50),
  SubscriptionType varchar(50),
  CreditWorthy number(1),
  ValidityStart date,
  ValidityEnd date,
  HomeZoneEnabled number(1),
  HomeZoneList varchar(50)[],
  ocsId varchar2(50),
  FriendsAndFamilyEnabled char(1),
  FriendsAndFamilyList varchar2(3000),
  ClosedUserGroupEnabled char(1),
  ClosedUserGroupList varchar2(3000),
  ClosedUserGroupPreferred int,
  CUGIncomingAccessAllowed char(1),
  CUGOutgoingAccessAllowed char(1),
  CfBusy varchar2(50),
  CfNoReply varchar2(50),
  PromotionList varchar2(50),
  PromotionValidityStartDates varchar2(50),
  PromotionValidityEndDates varchar2(50),
  CONSTRAINT pk_msisdn PRIMARY KEY (MSISDN) USING INDEX opencloud_subscribers_idx
);
CREATE UNIQUE INDEX opecloud_subscribers_idx ON OpenCloud_Subscribers (MSISDN);
COMMENT ON TABLE OpenCloud_Subscribers IS 'Store basic subscriber data for OpenCloud platform operator';

create table OpenCloud_SubscriberImsPublicUserIdentity (
  ImsPublicUserId varchar(50) not null,
  SubscriberId varchar(50) not null,
  CONSTRAINT pk_imsid PRIMARY KEY (ImsPublicUserId) USING INDEX opencloud_subscriberimsidentity_idx,
  CONSTRAINT fk_subscriberid FOREIGN KEY (SubscriberId)
  REFERENCES OpenCloud_Subscribers ON DELETE CASCADE
);
CREATE UNIQUE INDEX opencloud_subscriberimsidentity_idx ON OpenCloud_SubscriberImsPublicUserIdentity (ImsPublicUserId);
COMMENT ON TABLE OpenCloud_SubscriberSipIdentity IS 'Store subscriber IMS public user identities for OpenCloud platform operator';

