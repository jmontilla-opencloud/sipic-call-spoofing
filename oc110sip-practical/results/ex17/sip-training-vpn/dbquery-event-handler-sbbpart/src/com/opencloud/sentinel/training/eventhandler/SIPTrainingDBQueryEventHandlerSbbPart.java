/**
 * Copyright (c) 2014 Open Cloud Limited, a company incorporated in England and Wales (Registration Number 6000941) with its principal place of business at Edinburgh House, St John's Innovation Park, Cowley Road, Cambridge CB4 0DS.
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1  Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2  Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3  The name of the author may not be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * 4  The source code may not be used to create, develop, use or distribute software for use on any platform other than the Open Cloud Rhino and Open Cloud Rhino Sentinel platforms or any successor products.
 *
 * 5  Full license terms may be found https://developer.opencloud.com/devportal/display/OCDEV/Feature+Source+License
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE ARE EXCLUDED TO THE FULLEST EXTENT PERMITTED BY LAW.
 *
 * TO THE FULLEST EXTENT PERMISSIBLE BUY LAW, THE AUTHOR SHALL NOT BE LIABLE FOR ANY LOSS OF REVENUE, LOSS OF PROFIT, LOSS OF FUTURE BUSINESS, LOSS OF DATA OR ANY INDIRECT, SPECIAL, CONSEQUENTIAL, PUNITIVE OR OTHER LOSS OR DAMAGES ARISING OUT OF OR IN CONNECTION WITH THE SOFTWARE, WHETHER ARISING IN CONTRACT, TORT (INCLUDING NEGLIGENCE) MISREPRESENTATION OR OTHERWISE AND REGARDLESS OF WHETHER OPEN CLOUD HAS BEEN ADVISED OF THE POSSIBILITY OF ANY SUCH LOSS OR DAMAGE. THE AUTHORS MAXIMUM AGGREGATE LIABILITY WHETHER IN CONTRACT, TORT (INCLUDING NEGLIGENCE) OR OTHERWISE, SHALL NOT EXCEED EUR100.
 *
 * NOTHING IN THIS LICENSE SHALL LIMIT THE LIABILITY OF THE AUTHOR FOR DEATH OR PERSONAL INJURY RESULTING FROM NEGLIGENCE, FRAUD OR FRAUDULENT MISREPRESENTATION.
 *
 * Visit Open Cloud Developer's Portal for how-to guides, examples, documentation, forums and more: http://developer.opencloud.com
 */
package com.opencloud.sentinel.training.eventhandler;

import javax.inject.Inject;
import javax.inject.Named;
import javax.slee.ActivityContextInterface;
import javax.slee.CreateException;
import javax.slee.EventContext;
import javax.slee.InitialEventSelector;
import javax.slee.annotation.ComponentId;
import javax.slee.annotation.EventDirection;
import javax.slee.annotation.EventMethod;
import javax.slee.annotation.RATypeBinding;
import javax.slee.annotation.RATypeBindings;

import com.opencloud.rhino.facilities.Tracer;
import com.opencloud.rhino.slee.lifecycle.PostCreate;
import com.opencloud.rhino.slee.sbbpart.SbbPartContext;
import com.opencloud.sentinel.common.SentinelFireEventException;
import com.opencloud.sentinel.endpoint.SentinelEndpoint;
import com.opencloud.sentinel.sbb.SentinelActivityContextInterface;
import com.opencloud.slee.annotation.SBBPart;
import com.opencloud.slee.annotation.SBBPartActivityContextInterface;
import com.opencloud.slee.annotation.SBBPartClass;
import com.opencloud.slee.annotation.SBBPartClasses;
import com.opencloud.slee.annotation.SBBPartReference;
import com.opencloud.slee.resources.dbquery.DatabaseQueryFailureEvent;
import com.opencloud.slee.resources.dbquery.DatabaseResultEvent;


@SBBPart(
    id = @ComponentId(name = "@component.name@", vendor = "@component.vendor@", version = "@component.version@"),
    sbbPartRefs = {
        @SBBPartReference(id = @ComponentId(name = "@sentinel-sip-spi.SentinelSipFeatureSPI SBB Part.name@", vendor = "@sentinel-sip-spi.SentinelSipFeatureSPI SBB Part.vendor@", version = "@sentinel-sip-spi.SentinelSipFeatureSPI SBB Part.version@")),
    },
    sbbPartClasses = @SBBPartClasses(
        sbbPartClass = @SBBPartClass(
            className="com.opencloud.sentinel.training.eventhandler.SIPTrainingDBQueryEventHandlerSbbPart"
        ),
        activityContextInterface = @SBBPartActivityContextInterface(interfaceName = "com.opencloud.sentinel.sbb.SentinelActivityContextInterface")
    )
)
@RATypeBindings(
    raTypeBindings = {
        @RATypeBinding(
            activityContextInterfaceFactoryName = "slee/resources/dbquery/activitycontextinterfacefactory",
            resourceAdaptorObjectName = "slee/resources/dbquery/provider",
            resourceAdaptorEntityLink = "sentinel-dbquery",
            raType = @ComponentId(name = "Database Query", vendor = "OpenCloud", version = "2.0")
        )
    }
)
@SuppressWarnings("unused")
public class SIPTrainingDBQueryEventHandlerSbbPart {

    @PostCreate
    public void onCreate() throws CreateException {
        if(rootTracer.isFinerEnabled())
            rootTracer.finer("SBB part created");
    }

    public InitialEventSelector ies(InitialEventSelector ies) {
        return ies;
    }

    @EventMethod(
        eventDirection = EventDirection.Receive,
        eventType = @ComponentId(name = "com.opencloud.slee.resources.dbquery.DatabaseResult", vendor = "OpenCloud", version = "2.0")
    )
    public void onDatabaseResult(DatabaseResultEvent result, SentinelActivityContextInterface aci, EventContext eventContext) {
        if (rootTracer.isFinerEnabled())
            rootTracer.finer("SIPTrainingDBQueryEventHandlerSbbPart DB Query result received: " + result);

        // processEvent does not return until the event has been processed by Sentinel.
        try {
            final String target = aci.getTargetFeature();
            if (target == null || target.isEmpty()) {
                rootTracer.fine("No target feature for query result, ignore");
                return;
            }
            SentinelEndpoint sentinelEndpoint = (SentinelEndpoint)sbbPartContext.getSbbLocalObject();
            // ask sentinel to process the DB response by calling the 'featureToInvoke' feature
            sentinelEndpoint.processEvent(target, result, false, aci, eventContext);
            aci.detach(sbbPartContext.getSbbLocalObject());
        } catch (Exception e) {
            rootTracer.fine("Caught exception in processEvent for DBQueryResult", e);
            aci.detach(sbbPartContext.getSbbLocalObject());
        }

    }

    @EventMethod(
            eventDirection = EventDirection.Receive,
            eventType = @ComponentId(name = "com.opencloud.slee.resources.dbquery.DatabaseQueryFailure", vendor = "OpenCloud", version = "2.0")
    )
    public void onDatabaseQueryFailure(DatabaseQueryFailureEvent failure, SentinelActivityContextInterface aci, EventContext eventContext) {
        if (rootTracer.isFinerEnabled())
                    rootTracer.finer("SIPTrainingDBQueryEventHandlerSbbPart DB Query failure received: " + failure);

        // processEvent does not return until the event has been processed by Sentinel.
        try {
            final String target = aci.getTargetFeature();
            if (target == null || target.isEmpty()) {
                rootTracer.fine("No target feature for query result, ignore");
                return;
            }            
            SentinelEndpoint sentinelEndpoint = (SentinelEndpoint)sbbPartContext.getSbbLocalObject();

            // ask sentinel to process the DB response by calling the 'featureToInvoke' feature
            sentinelEndpoint.processEvent(target, failure, false, aci, eventContext);
            aci.detach(sbbPartContext.getSbbLocalObject());
        } catch (Exception e) {
            rootTracer.fine("Caught exception in processEvent for DBQueryFailure", e);
            aci.detach(sbbPartContext.getSbbLocalObject());
        }
    }


    @Inject
    private Tracer rootTracer;

    @Inject @Named("timer")
    private Tracer timerTracer;

    @Inject
    private SbbPartContext sbbPartContext;

}
