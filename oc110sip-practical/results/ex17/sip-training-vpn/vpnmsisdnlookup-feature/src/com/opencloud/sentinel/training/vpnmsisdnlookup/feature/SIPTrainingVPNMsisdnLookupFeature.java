/**
 * Copyright (c) 2014 Open Cloud Limited, a company incorporated in England and Wales (Registration Number 6000941) with its principal place of business at Edinburgh House, St John's Innovation Park, Cowley Road, Cambridge CB4 0DS.
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1  Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2  Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3  The name of the author may not be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * 4  The source code may not be used to create, develop, use or distribute software for use on any platform other than the Open Cloud Rhino and Open Cloud Rhino Sentinel platforms or any successor products.
 *
 * 5  Full license terms may be found https://developer.opencloud.com/devportal/display/OCDEV/Feature+Source+License
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE ARE EXCLUDED TO THE FULLEST EXTENT PERMITTED BY LAW.
 *
 * TO THE FULLEST EXTENT PERMISSIBLE BUY LAW, THE AUTHOR SHALL NOT BE LIABLE FOR ANY LOSS OF REVENUE, LOSS OF PROFIT, LOSS OF FUTURE BUSINESS, LOSS OF DATA OR ANY INDIRECT, SPECIAL, CONSEQUENTIAL, PUNITIVE OR OTHER LOSS OR DAMAGES ARISING OUT OF OR IN CONNECTION WITH THE SOFTWARE, WHETHER ARISING IN CONTRACT, TORT (INCLUDING NEGLIGENCE) MISREPRESENTATION OR OTHERWISE AND REGARDLESS OF WHETHER OPEN CLOUD HAS BEEN ADVISED OF THE POSSIBILITY OF ANY SUCH LOSS OR DAMAGE. THE AUTHORS MAXIMUM AGGREGATE LIABILITY WHETHER IN CONTRACT, TORT (INCLUDING NEGLIGENCE) OR OTHERWISE, SHALL NOT EXCEED EUR100.
 *
 * NOTHING IN THIS LICENSE SHALL LIMIT THE LIABILITY OF THE AUTHOR FOR DEATH OR PERSONAL INJURY RESULTING FROM NEGLIGENCE, FRAUD OR FRAUDULENT MISREPRESENTATION.
 *
 * Visit Open Cloud Developer's Portal for how-to guides, examples, documentation, forums and more: http://developer.opencloud.com
 */
package com.opencloud.sentinel.training.vpnmsisdnlookup.feature;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.slee.ActivityContextInterface;
import javax.slee.ActivityEndEvent;
import javax.slee.annotation.ComponentId;
import javax.slee.annotation.LibraryReference;
import javax.slee.annotation.LibraryReferences;
import javax.slee.facilities.Tracer;
import javax.slee.resource.StartActivityException;

import org.jainslee.resources.sip.SipRequest;
import org.jainslee.resources.sip.SipURI;
import org.jainslee.resources.sip.TelURL;
import org.jainslee.resources.sip.URI;

import com.opencloud.sce.fsmtool.Facilities;
import com.opencloud.sentinel.annotations.SentinelFeature;
import com.opencloud.sentinel.common.CallType;
import com.opencloud.sentinel.feature.ExecutionPhase;
import com.opencloud.sentinel.feature.impl.BaseFeature;
import com.opencloud.sentinel.feature.spi.FeatureError;
import com.opencloud.sentinel.feature.spi.SentinelSipMultiLegFeatureEndpoint;
import com.opencloud.sentinel.feature.spi.init.InjectResourceAdaptorProvider;
import com.opencloud.sentinel.training.vpn.session.SIPTrainingVPNSessionState;
import com.opencloud.slee.annotation.SBBPartReference;
import com.opencloud.slee.annotation.SBBPartReferences;
import com.opencloud.slee.resources.dbquery.DatabaseQueryActivity;
import com.opencloud.slee.resources.dbquery.DatabaseQueryActivityContextInterfaceFactory;
import com.opencloud.slee.resources.dbquery.DatabaseQueryFailureEvent;
import com.opencloud.slee.resources.dbquery.DatabaseQueryProvider;
import com.opencloud.slee.resources.dbquery.DatabaseResultEvent;
import com.opencloud.slee.resources.dbquery.NoDataSourcesAvailableException;
import com.opencloud.slee.resources.dbquery.QueryInfo;
import com.opencloud.slee.resources.dbquery.QueryInfo.ExecuteType;
import com.opencloud.slee.resources.dbquery.QueryInfo.RetryBehaviour;
import com.opencloud.slee.resources.dbquery.QueryInfo.StatementType;

/**
*
* An example feature.
*/
@SentinelFeature(
   featureName = SIPTrainingVPNMsisdnLookupFeature.NAME,
   componentName = "@component.name@",
   featureVendor = "@component.vendor@",
   featureVersion = "@component.version@",
   featureGroup = SentinelFeature.SIP_FEATURE_GROUP,
   executionPhases = ExecutionPhase.SipSessionPhase,
   raProviderJndiNames = {"slee/resources/dbquery/provider", "slee/resources/dbquery/activitycontextinterfacefactory"}
)
@SBBPartReferences(
   sbbPartRefs = {
           @SBBPartReference(id = @ComponentId(name = "@sentinel-sip-spi.SentinelSipFeatureSPI SBB Part.name@", vendor = "@sentinel-sip-spi.SentinelSipFeatureSPI SBB Part.vendor@", version = "@sentinel-sip-spi.SentinelSipFeatureSPI SBB Part.version@"))
   }
)
@LibraryReferences(
  libraryRefs = {
         @LibraryReference(library = @ComponentId(
              name = "@sip-training-vpn-session-state-library.name@",
              vendor = "@sip-training-vpn-session-state-library.vendor@",
              version = "@sip-training-vpn-session-state-library.version@")),
  }
)
@SuppressWarnings("unused")
public class SIPTrainingVPNMsisdnLookupFeature extends BaseFeature<SIPTrainingVPNSessionState, SentinelSipMultiLegFeatureEndpoint>
       implements InjectResourceAdaptorProvider {

   public SIPTrainingVPNMsisdnLookupFeature(SentinelSipMultiLegFeatureEndpoint caller, Facilities facilities, SIPTrainingVPNSessionState sessionState) {
       super(caller, facilities, sessionState);
   }

   public static final String NAME = "SIPTrainingVPNMsisdnLookupFeature";

   private DatabaseQueryProvider dbQueryProvider;
   private DatabaseQueryActivityContextInterfaceFactory dbACIFactory;

   private static final String SQL_MSISDN = "SELECT msisdn FROM VPNMembers WHERE VPNId = ? AND shortCode = ?";
   /**
    * All features must have a unique name.
    *
    * @return the name of this feature
    */
   @Override
   public String getFeatureName() { return NAME; }

   /**
    * Kick off the feature.
    *
    * @param trigger  a triggering context. The feature implementation must be able to cast this to a useful type for it to run
    * @param activity the slee activity object this feature is related to (may be null)
    * @param aci      the activity context interface of the slee activity this feature is related to
    */
   @Override
   public void startFeature(Object trigger, Object activity, ActivityContextInterface aci) {

       Tracer tracer = getTracer();
       if (tracer.isInfoEnabled()) {
           tracer.info("Starting " + NAME);
       }

        if (trigger instanceof DatabaseResultEvent) {
            tracer.finer("Success DBQuery result event received from DB");

            DatabaseResultEvent result = (DatabaseResultEvent) trigger;

            ResultSet rs = result.getResultSet();

            String translatedLongNumber;
		    try {
				translatedLongNumber = rs.next() ? rs.getString(1) : null;
	            rs.close(); 
           } catch (SQLException e) {
               if (tracer.isWarningEnabled())
                   tracer.warning("SQL Exception triggered processing result", e);
               getCaller().featureHasFinished();
               return;
           }

           if (translatedLongNumber != null) {
               if (tracer.isFinerEnabled())
                   tracer.finer("VPNMsisdn: " + translatedLongNumber);
               getSessionState().setVPNMsisdn(translatedLongNumber);
           } else {
               if (tracer.isFinerEnabled())
                   tracer.finer("No VPN MSISDN found");
 
           }
           getCaller().featureHasFinished();

       } else if (trigger instanceof DatabaseQueryFailureEvent) {
           if(tracer.isFinerEnabled()) tracer.finer("Failure DBQuery result event received from DB");
           getCaller().featureHasFinished();
       } else if (trigger instanceof ActivityEndEvent) {
           // I am being invoked with an ActivityEndEvent ... something went wrong!           
           if(tracer.isFineEnabled()) tracer.fine("Received an ActivityEndEvent ... feature has finished");
           getCaller().featureHasFinished();
       } else {
           tracer.finer("Retrieving data from Session State to compose query");
           
           //Get VPN Id from SessionState
           String vpnId = getSessionState().getVPNId();
           if(vpnId == null){
                if(tracer.isFineEnabled()) tracer.fine("No VPN ID stored in session state. Skipping query");
                getCaller().featureHasFinished();               
                return;
           }
           String shortCode = getSessionState().getShortCode();
           if(shortCode == null){
                if(tracer.isFineEnabled()) tracer.fine("No Short Code stored in session state. Skipping query");
                getCaller().featureHasFinished();               
                return;
           }           

           try {
               ActivityContextInterface dbQueryAci = getVPNMsisdn(vpnId, shortCode);
               if (getTracer().isFineEnabled()) {
                   getTracer().fine("Attaching to ACI and storing a reference to the feature for callback on event ");
               }
               getCaller().attach(dbQueryAci);
               getCaller().asSentinelActivityContextInterface(dbQueryAci).setTargetFeature(NAME);
               getCaller().featureWaiting(dbQueryAci);
           } catch (StartActivityException | NoDataSourcesAvailableException e) {
               if (tracer.isWarningEnabled())
                   tracer.warning("Exception triggered on getVPNMsisdn method", e);
               getCaller().featureHasFinished();
           }

           tracer.fine("Leaving feature after sending Query to DB");
       }
   }
   
   private ActivityContextInterface getVPNMsisdn(final String vpnId, final String shortCode)
		   throws StartActivityException, NoDataSourcesAvailableException{
       DatabaseQueryActivity queryActivity = dbQueryProvider.createActivity();

       ActivityContextInterface dbAci = dbACIFactory.getActivityContextInterface(queryActivity);      
       queryActivity.sendQuery(new QueryInfo() {
           public String getSql() {return SQL_MSISDN;}
           public void setParameters(PreparedStatement ps) throws SQLException {
               ps.setString(1, vpnId);
               ps.setString(2, shortCode);
           }
           public ExecuteType getExecuteType() {return ExecuteType.QUERY;}
           public RetryBehaviour getRetryBehaviour() {return RetryBehaviour.TRY_FIRST_AVAILABLE;}
           public StatementType getStatementType() {return StatementType.PREPARED;}
           });
   
       return dbAci;
   }   

   @Override
   public void injectResourceAdaptorProvider(Object provider) {
       if (provider instanceof DatabaseQueryProvider) {
           this.dbQueryProvider = (DatabaseQueryProvider) provider;
       } else if (provider instanceof DatabaseQueryActivityContextInterfaceFactory) {
           this.dbACIFactory = (DatabaseQueryActivityContextInterfaceFactory) provider;
       } else {
           if (provider != null)
              if (getTracer().isFineEnabled()) {getTracer().fine("Unknown provider " + provider);}
       }
   }

}
