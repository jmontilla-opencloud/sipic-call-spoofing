package com.opencloud.sentinel.train.vpn.addresslistprofile;


import com.opencloud.sentinel.feature.common.lists.profiles.AddressListEntryProfileCMP;

import javax.slee.annotation.ComponentId;
import javax.slee.annotation.LibraryReference;
import javax.slee.annotation.Profile;
import javax.slee.annotation.ProfileAbstractClass;
import javax.slee.annotation.ProfileClasses;
import javax.slee.annotation.ProfileLocalInterface;

@Profile(
    vendorExtensionID = "@component.name@",
    description = "SIP Training VPN Address List Profile",
    id = @ComponentId(name = "@component.name@", vendor = "@component.vendor@",
                      version = "@component.version@"),
    libraryRefs = {
        @LibraryReference(library = @ComponentId(name = 
            "@sentinel-profile-util-library.LibraryID.sentinel-profile-util-library.name@", 
            vendor = "@sentinel-profile-util-library.LibraryID.sentinel-profile-util-library.vendor@", 
            version = "@sentinel-profile-util-library.LibraryID.sentinel-profile-util-library.version@")),
        @LibraryReference(library = @ComponentId(name = "SentinelAddressList",
                vendor = "OpenCloud",
                version = "@sentinel-core.component.version@"))
    },
    profileClasses = @ProfileClasses(
        profileLocal = @ProfileLocalInterface(interfaceName =
            "com.opencloud.sentinel.train.vpn.addresslistprofile.SIPTrainingVPNListEntryProfileLocal"),
        profileAbstractClass = @ProfileAbstractClass(className =
            "com.opencloud.sentinel.train.vpn.addresslistprofile.SIPTrainingVPNListEntryProfileAbstractClass")
    ),
    singleProfile = false
)
@SuppressWarnings("unused")
public interface SIPTrainingVPNListEntryProfileCMP extends AddressListEntryProfileCMP{
    boolean getIsWhiteListed();
    void setIsWhiteListed(boolean value);
}

