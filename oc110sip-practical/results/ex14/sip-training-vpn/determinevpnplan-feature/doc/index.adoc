= Test documentation
:toc: left
:toclevels: 1
:icons: font
:idprefix:

ifdef::pdf-name[]
image::pdf.png[Download PDF, 48, 48, link="{pdf-name}"]
endif::[]

== Introduction

This is a test document, it is used for build purposes, so that we can get our asciidoc docs building.
The way we assemble documentation from asciidoc files is being done in another project.
For now, products just need to write asciidoc files that make sense.


ifeval::["{backend}" != "docbook5"]
link:../api/index.html[Test documentation].
endif::[]
ifeval::["{backend}" == "docbook5"]
Test documentation.
endif::[]

include::{includedir}/project-structure.adoc[]

include::{includedir}/module-structure.adoc[]

include::{includedir}/ivy-configurations.adoc[]

include::{includedir}/pojo-feature-example.adoc[]

