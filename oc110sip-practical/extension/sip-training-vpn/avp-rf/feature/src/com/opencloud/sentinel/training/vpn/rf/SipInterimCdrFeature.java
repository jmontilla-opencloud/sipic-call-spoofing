/**
 * Copyright (c) 2014 Open Cloud Limited, a company incorporated in England and Wales (Registration Number 6000941) with its principal place of business at Edinburgh House, St John's Innovation Park, Cowley Road, Cambridge CB4 0DS.
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1  Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2  Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3  The name of the author may not be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * 4  The source code may not be used to create, develop, use or distribute software for use on any platform other than the Open Cloud Rhino and Open Cloud Rhino Sentinel platforms or any successor products.
 *
 * 5  Full license terms may be found https://developer.opencloud.com/devportal/display/OCDEV/Feature+Source+License
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE ARE EXCLUDED TO THE FULLEST EXTENT PERMITTED BY LAW.
 *
 * TO THE FULLEST EXTENT PERMISSIBLE BUY LAW, THE AUTHOR SHALL NOT BE LIABLE FOR ANY LOSS OF REVENUE, LOSS OF PROFIT, LOSS OF FUTURE BUSINESS, LOSS OF DATA OR ANY INDIRECT, SPECIAL, CONSEQUENTIAL, PUNITIVE OR OTHER LOSS OR DAMAGES ARISING OUT OF OR IN CONNECTION WITH THE SOFTWARE, WHETHER ARISING IN CONTRACT, TORT (INCLUDING NEGLIGENCE) MISREPRESENTATION OR OTHERWISE AND REGARDLESS OF WHETHER OPEN CLOUD HAS BEEN ADVISED OF THE POSSIBILITY OF ANY SUCH LOSS OR DAMAGE. THE AUTHORS MAXIMUM AGGREGATE LIABILITY WHETHER IN CONTRACT, TORT (INCLUDING NEGLIGENCE) OR OTHERWISE, SHALL NOT EXCEED EUR100.
 *
 * NOTHING IN THIS LICENSE SHALL LIMIT THE LIABILITY OF THE AUTHOR FOR DEATH OR PERSONAL INJURY RESULTING FROM NEGLIGENCE, FRAUD OR FRAUDULENT MISREPRESENTATION.
 *
 * Visit Open Cloud Developer's Portal for how-to guides, examples, documentation, forums and more: http://developer.opencloud.com
 */
package com.opencloud.sentinel.training.vpn.rf;

import java.util.ArrayList;
import java.util.Arrays;

import javax.slee.ActivityContextInterface;
import javax.slee.annotation.ComponentId;
import javax.slee.annotation.LibraryReference;
import javax.slee.annotation.LibraryReferences;
import javax.slee.annotation.ProfileReference;
import javax.slee.annotation.ProfileReferences;

import org.jainslee.resources.diameter.base.AvpNotAllowedException;
import org.jainslee.resources.diameter.base.DiameterAvp;
import org.jainslee.resources.diameter.rf.types.vcb0.AccountingRequest;
import org.jainslee.resources.diameter.rf.vcb0.RfMessageFactory;

import com.opencloud.sce.fsmtool.Facilities;
import com.opencloud.sentinel.annotations.ConfigurationReader;
import com.opencloud.sentinel.annotations.FeatureProvisioning;
import com.opencloud.sentinel.annotations.ProvisioningConfig;
import com.opencloud.sentinel.annotations.ProvisioningField;
import com.opencloud.sentinel.annotations.ProvisioningProfile;
import com.opencloud.sentinel.annotations.ProvisioningProfileId;
import com.opencloud.sentinel.annotations.SentinelFeature;
import com.opencloud.sentinel.feature.ExecutionPhase;
import com.opencloud.sentinel.feature.common.cdr.util.SipAvpCdrUtil;
import com.opencloud.sentinel.feature.spi.FeatureError;
import com.opencloud.sentinel.feature.spi.SentinelSipMultiLegFeatureEndpoint;
import com.opencloud.sentinel.util.ExceptionTraceHelper;
import com.opencloud.slee.annotation.BinderTargets;
import com.opencloud.slee.annotation.SBBPartReference;
import com.opencloud.slee.annotation.SBBPartReferences;


@SentinelFeature(featureName = SipInterimCdrFeature.NAME,
                 featureGroup = SentinelFeature.SIP_FEATURE_GROUP,
                 componentName = "@component.name@",
                 featureVendor = "@component.vendor@",
                 featureVersion = "@component.version@",
                 raProviderJndiNames = {
                     "slee/resources/cdr/provider",
                     "slee/resources/diameterro/sentinel/provider",
                     "sentinel/servicetimerprovider",
                     "slee/resources/rf-control/provider",
                     "slee/resources/rf-control/activitycontextinterfacefactory"
                 },
        configurationReader = @ConfigurationReader(
                 readerInterface = SipInterimCdrConfigReader.class,
                 readerClass = SipInterimCdrConfigProfileReader.class),
                 executionPhases = { ExecutionPhase.SipSessionPhase },
                 usageStatistics = CDRStats.class,
    provisioning = @FeatureProvisioning(
        displayName = "Sip Interim Cdr",
        configs = {
            @ProvisioningConfig(
                type = "Config",
                fields = {
                    @ProvisioningField(
                        name = "interimTimerPeriod",
                        displayName = "Interim CDR Timer Interval",
                        type = "long",
                        description = "The maximum interval between writing interim CDRs for a session",
                        required = true
                    ),
                    @ProvisioningField(
                        name = "writeCdrOnSDPChange",
                        displayName = "Write CDR on SDP Change",
                        type = "boolean",
                        description = "Indicates whether or not to write CDRs on SDP changes",
                        required = true
                    )
                },
                profile = @ProvisioningProfile(
                    tableName = "SipInterimCdrProfileTable",
                    specification = @ProvisioningProfileId(name = "@sip-training-avp-rf-feature-profile.name@", vendor = "@sip-training-avp-rf-feature-profile.vendor@", version = "@sip-training-avp-rf-feature-profile.version@")
                )
            ),
            @ProvisioningConfig(
                type = "CDF Destination",
                fields = {
                    @ProvisioningField(
                        name = "destinationRealm",
                        displayName = "Destination Realm",
                        type = "String",
                        description = "The destination realm to use for sending Rf ACR messages to the CDF",
                        required = true
                    ),
                    @ProvisioningField(
                        name = "destinationHost",
                        displayName = "Destination Host",
                        type = "String",
                        description = "The destination host to use for sending Rf ACR messages to the CDF"
                    )
                },
                profile = @ProvisioningProfile(
                    tableName = "CdfDestinationProfileTable",
                    specification = @ProvisioningProfileId(name = "@sentinel-sip-cdf-destination-profile.name@", vendor = "@sentinel-sip-cdf-destination-profile.vendor@", version = "@sentinel-sip-cdf-destination-profile.version@")
                )
            )
        }
    )
)

@ProfileReferences(
    profileRefs = {
        @ProfileReference(
            profile = @ComponentId(name="@sip-training-avp-rf-feature-profile.name@",
                                   vendor="@sip-training-avp-rf-feature-profile.vendor@",
                                   version="@sip-training-avp-rf-feature-profile.version@")
        ),
        @ProfileReference(
            profile = @ComponentId(name="@sentinel-sip-cdf-destination-profile.name@",
                                   vendor="@sentinel-sip-cdf-destination-profile.vendor@",
                                   version="@sentinel-sip-cdf-destination-profile.version@")
        )
    }
)
@SBBPartReferences(
    sbbPartRefs = {
        @SBBPartReference(
            id = @ComponentId(name    = "@sentinel-sip-spi.SentinelSipFeatureSPI SBB Part.name@",
                              vendor  = "@sentinel-sip-spi.SentinelSipFeatureSPI SBB Part.vendor@",
                              version = "@sentinel-sip-spi.SentinelSipFeatureSPI SBB Part.version@")
        ),
        @SBBPartReference(
            id = @ComponentId(name    = "@sentinel-sip-avp-cdr-util.name@",
                              vendor  = "@sentinel-sip-avp-cdr-util.vendor@",
                              version = "@sentinel-sip-avp-cdr-util.version@"))
    }
)
@LibraryReferences(
    libraryRefs = {
        @LibraryReference(
            library = @ComponentId(name    = "@sentinel-sip-spi.SentinelSipFeatureSPI.name@",
                                   vendor  = "@sentinel-sip-spi.SentinelSipFeatureSPI.vendor@",
                                   version = "@sentinel-sip-spi.SentinelSipFeatureSPI.version@")),
        @LibraryReference(
            library = @ComponentId(name    = "@sentinel-sip-interim-cdr-session-state.name@",
                                   vendor  = "@sentinel-sip-interim-cdr-session-state.vendor@",
                                   version = "@sentinel-sip-interim-cdr-session-state.version@")),
        @LibraryReference(
            library = @ComponentId(name    = "@avp-cdr-format.name@",
                                   vendor  = "@avp-cdr-format.vendor@",
                                   version = "@avp-cdr-format.version@")),
        @LibraryReference(library = @ComponentId(
                name = "@sip-training-vpn-session-state-library.name@", 
                vendor = "@sip-training-vpn-session-state-library.vendor@", 
                version = "@sip-training-vpn-session-state-library.version@")) 
    }
)
@BinderTargets(services = "sip")
public class SipInterimCdrFeature extends BaseSipInterimCdrFeature {

    public SipInterimCdrFeature(SentinelSipMultiLegFeatureEndpoint caller, Facilities facilities, SipInterimVPNCdrSessionState sessionState) {
        super(caller, facilities, sessionState);
    }

    @Override
    public void startFeature(Object trigger, Object activity, ActivityContextInterface aci) {
        if (getTracer().isFineEnabled()) getTracer().fine(NAME + " started");
        final FeatureError error = doSipInterimCdrFeature(trigger, activity, aci);
        if (error != null)
            getCaller().featureCannotStart(error);

        if (getTracer().isFineEnabled()) getTracer().fine(NAME + " has finished");
        getCaller().featureHasFinished();
    }

    public static final String NAME = "SIPTrainingAvpRfInterimCdr";

    
    protected void extendRfAcr(AccountingRequest accountingRequest, RfMessageFactory messageFactory) {
        // No-op.
        // Intended as extension point for downstream features which just need to add a few AVPs (e.g. VoLTE Interim CDR Feature)
    	if (getTracer().isFineEnabled()) getTracer().fine("Setting Custom AVPs");
    	ArrayList<DiameterAvp> extensionAvps = (accountingRequest.getExtensionAvps() == null) ?
                new ArrayList<DiameterAvp>() :
                new ArrayList<DiameterAvp>(Arrays.asList(accountingRequest.getExtensionAvps()));
        if (getSessionState().getVPNId() != null) {
        	DiameterAvp vpnAvp = SipAvpCdrUtil.createCustomAvp(messageFactory.getBaseMessageFactory(), 10, "VPN", getSessionState().getVPNId() , getTracer());
        	extensionAvps.add(vpnAvp);
        }
        if (getSessionState().getShortCode() != null) {
        	DiameterAvp shortcodeAvp = SipAvpCdrUtil.createCustomAvp(messageFactory.getBaseMessageFactory(), 11, "Short-Code", getSessionState().getShortCode(), getTracer());
        	extensionAvps.add(shortcodeAvp);
        }
        try {
            if (extensionAvps.size() > 0) {
            	accountingRequest.setExtensionAvps(extensionAvps.toArray(new DiameterAvp[extensionAvps.size()]));
            }
        } catch (AvpNotAllowedException e) {
            ExceptionTraceHelper.traceException(getTracer(), "Failed to add Extension AVP(s) to CCR", e);
        }
    }
    
    @Override
    public String getFeatureName() {
        return NAME;
    }
}
