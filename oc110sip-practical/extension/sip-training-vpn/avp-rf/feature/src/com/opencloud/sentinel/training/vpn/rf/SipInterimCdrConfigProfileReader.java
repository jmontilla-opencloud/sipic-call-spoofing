/**
 * Copyright (c) 2014 Open Cloud Limited, a company incorporated in England and Wales (Registration Number 6000941) with its principal place of business at Edinburgh House, St John's Innovation Park, Cowley Road, Cambridge CB4 0DS.
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1  Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2  Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3  The name of the author may not be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * 4  The source code may not be used to create, develop, use or distribute software for use on any platform other than the Open Cloud Rhino and Open Cloud Rhino Sentinel platforms or any successor products.
 *
 * 5  Full license terms may be found https://developer.opencloud.com/devportal/display/OCDEV/Feature+Source+License
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE ARE EXCLUDED TO THE FULLEST EXTENT PERMITTED BY LAW.
 *
 * TO THE FULLEST EXTENT PERMISSIBLE BUY LAW, THE AUTHOR SHALL NOT BE LIABLE FOR ANY LOSS OF REVENUE, LOSS OF PROFIT, LOSS OF FUTURE BUSINESS, LOSS OF DATA OR ANY INDIRECT, SPECIAL, CONSEQUENTIAL, PUNITIVE OR OTHER LOSS OR DAMAGES ARISING OUT OF OR IN CONNECTION WITH THE SOFTWARE, WHETHER ARISING IN CONTRACT, TORT (INCLUDING NEGLIGENCE) MISREPRESENTATION OR OTHERWISE AND REGARDLESS OF WHETHER OPEN CLOUD HAS BEEN ADVISED OF THE POSSIBILITY OF ANY SUCH LOSS OR DAMAGE. THE AUTHORS MAXIMUM AGGREGATE LIABILITY WHETHER IN CONTRACT, TORT (INCLUDING NEGLIGENCE) OR OTHERWISE, SHALL NOT EXCEED EUR100.
 *
 * NOTHING IN THIS LICENSE SHALL LIMIT THE LIABILITY OF THE AUTHOR FOR DEATH OR PERSONAL INJURY RESULTING FROM NEGLIGENCE, FRAUD OR FRAUDULENT MISREPRESENTATION.
 *
 * Visit Open Cloud Developer's Portal for how-to guides, examples, documentation, forums and more: http://developer.opencloud.com
 */
package com.opencloud.sentinel.training.vpn.rf;

import com.opencloud.sce.fsmtool.Facilities;
import com.opencloud.sentinel.common.BaseConfigProfileReader;
import com.opencloud.sentinel.common.SentinelSelectionKey;
import com.opencloud.sentinel.feature.common.cdf.profiles.CdfDestinationProfileLocal;
import com.opencloud.sentinel.training.vpn.rf.profiles.SipInterimCdrProfile;
import com.opencloud.sentinel.training.vpn.rf.profiles.SipInterimCdrProfileLocal;

import javax.slee.profile.ProfileLocalObject;
import javax.slee.profile.ProfileTable;
import javax.slee.profile.UnrecognizedProfileTableNameException;


public class SipInterimCdrConfigProfileReader extends BaseConfigProfileReader<SipInterimCdrProfile> implements SipInterimCdrConfigReader {

    public SipInterimCdrConfigProfileReader(Facilities facilities) {
        super(facilities.getProfileFacility(), facilities.getTracer(), PROFILE_TABLE_NAME);
    }

    @Override
    public SipInterimCdrConfig getConfiguration(SentinelSelectionKey key) {
        if (null == key) {
            if (getTracer().isFineEnabled()) getTracer().fine("selectionKey is null");
            return null;
        }
        return findConfigurationProfile(key);
    }

    @Override
    public CdfDestinationConfig getCdfDestination(SentinelSelectionKey key) {
        if (null == key) {
            if (getTracer().isFineEnabled()) getTracer().fine("selectionKey is null");
            return null;
        }
        return findCdfDestinationConfig(key);
    }

    private SipInterimCdrConfig findConfigurationProfile(SentinelSelectionKey key) {
        final SipInterimCdrProfileLocal profile = findProfile(key);
        if (null == profile) {
            return null;
        }

        final SipInterimCdrConfig properties = new SipInterimCdrConfig() {

            @Override
            public long getInterimTimerPeriod() { return profile.getInterimTimerPeriod(); }

            @Override
            public boolean getWriteCdrOnSDPChange() {
                return profile.getWriteCdrOnSDPChange();
            }

            @Override
            public boolean getUseCdrRa() { return profile.getUseCdrRa(); }

            @Override
            public boolean getUseRfControlRa() { return profile.getUseRfControlRa(); }

            @Override
            public String toString() {
                return "SipInterimCdrProperties[profileName=" + profile.getProfileName() + ",interimTimerPeriod=" + profile.getInterimTimerPeriod()
                        + ",writeCdrOnSDPChange=" + profile.getWriteCdrOnSDPChange() + ",useCdrRa=" + profile.getUseCdrRa()
                        + ",useRfControlRa=" + profile.getUseRfControlRa() + "]";
            }
        };
        if (getTracer().isFinestEnabled()) getTracer().finest("Found configuration profile [" + properties.toString() + "]");
        return properties;
    }

    private CdfDestinationConfig findCdfDestinationConfig(SentinelSelectionKey key) {
        final CdfDestinationProfileLocal profile = findCdfDestinationProfile(key);
        if (null == profile) {
            return null;
        }

        final CdfDestinationConfig properties = new CdfDestinationConfig() {

            @Override
            public String getDestinationRealm() {
                return profile.getDestinationRealm();
            }

            @Override
            public String getDestinationHost() {
                return profile.getDestinationHost();
            }

            @Override
            public String toString() {
                return "CdfDestinationConfig[destinationRealm=" + profile.getDestinationRealm()
                        + ",destinationHost=" + profile.getDestinationHost() + "]";
            }
        };
        if (getTracer().isFinestEnabled()) getTracer().finest("Found CDF destination profile [" + properties.toString() + "]");
        return properties;
    }

    private SipInterimCdrProfileLocal findProfile(SentinelSelectionKey key) {
        final ProfileTable profileTable = getProfileTable();
        if (null == profileTable) {
            return null;
        }

        ProfileLocalObject profile = null;
        for (String configKey : key) {
            profile = profileTable.find(configKey);
            if (profile != null) {
                if (getTracer().isFinestEnabled()) getTracer().finest("Found profile [" + PROFILE_TABLE_NAME + "" + configKey + "]");
                break;
            }
        }
        if (profile == null) {
            if (getTracer().isSevereEnabled())
                getTracer().severe("Profile [" + PROFILE_TABLE_NAME + "" + key.toString() + "] not found :(");
            return null;
        }
        return (SipInterimCdrProfileLocal) profile;

    }

    private CdfDestinationProfileLocal findCdfDestinationProfile(SentinelSelectionKey key) {
        final ProfileTable profileTable = getCdfDestinationProfileTable();
        if (null == profileTable) {
            return null;
        }

        ProfileLocalObject profile = null;
        for (String configKey : key) {
            profile = profileTable.find(configKey);
            if (profile != null) {
                if (getTracer().isFinestEnabled()) getTracer().finest("Found profile [" + CDF_DESTINATION_PROFILE_TABLE_NAME + "/" + configKey + "]");
                break;
            }
        }
        if (profile == null) {
            if (getTracer().isFinestEnabled())
                getTracer().finest("Profile [" + CDF_DESTINATION_PROFILE_TABLE_NAME + "/" + key.toString() + "] not found");
            return null;
        }
        return (CdfDestinationProfileLocal) profile;
    }

    private ProfileTable getProfileTable() {
        try {
            return getProfileFacility().getProfileTable(PROFILE_TABLE_NAME);
        } catch (UnrecognizedProfileTableNameException e) {
            if (getTracer().isSevereEnabled()) getTracer().severe("Profile table [" + PROFILE_TABLE_NAME + "] not found :(", e);
        }
        return null;
    }

    private ProfileTable getCdfDestinationProfileTable() {
        try {
            return getProfileFacility().getProfileTable(CDF_DESTINATION_PROFILE_TABLE_NAME);
        } catch (UnrecognizedProfileTableNameException e) {
            if (getTracer().isFinestEnabled()) getTracer().finest("Profile table [" + CDF_DESTINATION_PROFILE_TABLE_NAME + "] not found", e);
        }
        return null;
    }

    private static final String PROFILE_TABLE_NAME = "SIPTrainingAvpRfProfileTable";
    private static final String CDF_DESTINATION_PROFILE_TABLE_NAME = "CdfDestinationProfileTable";

}
