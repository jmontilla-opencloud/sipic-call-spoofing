package com.opencloud.sentinel.training.vpn.rf;

import com.opencloud.sentinel.annotations.SessionStateInterface;
import com.opencloud.sentinel.feature.common.cdr.interim.sessionstate.SipInterimCdrSessionState;
import com.opencloud.sentinel.training.vpn.session.SIPTrainingVPNSessionState;

@SessionStateInterface()
public interface SipInterimVPNCdrSessionState extends SIPTrainingVPNSessionState,SipInterimCdrSessionState {

}
