/**
 * Copyright (c) 2014 Open Cloud Limited, a company incorporated in England and Wales (Registration Number 6000941) with its principal place of business at Edinburgh House, St John's Innovation Park, Cowley Road, Cambridge CB4 0DS.
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1  Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2  Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3  The name of the author may not be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * 4  The source code may not be used to create, develop, use or distribute software for use on any platform other than the Open Cloud Rhino and Open Cloud Rhino Sentinel platforms or any successor products.
 *
 * 5  Full license terms may be found https://developer.opencloud.com/devportal/display/OCDEV/Feature+Source+License
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE ARE EXCLUDED TO THE FULLEST EXTENT PERMITTED BY LAW.
 *
 * TO THE FULLEST EXTENT PERMISSIBLE BUY LAW, THE AUTHOR SHALL NOT BE LIABLE FOR ANY LOSS OF REVENUE, LOSS OF PROFIT, LOSS OF FUTURE BUSINESS, LOSS OF DATA OR ANY INDIRECT, SPECIAL, CONSEQUENTIAL, PUNITIVE OR OTHER LOSS OR DAMAGES ARISING OUT OF OR IN CONNECTION WITH THE SOFTWARE, WHETHER ARISING IN CONTRACT, TORT (INCLUDING NEGLIGENCE) MISREPRESENTATION OR OTHERWISE AND REGARDLESS OF WHETHER OPEN CLOUD HAS BEEN ADVISED OF THE POSSIBILITY OF ANY SUCH LOSS OR DAMAGE. THE AUTHORS MAXIMUM AGGREGATE LIABILITY WHETHER IN CONTRACT, TORT (INCLUDING NEGLIGENCE) OR OTHERWISE, SHALL NOT EXCEED EUR100.
 *
 * NOTHING IN THIS LICENSE SHALL LIMIT THE LIABILITY OF THE AUTHOR FOR DEATH OR PERSONAL INJURY RESULTING FROM NEGLIGENCE, FRAUD OR FRAUDULENT MISREPRESENTATION.
 *
 * Visit Open Cloud Developer's Portal for how-to guides, examples, documentation, forums and more: http://developer.opencloud.com
 */
package com.opencloud.sentinel.training.vpn.rf;

import com.opencloud.sentinel.feature.spi.SentinelFeatureStats;

public interface CDRStats extends SentinelFeatureStats {
    /**
     * CDR was successfully mapped and written via CDR RA
     */
    public void incrementCDRwritten(long delta);
    /**
     * Event CDR was successfully mapped and written via CDR RA
     */
    public void incrementEventCDRWritten(long delta);
    /**
     * Start CDR was successfully mapped and written via CDR RA
     */
    public void incrementStartCDRWritten(long delta);
    /**
     * Interim CDR was successfully mapped and written via CDR RA
     */
    public void incrementInterimCDRWritten(long delta);
    /**
     * End CDR was successfully mapped and written via CDR RA
     */
    public void incrementStopCDRWritten(long delta);
    /**
     * CDR could not be written due to mapping error, timeout or IO exception
     */
    public void incrementCDRWriteError(long delta);
    /**
     * Feature was triggered on an SDP change
     */
    public void incrementTriggeredOnSDPChange(long delta);
    /**
     * Feature was triggered on InterimCdrTimer fired
     */
    public void incrementTriggeredOnInterimCdrTimer(long delta);
    /**
     * No leg set in 'legForCdr' session state field
     */
    public void incrementNoLegForCdr(long delta);

    /**
     * rfControlActivity was lost from session state.
     *
     *  This can happen when Acct-Interim-Interval in ACA is more than twice the value in ACR, and the
     *  activity gets cleaned up by the liveness check while the call is still in progress.
     *
     *  incrementCdrNotSent() should also be called when this method is called.
     */
    void incrementRfControlActivityLost(long delta);

    /**
     * Rf Control RA session was resumed from a failed over session from another node
     * @param delta
     */
    void incrementRfControlResumedSession(long delta);
}
