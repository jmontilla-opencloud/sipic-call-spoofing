/**
 * Copyright (c) 2014 Open Cloud Limited, a company incorporated in England and Wales (Registration Number 6000941) with its principal place of business at Edinburgh House, St John's Innovation Park, Cowley Road, Cambridge CB4 0DS.
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1  Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2  Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3  The name of the author may not be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * 4  The source code may not be used to create, develop, use or distribute software for use on any platform other than the Open Cloud Rhino and Open Cloud Rhino Sentinel platforms or any successor products.
 *
 * 5  Full license terms may be found https://developer.opencloud.com/devportal/display/OCDEV/Feature+Source+License
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE ARE EXCLUDED TO THE FULLEST EXTENT PERMITTED BY LAW.
 *
 * TO THE FULLEST EXTENT PERMISSIBLE BUY LAW, THE AUTHOR SHALL NOT BE LIABLE FOR ANY LOSS OF REVENUE, LOSS OF PROFIT, LOSS OF FUTURE BUSINESS, LOSS OF DATA OR ANY INDIRECT, SPECIAL, CONSEQUENTIAL, PUNITIVE OR OTHER LOSS OR DAMAGES ARISING OUT OF OR IN CONNECTION WITH THE SOFTWARE, WHETHER ARISING IN CONTRACT, TORT (INCLUDING NEGLIGENCE) MISREPRESENTATION OR OTHERWISE AND REGARDLESS OF WHETHER OPEN CLOUD HAS BEEN ADVISED OF THE POSSIBILITY OF ANY SUCH LOSS OR DAMAGE. THE AUTHORS MAXIMUM AGGREGATE LIABILITY WHETHER IN CONTRACT, TORT (INCLUDING NEGLIGENCE) OR OTHERWISE, SHALL NOT EXCEED EUR100.
 *
 * NOTHING IN THIS LICENSE SHALL LIMIT THE LIABILITY OF THE AUTHOR FOR DEATH OR PERSONAL INJURY RESULTING FROM NEGLIGENCE, FRAUD OR FRAUDULENT MISREPRESENTATION.
 *
 * Visit Open Cloud Developer's Portal for how-to guides, examples, documentation, forums and more: http://developer.opencloud.com
 */
package com.opencloud.sentinel.training.vpn.rf;

import com.google.common.base.Strings;
import com.google.protobuf.Message;
import com.opencloud.cdrformat.AvpCdrFormat;
import com.opencloud.rhino.facilities.RhinoTimerOptions;
import com.opencloud.rhino.facilities.sessionownership.ConvergenceNameSessionOwnershipRecord;
import com.opencloud.rhino.slee.RhinoSbbContext;
import com.opencloud.sce.fsmtool.Facilities;
import com.opencloud.sentinel.charging.ChargingInstance;
import com.opencloud.sentinel.common.MccMnc;
import com.opencloud.sentinel.common.SentinelError;
import com.opencloud.sentinel.common.SentinelSelectionKey;
import com.opencloud.sentinel.feature.SipFeatureScriptExecutionPoint;
import com.opencloud.sentinel.feature.common.cdr.interim.sessionstate.SipInterimCdrSessionState;
import com.opencloud.sentinel.feature.common.cdr.util.SipAvpCdrUtil;
import com.opencloud.sentinel.feature.impl.BaseFeature;
import com.opencloud.sentinel.feature.spi.FeatureError;
import com.opencloud.sentinel.feature.spi.SentinelSipMultiLegFeatureEndpoint;
import com.opencloud.sentinel.feature.spi.init.InjectFeatureConfigurationReader;
import com.opencloud.sentinel.feature.spi.init.InjectFeatureStats;
import com.opencloud.sentinel.feature.spi.init.InjectResourceAdaptorProvider;
import com.opencloud.sentinel.multileg.Leg;
import com.opencloud.sentinel.multileg.SipMessageQueue;
import com.opencloud.sentinel.providers.ServiceTimerProvider;
import com.opencloud.sentinel.util.ExceptionTraceHelper;
import com.opencloud.sentinel.util.OpenCloudDiameter;
import com.opencloud.sentinel.util.SIPUtil;
import com.opencloud.slee.resource.rf_control.RfControlActivity;
import com.opencloud.slee.resource.rf_control.RfControlActivityContextInterfaceFactory;
import com.opencloud.slee.resource.rf_control.RfControlProvider;
import com.opencloud.slee.resources.cdr.CDRProvider;
import com.opencloud.slee.resources.cdr.WriteTimeoutException;
import org.jainslee.resources.diameter.base.AvpFilter;
import org.jainslee.resources.diameter.base.AvpNotAllowedException;
import org.jainslee.resources.diameter.base.DiameterAvp;
import org.jainslee.resources.diameter.base.DiameterMessageFactory;
import org.jainslee.resources.diameter.base.NoSuchAvpException;
import org.jainslee.resources.diameter.base.types.AccountingRecordType;
import org.jainslee.resources.diameter.base.types.DiameterIdentity;
import org.jainslee.resources.diameter.cca.types.SubscriptionId;
import org.jainslee.resources.diameter.cca.types.SubscriptionIdType;
import org.jainslee.resources.diameter.rf.RoToRfAvpFilter;
import org.jainslee.resources.diameter.cca.types.UserEquipmentInfo;
import org.jainslee.resources.diameter.rf.types.vcb0.AccountingRequest;
import org.jainslee.resources.diameter.rf.types.vcb0.PsInformation;
import org.jainslee.resources.diameter.rf.types.vcb0.ServiceInformation;
import org.jainslee.resources.diameter.rf.vcb0.RfMessageFactory;
import org.jainslee.resources.diameter.ro.RoProviderFactory;
import org.jainslee.resources.diameter.ro.types.vcb0.ImsInformation;
import org.jainslee.resources.diameter.ro.types.vcb0.MultipleServicesCreditControl;
import org.jainslee.resources.diameter.ro.vcb0.RoProvider;
import org.jainslee.resources.sip.SipRequest;

import javax.slee.ActivityContextInterface;
import javax.slee.facilities.TimerEvent;
import javax.slee.facilities.TimerID;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import static com.opencloud.sentinel.charging.SentinelSipAVPs.ACCOUNTING_RECORD_NUMBER_AVP;
import static com.opencloud.sentinel.charging.SentinelSipAVPs.ACCOUNTING_RECORD_TYPE_AVP;
import static com.opencloud.sentinel.charging.SentinelSipAVPs.OC_ACCESS_NETWORK_MCC_MNC_AVP;
import static com.opencloud.sentinel.charging.SentinelSipAVPs.OC_CALL_ID_AVP;
import static com.opencloud.sentinel.charging.SentinelSipAVPs.OC_CALL_TYPE_AVP;
import static com.opencloud.sentinel.charging.SentinelSipAVPs.OC_CHARGING_RESULT_AVP;
import static com.opencloud.sentinel.charging.SentinelSipAVPs.OC_END_SESSION_CAUSE_AVP;
import static com.opencloud.sentinel.charging.SentinelSipAVPs.OC_EVENT_ID_AVP;
import static com.opencloud.sentinel.charging.SentinelSipAVPs.OC_IMSI_MCC_MNC_AVP;
import static com.opencloud.sentinel.charging.SentinelSipAVPs.OC_INTERIM_CDR_LEG_AVP;
import static com.opencloud.sentinel.charging.SentinelSipAVPs.OC_INTERIM_CDR_REASON_AVP;
import static com.opencloud.sentinel.charging.SentinelSipAVPs.OC_INTERIM_CDR_SUPPLEMENTARY_REASON_AVP;
import static com.opencloud.sentinel.charging.SentinelSipAVPs.OC_INTERIM_CDR_TRIGGER_AVP;
import static com.opencloud.sentinel.charging.SentinelSipAVPs.OC_OCS_SESSION_ID_AVP;
import static com.opencloud.sentinel.charging.SentinelSipAVPs.OC_OCS_SESSION_TERMINATION_CAUSE_AVP;
import static com.opencloud.sentinel.charging.SentinelSipAVPs.OC_PLAY_ANNOUNCEMENT_ID_AVP;
import static com.opencloud.sentinel.charging.SentinelSipAVPs.OC_SENTINEL_ERROR_AVP;
import static com.opencloud.sentinel.charging.SentinelSipAVPs.OC_SENTINEL_SELECTION_KEY_AVP;
import static com.opencloud.sentinel.charging.SentinelSipAVPs.OC_SERVICE_TYPE_AVP;
import static com.opencloud.sentinel.charging.SentinelSipAVPs.OC_SESSION_END_TIME_AVP;
import static com.opencloud.sentinel.charging.SentinelSipAVPs.OC_SESSION_ESTABLISHED_TIME_AVP;
import static com.opencloud.sentinel.charging.SentinelSipAVPs.OC_SESSION_START_TIME_AVP;
import static com.opencloud.sentinel.charging.SentinelSipAVPs.OC_VISITED_NETWORK_MCC_MNC_AVP;
import static com.opencloud.sentinel.charging.SentinelSipAVPs.SERVICE_CONTEXT_AVP;
import static com.opencloud.sentinel.charging.SentinelSipAVPs.SESSION_ID_AVP;


public class BaseSipInterimCdrFeature extends BaseFeature<SipInterimVPNCdrSessionState, SentinelSipMultiLegFeatureEndpoint> implements InjectFeatureConfigurationReader<SipInterimCdrConfigReader>,
        InjectFeatureStats<CDRStats>, InjectResourceAdaptorProvider {

    public BaseSipInterimCdrFeature(SentinelSipMultiLegFeatureEndpoint caller, Facilities facilities, SipInterimVPNCdrSessionState sessionState) {
        super(caller, facilities, sessionState);
    }

    @Override
    public void startFeature(Object trigger, Object activity, ActivityContextInterface aci) {
        // This is just the base feature and cannot be run on its own
    }

    protected FeatureError doSipInterimCdrFeature(Object trigger, Object activity, ActivityContextInterface aci) {
        featureConfig = configReader.getConfiguration(getSessionState().getSentinelSelectionKey());
        if(featureConfig == null) {
            return new FeatureError(FeatureError.Cause.invalidConfiguration, "Unable to load feature configuration, no configuration found for: '" + getSessionState().getSentinelSelectionKey() + "'. Failed to update CDR");
        }

        final String legForCdrName = getSessionState().getLegForCdrs();
        if(Strings.isNullOrEmpty(legForCdrName)) {
            if (getTracer().isFineEnabled())
                getTracer().fine("'legForCdrs' not set in session state - not writing interim CDR");
            stats.incrementNoLegForCdr(1l);
            return null;
        }

        final Leg triggerLeg = getCaller().getLegManager().getLeg(aci);
        final Leg cdrLeg = getCaller().getLegManager().getLeg(legForCdrName);

        // Is the triggering leg the 'legForCdrs' leg, result is used to
        // avoid checking the pending message queue unnecessarily
        final boolean isTriggerLegCdrLeg = ((cdrLeg != null && triggerLeg != null)
                && (cdrLeg.getLegName().equals(triggerLeg.getLegName())));

        SipFeatureScriptExecutionPoint executionPoint = getSessionState().getCurrentSipFeatureExecutionPoint();
        String interimCdrSupplmentaryReason = getSessionState().getInterimCdrSupplementaryReason();
        List<InterimCDRTrigger> interimCdrTriggers = new LinkedList<>();
        switch (executionPoint) {
            case SipAccess_PartyRequest:
            case SipAccess_PartyResponse:
            case SipMidSession_PartyRequest:
            case SipMidSession_PartyResponse:
                // Avoid checking the outgoing message queue if the `legForCdrs`
                // is the triggeringLeg
                if ((isTriggerLegCdrLeg && isInitialInviteAck(cdrLeg, trigger)) ||
                    (!isTriggerLegCdrLeg && outgoingMessageQueueContainsInitialInviteAck(cdrLeg))) {
                    interimCdrTriggers.add(new InterimCDRTrigger(InterimCDRReason.ANSWERED, interimCdrSupplmentaryReason, legForCdrName));
                    getSessionState().setInterimCdrSupplementaryReason(null);
                } else  if (isConfiguredToWriteCdrOnSDPChange() && getSessionState().getNextCdrSequenceNumber() > 0) {
                        stats.incrementTriggeredOnSDPChange(1L);
                        interimCdrTriggers.add(new InterimCDRTrigger(InterimCDRReason.SDP, interimCdrSupplmentaryReason, legForCdrName));
                        getSessionState().setInterimCdrSupplementaryReason(null);
                }
                break;
            case SipAccess_ServiceTimer:
                if(isInterimCdrTimer(trigger)){
                    stats.incrementTriggeredOnInterimCdrTimer(1L);
                    interimCdrTriggers.add(new InterimCDRTrigger(InterimCDRReason.TIMER, interimCdrSupplmentaryReason, legForCdrName));
                    getSessionState().setInterimCdrSupplementaryReason(null);
                }
                break;
            // Charging
            case SipAccess_ChargingAbort:
            case SipAccess_ChargingReauth:
            case SipAccess_CreditAllocatedPostCC:
            case SipAccess_CreditLimitReachedPostCC:
            case SipAccess_ControlNotRequiredPostCC:
            case SipAccess_OCSFailurePostCC:
            case SipMidSession_ChargingAbort:
            case SipMidSession_ChargingReauth:
            case SipMidSession_CreditAllocatedPostCC:
            case SipMidSession_CreditLimitReachedPostCC:
            case SipMidSession_OCSFailurePostCC:
                // Check for outgoing ACKs on credit execution
                // points as 'legForCdrs' may have been resumed.
                if(outgoingMessageQueueContainsInitialInviteAck(cdrLeg)) {
                    interimCdrTriggers.add(new InterimCDRTrigger(InterimCDRReason.ANSWERED, interimCdrSupplmentaryReason, legForCdrName));
                    getSessionState().setInterimCdrSupplementaryReason(null);
                }
                break;
            case SipEndSession:
                // Write a CDR to capture charging information.
                interimCdrTriggers.add(new InterimCDRTrigger(InterimCDRReason.END, interimCdrSupplmentaryReason, legForCdrName));
                getSessionState().setInterimCdrSupplementaryReason(null);
                break;
            default:
                if(getTracer().isFinestEnabled()) {
                    getTracer().finest(getFeatureName() + " is running in an unsupported execution point");
                }
                break;
        }

        if(interimCdrTriggers.isEmpty()){
            if (getTracer().isFineEnabled()) getTracer().fine("Not writing a CDR");
        } else {
            performAccounting(interimCdrTriggers);
        }

        return null;
    }

    private void performAccounting(List<InterimCDRTrigger> interimCdrTriggers) {
        try {

            if (featureConfig.getUseCdrRa()) {
                getTracer().finest("Writing CDR via CDR RA");
                cdrProvider.writeCDR(createCdr(interimCdrTriggers));
            }

            SipInterimCdrConfig cdrConfig = configReader.getConfiguration(getSessionState().getSentinelSelectionKey());
            timerInterval = cdrConfig.getInterimTimerPeriod();

            if (featureConfig.getUseRfControlRa()) {
                getTracer().finest("Sending ACR via Rf Control RA");

                RfControlActivity rfControlActivity;
                ActivityContextInterface rfControlAci;

                final AccountingRecordType cdrType = determineCdrType();
                switch (cdrType.getValue()) {
                    case AccountingRecordType._EVENT_RECORD:
                        getTracer().finest("EVENT Record - using a one-off session");
                        rfControlActivity = rfControlProvider.newSession();
                        rfControlActivity.sendAccountingRequest(createAcr(interimCdrTriggers, rfControlActivity));
                        break;
                    case AccountingRecordType._START_RECORD:
                        getTracer().finest("START Record - creating a new session and attaching");
                        rfControlActivity = rfControlProvider.newSession();
                        rfControlAci = rfControlACIFactory.getActivityContextInterface(rfControlActivity);
                        getCaller().attach(rfControlAci);
                        getSessionState().setRfControlAci(rfControlAci);
                        getSessionState().setRfControlSessionId(rfControlActivity.getSessionId());
                        rfControlActivity.sendAccountingRequest(createAcr(interimCdrTriggers, rfControlActivity));
                        break;
                    case AccountingRecordType._INTERIM_RECORD:
                        getTracer().finest("INTERIM Record - using existing session");
                        rfControlActivity = getOrResumeRfControlActivity();
                        rfControlActivity.sendAccountingRequest(createAcr(interimCdrTriggers, rfControlActivity));
                        break;
                    case AccountingRecordType._STOP_RECORD:
                        getTracer().finest("STOP Record - using existing session and detaching");
                        rfControlActivity = getOrResumeRfControlActivity();
                        rfControlAci = getSessionState().getRfControlAci();
                        rfControlActivity.sendAccountingRequest(createAcr(interimCdrTriggers, rfControlActivity));
                        rfControlActivity.endSession();
                        getSessionState().setRfControlAci(null);
                        getCaller().detach(rfControlAci);
                        break;
                    default:
                        break;
                }
            }

            updateSessionStatePostSuccessfulWrite(cdrConfig);
        } catch (WriteTimeoutException e) {
            stats.incrementCDRWriteError(1);
            ExceptionTraceHelper.traceException(getTracer(), "Timed out writing CDR", e);
        } catch (IOException e) {
            stats.incrementCDRWriteError(1);
            ExceptionTraceHelper.traceException(getTracer(), "IOException writing CDR", e);
        }
    }

    private RfControlActivity getOrResumeRfControlActivity() {
        RfControlActivity rfControlActivity;
        ActivityContextInterface rfControlAci = getSessionState().getRfControlAci();
        if (rfControlAci == null) {
            // must be a session that has failed over from another node
            // create a new resumed Rf charging session from where we last left off
            rfControlActivity = rfControlProvider.newResumedSession(
                    getSessionState().getRfControlSessionId(),
                    getSessionState().getNextCdrSequenceNumber());
            rfControlAci = rfControlACIFactory.getActivityContextInterface(rfControlActivity);
            getCaller().attach(rfControlAci);
            getSessionState().setRfControlAci(rfControlAci);
            stats.incrementRfControlResumedSession(1);
        } else {
            rfControlActivity = (RfControlActivity) rfControlAci.getActivity();
        }
        return rfControlActivity;
    }

    @Override
    public String getFeatureName() {
        return null;
    }

    @Override
    public void injectFeatureConfigurationReader(SipInterimCdrConfigReader configurationReader) {
        this.configReader = configurationReader;

    }

    @Override
    public void injectFeatureStats(CDRStats stats) {
        this.stats = stats;
    }

    @Override
    public void injectResourceAdaptorProvider(Object provider) {
        if (provider instanceof RoProviderFactory) {
            this.roProviderFactory = (RoProviderFactory) provider;
        } else if (provider instanceof CDRProvider) {
            this.cdrProvider = (CDRProvider) provider;
        } else if (provider instanceof ServiceTimerProvider) {
            timerProvider = (ServiceTimerProvider) provider;
        } else if (provider instanceof RfControlProvider) {
            this.rfControlProvider = (RfControlProvider) provider;
        } else if (provider instanceof RfControlActivityContextInterfaceFactory) {
            this.rfControlACIFactory = (RfControlActivityContextInterfaceFactory) provider;
        } else {
            if (getFacilities().getTracer().isFineEnabled()) getFacilities().getTracer().fine("Warning! - Unexpected provider type injected!");
            if (getFacilities().getTracer().isFinerEnabled()) getFacilities().getTracer().finer("     Provider: " + provider);
        }
    }

    private void restartTimer(SipInterimCdrConfig cdrConfig) {
        // Cancel existing timer if it exists.
        TimerID timerID = getSessionState().getInterimCdrTimerID();
        if (timerID != null) {
            if (getTracer().isFinestEnabled()) getTracer().finest("Canceling the InterimCdrTimer with timerID=" + timerID);
            timerProvider.cancelTimer(timerID);
            getSessionState().setInterimCdrTimerID(null);
        }
        // Don't start the timer as the final (STOP RECORD) CDR is being written.
        if (getSessionState().getCurrentSipFeatureExecutionPoint().equals(SipFeatureScriptExecutionPoint.SipEndSession)){
            if (getTracer().isFinestEnabled()) getTracer().finest("Not setting InterimCdrTimer in SipEndSession ExecutionPoint.");
            return;
        }

        long timerInterval = 0;
        // respect the interval from the CDF, if non-zero
        ActivityContextInterface rfControlAci = getSessionState().getRfControlAci();
        if (rfControlAci != null) {
            RfControlActivity rfControlActivity = (RfControlActivity) rfControlAci.getActivity();
            if (rfControlActivity != null) {
                timerInterval = rfControlActivity.getInterimAccountingInterval();
            }
        }

        if(timerInterval == 0) {
            timerInterval = cdrConfig.getInterimTimerPeriod();

            if(timerInterval == 0) {
                if (getTracer().isFinestEnabled())
                    getTracer().finest("InterimCdrTimer not started - timer based InterimCdrs not configured (interimTimerPeriod=" + timerInterval + ")");
                return;
            }
        }

        long latestCdrWrittenTime = getSessionState().getLatestCdrWrittenTime();
        long expiryTime =  latestCdrWrittenTime + (timerInterval * 1000);
        // Start the timer
        RhinoTimerOptions timerOptions = new RhinoTimerOptions();
        if (getCaller().getLegManager().isSessionReplicationEnabled()) {
            timerOptions.setReplicationFactor(1); // TODO VOLTE-6524
            timerOptions.setConvergenceNameSessionOwnershipRecord(((RhinoSbbContext) getFacilities().getSbbContext()).getConvergenceNameSessionOwnershipRecord());
        }
        timerID = timerProvider.setTimer(expiryTime, timerOptions);
        getSessionState().setInterimCdrTimerID(timerID);
        if (getTracer().isFinestEnabled()) getTracer().finest("InterimCdrTimer started with timerID=" + timerID + ", duration=" + (timerInterval*1000) + "ms.");
    }

    private boolean isInterimCdrTimer(Object trigger){
        if (!(trigger instanceof TimerEvent)) return false;
        final TimerEvent timerEvent = (TimerEvent) trigger;
        return timerEvent.getTimerID().equals(getSessionState().getInterimCdrTimerID());
    }


    protected RoProviderFactory getRoProviderFactory() {
        return roProviderFactory;
    }

    protected RfControlProvider getRfControlProvider() {
        return rfControlProvider;
    }

    private boolean isConfiguredToWriteCdrOnSDPChange() {
        SipInterimCdrConfig cdrConfig = configReader.getConfiguration(getSessionState().getSentinelSelectionKey());
        // Service is configured to run on SDP change and SDPComparison feature has determined a Cdr should be written
        return (cdrConfig.getWriteCdrOnSDPChange() && getSessionState().getWriteCdrOnSDPChange());
    }

    private boolean outgoingMessageQueueContainsInitialInviteAck(Leg leg) {

        if(leg == null) {
            return false;
        }

        if(getTracer().isFinestEnabled())
            getTracer().finest("Checking messagesToSendQueue for leg '" + leg.getLegName() + "' for initial INVITE ACK");

        SipMessageQueue outgoingMessageQueue = leg.getMessagesToSend();
        if(leg.isSuspended() || outgoingMessageQueue.isEmpty()) {
            if(getTracer().isFinestEnabled())
                getTracer().finest("Found leg '" + leg.getLegName() + "' isSuspended=" + leg.isSuspended()
                        + " messagesToSendQueue isEmpty=" + outgoingMessageQueue.isEmpty());
            return false;
        }
        return isInitialInviteAck(leg, outgoingMessageQueue.peek());
    }

    private boolean isInitialInviteAck(Leg leg, Object trigger) {
        if(!(trigger instanceof SipRequest)) {
            return false;
        }

        if(getTracer().isFineEnabled())
            getTracer().fine("Checking if request is initial INVITE ACK");

        SipRequest sipRequest = (SipRequest) trigger;
        if(!sipRequest.getMethod().equals(SipRequest.ACK)) {
            if(getTracer().isFineEnabled())
                getTracer().fine("Request is not an ACK");
            return false;
        }

        SipRequest inviteRequest = leg.getInviteRequest();
        if(null == inviteRequest) {
            if(getTracer().isFinerEnabled())
                getTracer().finer("No invite request found on leg '" + leg.getLegName() + "' - ACK may not be for an initial Invite request");
            return false;
        }

        if(!SIPUtil.isInitialSipRequest(inviteRequest)){
            if(getTracer().isFineEnabled())
                getTracer().fine("Request is a Re-INVITE ACK");
            return false;
        }

        long inviteSeqNumber = leg.getInviteRequest().getSequenceNumber();
        if(inviteSeqNumber != sipRequest.getSequenceNumber()) {
            if(getTracer().isFineEnabled())
                getTracer().fine("Request is not initial INVITE ACK, sequence number does not match");
            return false;

        }

        if(getTracer().isFinestEnabled())
            getTracer().finest("Request is initial INVITE ACK");

        return true;
    }

    private AccountingRequest createAcr(List<InterimCDRTrigger> interimCdrTriggers, RfControlActivity rfControlActivity) {
        final String chargableLeg = getSessionState().getLegForCdrs();
        final RfControlProvider rfControlProvider = getRfControlProvider();
        final RoProvider roProvider = getRoProviderFactory().getRoProviderVcb0().getRoProviderVcb0();

        final RfMessageFactory rfMessageFactory = rfControlProvider.getRfMessageFactory();
        final DiameterMessageFactory messageFactory = rfMessageFactory.getBaseMessageFactory();

        /*
         * Standard AVPs
         */

        // Accounting-Record-Type
        final AccountingRecordType accountingRecordType = determineCdrType();
        final AccountingRequest acr = rfMessageFactory.createRfAccountingRequest(accountingRecordType);

        // Accounting-Record-Number
        acr.setAccountingRecordNumber(getSessionState().getNextCdrSequenceNumber());

        // Acct-Interim-Interval
        if(rfControlActivity.getInterimAccountingInterval() > 0) {
            timerInterval = rfControlActivity.getInterimAccountingInterval();
        }
        if(timerInterval > 0)
            acr.setAcctInterimInterval(timerInterval);

        // Service-Information
        final ServiceInformation serviceInformation = rfMessageFactory.createServiceInformation();
        acr.setServiceInformation(serviceInformation);

        // Subscription-Id
        if (getSessionState().getSubscriptionIdType()!= null && getSessionState().getSubscriptionId() != null) {
            serviceInformation.setSubscriptionId(rfMessageFactory.createSubscriptionId(getSessionState().getSubscriptionIdType(), getSessionState().getSubscriptionId()));
        }

        // IMS-Information
        final ImsInformation imsInformation = SipAvpCdrUtil.buildImsInformation(getSessionState(), isInitialRecord(accountingRecordType), isTerminatingRecord(accountingRecordType), getTracer());

        try {
            final org.jainslee.resources.diameter.rf.types.vcb0.ImsInformation rfImsInformation =
                    (org.jainslee.resources.diameter.rf.types.vcb0.ImsInformation)
                    rfControlProvider.convertRoAvpToRfAvp(imsInformation, getRoToRfAvpFilter());
            serviceInformation.setImsInformation(rfImsInformation);
        } catch (AvpNotAllowedException | NoSuchAvpException e) {
            ExceptionTraceHelper.traceException(getTracer(), "Unable to convert IMS-Information AVP to Rf", e);
        }

        // PS-Information
        // Only add this if UserEquipmentInfo is present
        if (getSessionState().getUserEquipmentInfo() != null) {
            PsInformation psInformation = rfMessageFactory.createPsInformation();
            UserEquipmentInfo userEquipmentInfo = rfMessageFactory.createUserEquipmentInfo(getSessionState().getUserEquipmentInfo().getUserEquipmentInfoType(), getSessionState().getUserEquipmentInfo().getUserEquipmentInfoValue());
            psInformation.setUserEquipmentInfo(userEquipmentInfo);
            serviceInformation.setPsInformation(psInformation);
        }

        ArrayList<DiameterAvp> extensionAvps = (acr.getExtensionAvps() == null) ?
            new ArrayList<DiameterAvp>() :
            new ArrayList<DiameterAvp>(Arrays.asList(acr.getExtensionAvps()));

        // Maintenance Note: MCC MNC inclusion logic is partially duplicated in {@link #createCdr(List<InterimCdrTriggers>)}
        if (getSessionState().getPaniMccsMncs() != null && !getSessionState().getPaniMccsMncs().isEmpty()) {
            for (MccMnc mccMnc : getSessionState().getPaniMccsMncs()) {
                DiameterAvp diameterAvp = SipAvpCdrUtil.createOcMccMncAvp(messageFactory, OC_ACCESS_NETWORK_MCC_MNC_AVP.getCode(), OC_ACCESS_NETWORK_MCC_MNC_AVP.getName(), mccMnc, getTracer());
                if (diameterAvp != null)
                    extensionAvps.add(diameterAvp);
            }
        }

        if (getSessionState().getPvniMccMnc() != null) {
            DiameterAvp diameterAvp = SipAvpCdrUtil.createOcMccMncAvp(messageFactory, OC_VISITED_NETWORK_MCC_MNC_AVP.getCode(), OC_VISITED_NETWORK_MCC_MNC_AVP.getName(), getSessionState().getPvniMccMnc(), getTracer());
            if (diameterAvp != null)
                extensionAvps.add(diameterAvp);
        }

        if (getSessionState().getImsiMccMnc() != null) {
            DiameterAvp diameterAvp = SipAvpCdrUtil.createOcMccMncAvp(messageFactory, OC_IMSI_MCC_MNC_AVP.getCode(), OC_IMSI_MCC_MNC_AVP.getName(), getSessionState().getImsiMccMnc(), getTracer());
            if (diameterAvp != null)
                extensionAvps.add(diameterAvp);
        }

        /*
         * Ro Charging Result
         */
        DiameterAvp diameterAvp = SipAvpCdrUtil.createCustomAvp(messageFactory, OC_CHARGING_RESULT_AVP.getCode(), OC_CHARGING_RESULT_AVP.getName(), getSessionState().getChargingResult(), getTracer());
        if (diameterAvp != null)
            if (getTracer().isFinestEnabled()) getTracer().finest("Adding OC-Charging-Result AVP to ACR");
            extensionAvps.add(diameterAvp);

        try {
            if (extensionAvps.size() > 0) {
                acr.setExtensionAvps(extensionAvps.toArray(new DiameterAvp[extensionAvps.size()]));
            }
        } catch (AvpNotAllowedException e) {
            ExceptionTraceHelper.traceException(getTracer(), "Failed to add Extension AVP(s) to CCR", e);
        }


        // Service Context Id
        if (getSessionState().getDiameterServiceContextId() != null && !getSessionState().getDiameterServiceContextId().isEmpty()) {
            acr.setServiceContextId(getSessionState().getDiameterServiceContextId());
        }

        /*
         * Destination Realm/Host
         */

        final CdfDestinationConfig cdfDestination = configReader.getCdfDestination(getSessionState().getSentinelSelectionKey());
        if (cdfDestination != null) {
            final String destinationRealm = cdfDestination.getDestinationRealm();
            if (destinationRealm != null) {
                acr.setDestinationRealm(new DiameterIdentity(destinationRealm));
            }

            final String destinationHost = cdfDestination.getDestinationHost();
            if(destinationHost != null) {
                acr.setDestinationHost(new DiameterIdentity(destinationHost));
            }
        }



        extendRfAcr(acr, rfMessageFactory);

        return acr;
    }

    private Message createCdr(List<InterimCDRTrigger> interimCdrTriggers) {

        final String chargableLeg = getSessionState().getLegForCdrs();
        final AvpCdrFormat.AvpCdr.Builder cdrBuilder = AvpCdrFormat.AvpCdr.newBuilder();

        final RoProvider roProvider = getRoProviderFactory().getRoProviderVcb0().getRoProviderVcb0();
        final DiameterMessageFactory messageFactory = roProvider.getBaseProvider().getDiameterMessageFactory();

        /*
         * Standard AVPs
         */

        final AccountingRecordType accountingRecordType = determineCdrType();

        // Accounting-Record-Type
        try {
            DiameterAvp accountingRecordTypeAvp = messageFactory.createAvp(ACCOUNTING_RECORD_TYPE_AVP.getCode(), accountingRecordType);
            SipAvpCdrUtil.addAvp(cdrBuilder, messageFactory, accountingRecordTypeAvp, getTracer());
        } catch (NoSuchAvpException e) {
            ExceptionTraceHelper.traceException(getTracer(), "Unable to create Accounting-Record-Type AVP", e);
        }

        // Accounting-Record-Number
        try {
            DiameterAvp accountingRecordNumber = messageFactory.createAvp(ACCOUNTING_RECORD_NUMBER_AVP.getCode(), getSessionState().getNextCdrSequenceNumber());
            SipAvpCdrUtil.addAvp(cdrBuilder, messageFactory, accountingRecordNumber, getTracer());
        } catch (NoSuchAvpException e) {
            ExceptionTraceHelper.traceException(getTracer(), "Unable to create Accounting-Record-Number AVP", e);
        }

        // Subscription-Id
        if (getSessionState().getSubscriptionId() != null) {
            SubscriptionId subscriptionId = roProvider.getRoMessageFactory().createSubscriptionId();

            subscriptionId.setSubscriptionIdData(getSessionState().getSubscriptionId());
            subscriptionId.setSubscriptionIdType(getSessionState().getSubscriptionIdType());

            SipAvpCdrUtil.addAvp(cdrBuilder, messageFactory, subscriptionId, getTracer());
        }

        // IMS-Information
        final ImsInformation imsInformation = SipAvpCdrUtil.buildImsInformation(getSessionState(), isInitialRecord(accountingRecordType), isTerminatingRecord(accountingRecordType), getTracer());

        SipAvpCdrUtil.addAvp(cdrBuilder, messageFactory, imsInformation, getTracer());

        // Service Context Id
        if (getSessionState().getDiameterServiceContextId() != null && !getSessionState().getDiameterServiceContextId().isEmpty()) {
            try {
                DiameterAvp serviceContextId = messageFactory.createAvp(SERVICE_CONTEXT_AVP.getCode(), getSessionState().getDiameterServiceContextId());
                SipAvpCdrUtil.addAvp(cdrBuilder, messageFactory, serviceContextId, getTracer());
            }
            catch (NoSuchAvpException e) {
                ExceptionTraceHelper.traceException(getTracer(), "Unable to create Service-Context-Id AVP", e);
            }
        }

        /*
         * Custom AVPs
         */

        // Maintenance Note: MCC MNC inclusion logic is partially duplicated in {@link #createAcr(List<InterimCdrTriggers>, RfControlActivity)}
        if (getSessionState().getPaniMccsMncs() != null && !getSessionState().getPaniMccsMncs().isEmpty()) {
            for (MccMnc mccMnc : getSessionState().getPaniMccsMncs()) {
                DiameterAvp diameterAvp = SipAvpCdrUtil.createOcMccMncAvp(messageFactory, OC_ACCESS_NETWORK_MCC_MNC_AVP.getCode(), OC_ACCESS_NETWORK_MCC_MNC_AVP.getName(), mccMnc, getTracer());
                if (diameterAvp != null)
                    SipAvpCdrUtil.addAvp(cdrBuilder, messageFactory, diameterAvp, getTracer());
            }
        }

        if (getSessionState().getPvniMccMnc() != null) {
            DiameterAvp diameterAvp = SipAvpCdrUtil.createOcMccMncAvp(messageFactory, OC_VISITED_NETWORK_MCC_MNC_AVP.getCode(), OC_VISITED_NETWORK_MCC_MNC_AVP.getName(), getSessionState().getPvniMccMnc(), getTracer());
            if (diameterAvp != null)
                SipAvpCdrUtil.addAvp(cdrBuilder, messageFactory, diameterAvp, getTracer());
        }

        if (getSessionState().getImsiMccMnc() != null) {
            DiameterAvp diameterAvp = SipAvpCdrUtil.createOcMccMncAvp(messageFactory, OC_IMSI_MCC_MNC_AVP.getCode(), OC_IMSI_MCC_MNC_AVP.getName(), getSessionState().getImsiMccMnc(), getTracer());
            if (diameterAvp != null)
                SipAvpCdrUtil.addAvp(cdrBuilder, messageFactory, diameterAvp, getTracer());

        }



        // OC-Session-Start-Time
        if (getSessionState().getSessionInitiated() > 0) {
            SipAvpCdrUtil.addCustomAvp(cdrBuilder, messageFactory, OC_SESSION_START_TIME_AVP.getCode(), OC_SESSION_START_TIME_AVP.getName(), new Date(getSessionState().getSessionInitiated()), getTracer());
        }

        // OC-Session-Established-Time
        if (getSessionState().getSessionEstablished() > 0) {
            SipAvpCdrUtil.addCustomAvp(cdrBuilder, messageFactory, OC_SESSION_ESTABLISHED_TIME_AVP.getCode(), OC_SESSION_ESTABLISHED_TIME_AVP.getName(), new Date(getSessionState().getSessionEstablished()), getTracer());
        }

        // OC-Session-End-Time
        if (isTerminatingRecord(accountingRecordType)) {
            final long endTime = getSessionState().getSessionEnded();
            if (endTime == 0) {
                SipAvpCdrUtil.addCustomAvp(cdrBuilder, messageFactory, OC_SESSION_END_TIME_AVP.getCode(), OC_SESSION_END_TIME_AVP.getName(), new Date(System.currentTimeMillis()), getTracer());
            } else {
                SipAvpCdrUtil.addCustomAvp(cdrBuilder, messageFactory, OC_SESSION_END_TIME_AVP.getCode(), OC_SESSION_END_TIME_AVP.getName(), new Date(endTime), getTracer());
            }
        }

        // Interim-CDR-Trigger
        for (InterimCDRTrigger entry : interimCdrTriggers) {
            DiameterAvp reasonAvp = SipAvpCdrUtil.createCustomAvp(messageFactory, OC_INTERIM_CDR_REASON_AVP.getCode(), OC_INTERIM_CDR_REASON_AVP.getName(),  entry.getReason().name(), getTracer());

            String legName = entry.getLegName();
            if(Strings.isNullOrEmpty(legName)) {
                SipAvpCdrUtil.addCustomAvp(cdrBuilder, messageFactory, OC_INTERIM_CDR_TRIGGER_AVP.getCode(), OC_INTERIM_CDR_TRIGGER_AVP.getName(), new DiameterAvp[]{reasonAvp}, getTracer());
                continue;
            }

            DiameterAvp legAvp = SipAvpCdrUtil.createCustomAvp(messageFactory, OC_INTERIM_CDR_LEG_AVP.getCode(), OC_INTERIM_CDR_LEG_AVP.getName(), legName, getTracer());

            if (entry.getSupplementaryReason() != null ){
                DiameterAvp supplementaryReasonAvp = SipAvpCdrUtil.createCustomAvp(messageFactory, OC_INTERIM_CDR_SUPPLEMENTARY_REASON_AVP.getCode(), OC_INTERIM_CDR_SUPPLEMENTARY_REASON_AVP.getName(), entry.getSupplementaryReason(), getTracer());
                SipAvpCdrUtil.addCustomAvp(cdrBuilder, messageFactory, OC_INTERIM_CDR_TRIGGER_AVP.getCode(), OC_INTERIM_CDR_TRIGGER_AVP.getName(),  new DiameterAvp[] { reasonAvp, supplementaryReasonAvp, legAvp }, getTracer());
            } else
                SipAvpCdrUtil.addCustomAvp(cdrBuilder, messageFactory, OC_INTERIM_CDR_TRIGGER_AVP.getCode(), OC_INTERIM_CDR_TRIGGER_AVP.getName(),  new DiameterAvp[] { reasonAvp, legAvp }, getTracer());
        }

        // Selection Key
        final SentinelSelectionKey selectionKey = getSessionState().getSentinelSelectionKey();
        SipAvpCdrUtil.addCustomAvp(cdrBuilder, messageFactory, OC_SENTINEL_SELECTION_KEY_AVP.getCode(), OC_SENTINEL_SELECTION_KEY_AVP.getName(), selectionKey.asString(), getTracer());

        // Announcement IDs
        if (getSessionState().getPlayedAnnouncementIDs() != null && getSessionState().getPlayedAnnouncementIDs().length > 0) {
            for (int id : getSessionState().getPlayedAnnouncementIDs()) {
                SipAvpCdrUtil.addCustomAvp(cdrBuilder, messageFactory, OC_PLAY_ANNOUNCEMENT_ID_AVP.getCode(), OC_PLAY_ANNOUNCEMENT_ID_AVP.getName(), id, getTracer());
            }
        }

        // Call Type
        if (getSessionState().getCallType() != null) {
            switch (getSessionState().getCallType()) {
                case MobileTerminating:
                    SipAvpCdrUtil.addCustomAvp(cdrBuilder, messageFactory, OC_CALL_TYPE_AVP.getCode(), OC_CALL_TYPE_AVP.getName(), SipAvpCdrUtil.CallType.MTC, getTracer());
                    break;
                case MobileOriginating:
                    SipAvpCdrUtil.addCustomAvp(cdrBuilder, messageFactory, OC_CALL_TYPE_AVP.getCode(), OC_CALL_TYPE_AVP.getName(), SipAvpCdrUtil.CallType.MOC, getTracer());
                    break;
                case MobileForwarded:
                    SipAvpCdrUtil.addCustomAvp(cdrBuilder, messageFactory, OC_CALL_TYPE_AVP.getCode(), OC_CALL_TYPE_AVP.getName(), SipAvpCdrUtil.CallType.MFC, getTracer());
                    break;
                case NetworkInitiated:
                    SipAvpCdrUtil.addCustomAvp(cdrBuilder, messageFactory, OC_CALL_TYPE_AVP.getCode(), OC_CALL_TYPE_AVP.getName(), SipAvpCdrUtil.CallType.MOC_3RDPTY, getTracer());
                    break;
                case EmergencyCall:
                    SipAvpCdrUtil.addCustomAvp(cdrBuilder, messageFactory, OC_CALL_TYPE_AVP.getCode(), OC_CALL_TYPE_AVP.getName(), SipAvpCdrUtil.CallType.EMERGENCY_CALL, getTracer());
                    break;
            }
        }

        // Service Type
        if (null != getSessionState().getSipServiceType()) {
            switch(getSessionState().getSipServiceType()) {
                case SipCall:
                    SipAvpCdrUtil.addCustomAvp(cdrBuilder, messageFactory, OC_SERVICE_TYPE_AVP.getCode(), OC_SERVICE_TYPE_AVP.getName(), SipAvpCdrUtil.SipServiceType.SipCall, getTracer());
                    break;
                case Message:
                    SipAvpCdrUtil.addCustomAvp(cdrBuilder, messageFactory, OC_SERVICE_TYPE_AVP.getCode(), OC_SERVICE_TYPE_AVP.getName(), SipAvpCdrUtil.SipServiceType.Message, getTracer());
                    break;
                case Subscription:
                    SipAvpCdrUtil.addCustomAvp(cdrBuilder, messageFactory, OC_SERVICE_TYPE_AVP.getCode(), OC_SERVICE_TYPE_AVP.getName(), SipAvpCdrUtil.SipServiceType.Subscription, getTracer());
                    break;
                default:
                    SipAvpCdrUtil.addCustomAvp(cdrBuilder, messageFactory, OC_SERVICE_TYPE_AVP.getCode(), OC_SERVICE_TYPE_AVP.getName(), SipAvpCdrUtil.SipServiceType.Unknown, getTracer());
                    break;
            }
        } else {
            SipAvpCdrUtil.addCustomAvp(cdrBuilder, messageFactory, OC_SERVICE_TYPE_AVP.getCode(), OC_SERVICE_TYPE_AVP.getName(), SipAvpCdrUtil.SipServiceType.Unknown, getTracer());
        }

        // Charging result
        // The Result-Code AVP only exists in ACA/CCA messages, so we have to use a custom AVP here
        SipAvpCdrUtil.addCustomAvp(cdrBuilder, messageFactory, OC_CHARGING_RESULT_AVP.getCode(), OC_CHARGING_RESULT_AVP.getName(), getSessionState().getChargingResult(), getTracer());

        // Write list of all OCS sessions that have been opened to the
        // OCS in this session to the CDR
        final String[] ocsSessionIds = getSessionState().getOcsSessionIds();
        if (ocsSessionIds != null) {
            for (String sessionId : ocsSessionIds) {
                SipAvpCdrUtil.addCustomAvp(cdrBuilder, messageFactory, OC_OCS_SESSION_ID_AVP.getCode(), OC_OCS_SESSION_ID_AVP.getName(), sessionId, getTracer());
            }

            // Write the session ID of the first OCS session as the Session-ID AVP
            if (ocsSessionIds.length > 0) {
                try {
                    String firstSessionId = ocsSessionIds[0];
                    DiameterAvp sessionID = messageFactory.createAvp(SESSION_ID_AVP.getCode(), firstSessionId);
                    SipAvpCdrUtil.addAvp(cdrBuilder, messageFactory, sessionID, getTracer());
                } catch (NoSuchAvpException e) {
                    ExceptionTraceHelper.traceException(getTracer(), "Unable to create Session-Id AVP", e);
                }
            }
        }

        // OCS session termination cause
        if (getSessionState().getOcsSessionTerminationCause() != null && SipAvpCdrUtil.isUsingUnitReservationCharging(getCaller().getChargingManager(), getTracer())) {
            SipAvpCdrUtil.addCustomAvp(cdrBuilder, messageFactory, OC_OCS_SESSION_TERMINATION_CAUSE_AVP.getCode(), OC_OCS_SESSION_TERMINATION_CAUSE_AVP.getName(),
                    getSessionState().getOcsSessionTerminationCause(), getTracer());
        }


        // Sentinel Error
        if (getSessionState().getLatestOcsAnswer() != null
                && OpenCloudDiameter.ccaHasOpenCloudResult(getSessionState().getLatestOcsAnswer())) {

            int resultCode = OpenCloudDiameter.getOpenCloudResult(getSessionState().getLatestOcsAnswer());

            SentinelError error = SentinelError.fromValue(resultCode);

            if (error == null)
                SipAvpCdrUtil.addCustomAvp(cdrBuilder, messageFactory, OC_SENTINEL_ERROR_AVP.getCode(), OC_SENTINEL_ERROR_AVP.getName(),
                        SipAvpCdrUtil.SentinelErrorAvpValues.OtherError, getTracer());
            else
                switch(error) {
                    case ocsTimeout:
                        SipAvpCdrUtil.addCustomAvp(cdrBuilder, messageFactory, OC_SENTINEL_ERROR_AVP.getCode(), OC_SENTINEL_ERROR_AVP.getName(),
                                SipAvpCdrUtil.SentinelErrorAvpValues.OcsTimeout, getTracer());
                        break;

                    case ocsCommunicationFailure:
                        SipAvpCdrUtil.addCustomAvp(cdrBuilder, messageFactory, OC_SENTINEL_ERROR_AVP.getCode(), OC_SENTINEL_ERROR_AVP.getName(),
                                SipAvpCdrUtil.SentinelErrorAvpValues.OcsCommunicationFailure, getTracer());
                        break;

                    case sentinelOverload:
                        SipAvpCdrUtil.addCustomAvp(cdrBuilder, messageFactory, OC_SENTINEL_ERROR_AVP.getCode(), OC_SENTINEL_ERROR_AVP.getName(),
                                SipAvpCdrUtil.SentinelErrorAvpValues.SentinelOverload, getTracer());
                        break;

                    case protocolError:
                        SipAvpCdrUtil.addCustomAvp(cdrBuilder, messageFactory, OC_SENTINEL_ERROR_AVP.getCode(), OC_SENTINEL_ERROR_AVP.getName(),
                                SipAvpCdrUtil.SentinelErrorAvpValues.ProtocolError, getTracer());
                        break;

                    case internalError:
                        SipAvpCdrUtil.addCustomAvp(cdrBuilder, messageFactory, OC_SENTINEL_ERROR_AVP.getCode(), OC_SENTINEL_ERROR_AVP.getName(),
                                SipAvpCdrUtil.SentinelErrorAvpValues.InternalError, getTracer());
                        break;

                    case mappingError:
                        SipAvpCdrUtil.addCustomAvp(cdrBuilder, messageFactory, OC_SENTINEL_ERROR_AVP.getCode(), OC_SENTINEL_ERROR_AVP.getName(),
                                SipAvpCdrUtil.SentinelErrorAvpValues.MappingError, getTracer());
                        break;

                    default:
                        SipAvpCdrUtil.addCustomAvp(cdrBuilder, messageFactory, OC_SENTINEL_ERROR_AVP.getCode(), OC_SENTINEL_ERROR_AVP.getName(),
                                SipAvpCdrUtil.SentinelErrorAvpValues.OtherError, getTracer());
                        break;
                }
        } else {
            SipAvpCdrUtil.addCustomAvp(cdrBuilder, messageFactory, OC_SENTINEL_ERROR_AVP.getCode(), OC_SENTINEL_ERROR_AVP.getName(), SipAvpCdrUtil.SentinelErrorAvpValues.None, getTracer());
        }

        // MultipleServicesCreditControl AVPs from final OCS CCA
        if (getSessionState().getLatestOcsAnswer() != null) {
            final MultipleServicesCreditControl[] msccs =
                    getSessionState().getLatestOcsAnswer().getMultipleServicesCreditControls();
            if (msccs != null) {
                for (MultipleServicesCreditControl mscc : msccs) {
                    SipAvpCdrUtil.addAvp(cdrBuilder, messageFactory, mscc, getTracer());
                }
            }
        }

        // Charging instances
        final Collection<ChargingInstance> chargingInstances =
                getCaller().getChargingManager().getChargingInstances();

        if (chargingInstances != null)
            for (ChargingInstance chargingInstance : chargingInstances)
                SipAvpCdrUtil.addChargingInstance(cdrBuilder, messageFactory, chargingInstance, getTracer());

        // Event Id
        if (getSessionState().getEventId() != null)
            SipAvpCdrUtil.addCustomAvp(cdrBuilder, messageFactory, OC_EVENT_ID_AVP.getCode(), OC_EVENT_ID_AVP.getName(), getSessionState().getEventId(), getTracer());

        // Call Id
        if (getSessionState().getCallId() != null)
            SipAvpCdrUtil.addCustomAvp(cdrBuilder, messageFactory, OC_CALL_ID_AVP.getCode(), OC_CALL_ID_AVP.getName(), getSessionState().getCallId(), getTracer());

        // End Session Cause
        if (getSessionState().getEndSessionCause()!=null)
            SipAvpCdrUtil.addCustomAvp(cdrBuilder, messageFactory, OC_END_SESSION_CAUSE_AVP.getCode(), OC_END_SESSION_CAUSE_AVP.getName(), getSessionState().getEndSessionCause(), getTracer());


        if (getSessionState().getUserEquipmentInfo() != null)

            SipAvpCdrUtil.addAvp(cdrBuilder,messageFactory, getSessionState().getUserEquipmentInfo(), getTracer());

        extendSipCdr(cdrBuilder, messageFactory);

        return cdrBuilder.build();
    }

    protected void extendSipCdr(AvpCdrFormat.AvpCdr.Builder cdrBuilder, DiameterMessageFactory messageFactory) {
        // No-op.
        // Intended as extension point for downstream features which just need to add a few AVPs (e.g. VoLTE Interim CDR Feature)
    }

    protected void extendRfAcr(AccountingRequest accountingRequest, RfMessageFactory messageFactory) {
        // No-op.
        // Intended as extension point for downstream features which just need to add a few AVPs (e.g. VoLTE Interim CDR Feature)  	
    }

    private void updateSessionStatePostSuccessfulWrite(SipInterimCdrConfig cdrConfig) {
        getSessionState().setLatestCdrWrittenTime(System.currentTimeMillis());
        restartTimer(cdrConfig);
        AccountingRecordType cdrType = determineCdrType();
        // Increment stat to indicate overall CDRs written
        stats.incrementCDRwritten(1L);
        switch (cdrType.getValue()) {
            case AccountingRecordType._EVENT_RECORD:
                stats.incrementEventCDRWritten(1L);
                break;
            case AccountingRecordType._START_RECORD:
                stats.incrementStartCDRWritten(1L);
                break;
            case AccountingRecordType._INTERIM_RECORD:
                stats.incrementInterimCDRWritten(1L);
                break;
            case AccountingRecordType._STOP_RECORD:
                stats.incrementStopCDRWritten(1L);
                break;
            default:
                break;
        }

        // Increment this last, as the previous value is required to determine the AccountingRecordType
        getSessionState().setNextCdrSequenceNumber(getSessionState().getNextCdrSequenceNumber() + 1);
        // Reset the session state field so we don't write duplicate CDRs
        getSessionState().setWriteCdrOnSDPChange(false);
    }

    private boolean isInitialRecord(AccountingRecordType accountingRecordType) {
        return (AccountingRecordType.START_RECORD.equals(accountingRecordType) || AccountingRecordType.EVENT_RECORD.equals(accountingRecordType));
    }

    private boolean isTerminatingRecord(AccountingRecordType accountingRecordType) {
        return (AccountingRecordType.STOP_RECORD.equals(accountingRecordType) || AccountingRecordType.EVENT_RECORD.equals(accountingRecordType));
    }

    private AccountingRecordType determineCdrType() {
        SipFeatureScriptExecutionPoint executionPoint = getSessionState().getCurrentSipFeatureExecutionPoint();
        if (executionPoint.equals(SipFeatureScriptExecutionPoint.SipEndSession)) {
            if(getSessionState().getNextCdrSequenceNumber() == 0) {
                return AccountingRecordType.EVENT_RECORD;
            }
            return AccountingRecordType.STOP_RECORD;
        }

        if (getSessionState().getNextCdrSequenceNumber() == 0) {
            return AccountingRecordType.START_RECORD;
        }
        return AccountingRecordType.INTERIM_RECORD;
    }

    private static class InterimCDRTrigger {
        private final String legName;
        private final InterimCDRReason reason;
        private final String supplementaryReason;

        InterimCDRTrigger(InterimCDRReason reason, String supplementaryReason, String legName) {
            this.reason = reason;
            this.supplementaryReason = supplementaryReason;
            this.legName = legName;
        }

        InterimCDRReason getReason(){
            return reason;
        }

        String getSupplementaryReason(){
            return supplementaryReason;
        }

        String getLegName() {
            return legName;
        }
    }

    private enum InterimCDRReason {

        SDP, START, ANSWERED, TIMER, END;

    }

    /**
     * So derivations of the base class may use an RoToRfAvpFilter
     */
    protected final AvpFilter getRoToRfAvpFilter() { return RO_TO_RF_AVP_FILTER; }

    private SipInterimCdrConfig featureConfig;
    private CDRProvider cdrProvider;
    private RfControlProvider rfControlProvider;
    private RoProviderFactory roProviderFactory;
    private ServiceTimerProvider timerProvider;
    private RfControlActivityContextInterfaceFactory rfControlACIFactory;

    private long timerInterval;
    private SipInterimCdrConfigReader configReader;
    private CDRStats stats;

    private static final AvpFilter RO_TO_RF_AVP_FILTER = new RoToRfAvpFilter();
}
