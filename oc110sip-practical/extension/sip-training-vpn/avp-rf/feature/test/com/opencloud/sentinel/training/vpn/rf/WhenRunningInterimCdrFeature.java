/**
 * Copyright (c) 2014 Open Cloud Limited, a company incorporated in England and Wales (Registration Number 6000941) with its principal place of business at Edinburgh House, St John's Innovation Park, Cowley Road, Cambridge CB4 0DS.
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1  Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2  Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3  The name of the author may not be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * 4  The source code may not be used to create, develop, use or distribute software for use on any platform other than the Open Cloud Rhino and Open Cloud Rhino Sentinel platforms or any successor products.
 *
 * 5  Full license terms may be found https://developer.opencloud.com/devportal/display/OCDEV/Feature+Source+License
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE ARE EXCLUDED TO THE FULLEST EXTENT PERMITTED BY LAW.
 *
 * TO THE FULLEST EXTENT PERMISSIBLE BUY LAW, THE AUTHOR SHALL NOT BE LIABLE FOR ANY LOSS OF REVENUE, LOSS OF PROFIT, LOSS OF FUTURE BUSINESS, LOSS OF DATA OR ANY INDIRECT, SPECIAL, CONSEQUENTIAL, PUNITIVE OR OTHER LOSS OR DAMAGES ARISING OUT OF OR IN CONNECTION WITH THE SOFTWARE, WHETHER ARISING IN CONTRACT, TORT (INCLUDING NEGLIGENCE) MISREPRESENTATION OR OTHERWISE AND REGARDLESS OF WHETHER OPEN CLOUD HAS BEEN ADVISED OF THE POSSIBILITY OF ANY SUCH LOSS OR DAMAGE. THE AUTHORS MAXIMUM AGGREGATE LIABILITY WHETHER IN CONTRACT, TORT (INCLUDING NEGLIGENCE) OR OTHERWISE, SHALL NOT EXCEED EUR100.
 *
 * NOTHING IN THIS LICENSE SHALL LIMIT THE LIABILITY OF THE AUTHOR FOR DEATH OR PERSONAL INJURY RESULTING FROM NEGLIGENCE, FRAUD OR FRAUDULENT MISREPRESENTATION.
 *
 * Visit Open Cloud Developer's Portal for how-to guides, examples, documentation, forums and more: http://developer.opencloud.com
 */
package com.opencloud.sentinel.training.vpn.rf;

import com.google.common.collect.ImmutableListMultimap;
import com.opencloud.sce.fsmtool.Facilities;
import com.opencloud.sentinel.common.SentinelSelectionKey;
import com.opencloud.sentinel.feature.SipFeatureScriptExecutionPoint;
import com.opencloud.sentinel.feature.spi.SentinelSipMultiLegFeatureEndpoint;
import com.opencloud.sentinel.multileg.Leg;
import com.opencloud.sentinel.multileg.LegManager;
import com.opencloud.sentinel.providers.ServiceTimerProvider;
import com.opencloud.slee.resource.rf_control.RfControlActivity;
import com.opencloud.slee.resource.rf_control.RfControlActivityContextInterfaceFactory;
import com.opencloud.slee.resource.rf_control.RfControlProvider;
import org.jainslee.resources.diameter.base.DiameterMessageFactory;
import org.jainslee.resources.diameter.base.types.AccountingRecordType;
import org.jainslee.resources.diameter.rf.types.vcb0.AccountingRequest;
import org.jainslee.resources.diameter.rf.types.vcb0.ServiceInformation;
import org.jainslee.resources.diameter.rf.vcb0.RfMessageFactory;
import org.jainslee.resources.diameter.ro.types.vcb0.EarlyMediaDescription;
import org.jainslee.resources.diameter.ro.types.vcb0.ImsInformation;
import org.jainslee.resources.diameter.ro.types.vcb0.SdpMediaComponent;
import org.jainslee.resources.diameter.ro.vcb0.RoProvider;

import javax.slee.ActivityContextInterface;
import javax.slee.facilities.TimerEvent;
import javax.slee.facilities.TimerID;
import javax.slee.facilities.TraceLevel;
import javax.slee.facilities.Tracer;

import com.opencloud.sentinel.FakeTracer;

import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;
import org.mockito.Mock;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class WhenRunningInterimCdrFeature {
    @Mock private SentinelSipMultiLegFeatureEndpoint caller;
    @Mock private Facilities facilities;
    @Mock private SipInterimVPNCdrSessionState sessionState;
    @Mock private ActivityContextInterface aci;
    @Mock private SipInterimCdrConfigReader configurationReader;
    @Mock private SipInterimCdrConfig featureConfig;
    @Mock private LegManager legManager;
    @Mock private Leg leg;
    @Mock private CDRStats stats;
    @Mock private TimerEvent timerEvent;
    @Mock private TimerID timerId;
    @Mock private RfControlProvider rfControlProvider;
    @Mock private RfControlActivity rfControlActivity;
    @Mock private RfControlActivityContextInterfaceFactory rfControlACIFactory;
    @Mock private ActivityContextInterface rfControlAci;
    @Mock private RoProvider roProvider;
    @Mock private RfMessageFactory rfMessageFactory;
    @Mock private DiameterMessageFactory baseMessageFactory;
    @Mock private ServiceTimerProvider timerProvider;

    private FakeTracer tracer = new FakeTracer("InterimCdrFeature", TraceLevel.FINEST);

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        when(facilities.getTracer()).thenReturn(tracer);

        when(configurationReader.getConfiguration(any(SentinelSelectionKey.class))).thenReturn(featureConfig);
        when(sessionState.getLegForCdrs()).thenReturn("CallingLeg");
        when(caller.getLegManager()).thenReturn(legManager);
        when(legManager.getLeg(any(String.class))).thenReturn(leg);
        when(legManager.getLeg(aci)).thenReturn(leg);
        when(leg.getLegName()).thenReturn("CallingLeg");
        when(sessionState.getInterimCdrSupplementaryReason()).thenReturn("some interimCdrSupplmentaryReason");
        // so that should get us to the call to performAccounting
        // for SipFeatureScriptExecutionPoint.SipAccess_ServiceTimer or for SipFeatureScriptExecutionPoint.SipEndSession
        when(featureConfig.getUseCdrRa()).thenReturn(false);
        when(featureConfig.getUseRfControlRa()).thenReturn(true);
        when(featureConfig.getInterimTimerPeriod()).thenReturn(5L);
        when(roProvider.getRoProviderVcb0()).thenReturn(roProvider);
        when(rfControlProvider.getRfMessageFactory()).thenReturn(rfMessageFactory);
        when(rfMessageFactory.createRfAccountingRequest(any(AccountingRecordType.class))).thenReturn(mock(AccountingRequest.class));
        when(rfMessageFactory.createServiceInformation()).thenReturn(mock(ServiceInformation.class));
        when(rfMessageFactory.getBaseMessageFactory()).thenReturn(baseMessageFactory);
        when(sessionState.getPerLegEarlyMediaDescriptions()).thenReturn(ImmutableListMultimap.<String, EarlyMediaDescription>of());
        when(sessionState.getPerLegSdpMediaComponents()).thenReturn(ImmutableListMultimap.<String, SdpMediaComponent>of());
        when(sessionState.getPerLegCallingPartyAddresses()).thenReturn(ImmutableListMultimap.<String, String>of());
        when(sessionState.getImsInformation()).thenReturn(mock(ImsInformation.class));
    }

    @Test
    public void startsNewResumedRfSessionWhenSessionStateHasNoRfControlAciForInterimRecord() {
        BaseSipInterimCdrFeature feature = createFeature(caller, facilities, sessionState);

        // so determineCdrType() returns AccountingRecordType.INTERIM_RECORD
        when(sessionState.getCurrentSipFeatureExecutionPoint()).thenReturn(SipFeatureScriptExecutionPoint.SipAccess_ServiceTimer);
        when(timerEvent.getTimerID()).thenReturn(timerId);
        when(sessionState.getInterimCdrTimerID()).thenReturn(timerId);
        when(sessionState.getNextCdrSequenceNumber()).thenReturn(1);
        when(sessionState.getRfControlSessionId()).thenReturn("test-session");
        when(rfControlProvider.newResumedSession(any(String.class), anyInt())).thenReturn(rfControlActivity);
        when(rfControlACIFactory.getActivityContextInterface(same(rfControlActivity))).thenReturn(rfControlAci);

        // simulate no rfControlAci in session state initially
        when(sessionState.getRfControlAci()).thenReturn(null, rfControlAci);

        feature.doSipInterimCdrFeature(timerEvent, null, aci);

        verify(stats, times(1)).incrementRfControlResumedSession(1);
    }

    @Test
    public void startsNewResumedRfSessionWhenSessionStateHasNoRfControlAciForStopRecord() {
        BaseSipInterimCdrFeature feature = createFeature(caller, facilities, sessionState);

        // so determineCdrType() returns AccountingRecordType.STOP_RECORD
        when(sessionState.getCurrentSipFeatureExecutionPoint()).thenReturn(SipFeatureScriptExecutionPoint.SipEndSession);
        when(sessionState.getNextCdrSequenceNumber()).thenReturn(1);
        when(sessionState.getRfControlSessionId()).thenReturn("test-session");
        when(rfControlProvider.newResumedSession(any(String.class), anyInt())).thenReturn(rfControlActivity);
        when(rfControlACIFactory.getActivityContextInterface(same(rfControlActivity))).thenReturn(rfControlAci);

        // simulate no rfControlAci in session state initially
        when(sessionState.getRfControlAci()).thenReturn(null, rfControlAci);

        feature.doSipInterimCdrFeature(null, null, aci);

        verify(stats, times(1)).incrementRfControlResumedSession(1);
    }

    private BaseSipInterimCdrFeature createFeature(SentinelSipMultiLegFeatureEndpoint caller, Facilities facilities,
                                                   SipInterimVPNCdrSessionState sessionState) {
        BaseSipInterimCdrFeature feature = new BaseSipInterimCdrFeature(caller, facilities, sessionState);
        feature.injectFeatureConfigurationReader(configurationReader);
        feature.injectFeatureStats(stats);
        feature.injectResourceAdaptorProvider(rfControlProvider);
        feature.injectResourceAdaptorProvider(rfControlACIFactory);
        feature.injectResourceAdaptorProvider(roProvider);
        feature.injectResourceAdaptorProvider(timerProvider);
        feature.dependenciesInjected();
        return feature;
    }

}
