/**
 * Copyright (c) 2014 Open Cloud Limited, a company incorporated in England and Wales (Registration Number 6000941) with its principal place of business at Edinburgh House, St John's Innovation Park, Cowley Road, Cambridge CB4 0DS.
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1  Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2  Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3  The name of the author may not be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * 4  The source code may not be used to create, develop, use or distribute software for use on any platform other than the Open Cloud Rhino and Open Cloud Rhino Sentinel platforms or any successor products.
 *
 * 5  Full license terms may be found https://developer.opencloud.com/devportal/display/OCDEV/Feature+Source+License
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE ARE EXCLUDED TO THE FULLEST EXTENT PERMITTED BY LAW.
 *
 * TO THE FULLEST EXTENT PERMISSIBLE BUY LAW, THE AUTHOR SHALL NOT BE LIABLE FOR ANY LOSS OF REVENUE, LOSS OF PROFIT, LOSS OF FUTURE BUSINESS, LOSS OF DATA OR ANY INDIRECT, SPECIAL, CONSEQUENTIAL, PUNITIVE OR OTHER LOSS OR DAMAGES ARISING OUT OF OR IN CONNECTION WITH THE SOFTWARE, WHETHER ARISING IN CONTRACT, TORT (INCLUDING NEGLIGENCE) MISREPRESENTATION OR OTHERWISE AND REGARDLESS OF WHETHER OPEN CLOUD HAS BEEN ADVISED OF THE POSSIBILITY OF ANY SUCH LOSS OR DAMAGE. THE AUTHORS MAXIMUM AGGREGATE LIABILITY WHETHER IN CONTRACT, TORT (INCLUDING NEGLIGENCE) OR OTHERWISE, SHALL NOT EXCEED EUR100.
 *
 * NOTHING IN THIS LICENSE SHALL LIMIT THE LIABILITY OF THE AUTHOR FOR DEATH OR PERSONAL INJURY RESULTING FROM NEGLIGENCE, FRAUD OR FRAUDULENT MISREPRESENTATION.
 *
 * Visit Open Cloud Developer's Portal for how-to guides, examples, documentation, forums and more: http://developer.opencloud.com
 */
package com.opencloud.sentinel.feature.common.cdr.interim;

import com.opencloud.modulepack.transformer.SimpleConfigurationOption;
import com.opencloud.modulepack.transformer.SimpleModulePackTransformer;
import com.opencloud.modulepack.transformer.TransformerException;
import com.opencloud.modulepack.transformer.TransformerUtils;

import java.io.File;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This module pack transformer is used to refactor the parts of the Interim CDR feature
 * module pack that the generic renamespacing support in sdkadm doesn't cover. This includes profile table names
 * (configuration), build files, and various inputs into the build files.
 */
public class ModulePackTransformer implements SimpleModulePackTransformer {

    private static final String originalFeatureConfigurationProfileTableName = "SipInterimCdrProfileTable";

    @Override
    public void transform(File modulePackRoot, Map<String, String> configurationOptions) throws TransformerException {
        final String replacementFeatureConfigurationProfileTableName =
            configurationOptions.get(originalFeatureConfigurationProfileTableName);

        if (replacementFeatureConfigurationProfileTableName == null)
            throw new IllegalArgumentException("Missing Interim CDR feature configuration profile table rename option.");
        if (!TransformerUtils.isValidProfileTableName(replacementFeatureConfigurationProfileTableName))
            throw new IllegalArgumentException("Invalid replacement Interim CDR feature configuration profile table name: "
                                               + replacementFeatureConfigurationProfileTableName);

        // Replace table name in all files named "provisioning.xml"
        final Set<File> provisioningFiles =
            TransformerUtils.findFilesMatchingPattern(modulePackRoot, "provisioning\\.xml");
        TransformerUtils.replaceAllInFiles(provisioningFiles,
                                           Pattern.quote(originalFeatureConfigurationProfileTableName),
                                           Matcher.quoteReplacement(replacementFeatureConfigurationProfileTableName));

        // Update profile table names in associated .java and .yaml files
        updateProfileTableName(
            originalFeatureConfigurationProfileTableName,
            replacementFeatureConfigurationProfileTableName,
            "FEATURE_PROFILE_TABLE_NAME",
            new File(modulePackRoot, "feature/src/com/opencloud/sentinel/feature/common/cdr/interim/BaseSipInterimCdrFeature.java").getAbsolutePath(),
            new File(modulePackRoot, "profile/config/SipInterimCdrProfileTable.yaml").getAbsolutePath()
        );
    }

    @Override
    public List<SimpleConfigurationOption> getConfigurationOptions(File sdkRoot) {
        final LinkedList<SimpleConfigurationOption> options = new LinkedList<>();
        options.add(new SimpleConfigurationOption(originalFeatureConfigurationProfileTableName,
                                                  "Rename profile table used for Interim CDR feature configuration.",
                                                  originalFeatureConfigurationProfileTableName));
        return options;
    }

    private void updateProfileTableName(String oldName, String newName, String variableName,
                                        String javaFile, String configFile) throws TransformerException {
        // Replace VARIABLE = "TableName" in java file
        TransformerUtils.replaceAllInFile(new File(javaFile),
                "(" + variableName + "\\s*=\\s*\")" + Pattern.quote(oldName) + "(\")",
                "$1" + Matcher.quoteReplacement(newName) + "$2"
        );
        // Replace "TableName:" in config file
        TransformerUtils.replaceAllInFile(new File(configFile),
                Pattern.quote(oldName + ":"),
                Matcher.quoteReplacement(newName + ":")
        );
    }

    private void updateProfileTableName(String oldName, String newName, String configFile) throws TransformerException {
        // Replace "TableName:" in config file
        TransformerUtils.replaceAllInFile(new File(configFile),
                Pattern.quote(oldName + ":"),
                Matcher.quoteReplacement(newName + ":")
        );
    }
}
