
package com.opencloud.sentinel.training.vpnmd.feature;

import com.opencloud.sentinel.annotations.SessionStateInterface;
import com.opencloud.sentinel.training.vpn.session.SIPTrainingVPNSessionState;

@SessionStateInterface()
public interface SIPTrainingVPNMDSessionState extends SIPTrainingVPNSessionState{

	void setMDCalledPartyLegName(String name);
	String getMDCalledPartyLegName();
	
	void setMDCounter(int c);
	int getMDCounter();
}