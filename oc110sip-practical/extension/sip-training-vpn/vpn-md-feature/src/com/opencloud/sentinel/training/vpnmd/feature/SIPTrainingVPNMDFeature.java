package com.opencloud.sentinel.training.vpnmd.feature;

import java.util.Objects;

import javax.slee.ActivityContextInterface;
import javax.slee.annotation.ComponentId;
import javax.slee.annotation.LibraryReference;
import javax.slee.annotation.LibraryReferences;
import javax.slee.annotation.ProfileReference;
import javax.slee.annotation.ProfileReferences;
import javax.slee.facilities.TimerEvent;
import javax.slee.facilities.TimerID;
import javax.slee.facilities.Tracer;

import org.jainslee.resources.sip.EasySipActivityContextInterfaceFactory;
import org.jainslee.resources.sip.IncomingSipRequest;
import org.jainslee.resources.sip.SipFactory;
import org.jainslee.resources.sip.SipRequest;
import org.jainslee.resources.sip.SipResponse;

import com.opencloud.sce.fsmtool.Facilities;
import com.opencloud.sentinel.annotations.PojoFsm;
import com.opencloud.sentinel.annotations.PojoSentinelFeature;
import com.opencloud.sentinel.annotations.SentinelFeature;
import com.opencloud.sentinel.cmp.persist.SipFsmObjectCodec;
import com.opencloud.sentinel.feature.ExecutionPhase;
import com.opencloud.sentinel.feature.SipFeatureScriptExecutionPoint;
import com.opencloud.sentinel.feature.impl.BaseFeature;
import com.opencloud.sentinel.feature.spi.SentinelSipMultiLegFeatureEndpoint;
import com.opencloud.sentinel.feature.spi.init.FsmBuilder;
import com.opencloud.sentinel.feature.spi.init.InjectFeatureStats;
import com.opencloud.sentinel.feature.spi.init.InjectFsmBuilder;
import com.opencloud.sentinel.feature.spi.init.InjectResourceAdaptorProvider;
import com.opencloud.sentinel.multileg.Leg;
import com.opencloud.sentinel.providers.ServiceTimerProvider;
import com.opencloud.sentinel.training.vpnmd.fsm.SIPTrainingVPNMDFeatureFSM;
import com.opencloud.slee.annotation.SBBPartReference;
import com.opencloud.slee.annotation.SBBPartReferences;


/**
 *
 * An The VPN feature.
 */

 
@SentinelFeature(
    featureName = SIPTrainingVPNMDFeature.NAME,
    componentName = "@component.name@",
    featureVendor = "@component.vendor@",
    featureVersion = "@component.version@",
    featureGroup = SentinelFeature.SIP_FEATURE_GROUP,

    useAddressLists = false,
   
    usageStatistics = SIPTrainingVPNMDUsageStats.class,
    executionPhases = {ExecutionPhase.SipSessionPhase},
    raProviderJndiNames = {"slee/resources/sip/provider",
            "slee/resources/sip/activitycontextinterfacefactory",
            "sentinel/servicetimerprovider"}    
)

// Annotation to include a FSM
@PojoSentinelFeature(
	    fsms = @PojoFsm(
	        specification = "com/opencloud/sentinel/training/vpnmd/model/SIPTrainingVPNMD.fsm_dsl",
	        fsmClass = SIPTrainingVPNMDFeatureFSM.class,
	        fsmCodecClass = SipFsmObjectCodec.class
	    )
	)

@SBBPartReferences(
    sbbPartRefs = {
            @SBBPartReference(id = @ComponentId(name = "@sentinel-sip-spi.SentinelSipFeatureSPI SBB Part.name@", vendor = "@sentinel-sip-spi.SentinelSipFeatureSPI SBB Part.vendor@", version = "@sentinel-sip-spi.SentinelSipFeatureSPI SBB Part.version@"))
    }
)
@LibraryReferences(
		   libraryRefs = {
		          @LibraryReference(library = @ComponentId(
		               name = "@sip-training-vpn-session-state-library.name@", 
		               vendor = "@sip-training-vpn-session-state-library.vendor@", 
		               version = "@sip-training-vpn-session-state-library.version@")),
		   }
		)
@ProfileReferences(
	    profileRefs = {
	        @ProfileReference(profile = @ComponentId(
	            name="@sip-training-vpn-profile.name@",
	            vendor="@sip-training-vpn-profile.vendor@",
	            version="@sip-training-vpn-profile.version@")),
	        @ProfileReference(profile = @ComponentId(
	                name="@sip-training-vpn-addresslistprofile.name@", 
	                vendor="@sip-training-vpn-addresslistprofile.vendor@", 
	                version="@sip-training-vpn-addresslistprofile.version@"))
	    }
	)

public class SIPTrainingVPNMDFeature
        extends BaseFeature<SIPTrainingVPNMDSessionState, SentinelSipMultiLegFeatureEndpoint>
		implements InjectResourceAdaptorProvider,
        InjectFsmBuilder<SIPTrainingVPNMDFeatureFSM>,
        InjectFeatureStats<SIPTrainingVPNMDUsageStats>{

    /** Feature name */
    public static final String NAME = "SIPTrainingVPNMDFeature";

    public SIPTrainingVPNMDFeature(SentinelSipMultiLegFeatureEndpoint caller,
            Facilities facilities, SIPTrainingVPNMDSessionState sessionState) {
        super(caller, facilities, sessionState);
    }
   
    private SIPTrainingVPNMDUsageStats featureStats;
    @Override
    public void injectFeatureStats(SIPTrainingVPNMDUsageStats featureStats) {
        this.featureStats = featureStats;
    }
    
    private SipFactory sipProvider;
    private EasySipActivityContextInterfaceFactory sipAciFactory;
    private ServiceTimerProvider timerProvider;
    
    
    @Override
    public void injectResourceAdaptorProvider(Object provider) {
        if (provider instanceof SipFactory) {
            this.sipProvider = (SipFactory) provider;
        } else if (provider instanceof EasySipActivityContextInterfaceFactory) {
            sipAciFactory = (EasySipActivityContextInterfaceFactory) provider;
        }
        else if (provider instanceof ServiceTimerProvider) {
            timerProvider = (ServiceTimerProvider) provider;
        }
        else {
            if (provider != null && getTracer().isFineEnabled()) {
                getTracer().fine("Unknown provider " + provider);
            }
        }
    }

    /**
     * All features must have a unique name.
     *
     * @return the name of this feature
     */
    @Override
    public String getFeatureName() { return NAME; }

    /**
     * Kick off the feature.
     *
     * @param trigger  a triggering context. The feature implementation must be able to cast this to a useful type for it to run
     * @param activity the slee activity object this feature is related to (may be null)
     * @param aci      the activity context interface of the slee activity this feature is related to
     */
    @Override
    public void startFeature(Object trigger, Object activity, ActivityContextInterface aci) {

        Tracer tracer = getTracer();

        if (tracer.isInfoEnabled()) {
             tracer.info("Starting " + NAME);
        }

        SipFeatureScriptExecutionPoint executionPoint = getSessionState().getCurrentSipFeatureExecutionPoint();
        if (getTracer().isFineEnabled()) {
        	getTracer().fine("Received trigger: "+trigger);
        	final Leg triggeringLeg = getCaller().getLegManager().getLeg(aci);
        	if (triggeringLeg != null)
        		getTracer().fine("    on leg: "+triggeringLeg.getLegName());
        		
        	getTracer().fine("    current MD leg: "+getSessionState().getMDCalledPartyLegName());
        	getTracer().fine("    at execution point: " + executionPoint.toString());
        }
        
        final SIPTrainingVPNMDFeatureFSM fsm = loadFSM(trigger);
        if(!raiseInputs(fsm, aci, trigger)){
            if (getTracer().isFineEnabled())
                getTracer().fine("No new inputs");
            getCaller().featureHasFinished();
            return;
        }
 
        
        if (getTracer().isFineEnabled()) getTracer().fine("Sip Training VPN MD running at execution point: " + executionPoint.toString());
        fsm.execute();
             
        getCaller().featureHasFinished();
        if (getTracer().isFineEnabled()) getTracer().fine("Sip Training VPN MD Complete");
        
    }

    boolean raiseInputs(SIPTrainingVPNMDFeatureFSM fsm, ActivityContextInterface aci, Object trigger){

        final Leg triggeringLeg = getCaller().getLegManager().getLeg(aci);

        if (trigger instanceof TimerEvent) {
            if (getTracer().isFineEnabled()) {
                getTracer().fine("Triggered for timer: " + trigger + ". Checking if it is noResponseTimer. "+trigger);
            }
            if (getTracer().isFineEnabled()) {
                getTracer().fine("Caller: "+getCaller());
            }
            Leg callingPartyLeg = getCaller().getLegManager().getCallingPartyLeg();

            // Example: Check if timerIDs match. The TimerID should be stored as an AssociatedObject in a durable FSM input.
            //            if we had several different timers, we can check here which one was fired
            //
		    final TimerID timerID = (TimerID) fsm.getInputs().noResponseTimer.timerId.getAssociatedObject();
		    final TimerID triggerId = ((TimerEvent) trigger).getTimerID();
		    if (timerID != null && timerID.equals(triggerId)) {
               if (getTracer().isFineEnabled()) {
                   getTracer().fine("Raising noResponseTimer.timerExpired on VPN MD FSM");
               }
               fsm.getInputScheduler().raise(fsm.getInputs().noResponseTimer.timerExpired);
               return true;
		    }
        }

        if(fsm.isInInitialState()) {
            Leg callingPartyLeg = getCaller().getLegManager().getCallingPartyLeg();
            if(callingPartyLeg == null) {
                if(getTracer().isFineEnabled())
                    getTracer().fine("Could not determine calling party leg");
                return false;
            }
            if( getTracer().isFineEnabled() )
                getTracer().fine("Initiating VPN MD FSM");
            fsm.getEndpoints().callingParty.setAci(callingPartyLeg.getAci());
            fsm.getInputScheduler().raise(fsm.getInputs().local.init);

            return true;
        }

        if (triggeringLeg == null)
            return false;

        /**
         *  You can classify the trigger based on trigger type, Leg Name (or ACI, they should correspond 1:1, except in case of forking), etc
         */

        final Leg callingPartyLeg = getCaller().getLegManager().getCallingPartyLeg();
        
        //Note this corresponds to the current Called Party Leg / Aci , which may change over time
        // when a new downstream leg is created, we update 'calledParty' endpoint ACI and the 'calledPartyLegName' session state
        // This is fine when only one downstream leg is active at a time
        final String calledPartyLegName = getSessionState().getMDCalledPartyLegName();
        ActivityContextInterface calledPartyACI = fsm.getEndpoints().calledParty.getAci();
        
       // Classify trigger
        if( trigger instanceof SipResponse ){
        	Leg triggerLeg = getCaller().getLegManager().getLeg(aci);
        	if(getTracer().isFineEnabled())
                getTracer().fine("Received response on leg: "+(triggerLeg!=null?triggerLeg.getLegName():"Null Leg"));
            // Classify leg: note that in order to handle forking properly we should keep a record of all downstream forked legs and
        	//  check all of them here
            if(triggeringLeg.getAci().equals(calledPartyACI)) {
                // Classify latest response
                switch(triggeringLeg.getLatestResponse().getStatus()/100){
                    case 1:
                        if(getTracer().isFineEnabled())
                            getTracer().fine("Raising CalledParty.provisional on VPN MD FSM");
                        fsm.getInputScheduler().raise(fsm.getInputs().calledParty.sipProvisional, triggeringLeg.getLatestResponse());
                        return true;
                    case 2:
                        if (Objects.equals(triggeringLeg.getLatestResponse().getMethod(), SipRequest.UPDATE)) {
                            if (getTracer().isFineEnabled()) getTracer().fine("Received Success response to UPDATE on VPN MD FSM");
                            //no input implemented
                        }
                        else if (Objects.equals(triggeringLeg.getLatestResponse().getMethod(), SipRequest.INVITE)) {
                            if (getTracer().isFineEnabled()) getTracer().fine("Raising CalledParty.sipSuccess on VPN MD FSM");
                            fsm.getInputScheduler().raise(fsm.getInputs().calledParty.sipSuccess, triggeringLeg.getLatestResponse());
                            return true;
                        }else {
                        	if (getTracer().isFineEnabled()) getTracer().fine("Received Success response on VPN MD FSM");
                        }
                        return false;
                    case 4:
                    case 5:
                    case 6:
                        if (Objects.equals(triggeringLeg.getLatestResponse().getMethod(), SipRequest.UPDATE)) {
                            if (getTracer().isFineEnabled()) getTracer().fine("Raising CalledParty.updateError on VPN MD FSM");
                            //no input implemented
                            return false;
                        }
                        else {
                            if (getTracer().isFineEnabled()) getTracer().fine("Raising calledParty.sipError on VPN MD FSM");
                            fsm.getInputScheduler().raise(fsm.getInputs().calledParty.sipError, triggeringLeg.getLatestResponse());
                        }
                        return true;
                }

            } else if(triggeringLeg.getLegName().equals(calledPartyLegName)) {
                if (SipResponse.SC_OK == triggeringLeg.getLatestResponse().getStatus() && triggeringLeg.getLatestResponse().getMethod().equalsIgnoreCase(SipRequest.INVITE)) {
                    if (getTracer().isFineEnabled()) getTracer().fine("Raising calledParty.OK on VPN MD FSM");
                    fsm.getInputScheduler().raise(fsm.getInputs().calledParty.sipSuccess, triggeringLeg.getLatestResponse());
                    return true;
                }

            } else if (callingPartyLeg.equals(triggeringLeg)) {
                if (SipResponse.SC_OK == triggeringLeg.getLatestResponse().getStatus() && triggeringLeg.getLatestResponse().getMethod().equalsIgnoreCase(SipRequest.UPDATE)) {
                    if (getTracer().isFineEnabled()) getTracer().fine("Raising callingParty.updateSuccess on VPN MD FSM");
                    //no input implemented
                    return false;
                }
            }
        }
        else if( trigger instanceof SipRequest ) {
        	Leg triggerLeg = getCaller().getLegManager().getLeg(aci);
        	if(getTracer().isFineEnabled())
                getTracer().fine("Received request on leg: "+(triggerLeg!=null?triggerLeg.getLegName():"Null Leg"));
            // Classify leg
            if(triggeringLeg.getAci().equals(calledPartyACI)) {
                getTracer().fine("CalledParty leg detected");
                // Classify latest request
                if(triggeringLeg.getLatestRequest().getMethod().equalsIgnoreCase(IncomingSipRequest.BYE)){
                    getTracer().fine("CalledParty BYE detected");
                    if(getTracer().isFineEnabled())
                        getTracer().fine("Raising CalledParty.BYE on Early Media FSM");
                    fsm.getInputScheduler().raise(fsm.getInputs().calledParty.BYE, triggeringLeg.getLatestRequest());
                    return true;
                }
            }
            else if(callingPartyLeg.equals(triggeringLeg)){
                // Classify latest request
                if(triggeringLeg.getLatestRequest().getMethod().equalsIgnoreCase(IncomingSipRequest.PRACK)){
                    if(getTracer().isFineEnabled())
                        getTracer().fine("Raising callingParty.PRACK on Early Media FSM");
                    //fsm.getInputScheduler().raise(fsm.getInputs().callingParty.PRACK, triggeringLeg.getLatestRequest());
                    return true;
                }
                else if(triggeringLeg.getLatestRequest().getMethod().equalsIgnoreCase(IncomingSipRequest.CANCEL)) {
                    if (getTracer().isFineEnabled())
                        getTracer().fine("Raising callingParty.CANCEL on Early Media FSM");
                    //fsm.getInputScheduler().raise(fsm.getInputs().callingParty.CANCEL, triggeringLeg.getLatestRequest());
                    return true;
                }
                else if(triggeringLeg.getLatestRequest().getMethod().equalsIgnoreCase(IncomingSipRequest.UPDATE)) {
                    if(getTracer().isFineEnabled())
                        getTracer().fine("Raising callingParty.UPDATE on Early Media FSM");
                    //fsm.getInputScheduler().raise(fsm.getInputs().callingParty.UPDATE, triggeringLeg.getLatestRequest());
                    return true;
                }
                if(triggeringLeg.getLatestRequest().getMethod().equalsIgnoreCase(IncomingSipRequest.ACK)){
                    if(getTracer().isFineEnabled())
                        getTracer().fine("Raising callingParty.ACK on Early Media FSM");
                    //fsm.getInputScheduler().raise(fsm.getInputs().callingParty.ACK, triggeringLeg.getLatestRequest());
                    return true;
                }
            }
        }

        return false;
    }
    private SIPTrainingVPNMDFeatureFSM loadFSM(Object trigger) {
        final SIPTrainingVPNMDFeatureFSM fsm = sipTrainingVPNMDFeatureFSMFsmBuilder.getFSMInstance();
        fsm.registerActionImplementation(
                new SIPTrainingVPNFTActionsImpl(
                        getSessionState(), 
                        getCaller(), 
                        sipProvider, 
                        getCaller().getLegManager(), 
                        sipAciFactory, 
                        getFacilities(),
                        featureStats, 
                        timerProvider,
                        trigger));

        return fsm;
    } 
    @Override
    public void injectFsmBuilder(FsmBuilder<SIPTrainingVPNMDFeatureFSM> sipTrainingVPNMDFeatureFSMFsmBuilder) {
        this.sipTrainingVPNMDFeatureFSMFsmBuilder = sipTrainingVPNMDFeatureFSMFsmBuilder;
    } 
    
    private FsmBuilder<SIPTrainingVPNMDFeatureFSM> sipTrainingVPNMDFeatureFSMFsmBuilder;
}
