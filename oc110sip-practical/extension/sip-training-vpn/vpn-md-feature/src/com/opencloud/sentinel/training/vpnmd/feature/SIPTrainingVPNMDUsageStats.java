package com.opencloud.sentinel.training.vpnmd.feature;

import com.opencloud.sentinel.feature.spi.SentinelFeatureStats;

public interface SIPTrainingVPNMDUsageStats extends SentinelFeatureStats {

    /**
     * Increments the VPN Calls counter
     */
    void incrementMDCalls(long l);  
    /**
     * Increments the VPN Calls counter
     */
    void incrementMDLegs(long l);    
    
    /**
     * Increments the Barred Calls counter
     */    
	void incrementMDCallEstablished(long i);
    
}
