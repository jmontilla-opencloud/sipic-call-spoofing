
package com.opencloud.sentinel.training.vpnmd.feature;

import java.util.Collection;

import javax.annotation.Nonnull;
import javax.sip.header.ContactHeader;
import javax.sip.header.RouteHeader;
import javax.sip.header.ToHeader;
import javax.slee.ActivityContextInterface;
import javax.slee.facilities.TimerID;

import org.jainslee.resources.sip.Address;
import org.jainslee.resources.sip.EasySipActivityContextInterfaceFactory;
import org.jainslee.resources.sip.IncomingSipRequest;
import org.jainslee.resources.sip.IncomingSipResponse;
import org.jainslee.resources.sip.OutgoingSipRequest;
import org.jainslee.resources.sip.OutgoingSipResponse;
import org.jainslee.resources.sip.SipException;
import org.jainslee.resources.sip.SipFactory;
import org.jainslee.resources.sip.SipParseException;
import org.jainslee.resources.sip.SipRequest;
import org.jainslee.resources.sip.SipSession;
import org.jainslee.resources.sip.SipURI;
import org.jainslee.resources.sip.URI;

import com.opencloud.sce.fsmtool.FSMInput;
import com.opencloud.sce.fsmtool.Facilities;
import com.opencloud.sce.fsmtool.InputScheduler;
import com.opencloud.sentinel.feature.spi.SentinelSipMultiLegFeatureEndpoint;
import com.opencloud.sentinel.multileg.DuplicateLegException;
import com.opencloud.sentinel.multileg.Leg;
import com.opencloud.sentinel.multileg.LegManager;
import com.opencloud.sentinel.multileg.LegsAlreadyLinkedException;
import com.opencloud.sentinel.multileg.TransactionInProgressException;
import com.opencloud.sentinel.providers.ServiceTimerProvider;
import com.opencloud.sentinel.training.vpnmd.fsm.SIPTrainingVPNMDFeatureFSM;
import com.opencloud.sentinel.training.vpnmd.fsm.SIPTrainingVPNMDFeatureFSM.Endpoints;
import com.opencloud.sentinel.training.vpnmd.fsm.SIPTrainingVPNMDFeatureFSM.Inputs;
import com.opencloud.sentinel.util.SipPrettyPrinter;

public class SIPTrainingVPNFTActionsImpl implements SIPTrainingVPNMDFeatureFSM.SIPTrainingVPNMDFeatureActions{

	public SIPTrainingVPNFTActionsImpl(SIPTrainingVPNMDSessionState sessionState,
            SentinelSipMultiLegFeatureEndpoint endpoint,
            SipFactory sipProvider,
            @Nonnull final LegManager legManager,
            EasySipActivityContextInterfaceFactory easySipActivityContextInterfaceFactory,
            Facilities facilities,
            SIPTrainingVPNMDUsageStats stats,
            ServiceTimerProvider timerProvider,
            Object trigger) {

			this.sessionState = sessionState;
			this.endpoint = endpoint;
			this.sipProvider = sipProvider;
			this.legManager = legManager;
			this.timerProvider = timerProvider;
			this.easySipActivityContextInterfaceFactory = easySipActivityContextInterfaceFactory;
			this.stats = stats;
			this.trigger = trigger;
	}

	@Override
	public void checkDestinationListAction(Inputs inputs, InputScheduler<FSMInput> inputScheduler, Endpoints endpoints,
			Facilities facilities) {
		int counter = sessionState.getMDCounter();
		if(counter < 3) {
			counter+=1;
			inputScheduler.raise(inputs.local.entryAvailable,"667891000"+counter);	
			sessionState.setMDCounter(counter);
			if (facilities.getTracer().isFineEnabled())
				facilities.getTracer().fine("MD counter: "+counter);			
		}else {
			inputScheduler.raise(inputs.local.noMoreEntries);
			if (facilities.getTracer().isFineEnabled())
				facilities.getTracer().fine("MD counter terminated");			
		}
		
	}

    private void clearMessagesQueue(Leg leg){
        leg.getMessagesToSend().clear();
    }
    
	@Override
	public void createMDLegAction(Inputs inputs, InputScheduler<FSMInput> inputScheduler, Endpoints endpoints,
			Facilities facilities) {

        try {
            Leg callingPartyLeg = legManager.getCallingPartyLeg();
            Leg linkedLeg = callingPartyLeg.getLinkedLeg();
            if (linkedLeg != null) {
            	if (facilities.getTracer().isFineEnabled())
                    facilities.getTracer().fine("Unlinking and releasing calledPartyLeg from "+linkedLeg.getLegName());
            	clearMessagesQueue(callingPartyLeg);
            	callingPartyLeg.unlinkLeg();
            	legManager.releaseLeg(linkedLeg);
            }
            final IncomingSipRequest initialRequest = (IncomingSipRequest)callingPartyLeg.getInviteRequest();
        	//final OutgoingSipRequest outgoingRequest = sipProvider.createRequest(initialRequest.getMethod(), initialRequest.getFrom().getURI(), initialRequest.getTo().getURI());
            final OutgoingSipRequest outgoingRequest = sipProvider.createRequest(initialRequest,false);
            
        	//The SipSession is created with the INVITE
            final SipSession sipSession = outgoingRequest.getSession(true);
            final ActivityContextInterface aci = this.easySipActivityContextInterfaceFactory.getActivityContextInterface(sipSession);        	
        	endpoints.calledParty.setAci(aci);
        	endpoints.calledParty.attachToAci();
        	
            if (facilities.getTracer().isFineEnabled())
                facilities.getTracer().fine("createMDLeg: Created session: " + sipSession);

            if (facilities.getTracer().isFineEnabled())
                facilities.getTracer().fine("Route Header:= " + outgoingRequest.getHeader(RouteHeader.NAME));

            String newMDLegName = "MD_" + sessionState.getMDCounter() + "_" + outgoingRequest.getCallId();
            Leg newLeg = legManager.createLeg(outgoingRequest, newMDLegName);
            callingPartyLeg.linkLeg(newLeg);
            sessionState.setMDCalledPartyLegName(newMDLegName);
            final Address userContact = initialRequest.getAddressHeader(ContactHeader.NAME);
            sipSession.setLocalContactAddress(userContact);
            if (facilities.getTracer().isFinerEnabled()) facilities.getTracer().finer("Set local Contact address for MD leg [" + userContact + "]");
			// set a timer to expire in 5000ms, and cause a feature to be invoked.

			long nowT = System.currentTimeMillis();
			long expireT = nowT + 3000; // expire in 5000ms from now
			TimerID timerId = timerProvider.setTimer(SIPTrainingVPNMDFeature.NAME, expireT);
			inputScheduler.raise(inputs.noResponseTimer.timerId, timerId);
			if (facilities.getTracer().isFineEnabled())
				facilities.getTracer().fine("No response timer set to 3s: "+ timerId);
            stats.incrementMDLegs(1);
	
        } catch (SipParseException | DuplicateLegException e) {
            if (facilities.getTracer().isWarningEnabled())
            	facilities.getTracer().warning("Exception updating Address Headers", e);
        } catch (SipException e) {
        	if (facilities.getTracer().isWarningEnabled())
            	facilities.getTracer().warning("Exception updating Address Headers", e);
		} catch (IllegalArgumentException e) {
			if (facilities.getTracer().isWarningEnabled())
            	facilities.getTracer().warning("Exception updating Address Headers", e);
		} catch (LegsAlreadyLinkedException e) {
			if (facilities.getTracer().isWarningEnabled())
            	facilities.getTracer().warning("Exception updating Address Headers", e);
		}     
		
	}


	@Override
	public void cancelMDLegAction (Inputs inputs, InputScheduler<FSMInput> inputScheduler, Endpoints endpoints,
								   Facilities facilities) {
		if (legManager.getCallingPartyLeg() != null && legManager.getCallingPartyLeg().isLinked()) {
			if (facilities.getTracer().isFineEnabled())
				facilities.getTracer().fine("Cancelling MD Leg: " + legManager.getCallingPartyLeg().getLinkedLeg().getLegName());
			legManager.getCallingPartyLeg().getLinkedLeg().sendCancel();
		} else {
			if (facilities.getTracer().isFineEnabled())
				facilities.getTracer().fine("!!!! No leg to cancel. CallingPartyLeg: "+ legManager.getCallingPartyLeg());
		}
	}
	@Override
	public void cancelTimerAction (Inputs inputs, InputScheduler<FSMInput> inputScheduler, Endpoints endpoints,
								   Facilities facilities) {
		if (inputs.noResponseTimer.timerId.isRaised()) {
			final TimerID timerID = (TimerID) inputs.noResponseTimer.timerId.getAssociatedObject();
			if (timerID != null) {
				if (facilities.getTracer().isFineEnabled())
					facilities.getTracer().fine("Cancelling NoResponse timer: " + timerID);
				// if we have a timer, then cancel it
				timerProvider.cancelTimer(timerID);
			} else {
				if (facilities.getTracer().isFineEnabled())
					facilities.getTracer().fine("!!!!!timerId input set but no timer associated");
			}
			// Clear the durable input
			inputScheduler.clear(inputs.noResponseTimer.timerId);
		} else {
			if (facilities.getTracer().isFineEnabled())
				facilities.getTracer().fine("No timer set. Nothing to cancel");
		}
	}
	
	@Override
	public void setRequestURLAction(Inputs inputs, InputScheduler<FSMInput> inputScheduler, Endpoints endpoints,
			Facilities facilities) {

		final Leg outgoingLeg = legManager.getLeg(sessionState.getMDCalledPartyLegName());
        // Get Outgoing Leg request from Leg manager
        SipRequest outgoingRequest = (SipRequest) outgoingLeg.getMessagesToSend().peek();

        try {
            // Set Outgoing request URI, P-Served-User, and  TO URI's to the translated number
            URI outRequestURI = null;
            String newDestination = (String)inputs.local.entryAvailable.getAssociatedObject();
            if (outgoingRequest.getRequestURI().isSipURI()){
                String host = ((SipURI) outgoingRequest.getRequestURI()).getHost();
                int port = ((SipURI) outgoingRequest.getRequestURI()).getPort();
                outRequestURI = sipProvider.createSipURI(newDestination, host + ":" + port);
            } else {
                    // TEL URL
                outRequestURI = sipProvider.createURI("tel:" + newDestination);
            }
            outgoingRequest.setRequestURI(outRequestURI);
            Address outToAddress = sipProvider.createAddress(outRequestURI);
            outgoingRequest.setAddressHeader(ToHeader.NAME, outToAddress);
            outgoingRequest.setAddressHeader("P-Asserted-Identity", outToAddress);
            outgoingLeg.setInviteRequest(outgoingRequest);
            outgoingLeg.getMessagesToSend().remove();
            outgoingLeg.getMessagesToSend().add(outgoingRequest);		
            if (facilities.getTracer().isFineEnabled()) {
                facilities.getTracer().fine("Outgoing INVITE: " + SipPrettyPrinter.prettyPrint(outgoingRequest));
            }            
        } catch (SipParseException e) {
            if (facilities.getTracer().isFineEnabled())
            	facilities.getTracer().fine ("Exception updating Address Headers", e);
        }

	}
	@Override
	public void send181Action(Inputs inputs, InputScheduler<FSMInput> inputScheduler, Endpoints endpoints,
			Facilities facilities) {
		if (facilities.getTracer().isFineEnabled())
        	facilities.getTracer().fine ("send181Action action");
		Leg callingPartyLeg = legManager.getCallingPartyLeg();
		OutgoingSipResponse response = ((IncomingSipRequest)callingPartyLeg.getInviteRequest()).createResponse(181);
		try {
			callingPartyLeg.sendMessage(response);
	    } catch (TransactionInProgressException e) {
	        if (facilities.getTracer().isFineEnabled()) facilities.getTracer().fine("Transaction is currently in progress cannot send message" , e);	        
	    }
	}	
	@Override
	public void terminateAPartyAction(Inputs inputs, InputScheduler<FSMInput> inputScheduler, Endpoints endpoints,
			Facilities facilities) {
		if (facilities.getTracer().isFineEnabled())
        	facilities.getTracer().fine ("TerminateAParty action");
		Leg callingPartyLeg = legManager.getCallingPartyLeg();
		Leg linkedLeg = callingPartyLeg.getLinkedLeg();
		if (linkedLeg != null) {
			if (facilities.getTracer().isFineEnabled())
                facilities.getTracer().fine("Unlinking and releasing callingPartyLeg from "+linkedLeg.getLegName());
			clearMessagesQueue(callingPartyLeg);
        	callingPartyLeg.unlinkLeg();			
			legManager.releaseLeg(linkedLeg);
		}
		legManager.endSession(404);
	
	}

	@Override
	public void monitorAction(Inputs inputs, InputScheduler<FSMInput> inputScheduler, Endpoints endpoints,
			Facilities facilities) {
		if (facilities.getTracer().isFineEnabled())
        	facilities.getTracer().fine ("Monitor action");
		Leg callingPartyLeg = legManager.getCallingPartyLeg();
		IncomingSipResponse downstreamResponse = (IncomingSipResponse)inputs.calledParty.sipSuccess.getAssociatedObject();
		stats.incrementMDCallEstablished(1);
		
	}

	@Override
	public void cleanAction(Inputs inputs, InputScheduler<FSMInput> inputScheduler, Endpoints endpoints,
			Facilities facilities) {
		if (facilities.getTracer().isFineEnabled())
        	facilities.getTracer().fine ("Clean action");
		legManager.detachFromAllLegs();
	}

	
 

    private final SIPTrainingVPNMDSessionState sessionState;
    private final SentinelSipMultiLegFeatureEndpoint endpoint;
    private final SipFactory sipProvider;
    private final LegManager legManager;
    private final ServiceTimerProvider timerProvider;
    private final EasySipActivityContextInterfaceFactory easySipActivityContextInterfaceFactory;
    private final SIPTrainingVPNMDUsageStats stats;


    private final Object trigger;


	@Override
	public void initAction(Inputs inputs, InputScheduler<FSMInput> inputScheduler, Endpoints endpoints,
			Facilities facilities) {
		if (facilities.getTracer().isFineEnabled())
			facilities.getTracer().fine("INIT action");
		Leg callingPartyLeg = legManager.getCallingPartyLeg();
		Leg linkedLeg = callingPartyLeg.getLinkedLeg();
		if (linkedLeg != null) {
			if (facilities.getTracer().isFineEnabled())
				facilities.getTracer().fine("Unlinking and releasing " + linkedLeg.getLegName());
			clearMessagesQueue(linkedLeg);
			callingPartyLeg.unlinkLeg();
			legManager.detachFromLeg(linkedLeg);
		}
		stats.incrementMDCalls(1);
		
	}

	@Override
	public void printLegsAction(Inputs inputs, InputScheduler<FSMInput> inputScheduler, Endpoints endpoints,
			Facilities facilities) {
		Collection<Leg> legcollection = legManager.getLegs();
		for (Leg leg:legcollection) {				
			if(leg.isLinked()) {
				if (facilities.getTracer().isFineEnabled())
	                facilities.getTracer().fine("Leg Collection: "+leg.getLegName()+" --> "+leg.getLinkedLeg().getLegName());
			}else {
				if (facilities.getTracer().isFineEnabled())
	                facilities.getTracer().fine("Leg Collection: "+leg.getLegName());
			}
		}
		
	}



}
