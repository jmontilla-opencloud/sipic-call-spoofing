DROP TABLE IF EXISTS Training_Subscribers; 
CREATE TABLE Training_Subscribers (
  MSISDN varchar(50) not null,
  IMSI varchar(50),
  SubscriberLanguage varchar(50),
  Account varchar(50),
  SubscriptionType varchar(50),
  ValidityStart date,
  ValidityEnd date,
  HomeZoneEnabled boolean,
  HomeZoneList varchar(50),
  OcsId varchar(20),
  FriendsAndFamilyEnabled boolean,
  FriendsAndFamilyList varchar(50),
  ClosedUserGroupEnabled boolean,
  ClosedUserGroupList varchar(50),
  ClosedUserGroupPreferred integer,
  CUGIncomingAccessAllowed boolean,
  CUGOutgoingAccessAllowed boolean,
  CfBusy varchar(20),
  CfNoReply varchar(20),
  VPNId varchar(10),
  primary key (MSISDN)
  
);
COMMENT ON TABLE Training_Subscribers IS 'Store basic subscriber data for Training platform operator';

INSERT INTO Training_Subscribers (MSISDN, VPNId, Account, OcsId)
VALUES ('+886555110201', 'VPN1', '+886555110201', 'OCS01');

INSERT INTO Training_Subscribers (MSISDN, VPNId, Account, OcsId)
VALUES ('+886555110202', 'VPN1', '+886555110202', 'OCS01');

INSERT INTO Training_Subscribers (MSISDN, VPNId, Account, OcsId)
VALUES ('+886555110204', 'VPN1', '+886555110204', 'OCS01');

INSERT INTO Training_Subscribers (MSISDN, VPNId, Account, OcsId)
VALUES ('+886555110205', 'VPN1', '+886555110205', 'OCS01');

INSERT INTO Training_Subscribers (MSISDN, VPNId, Account, OcsId)
VALUES ('+886555110206', 'VPN2', '+886555110206', 'OCS01');

INSERT INTO Training_Subscribers (MSISDN, VPNId, Account, OcsId)
VALUES ('+886555110207', 'VPN2', '+886555110207', 'OCS01');

INSERT INTO Training_Subscribers (MSISDN, VPNId, Account, OcsId)
VALUES ('+886555110209', 'VPN2', '+886555110209', 'OCS01');

INSERT INTO Training_Subscribers (MSISDN, VPNId, Account, OcsId)
VALUES ('+886555110210', 'VPN2', '+886555110210', 'OCS01');
