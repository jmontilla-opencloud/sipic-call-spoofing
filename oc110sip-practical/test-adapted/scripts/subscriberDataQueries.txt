
List SQL:
SELECT MSISDN FROM Training_Subscribers ORDER BY MSISDN

Load SQL:
SELECT * FROM Training_Subscribers WHERE MSISDN = ?

Update SQL:
UPDATE Training_Subscribers SET IMSI = ?,subscriberlanguage = ?,Account = ?,SubscriptionType = ?,ValidityStart = ?,ValidityEnd = ?,HomeZoneEnabled = ?,HomeZoneList = ?,ocsId = ?,FriendsAndFamilyEnabled = ?,FriendsAndFamilyList = ?,ClosedUserGroupEnabled = ?,ClosedUserGroupList = ?,ClosedUserGroupPreferred = ?,CUGIncomingAccessAllowed = ?,CUGOutgoingAccessAllowed = ?,CfBusy = ?,CfNoReply = ?,VPNId = ? WHERE MSISDN = ?

Delete SQL:
DELETE FROM Training_Subscribers WHERE MSISDN = ?

Insert SQL:
INSERT INTO Training_Subscribers (MSISDN,IMSI,subscriberlanguage,Account,SubscriptionType,ValidityStart,ValidityEnd,HomeZoneEnabled,HomeZoneList,ocsId,FriendsAndFamilyEnabled,FriendsAndFamilyList,ClosedUserGroupEnabled,ClosedUserGroupList,ClosedUserGroupPreferred,CUGIncomingAccessAllowed,CUGOutgoingAccessAllowed,CfBusy,CfNoReply,VPNId) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)

Database request timeout:
1000
