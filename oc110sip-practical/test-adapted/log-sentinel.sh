if [[ "$1" == "-v" ]]
then
  echo Verbose
  tail -F /home/rhino/opencloud/sentinel-express-sdk/rhino-sdk/RhinoSDK/work/log/rhino.log | awk '/ featureName=/ || /executionPoint=/ { print $1, $2, $9, $11 } /flow/ || /instantiateMapper:/ || /Sending CC/ || /Received CC/ {$3=$4=$5=$6="" ; print $0} '
else
  echo "No verbose"
  tail -F /home/rhino/opencloud/sentinel-express-sdk/rhino-sdk/RhinoSDK/work/log/rhino.log | awk '/feature=SIPTr/ || / featureName=SIPTr/ {$3=$4=$5=$6=$7="" ; print $0}  /flow/  {$3=$4=$5=$6="" ; print $0} '
fi
