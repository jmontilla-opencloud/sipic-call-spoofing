package com.opencloud.sentinel.training.vpn.feature.vpnlist;


import com.opencloud.sce.fsmtool.Facilities;
import com.opencloud.sentinel.feature.common.lists.AddressMatchResult;
import com.opencloud.sentinel.feature.common.lists.ProfileBasedAddressListEntryReader;
import com.opencloud.sentinel.feature.common.lists.profiles.AddressListEntryProfileLocal;
import com.opencloud.sentinel.train.vpn.addresslistprofile.SIPTrainingVPNListEntryProfileLocal;

import javax.slee.facilities.AlarmFacility;
import javax.slee.facilities.Tracer;
import javax.slee.profile.ProfileFacility;

public class SIPTrainingVPNListEntryProfileReader extends ProfileBasedAddressListEntryReader<SIPTrainingVPNListEntry> {

    public SIPTrainingVPNListEntryProfileReader(Facilities facilities, String schemaName) {
        super(facilities.getProfileFacility(), facilities.getAlarmFacility(), facilities.getTracer(), schemaName);
    }

    public SIPTrainingVPNListEntryProfileReader(ProfileFacility profileFacility, AlarmFacility alarmFacility, Tracer tracer, String schemaName) {
        super(profileFacility, alarmFacility, tracer, schemaName);
    }


    /**
    * Return a list entry object for the matched cc. Features that define their own addition data associated with an address list
    * entry override this method to build a CustomAddressListEntry
    *
    * @param matchedAddress the matched address
    * @param matchEntry     the profile that corresponds to the matched address
    * @return a list entry object for the matched address.
    */
    @Override
    protected final SIPTrainingVPNListEntry getCustomListEntry(AddressMatchResult matchedAddress, AddressListEntryProfileLocal matchEntry) {
     final SIPTrainingVPNListEntryProfileLocal scMatchEntry = (SIPTrainingVPNListEntryProfileLocal) matchEntry;

     return new SIPTrainingVPNListEntry() {

         @Override
         public String getAddress() {

             return scMatchEntry.getAddress();
         }
         @Override
         public boolean getIsWhiteListed() {

             return scMatchEntry.getIsWhiteListed();
         }

      };
    }
}
