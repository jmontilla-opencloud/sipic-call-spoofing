#!/bin/bash
mysql -h 127.0.0.1 -u root --port 3306 -e "create database if not exists vpn;"
mysql -h 127.0.0.1 -u root --port 3306 vpn < VPNMembers.sql
mysql -h 127.0.0.1 -u root --port 3306 vpn < insert_vpn_members.sql
mysql -h 127.0.0.1 -u root --port 3306 vpn -e "CREATE TABLE mappingTable (
  MSISDN varchar(50) not null,
  SHORTCODE varchar(50)
 );"
