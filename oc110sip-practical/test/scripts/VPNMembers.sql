DROP TABLE IF EXISTS VPNMembers; 
CREATE TABLE VPNMembers (
  MSISDN varchar(50) not null,
  VPNId varchar(20),
  shortCode varchar(8),
  barring boolean,
  primary key (MSISDN)  
);
CREATE UNIQUE INDEX VPNShortCodes ON VPNMembers (
  VPNId,
  shortCode
);
