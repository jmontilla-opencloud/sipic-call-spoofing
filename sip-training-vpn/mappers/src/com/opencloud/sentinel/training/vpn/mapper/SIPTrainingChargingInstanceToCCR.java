package com.opencloud.sentinel.training.vpn.mapper;

import com.opencloud.sentinel.annotations.Mapping;
import com.opencloud.sentinel.annotations.SentinelMapper;
import com.opencloud.sentinel.annotations.SentinelMapperInjectedProviderLookupAlias;
import com.opencloud.sentinel.charging.ChargingInstance;
import com.opencloud.sentinel.common.SentinelSipSessionState;
import com.opencloud.sentinel.config.DiameterMediationConfigurationReader;
import com.opencloud.sentinel.mapper.MapperFacilities;
import com.opencloud.sentinel.mapper.impl.ChargingInstanceToCCR;
import com.opencloud.sentinel.training.vpn.session.SIPTrainingVPNSessionState;
import org.jainslee.resources.diameter.base.AvpNotAllowedException;
import org.jainslee.resources.diameter.base.DiameterAvp;
import org.jainslee.resources.diameter.base.DiameterMessageFactory;
import org.jainslee.resources.diameter.base.NoSuchAvpException;
import org.jainslee.resources.diameter.ro.RoProviderFactory;
import org.jainslee.resources.diameter.ro.types.vcb0.CreditControlRequest;
import org.jainslee.resources.diameter.ro.vcb0.RoMessageFactory;

import javax.slee.annotation.ComponentId;
import javax.slee.annotation.LibraryReference;
import javax.slee.annotation.LibraryReferences;
import javax.slee.facilities.Tracer;

@SentinelMapper(
    mappings = {
        @Mapping(name = "SIPTrainingChargingInstanceToCCR", fromClass = ChargingInstance.class, toClass = CreditControlRequest.class)
    }
)

public class SIPTrainingChargingInstanceToCCR extends ChargingInstanceToCCR {
    /**The mapper name.*/
    private static final String MAPPER_NAME =
            "SIPTrainingChargingInstanceToCCR";

    /** The Sbb tracer.*/
    private Tracer tracer;

    private static final int VENDOR_ID = 19808;
    private static final int AVP_VPN = 3925;
    private static final int AVP_SHORT_CODE = 3926;

    public SIPTrainingChargingInstanceToCCR(
                @SentinelMapperInjectedProviderLookupAlias("slee/resources/diameterro/sentinel/provider") RoProviderFactory sentinelProviderFactory,
                @SentinelMapperInjectedProviderLookupAlias("sentinel/mediationconfigreader") DiameterMediationConfigurationReader diameterMediationConfigurationReader) {
        super(sentinelProviderFactory, diameterMediationConfigurationReader);
    }

    /**
     * Adds or changes those mappings already implemented by the extended
     * ChargingInstanceToCCR class.
     * @param arg the InitialDP
     * @param state the session state interface
     * @param facilities Mapper Facilities interface
     * @return the CreditControlRequest operation argument
     */
    @Override
    public Object map(Object arg, SentinelSipSessionState state,
            MapperFacilities facilities) {
        tracer = facilities.getTracer();

        if (tracer.isFinerEnabled()) {
            tracer.finer(MAPPER_NAME
                    + " Starting mapper");
        }
        final ChargingInstance instance = (ChargingInstance) arg;

        final RoMessageFactory roMessageFactory = getSentinelProviderFactory().getRoProviderVcb0().getRoMessageFactory();

        final CreditControlRequest ccr =
                (CreditControlRequest) super.map(arg, state, facilities);

        DiameterMessageFactory factory = getSentinelProviderFactory().getRoProviderVcb0()
                    .getBaseProvider().getDiameterMessageFactory();

        addVpnAvps(ccr, state, facilities.getTracer());

        return ccr;

    }

    private void addVpnAvps(CreditControlRequest ccr,
            SentinelSipSessionState sessionState, Tracer tracer) {
        SIPTrainingVPNSessionState state = (SIPTrainingVPNSessionState) sessionState;
        if (state.getShortCode() != null && state.getVPNId()!=null) {
            if (tracer.isFinerEnabled()) tracer.finer(MAPPER_NAME+" Adding Extension AVPs");
            try {
                DiameterAvp vpn = getSentinelProviderFactory().getRoProviderVcb0()
                        .getBaseProvider().getDiameterMessageFactory()
                        .createAvp(19808, 10, state.getVPNId());
                DiameterAvp shortCode = getSentinelProviderFactory().getRoProviderVcb0()
                        .getBaseProvider().getDiameterMessageFactory()
                        .createAvp(19808, 11, state.getShortCode());
                ccr.setExtensionAvps(new DiameterAvp[] { vpn, shortCode } );
            } catch (Exception e) {
                tracer.warning(MAPPER_NAME + " Error adding VPN AVPs: " + e.getMessage());
            }
        }else {
            if (tracer.isFinerEnabled()) tracer.finer(MAPPER_NAME+" No extension AVPs to add");
        }
    }   
}
