//
// NOTE: This class was automatically generated.  Do not edit.
//

package com.opencloud.sentinel.training.vpn.cdrfeature;

import com.opencloud.rhino.facilities.sas.EnumParameter;

/**
 * Java enum to represent the SasAvpType enum values.
 *
 * A SAS enum parameter. Encodes as static data.
 * In SAS this is decoded in the event summary or detail with
 * "{{ static_data[0] | enum: "SasAvpType" }}" using the value attribute
 */
public enum SasAvpType implements EnumParameter {

    Service_Context_Id ( 1 ),
    Session_Id ( 2 ),
    ;

    private SasAvpType(int value) {
        this.value = value;
    }

    public int value() {
        return value;
    }

    private int value;
}
