//
// NOTE: This class was automatically generated.  Do not edit.
//

package com.opencloud.sentinel.training.vpn.cdrfeature;

import com.opencloud.rhino.facilities.sas.SasEventEnum;

/**
 * Java enum to represent the SasEvent event types.
 */
public enum SasEvent implements SasEventEnum {

    UNABLE_TO_CREATE_AVP ( 0 ),
    TIMED_OUT ( 1 ),
    IOEXCEPTION ( 2 ),
    ;

    SasEvent(int id) {
        this.id = id;
    }

    @Override
    public int id() {
        return id;
    }

    private int id;
}
