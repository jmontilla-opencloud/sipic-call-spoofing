package com.opencloud.sentinel.training.vpn.session;

import com.opencloud.sentinel.annotations.SessionStateInterface;
import com.opencloud.sentinel.common.SentinelSipSessionState;

@SessionStateInterface()
public interface SIPTrainingVPNSessionState extends SentinelSipSessionState  {
    String getVPNId();
    void setVPNId(String vpnId);
    String getShortCode();
    void setShortCode(String shortCode);
    boolean getVPNBarredWithAnnouncement();
    void setVPNBarredWithAnnouncement(boolean bool);
    int getEndSessionAfterAnnouncement();
    void setEndSessionAfterAnnouncement(int cause);
    String getVPNMsisdn();
    void setVPNMsisdn(String translatedLongNumber);
}